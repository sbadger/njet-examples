#include <cmath>
#include <vector>
#include <cstdlib>
#include <iostream>

#include "njet.h"
#include "tools/PhaseSpace.h"
#include "ngluon2/EpsTriplet.h"

using std::cout;
using std::endl;
using std::ios_base;

int main()
{
  cout.setf(ios_base::scientific, ios_base::floatfield);
  cout.precision(16);

  cout << endl;
  cout << "  NJet: simple example of the BLHA interface" << endl;
  cout << endl;

  const int legs = 4;
  const int pspoints = 1;

  double theta = M_PI/3.;
  double phi = M_PI/4.;
  double sqrts = 91.188;

  double Momenta[pspoints][legs][4] = {
    {
      {sqrts/2, 0., 0., sqrts/2},
      {sqrts/2, 0., 0., -sqrts/2},
      {sqrts/2, sqrts/2*sin(theta)*sin(phi), sqrts/2*sin(theta)*cos(phi), sqrts/2*cos(theta)},
      {sqrts/2, -sqrts/2*sin(theta)*sin(phi), -sqrts/2*sin(theta)*cos(phi), -sqrts/2*cos(theta)},
    }
  };

  int status;
  OLP_Start("OLP_contract_ee2j.lh", &status);
  if (status) {
    cout << "OLP read in correctly" << endl;
  } else {
    cout << "seems to be a problem with the contract file..." << endl;
    exit(1);
  }

  char olpname[15];
  char olpversion[15];
  char olpmessage[255];
  OLP_Info(olpname, olpversion, olpmessage);
  cout << "Running " << olpname
       << " version " << olpversion
       << " note " << olpmessage << endl;

  cout << endl;
  cout << "Notation: Tree     = A0.cA0" << endl;
  cout << "          Loop(-2) = 2*Re(A1.cA0)/eps^2" << endl;
  cout << "          Loop(-1) = 2*Re(A1.cA0)/eps^1" << endl;
  cout << "          Loop( 0) = 2*Re(A1.cA0)/eps^0" << endl;
  cout << endl;

  for (int pts=0; pts<pspoints; pts++) {
    cout << "==================== Test point " << pts+1 << " ====================" << endl;
    double LHMomenta[legs*5];
    for (int p=0; p<legs; p++) {
      for (int mu=0; mu<4; mu++ ) {
        LHMomenta[mu+p*5] = Momenta[pts][p][mu];
        cout << Momenta[pts][p][mu] << " ";
      }
      LHMomenta[4+p*5] = 0.;
      cout << endl;
    }
    cout << endl;

    double shat = pow(Momenta[pts][0][0] + Momenta[pts][0][0],2);
    std::vector<MOM<double> > NJmom(legs);
    MOM<double> msum;
    for (int p=0; p<legs; p++) {
      NJmom[p] = MOM<double>(Momenta[pts][p][0], Momenta[pts][p][1], Momenta[pts][p][2], Momenta[pts][p][3]);
      if (p>1) {
        msum += NJmom[p];
      } else {
        msum -= NJmom[p];
      }
      cout << "on-shell p" << p << "^2/shat " << S(NJmom[p])/shat << endl;
    }
    cout << "momentum conservation " << msum << endl;

    const int channels = 1;
    for (int p=1; p<=channels; p++) {
      double acc = 0.;
      double out[7] = {};
      int rstatus;

      const double alphas = 0.118;
      const double zero = 0.;
      const double mur = 91.188;

      OLP_SetParameter("alphas", &alphas, &zero, &rstatus);
      if (rstatus == 1) {
        cout << "Setting AlphaS: OK" << endl;
      } else if (rstatus == 0) {
        cout << "Setting AlphaS: FAIL" << endl;
      } else {
        cout << "Setting AlphaS: UNKNOWN" << endl;
        exit(2);
      }
      OLP_EvalSubProcess2(&p, LHMomenta, &mur, out, &acc);

      cout << "---- process number " << p << " ----" << endl;
      cout << "OLP accuracy check: " << acc << endl;

      cout << "Tree            = " << out[3] << endl;
      cout << "Loop(-2)        = " << out[0] << endl;
      cout << "Loop(-1)        = " << out[1] << endl;
      cout << "Loop( 0)        = " << out[2] << endl;
      cout << "Loop( -2)/Tree   = " << out[0]/out[3]/alphas*(2.*M_PI) << endl;
      cout << "Loop( -1)/Tree   = " << out[1]/out[3]/alphas*(2.*M_PI) << endl;
      cout << "Loop(  0)/Tree   = " << out[2]/out[3]/alphas*(2.*M_PI) << endl;
      cout << " only if NJetReturnAccuracy is used:"<< endl;
      cout << "Loop(-2) error  = " << out[4] << endl;
      cout << "Loop(-1) error  = " << out[5] << endl;
      cout << "Loop( 0) error  = " << out[6] << endl;
      cout << endl;

      double CF = 4./3.;
      EpsTriplet<double> res = 1./out[3]/alphas*(2.*M_PI)*EpsTriplet<double>(out[2], out[1], out[0]);
      // CDR result
//      EpsTriplet<double> check = CF*EpsTriplet<double>(-8.+M_PI*M_PI, -3., -2.);
      // DR = FDH result
      EpsTriplet<double> check = CF*EpsTriplet<double>(-7.+M_PI*M_PI, -3., -2.);
      cout << res << endl;
      cout << check << endl;
    }

    cout << endl;
  }

  cout << endl;

  return 0;
}
