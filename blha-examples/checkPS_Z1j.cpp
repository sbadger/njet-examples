#include <cmath>
#include <vector>
#include <cstdlib>
#include <iostream>

#include "njet.h"
#include "tools/PhaseSpace.h"

using std::cout;
using std::endl;
using std::ios_base;

int main()
{
  cout.setf(ios_base::scientific, ios_base::floatfield);
  cout.precision(16);

  cout << endl;
  cout << "  NJet: simple example of the BLHA interface" << endl;
  cout << endl;

  const int legs = 5;
  const int pspoints = 2;
  double Momenta[pspoints][legs][4] = {
    {
/*A: [2]*/  {3360.400204181565, 0, 0, 3360.400204181565},
/*B: [21]*/ {3050.848038599283, 0, 0, -3050.848038599283},
/*[-11]*/   {1757.898540916014, 113.1958662302845, 96.27144550943243, 1751.60662958398},
/*[11]*/    {1602.125026692091, 72.66788379657959, 141.5510980676999, 1594.204273753363},
/*[2]*/     {3051.224675172743, -185.8637500268641, -237.8225435771323, -3036.25873775506}
    },{
/*A: [3]*/  {3455.879122998046, 0, 0, 3455.879122998046},
/*B: [21]*/ {3123.788490537244, 0, 0, -3123.788490537244},
/*[-11]*/   {3174.669776777925, 270.1779777141662, 4.584909164551402, 3163.148910587902},
/*[11]*/    {280.7265555467369, 41.64310669681154, -2.159026134513353, 277.6123002677},
/*[3]*/     {3124.271281210628, -311.8210844109777, -2.42588303003805, -3108.670578394801}
    }
  };

  int status;
  OLP_Start("OLP_contract_Z1j.lh", &status);
  if (status) {
    cout << "OLP read in correctly" << endl;
  } else {
    cout << "seems to be a problem with the contract file..." << endl;
    exit(1);
  }

  char olpname[15];
  char olpversion[15];
  char olpmessage[255];
  OLP_Info(olpname, olpversion, olpmessage);
  cout << "Running " << olpname
       << " version " << olpversion
       << " note " << olpmessage << endl;

  cout << endl;
  cout << "Notation: Tree     = A0.cA0" << endl;
  cout << "          Loop(-2) = 2*Re(A1.cA0)/eps^2" << endl;
  cout << "          Loop(-1) = 2*Re(A1.cA0)/eps^1" << endl;
  cout << "          Loop( 0) = 2*Re(A1.cA0)/eps^0" << endl;
  cout << endl;

  for (int pts=0; pts<pspoints; pts++) {
    cout << "==================== Test point " << pts+1 << " ====================" << endl;
    double LHMomenta[legs*5];
    for (int p=0; p<legs; p++) {
      for (int mu=0; mu<4; mu++ ) {
        LHMomenta[mu+p*5] = Momenta[pts][p][mu];
        cout << Momenta[pts][p][mu] << " ";
      }
      LHMomenta[4+p*5] = 0.;
      cout << endl;
    }
    cout << endl;

    double shat = pow(Momenta[pts][0][0] + Momenta[pts][0][0],2);
    std::vector<MOM<double> > NJmom(legs);
    MOM<double> msum;
    for (int p=0; p<legs; p++) {
      NJmom[p] = MOM<double>(Momenta[pts][p][0], Momenta[pts][p][1], Momenta[pts][p][2], Momenta[pts][p][3]);
      if (p>1) {
        msum += NJmom[p];
      } else {
        msum -= NJmom[p];
      }
      cout << "on-shell p" << p << "^2/shat " << S(NJmom[p])/shat << endl;
    }
    cout << "momentum conservation " << msum << endl;

    const int channels = 1;
    for (int p=1; p<=channels; p++) {
      double out[7] = {};
      double acc = 0.;
      int rstatus;

      const double alphas = 0.118;
      const double zero = 0.;
      const double mur = 91.188;

      OLP_SetParameter("alphas", &alphas, &zero, &rstatus);
      if (rstatus == 1) {
        cout << "Setting AlphaS: OK" << endl;
      } else if (rstatus == 0) {
        cout << "Setting AlphaS: FAIL" << endl;
      } else {
        cout << "Setting AlphaS: UNKNOWN" << endl;
        exit(2);
      }
      OLP_EvalSubProcess2(&p, LHMomenta, &mur, out, &acc);

      cout << "---- process number " << p << " ----" << endl;
      cout << "OLP accuracy check: " << acc << endl;

      cout << "Tree            = " << out[3] << endl;
      cout << "Loop(-2)        = " << out[0] << endl;
      cout << "Loop(-1)        = " << out[1] << endl;
      cout << "Loop( 0)        = " << out[2] << endl;
      cout << "Loop( -2)/Tree   = " << out[0]/out[3]/alphas*(2.*M_PI) << endl;
      cout << " only if NJetReturnAccuracy is used:"<< endl;
      cout << "Loop(-2) error  = " << out[4] << endl;
      cout << "Loop(-1) error  = " << out[5] << endl;
      cout << "Loop( 0) error  = " << out[6] << endl;
      cout << endl;
    }
    cout << endl;
  }

  cout << endl;

  return 0;
}
