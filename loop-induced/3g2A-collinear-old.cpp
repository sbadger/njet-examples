#include <iostream>
#include <vector>
#include <complex>
#include <cmath>

#include "ngluon2/Initialize.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

#include "phaseSpace.h"
#include "common.h"

using std::vector;
using std::complex;
using std::abs;
using std::cout;
using std::endl;
using std::pow;

template<typename T>
void checkCollinearggggg(const T l) {
    const T MuR{91.188};

    const int mul{5};
    const Flavour<double> flavours[mul] = {g, g, g, g, g};
    const int helicity[mul] = {+1, +1, +1, +1, +1};
    vector <MOM<T>> mom = getCollinearPhaseSpacePoint5<T>(l);
    NParton2 <T> amp;
    amp.setProcess(mul, flavours);
    amp.setMuR2(pow(MuR, 2.));
    amp.setHelicity(helicity);
    amp.setMomenta(mom);

    const int fact_mul{mul - 1};
    const Flavour<double> fact_flavours[fact_mul] = {g, g, g, g};
    const int fact_helicity[fact_mul] = {+1, +1, +1, +1};
    NParton2 <T> fact_amp;
    fact_amp.setProcess(fact_mul, fact_flavours);
    fact_amp.setMuR2(pow(MuR, 2.));
    fact_amp.setHelicity(fact_helicity);
    vector <MOM<T>> fact_mom(fact_mul);

    // Catani-Seymour momentum mapping 12;3
    const MOM <T> p1{mom[3]};
    const MOM <T> p2{mom[4]};
    const MOM <T> p12{p1 + p2};
    const MOM <T> p3{mom[2]};
    const T s12{S(p12)};
    const T x12_3{s12 / T(2.) / dot(p12, p3)};
    const MOM <T> p12t{p12 - x12_3 * p3};
    const MOM <T> p3t{(1. + x12_3) * p3};

    fact_mom[0] = mom[0];
    fact_mom[1] = mom[1];
    fact_mom[2] = p3t;
    fact_mom[3] = p12t;
    fact_amp.setMomenta(fact_mom);

    const complex <T> z1A{spA(p1, p3) / spA(p12, p3)};
    const complex <T> z2A{spA(p2, p3) / spA(p12, p3)};

    const complex <T> split{-spA(p1, p2) / z1A / z2A};
    const T s34{2 * dot(mom[3], mom[4])};
    const complex <T> limit{fact_amp.eval(1).get0() * split / s34};

    cout << "lambda = " << l << " loop/limit = " << amp.eval(1).get0() / limit << endl;
}

template<typename T>
void checkCollinearAAggg(const T l) {
    const T MuR{91.188};
    const int mul{5};

    const Flavour<double> flavours[mul] = {A, A, g, g, g};
    const int helicity[mul] = {+1, +1, +1, +1, +1};
    vector <MOM<T>> mom = getCollinearPhaseSpacePoint5<T>(l);
    NParton2 <T> amp;
    amp.setProcess(mul, flavours);
    amp.setMuR2(pow(MuR, 2.));
    amp.setLoopInduced();
    amp.setHelicity(helicity);
    amp.setMomenta(mom);

    const int fact_mul{mul - 1};
    const Flavour<double> fact_flavours[fact_mul] = {A, A, g, g};
    const int fact_helicity[fact_mul] = {+1, +1, +1, +1};
    NParton2 <T> fact_amp;
    fact_amp.setProcess(fact_mul, fact_flavours);
    fact_amp.setMuR2(pow(MuR, 2.));
    fact_amp.setLoopInduced();
    fact_amp.setHelicity(fact_helicity);

    // Catani-Seymour momentum mapping 12;3
    const MOM <T> p1{mom[3]};
    const MOM <T> p2{mom[4]};
    const MOM <T> p12{p1 + p2};
    const MOM <T> p3{mom[2]};
    const T s12{S(p12)};
    const T x12_3{s12 / T(2.) / dot(p12, p3)};
    const MOM <T> p12t{p12 - x12_3 * p3};
    const MOM <T> p3t{(1. + x12_3) * p3};

    vector <MOM<T>> fact_mom(fact_mul);
    fact_mom[0] = mom[0];
    fact_mom[1] = mom[1];
    fact_mom[2] = p3t;
    fact_mom[3] = p12t;
    fact_amp.setMomenta(fact_mom);

    const complex <T> z1A{spA(p1, p3) / spA(p12, p3)};
    const complex <T> z2A{spA(p2, p3) / spA(p12, p3)};

    const complex <T> split{-spA(p1, p2) / z1A / z2A};
    const T s34{2 * dot(mom[3], mom[4])};

    const int orders[12][mul] = {
        {0, 1, 2, 3, 4,},
        {0, 1, 3, 4, 2,},
        {0, 1, 4, 2, 3,},
        {0, 2, 1, 3, 4,},
        {0, 3, 1, 4, 2,},
        {0, 4, 1, 2, 3,},
        {0, 2, 3, 1, 4,},
        {0, 3, 4, 1, 2,},
        {0, 4, 2, 1, 3,},
        {0, 2, 3, 4, 1,},
        {0, 3, 4, 2, 1,},
        {0, 4, 2, 3, 1,},
    };
    EpsTriplet <T> loop;
    for (auto order: orders) {
        amp.setOrder(order);
        loop += amp.eval(1);;
    }

    const int fact_orders[6][fact_mul] = {
        {0, 1, 2, 3,},
        {0, 1, 3, 2,},
        {0, 2, 1, 3,},
        {0, 3, 1, 2,},
        {0, 2, 3, 1,},
        {0, 3, 2, 1,},
    };
    EpsTriplet <T> fact;
    for (auto fact_order: fact_orders) {
        fact_amp.setOrder(fact_order);
        fact += fact_amp.eval(1);
    }

    const complex <T> limit{fact.get0() * split / s34};
    cout << "lambda = " << l << " loop/limit = " << loop.get0() / limit << endl;
}

template<typename T>
void checkCollinearAggAg(const T l) {
    const T MuR{91.188};

    const int mul{5};
    const Flavour<double> flavours[mul] = {A, g, g, A, g};
    const int helicity[5] = {+1, +1, +1, +1, +1};
    vector <MOM<T>> mom = getCollinearPhaseSpacePoint5<T>(l);
    NParton2 <T> amp;
    amp.setProcess(mul, flavours);
    amp.setLoopInduced();
    amp.setMuR2(pow(MuR, 2.));
    amp.setHelicity(helicity);
    amp.setMomenta(mom);

    const int orders[12][mul] = {
        {0, 3, 1, 2, 4},
        {0, 3, 2, 4, 1},
        {0, 3, 4, 1, 2},
        {0, 1, 3, 2, 4},
        {0, 2, 3, 4, 1},
        {0, 4, 3, 1, 2},
        {0, 1, 2, 3, 4},
        {0, 2, 4, 3, 1},
        {0, 4, 1, 3, 2},
        {0, 1, 2, 4, 3},
        {0, 2, 4, 1, 3},
        {0, 4, 1, 2, 3},
    };

    EpsTriplet <T> sol;
    for (auto order: orders) {
        amp.setOrder(order);
        // 1 = fermion loop primitive
        sol += amp.eval(1);;
    }
    cout << "lambda = " << l << " abs(loop) = " << abs(sol.get0()) << endl;
}

template<typename T>
void checkCollineargggAAppppp(const T l) {
    const T MuR{91.188};

    const int mul{5};
    const Flavour<double> flavours[mul] = {g, g, g, A, A};
    const int helicity[5] = {+1, +1, +1, +1, +1};
    vector <MOM<T>> mom = getCollinearPhaseSpacePoint5<T>(l);
    NParton2 <T> amp;
    amp.setProcess(mul, flavours);
    amp.setLoopInduced();
    amp.setMuR2(pow(MuR, 2.));
    amp.setHelicity(helicity);
    amp.setMomenta(mom);

    const int orders[12][mul] = {
        {3, 4, 0, 1, 2,},
        {3, 4, 1, 2, 0,},
        {3, 4, 2, 0, 1,},
        {3, 0, 4, 1, 2,},
        {3, 1, 4, 2, 0,},
        {3, 2, 4, 0, 1,},
        {3, 0, 1, 4, 2,},
        {3, 1, 2, 4, 0,},
        {3, 2, 0, 4, 1,},
        {3, 0, 1, 2, 4,},
        {3, 1, 2, 0, 4,},
        {3, 2, 0, 1, 4,},
    };

    EpsTriplet <T> sol;
    for (auto order: orders) {
        amp.setOrder(order);
        // 1 = fermion loop primitive
        sol += amp.eval(1);;
    }
    cout << "lambda = " << l << " loop = " << sol << endl;
}

template<typename T>
void checkCollineargggAAmmppp(const T l) {
    const T MuR{91.188};
    const int mul{5};

    const Flavour<double> flavours[mul] = {g, g, g, A, A};
    const int helicity[mul] = {-1, -1, +1, +1, +1};
    vector <MOM<T>> mom = getCollinearPhaseSpacePoint5<T>(l);
    NParton2 <T> amp;
    amp.setProcess(mul, flavours);
    amp.setMuR2(pow(MuR, 2.));
    amp.setLoopInduced();
    amp.setHelicity(helicity);
    amp.setMomenta(mom);

    //    const int fact_mul{mul - 1};
    //    const Flavour<double> fact_flavours[fact_mul] = {g, g, g, g};
    //    const int fact_helicity[fact_mul] = {-1, -1, +1, +1};
    //    NParton2 <T> fact_amp;
    //    fact_amp.setProcess(fact_mul, fact_flavours);
    //    fact_amp.setMuR2(pow(MuR, 2.));
    //    fact_amp.setHelicity(fact_helicity);

    // Catani-Seymour momentum mapping 12;3
    //    const MOM <T> p1{mom[3]};
    //    const MOM <T> p2{mom[4]};
    //    const MOM <T> p12{p1 + p2};
    //    const MOM <T> p3{mom[2]};
    //    const T s12{S(p12)};
    //    const T x12_3{s12 / T(2.) / dot(p12, p3)};
    //    const MOM <T> p12t{p12 - x12_3 * p3};
    //    const MOM <T> p3t{(1. + x12_3) * p3};
    //
    //    vector <MOM<T>> fact_mom(fact_mul);
    //    fact_mom[0] = mom[0];
    //    fact_mom[1] = mom[1];
    //    fact_mom[2] = p3t;
    //    fact_mom[3] = p12t;
    //    fact_amp.setMomenta(fact_mom);
    //
    //    const complex <T> z1A{spA(p1, p3) / spA(p12, p3)};
    //    const complex <T> z2A{spA(p2, p3) / spA(p12, p3)};
    //
    //    const complex <T> z1{dot(p1, p3) / dot(p12, p3)};
    //    const complex <T> z2{dot(p2, p3) / dot(p12, p3)};
    //
    //    const complex <T> split{spA(p1, p2) / z1A / z2A * (-T(5.) / T(9.)) * z1 * z2};
    //    const complex <T> s34{2 * dot(mom[3], mom[4])};

    const int orders[12][mul] = {
        {3, 4, 0, 1, 2,},
        {3, 4, 1, 2, 0,},
        {3, 4, 2, 0, 1,},
        {3, 0, 4, 1, 2,},
        {3, 1, 4, 2, 0,},
        {3, 2, 4, 0, 1,},
        {3, 0, 1, 4, 2,},
        {3, 1, 2, 4, 0,},
        {3, 2, 0, 4, 1,},
        {3, 0, 1, 2, 4,},
        {3, 1, 2, 0, 4,},
        {3, 2, 0, 1, 4,},
    };
    EpsTriplet <T> loop;
    for (auto order: orders) {
        amp.setOrder(order);
        loop += amp.eval(1);
    }

    //    const int fact_orders[6][fact_mul] = {
    //            {0, 1, 2, 3,},
    //            {0, 1, 3, 2,},
    //            {0, 2, 1, 3,},
    //            {0, 3, 1, 2,},
    //            {0, 2, 3, 1,},
    //            {0, 3, 2, 1,},
    //    };
    //    EpsTriplet <T> fact;
    //    for (auto fact_order: fact_orders) {
    //        fact_amp.setOrder(fact_order);
    //        fact += fact_amp.eval(1);
    //    }

    //    const complex <T> limit{fact_amp.evalTree() * split / s34};
    //    cout << fact_amp.evalTree() << endl;
    //    const complex <T> i(0., 1.);
    //    cout << i * pow(spA(fact_mom[0], fact_mom[1]), 3) /
    //            (spA(fact_mom[1], fact_mom[2]) * spA(fact_mom[2], fact_mom[3]) * spA(fact_mom[3], fact_mom[0])) << endl;
    //    cout << "split = " << split << endl;
    //    cout << "limit = " << limit << endl;
    cout << "lambda = " << l << " loop = " << loop << endl;
    //    cout << "lambda = " << l << " loop/limit = " << loop.get0() / limit << endl;
}

int main(int argc, char **argv) {
    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(16);

    cout << endl << "64 digit precision. 3||4 <=> lambda->0:" << endl;
    for (int p = 1; p < 64; p += 30) {
        testPhaseSpacePoint5<qd_real>(pow(10., -p));
    }

    cout << endl << "64 digit precision. ggggg. +++++. Ratio should be unity in 3||4:" << endl;
    for (int p = 1; p < 32; p += 3) {
        checkCollinearggggg<qd_real>(pow(10., -p));
    }

    cout << endl << "64 digit precision. AggAg. +++++. Should be constant [order(lambda^0)] in 3||4:" << endl;
    for (int p = 1; p < 32; p += 3) {
        checkCollinearAggAg<qd_real>(pow(10., -p));
    }

    cout << endl << "64 digit precision. AAggg. +++++. Ratio should be unity in 3||4:" << endl;
    for (int p = 1; p < 32; p += 3) {
        checkCollinearAAggg<qd_real>(pow(10., -p));
    }

    cout << endl << "64 digit precision. gggAA. +++++. Should be zero in 3||4:" << endl;
    for (int p = 1; p < 32; p += 3) {
        checkCollineargggAAppppp<qd_real>(pow(10., -p));
    }

    cout << endl
        << "64 digit precision. gggAA. --+++. Should be zero/constant in 3||4 (by colour factor of splitting function):"
        << endl;
    for (int p = 1; p < 32; p += 3) {
        checkCollineargggAAmmppp<qd_real>(pow(10, -p));
    }

    cout << endl;
    return 1;
}
