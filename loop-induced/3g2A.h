#ifndef NJET_EXAMPLES_3G2A_H
#define NJET_EXAMPLES_3G2A_H

#include <vector>
#include <complex>

#include "tools/PhaseSpace.h"

#include "amp-gA.h"
#include "common.h"
#include "EpsQuintuplet.h"

using std::vector;
using std::complex;
using std::conj;
using std::real;

template<typename T>
class Amp_3g2A : public Amp_gA<T, 5> {
private:
    static const unsigned int mul{5};
    static const unsigned int num{12};
    const Flavour<double> flavours[mul]{g, g, g, A, A};
    const unsigned int orders[num][mul]{
            {3, 4, 0, 1, 2,},
            {3, 4, 1, 2, 0,},
            {3, 4, 2, 0, 1,},
            {3, 0, 4, 1, 2,},
            {3, 1, 4, 2, 0,},
            {3, 2, 4, 0, 1,},
            {3, 0, 1, 4, 2,},
            {3, 1, 2, 4, 0,},
            {3, 2, 0, 4, 1,},
            {3, 0, 1, 2, 4,},
            {3, 1, 2, 0, 4,},
            {3, 2, 0, 1, 4,},
    };
public:
    const EpsTriplet <T> evalAmp();

    const EpsQuintuplet<T> evalKinAmp2();

    const EpsQuintuplet<T> evalAmp2_f();

    const EpsQuintuplet<T> evalAmp2_rf();

    const EpsQuintuplet<T> evalAmp2(const Basis basis);

    Amp_3g2A();
};

// Initialise amp (class constructor)
template<typename T>
Amp_3g2A<T>::Amp_3g2A() {
    this->setProcess(mul, flavours);
    this->setLoopInduced();
}

// Kinematic part of primitive amplitude
template<typename T>
const EpsTriplet <T> Amp_3g2A<T>::evalAmp() {
    return Amp_gA<T, mul>::evalAmp(orders);
}

// Kinematic part of helicity amplitude squared, no quark charge or sum
template<typename T>
const EpsQuintuplet<T> Amp_3g2A<T>::evalKinAmp2() {
    return sq_amp(this->evalAmp());
}

// Full helicity amplitude squared (reflected fundamental colour basis)
template<typename T>
const EpsQuintuplet<T> Amp_3g2A<T>::evalAmp2_rf() {
    EpsQuintuplet<T> sol{this->evalKinAmp2()};
    const T colFac{Nc * (Nc2 - T(1.)) / T(4.)};
    return Nf2<T> * colFac * sol;
}

// and using fundamental basis
template<typename T>
const EpsQuintuplet<T> Amp_3g2A<T>::evalAmp2_f() {
    const unsigned int orderZero[]{0, 1, 2, 3, 4};
    this->setPrimOrder(orderZero);
    EpsTriplet <T> A01234{this->evalAmp()};

    // define other partial amplitudes exploiting reflection (and cyclicity)
    const EpsTriplet <T> A02134{-A01234};

    // vector of partial amplitudes
    const EpsTriplet <T> ampVec[2]{
            A01234,
            A02134
    };

    // elements of colour matrix
    const T colMatEl[2]{
            -T(2.) * Nc2 + Nc4,
            -T(2.) * Nc2
    };

    // colour matrix (len x len) data; just the upper triangle (without diagonal) since symmetric
    const unsigned int colMatOrder[3]{
            0, 1, 0
    };

    const EpsQuintuplet<T> amp2{this->evalColourSum(ampVec, colMatEl, colMatOrder)};

    const T colNorm{(-T(1.) + Nc2) / (T(8.) * Nc3)};

    return Nf2<T> * colNorm * amp2;
}

template<typename T>
const EpsQuintuplet<T> Amp_3g2A<T>::evalAmp2(const Basis basis) {
    switch (basis) {
        case fundamental:
            return evalAmp2_f();
        case reflectedFundamental:
            return evalAmp2_rf();
        default:
            cout << "Warning! Unrecognised colour basis. Returning zero." << endl;
            return EpsQuintuplet<T>();
    }
}

#endif //NJET_EXAMPLES_3G2A_H
