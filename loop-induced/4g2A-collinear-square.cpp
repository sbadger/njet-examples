// Test 2||3 (g||g) & 3||4 (g||A) limits of 4g2A in mhv helicity configurations for helicity |amp|^2 (inc. colour sum)
// Takes one CLI argument, v, to switch on debug output, i.e. `./4g2A-collinear-square v` for debug output

#include <iostream>
#include <vector>
#include <complex>
#include <string>
//#include <array>
#include <cmath>

#include "ngluon2/Initialize.h"
#include "tools/PhaseSpace.h"

#include "3g2A.h"
#include "4g2A.h"
#include "phaseSpace.h"
#include "common.h"
#include "collinear.h"
#include "EpsQuintuplet.h"

using std::cout;
using std::endl;
using std::vector;
using std::complex;
using std::string;
using std::to_string;
//using std::array;
using std::pow;

// Choose to use spin correlation framework here
const bool spinCorrelations{true};

template<typename T, class redAmpC>
T lim_rf(CollinearLimit<T, 6> &colLim, redAmpC &redAmpClass, const bool verbose) {
    EpsQuintuplet<T> limKin2;
    if (spinCorrelations) {
        limKin2 = colLim.spinCorrelate(redAmpClass);
    } else {
        const EpsTriplet <T> limKin{colLim.eval(redAmpClass)};
        limKin2 = sq_amp(limKin);
    }
    print("limKin2", limKin2, verbose);
    const T colFac{-Nc2 / 4. + Nc4 / 4.};
    return colFac * limKin2.get0().real();
}

template<typename T, unsigned int mul, class ampC, class redAmpC>
T
getCollinearLimitAmp2Ratio_4g2A_gg(const vector <MOM<T>> &momenta, const int (&helicity)[mul], ampC &ampClass,
                                   redAmpC &redAmpClass, const int internalLeg, const Basis basis,
                                   const bool verbose = false) {
    if (verbose) {
        testCollinearPhaseSpacePoint(momenta, mul);
    }

    printHelicity("helicity", helicity, verbose);

    ampClass.setMomenta(momenta);
    ampClass.setHelicity(helicity);

    CollinearLimit<T, mul> colLim(verbose);
    colLim.init(internalLeg, helicity, momenta);

    const T sij{2 * dot(momenta[colLim.pos1], momenta[colLim.pos2])};
    print("s" + to_string(colLim.pos1) + to_string(colLim.pos2), sij, !verbose);

    EpsQuintuplet<T> amp2;
    T limPre2;
    switch (basis) {
        case reflectedFundamental:
            amp2 = ampClass.evalAmp2(reflectedFundamental);
            limPre2 = lim_rf(colLim, redAmpClass, verbose);
            break;
        case fundamental:
            cout << "Warning: Don't know how to do colour matrix and spin matrix at same time!" << endl;

//            amp2 = ampClass.evalAmp2_f();
//
//            redAmpClass.setMomenta(colLim.factMomenta);
//            redAmpClass.setHelicity(colLim.factHelicity_p);
//            complex <T> ampPlus0{redAmpClass.evalAmp().get0()};
//            const int orderOne[]{0, 2, 1, 3, 4};
//            redAmpClass.setPrimOrder(orderOne);
//            complex <T> ampPlus1{redAmpClass.evalAmp().get0()};
//            const complex <T> ampVec[]{
//                    ampPlus0,
//                    ampPlus1
//            };
//            const T colMatEl[]{-2 * Nc2 + Nc4, -2 * Nc2};
//            const int colMatOrder[]{0, 1, 0};
//            const T colNorm{(-1 + Nc2) / (8. * Nc2)};
//            const T ampPlus{redAmpClass.evalColourSum(ampVec, colMatEl, colMatOrder)};
//
//            break;
        default:
            cout << "Warning! Unrecognised colour basis. Returning zero for |amp|^2 and |lim|^2." << endl;
            limPre2 = 0.;
    }
    print("amp^2", amp2, verbose);
    T lim2{Nf2<T> * limPre2};
    print("limit2", lim2, verbose);

    return amp2.get0().real() / lim2;
}

// template for general 4g2A |amp|^2, returns ratio of |amp|^2 to |lim|^2 at 2||3 (g||g) PS point
template<typename T>
T
getCollinearLimitAmp2Ratio_23(const int (&helicity)[6], const T lambda, const Basis basis, const bool verbose = false) {
    vector <MOM<T>> mom = getCollinear23PhaseSpacePoint6(lambda, verbose);
    Amp_3g2A<T> amp_3g2A;
    Amp_4g2A<T> amp_4g2A;
    return getCollinearLimitAmp2Ratio_4g2A_gg(mom, helicity, amp_4g2A, amp_3g2A, 2, basis, verbose);
}

//// template for general 3||4 (g||A) 4g2A primitive amplitude, returns amp (should be zero)
//template<typename T>
//complex <T>
//getCollinearLimitAmp2_34(const int (&helicity)[6], const T lambda, const bool verbose = false) {
//    vector <MOM<T>> mom = getCollinear34PhaseSpacePoint6(lambda, verbose);
//    Amp_4g2A<T> amp_4g2A;
//    return getCollinearLimitAmp(mom, helicity, amp_4g2A, 3, verbose);
//}

// for --++++ |amp|^2, returns ratio of |amp|^2 to 2||3 |lim|^2 at PS point
template<typename T>
void scanAmp2_gp_gp(const T lim, const T step, const Basis basis = reflectedFundamental, const bool verbose = false) {
    scanAmp2(getCollinearLimitAmp2Ratio_23<T>, lim, step, {-1, -1, +1, +1, +1, +1}, basis, verbose);
}

//// for --++++ |amp|^2, returns |amp|^2 in 3||4 (g||A) |lim|^2 at PS point
//template<typename T>
//void scanAmp2_gp_Ap(const T lim, const T step, const Basis basis = reflectedFundamental, const bool verbose = false) {
//    scanAmp2(getCollinearLimitAmp2_34<T>, lim, step, {-1, -1, +1, +1, +1, +1}, basis, verbose, "|amp|^2");
//}

// for ++--++ |amp|^2, returns ratio of |amp|^2 to 2||3 |lim|^2 at PS point
template<typename T>
void scanAmp2_gm_gm(const T lim, const T step, const Basis basis = reflectedFundamental, const bool verbose = false) {
    scanAmp2(getCollinearLimitAmp2Ratio_23<T>, lim, step, {+1, +1, -1, -1, +1, +1}, basis, verbose);
}

// for +--+++ |amp|^2, returns ratio of |amp|^2 to 2||3 |lim|^2 at PS point
template<typename T>
void scanAmp2_gm_gp(const T lim, const T step, const Basis basis = reflectedFundamental, const bool verbose = false) {
    scanAmp2(getCollinearLimitAmp2Ratio_23<T>, lim, step, {+1, -1, -1, +1, +1, +1}, basis, verbose);
}

// for +++--+ |amp|^2, returns ratio of |amp|^2 to 2||3 |lim|^2 at PS point
template<typename T>
void scanAmp2_gp_gm(const T lim, const T step, const Basis basis = reflectedFundamental, const bool verbose = false) {
    scanAmp2(getCollinearLimitAmp2Ratio_23<T>, lim, step, {+1, +1, +1, -1, -1, +1}, basis, verbose);
}

int main(int argc, char **argv) {
    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(16);

    const bool verbose{argc == 2 ? (string(argv[1]) == "v") : false};

    const double dLim{1.8};
    const double dStep{0.09};

    const dd_real ddLim{10.};
    const dd_real ddStep{1.};

    const qd_real qdLim{16.};
    const qd_real qdStep{2.};

    cout << endl << "4g2A" << endl;

    cout << endl << "Amplitude squared level" << endl; //WIP

    cout << endl << "16 digit precision" << endl;

    cout << endl << "g+||g+" << endl; // Works
    scanAmp2_gp_gp<double>(dLim, dStep, reflectedFundamental, verbose);
//    cout << endl;
//    scanAmp2_gp_gp<double>(dLim, dStep, fundamental, verbose);

    cout << endl << "g-||g-" << endl; // Works
//    scanAmp2_gm_gm<double>(dLim, dStep, reflectedFundamental, verbose);

    cout << endl << "g-||g+" << endl; // Works
//    scanAmp2_gm_gp<double>(dLim, dStep, reflectedFundamental, verbose);

    cout << endl << "g+||g-" << endl; // Works
//    scanAmp2_gp_gm<double>(dLim, dStep, reflectedFundamental, verbose);

    cout << endl << "32 digit precision" << endl;

    cout << endl << "g+||g+" << endl; // Works
//    scanAmp2_gp_gp<dd_real>(ddLim, ddStep, reflectedFundamental, verbose);
//    cout << endl;
//    scanAmp2_gp_gp<dd_real>(ddLim, ddStep, fundamental, verbose);

    cout << endl << "g-||g-" << endl; // Works
//    scanAmp2_gm_gm<dd_real>(ddLim, ddStep, reflectedFundamental, verbose);

    cout << endl << "g-||g+" << endl; // Works
//    scanAmp2_gm_gp<dd_real>(ddLim, ddStep, reflectedFundamental, verbose);

    cout << endl << "g+||g-" << endl; // Works
//    scanAmp2_gp_gm<dd_real>(ddLim, ddStep, reflectedFundamental, verbose);

    cout << endl << "64 digit precision" << endl;

    cout << endl << "g+||g+" << endl; // Works
//    scanAmp2_gp_gp<qd_real>(qdLim, qdStep, reflectedFundamental, verbose);
//    scanAmp2_gp_gp<qd_real>(qdLim, qdStep, fundamental, verbose);

    cout << endl;

    return 1;
}
