// Test 2||3 (g||g) & 3||4 (g||A) limits of 4g2A in mhv helicity configurations
// Takes one CLI argument, v, to switch on debug output, i.e. `./4g2A-collinear v` for debug output

#include <iostream>
#include <vector>
#include <complex>
#include <string>
//#include <array>
#include <cmath>

#include "ngluon2/Initialize.h"
#include "tools/PhaseSpace.h"

#include "3g2A.h"
#include "4g2A.h"
#include "phaseSpace.h"
#include "common.h"
#include "collinear.h"

using std::cout;
using std::endl;
using std::vector;
using std::complex;
using std::string;
using std::to_string;
//using std::array;
using std::pow;

// template for general 4g2A primitive amplitude, returns ratio of amp to limit at 2||3 PS point
template<typename T>
complex <T>
getCollinearLimitPrimAmpRatio_23(const int (&helicity)[6], const T lambda, const bool verbose = false) {
    vector <MOM<T>> mom = getCollinear23PhaseSpacePoint6(lambda, verbose);
    Amp_3g2A<T> amp_3g2A;
    Amp_4g2A<T> amp_4g2A;
    return getCollinearLimitAmpRatio(mom, helicity, amp_4g2A, amp_3g2A, 2, verbose);
}

// template for general 3||4 (g||A) 4g2A primitive amplitude, returns amp (should be zero)
template<typename T>
complex <T>
getCollinearLimitPrimAmp_34(const int (&helicity)[6], const T lambda, const bool verbose = false) {
    vector <MOM<T>> mom = getCollinear34PhaseSpacePoint6(lambda, verbose);
    Amp_4g2A<T> amp_4g2A;
    return getCollinearLimitAmp(mom, helicity, amp_4g2A, 3, verbose);
}

// for --++++ primitive amplitude, returns ratio of amp to 2||3 limit at PS point
template<typename T>
void scanPrimAmp_gp_gp(const T lim, const T step, const bool verbose = false) {
    scanAmp(getCollinearLimitPrimAmpRatio_23<T>, lim, step, {-1, -1, +1, +1, +1, +1}, "amp/lim", verbose);
}

// for --++++ primitive amplitude, returns amp in 3||4 (g||A) limit at PS point
template<typename T>
void scanPrimAmp_gp_Ap(const T lim, const T step, const bool verbose = false) {
    scanAmp(getCollinearLimitPrimAmp_34<T>, lim, step, {-1, -1, +1, +1, +1, +1}, "amp", verbose);
}

// for ++--++ primitive amplitude, returns ratio of amp to 2||3 limit at PS point
template<typename T>
void scanPrimAmp_gm_gm(const T lim, const T step, const bool verbose = false) {
    scanAmp(getCollinearLimitPrimAmpRatio_23<T>, lim, step, {+1, +1, -1, -1, +1, +1}, "amp/lim", verbose);
}

// for +--+++ primitive amplitude, returns ratio of amp to 2||3 limit at PS point
template<typename T>
void scanPrimAmp_gm_gp(const T lim, const T step, const bool verbose = false) {
    scanAmp(getCollinearLimitPrimAmpRatio_23<T>, lim, step, {+1, -1, -1, +1, +1, +1}, "amp/lim", verbose);
}

// for +++--+ primitive amplitude, returns ratio of amp to 2||3 limit at PS point
template<typename T>
void scanPrimAmp_gp_gm(const T lim, const T step, const bool verbose = false) {
    scanAmp(getCollinearLimitPrimAmpRatio_23<T>, lim, step, {+1, +1, +1, -1, -1, +1}, "amp/lim", verbose);
}

int main(int argc, char **argv) {
    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(16);

    const bool verbose{argc == 2 ? (string(argv[1]) == "v") : false};

    const double dLim{1.8};
    const double dStep{0.09};

    const dd_real ddLim{10.};
    const dd_real ddStep{1.};

    const qd_real qdLim{16.};
    const qd_real qdStep{2.};

    cout << endl << "4g2A" << endl;

    cout << endl << "Amplitude level" << endl;

    cout << endl << "16 digit precision" << endl;

    cout << endl << "g+||g+" << endl; // Works (goes to 1)
    scanPrimAmp_gp_gp<double>(dLim, dStep, verbose);

    cout << endl << "g-||g-" << endl; // Works
//    scanPrimAmp_gm_gm<double>(dLim, dStep, verbose);

    cout << endl << "g-||g+" << endl; // Works
    scanPrimAmp_gm_gp<double>(dLim, dStep, verbose);

    cout << endl << "g+||g-" << endl; // Works
//    scanPrimAmp_gp_gm<double>(dLim, dStep, verbose);

    cout << endl << "g+||A+" << endl; // Works (goes to constant)
//    scanPrimAmp_gp_Ap<double>(dLim, dStep, verbose);

    cout << endl << "32 digit precision" << endl;
//
    cout << endl << "g+||g+" << endl; // Works
//    scanPrimAmp_gp_gp<dd_real>(ddLim, ddStep, verbose);

    cout << endl << "g-||g-" << endl; // Works
//    scanPrimAmp_gm_gm<dd_real>(ddLim, ddStep, verbose);

    cout << endl << "g-||g+" << endl; // Works
//    scanPrimAmp_gm_gp<dd_real>(ddLim, ddStep, verbose);

    cout << endl << "g+||g-" << endl; // Works
//    scanPrimAmp_gp_gm<dd_real>(ddLim, ddStep, verbose);

    cout << endl << "g+||A+" << endl; // Works
//    scanPrimAmp_gp_Ap<dd_real>(ddLim, ddStep, verbose);

    cout << endl << "64 digit precision" << endl;

    cout << endl << "g+||g+" << endl; // Works
//    scanPrimAmp_gp_gp<qd_real>(qdLim, qdStep, verbose);

    cout << endl << "g-||g-" << endl; // Works
//    scanPrimAmp_gm_gm<qd_real>(qdLim, qdStep, verbose);

    cout << endl << "g-||g+" << endl; // Works
//    scanPrimAmp_gm_gp<qd_real>(qdLim, qdStep, verbose);

    cout << endl << "g+||g-" << endl; // Works
//    scanPrimAmp_gp_gm<qd_real>(qdLim, qdStep, verbose);

    cout << endl << "g+||A+" << endl; // Works
//    scanPrimAmp_gp_Ap<qd_real>(qdLim, qdStep, verbose);

    cout << endl;

    return 1;
}
