#ifndef NJET_EXAMPLES_1G2A_H
#define NJET_EXAMPLES_1G2A_H

#include <vector>
#include <complex>

#include "tools/PhaseSpace.h"

#include "amp-gA.h"
#include "common.h"

using std::vector;
using std::complex;

template<typename T>
complex <T> evalAmp_1g2A(const vector <MOM<T>> &momenta, const int (&helicity)[3]) {
    const Flavour<double> flavours[]{g, A, A};

    const int orders[][3]{
        {1, 2, 0,},
        {1, 0, 2,},
    };

    return evalPrimitiveAmplitude(flavours, momenta, helicity, orders).get0();
}

#endif //NJET_EXAMPLES_1G2A_H
