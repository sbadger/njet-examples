#include <iostream>
#include <getopt.h>
#include <cstdio>

#include "tools/PhaseSpace.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

using std::cout;
using std::endl;
using std::vector;
using std::conj;
using std::complex;
using std::pow;

// Class for amplitudes and phase space generator

#include <ngluon2/Model.h>
#include <tools/PhaseSpace.h>

#include <cuba.h>

#define NDIM 1
#define NCOMP 1
#define USERDATA NULL
#define NVEC 1
#define EPSREL 1e-05
#define EPSABS 1e-05
#define LAST 1
#define SEED 0
#define MINEVAL 0
#define MAXEVAL 50000

#define NSTART 3000
#define NINCREASE 700
#define NBATCH 1000
#define GRIDNO 0
#define STATEFILE NULL

// hard code for now. could set as command line options in the future
int opt_rseed = 234;
int opt_maxpts = 10000;

const Flavour<double> A = StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());
const Flavour<double> g = StandardModel::G();

using namespace std;

class AmplitudeGGAA
{
    typedef std::vector<MOM<double> > Momenta;
    public:
    // ps(flavs, rseed, sqrtS, ptmin, etamax, deltaR)
    // ptmin is relative to sqrtS
    // using setDecay will change sqrtS to mass of flavs[0]
    AmplitudeGGAA() :
        ps(4, flavours, 2453, 1., 1e-10, 1e10, 1e-3)
    {
        const double MuR = 91.188;
        amp.setProcess(4, flavours);
        amp.setLoopInduced();
        amp.setMuR2(MuR*MuR);

    }

    double getampsq() {

        EpsTriplet<double> res(0.,0.,0.);
        double ans = 0.;

        for (int hh = 0; hh < nhels; ++hh) {
            amp.setHelicity(hels[hh]);
            for (int pp = 0; pp < nperms; ++pp) {
                amp.setOrder(perms[pp]);
                amp.eval(1);
                res += amp.getres();
            }
            ans += pow(abs(res.get0()),2.);
        }

        return ans;
    };

    int me_psvol(const int *ndim, const double x[], const int *ncomp, double f[])
    {
        moms = ps.getPSpoint(x);
        double me_val;
        if (ps.checkPS()) {
            me_val = 0.;
        } else {
            me_val = 1.;
        }
        f[0] = ps.getWeight()*me_val;
        return 0;
    }

    int me_born(const int *ndim, const double x[], const int *ncomp, double f[])
    {
        moms = ps.getPSpoint(x);
        amp.setMomenta(moms);
        double me_val;
        if (ps.checkPS()) {
            me_val = 0.;
        } else {
            me_val = getampsq();
        }
        f[0] = ps.getWeight()*me_val;
        return 0;
    }

    private:
    static int nn, nhels, nperms;
    static int hels[6][4], perms[6][4];
    static Flavour<double> flavours[4];
    Momenta moms;

    PhaseSpace<double> ps;
    NParton2<double> amp;
};

// declare static arrays

int AmplitudeGGAA::nn = 4;

Flavour<double> AmplitudeGGAA::flavours[4] = {g,g,A,A};

// number of permuations (each call is for an ordered amplitude)
int AmplitudeGGAA::nperms = 6;
// list of permuations
int AmplitudeGGAA::perms[6][4] = {
    {0,1,2,3},
    {0,2,1,3},
    {1,0,2,3},
    {1,2,0,3},
    {2,0,1,3},
    {2,1,0,3}
};

// number of indep. helicities
int AmplitudeGGAA::nhels = 6;
// list of indep. helicities
int AmplitudeGGAA::hels[6][4] = {
    {-1,-1,+1,+1},
    {-1,+1,-1,+1},
    {-1,+1,+1,-1},
    {+1,-1,-1,-1},
    {+1,-1,-1,-1},
    {+1,-1,+1,-1}
};

int IntegrandVol(const int *ndim, const double x[], const int *ncomp, double f[], void *userdata)
{
    return reinterpret_cast<AmplitudeGGAA*>(userdata)->me_psvol(ndim, x, ncomp, f);
}

int IntegrandB(const int *ndim, const double x[], const int *ncomp, double f[], void *userdata)
{
    return reinterpret_cast<AmplitudeGGAA*>(userdata)->me_born(ndim, x, ncomp, f);
}

int main(int argc, char **argv)
{

    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(16);

    // start initialise amplitude
    cout << "here" << endl;

    AmplitudeGGAA GGAA;

    cout << "here" << endl;
    // finish initialise amplitude

    // start set integation parameters
    int verbose = 1;
    int spin = -1;
    int neval, fail;
    double integral[NCOMP], error[NCOMP], prob[NCOMP];

    int nfinalstate = 2;
    int ndim = 3*nfinalstate - 4;
    // finish integation parameters

    Vegas(ndim, NCOMP, &IntegrandVol, reinterpret_cast<void*>(&GGAA), NVEC,
            EPSREL, EPSABS, verbose, opt_rseed,
            MINEVAL, opt_maxpts, NSTART, NINCREASE, NBATCH,
            GRIDNO, STATEFILE,
            &spin, &neval, &fail, integral, error, prob);

    cout << "### ans = " << integral[0] << " +/- " << error[0]
        << " (" << (error[0]/integral[0]*100.) << "%)"
        << endl;

    Vegas(ndim, NCOMP, &IntegrandB, reinterpret_cast<void*>(&GGAA), NVEC,
            EPSREL, EPSABS, verbose, opt_rseed,
            MINEVAL, opt_maxpts, NSTART, NINCREASE, NBATCH,
            GRIDNO, STATEFILE,
            &spin, &neval, &fail, integral, error, prob);

    cout << "### ans = " << integral[0] << " +/- " << error[0]
        << " (" << (error[0]/integral[0]*100.) << "%)"
        << endl;

    return 1;
}
