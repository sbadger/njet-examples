// BROKEN - need to generate well behaved phase space point

#include <iostream>
#include <vector>
#include <complex>
#include <cmath>

#include "tools/PhaseSpace.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

#include "amp-gA.h"
#include "phaseSpace.h"
#include "common.h"
#include "collinear.h"
#include "2g2A.h"
#include "1g2A.h"

using std::cout;
using std::endl;
using std::vector;
using std::conj;
using std::complex;
using std::pow;

template<typename T>
complex <T> evalAmp_2g2A_mmpp(const T theta, const bool verbose = false) {
    const int helicity[]{-1, -1, +1, +1};
    //    const vector <MOM<T>> momenta{getCollinearPhaseSpacePoint4<T>(theta, verbose)};
    const vector <MOM<T>> momenta{getMasslessPhaseSpacePoint<T>(4, verbose)}; // temp
    return getCollinearLimitAmpRatio(momenta, helicity, evalAmp_2g2A<T>, evalAmp_1g2A<T>, 0, verbose);
}

template<typename T>
void testMomenta(const T lim, const T step) {
    for (T p{T(1.)}; p < lim; p += step) {
        testCollinearPhaseSpacePoint4(pow(10, -p));
    }
}

int main(int argc, char **argv) {
    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(8);

    const bool verbose{argc == 2 ? (string(argv[1]) == "v") : false};

    //    testMomenta<qd_real>(10,1);

    // single check point
    complex<double> amp{evalAmp_2g2A_mmpp<double>(double(0.1), verbose)};
    cout << "amp/lim=" << amp << endl << endl;

    //    const dd_real ddLim{10.};
    //    const dd_real ddStep{1.};
    //    scanPrimAmp(evalAmp_2g2A_mmpp<dd_real>, ddLim, ddStep, "amp/lim", verbose);

    return 1;
}
