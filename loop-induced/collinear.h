#ifndef NJET_EXAMPLES_COLLINEAR_H
#define NJET_EXAMPLES_COLLINEAR_H

#include <iostream>
#include <vector>
#include <complex>
#include <string>
//#include <algorithm>
#include <cmath>

#include "EpsQuintuplet.h"
#include "common.h"

using std::cout;
using std::endl;
using std::vector;
using std::complex;
using std::conj;
using std::real;
using std::string;
//using std::copy;
using std::floor;

template<typename T, unsigned int mul>
class CollinearLimit {
private:
    static const int unsigned factMul{mul - 1};

    bool verbose;

    unsigned int pos1;
    unsigned int pos2;
    unsigned int factPos2;
    unsigned int factPos3;

    int h1;
    int h2;
    int factHelicity_p[factMul];
    int factHelicity_m[factMul];

    vector <MOM<T>> factMomenta;
    MOM <T> p1;
    MOM <T> p2;
    complex <T> z1A;
    complex <T> z1B;
    complex <T> z2A;
    complex <T> z2B;

    void copyHelicities(const int (&helicity)[mul], int (&fact_helicity)[factMul]);

    const complex <T> splitFuncTree_ggg_mpp();

    const complex <T> splitFuncTree_ggg_pmp();

    const complex <T> splitFuncTree_ggg_ppm();

    const complex <T> splitFuncTree_ggg_ppp();

    const complex <T> splitFuncTree_ggg_pmm();

    const complex <T> splitFuncTree_ggg_mmp();

    const complex <T> splitFuncTree_ggg_mpm();

    const complex <T> splitFuncTree_ggg_mmm();

public:
    unsigned int get_pos1(){
        return this->pos1;
    }
    unsigned int get_pos2(){
        return this->pos2;
    }

    // Must do before setHelicity!
    void setFactorisationChannel(const int firstCollinearLeg);

    void setHelicities(const int (&helicity)[mul]);

    void setMomenta(const vector <MOM<T>> (&momenta));

    const complex <T> splitFuncTree_ggg_hel(const int hP);

    const complex <T> elementSplit(const int h1_, const int h2_);

    void init(const int firstCollinearLeg, const int (&helicity)[mul], const vector <MOM<T>> (&momenta));

    template<class redAmpC>
    EpsTriplet <T> eval(redAmpC &redAmpClass);

    // For trivial colour, returns kinematics only
    template<class redAmpC>
    EpsQuintuplet<T> spinCorrelate(redAmpC &redAmpClass);

    // For fundamental basis colour, collinear limit of 4g2A
    template<class redAmpC>
    EpsQuintuplet<T> spinCorrelateAlt(redAmpC &redAmpClass);

    CollinearLimit(const bool verbose_ = false);
};

template<typename T, unsigned int mul>
CollinearLimit<T, mul>::CollinearLimit(const bool verbose_) {
    verbose = verbose_;
    factMomenta.reserve(factMul);
}

// Logic assumes that for i||j, j=i+1 & i<=mul-2 (=> j<=mul-1)
template<typename T, unsigned int mul>
void CollinearLimit<T, mul>::setFactorisationChannel(const int firstCollinearLeg) {
    pos1 = firstCollinearLeg;
    pos2 = (pos1 + 1) % mul;
    factPos2 = (pos1 + 1) % factMul;
    factPos3 = (pos1 + 2) % factMul;
}

template<typename T, unsigned int mul>
void CollinearLimit<T, mul>::copyHelicities(const int (&helicity)[mul], int (&fact_helicity)[factMul]) {
    unsigned int freeLegs[mul - 2];
    int j{0};
    for (unsigned int i{0}; i < mul; ++i) {
        if ((i != pos1) && (i != pos2)) {
            freeLegs[j] = i;
            ++j;
        }
    }
    for (auto i: freeLegs) {
        fact_helicity[i > pos1 ? i - 1 : i] = helicity[i];
    }
}

template<typename T, unsigned int mul>
void CollinearLimit<T, mul>::setHelicities(const int (&helicity)[mul]) {
    h1 = helicity[pos1];
    h2 = helicity[pos2];

    copyHelicities(helicity, factHelicity_p);
    factHelicity_p[pos1] = 1;
    printHelicity("factHel h+", factHelicity_p, verbose);

    copyHelicities(helicity, factHelicity_m);
    factHelicity_m[pos1] = -1;
    printHelicity("factHel h-", factHelicity_m, verbose);
}

template<typename T, unsigned int mul>
void CollinearLimit<T, mul>::setMomenta(const vector <MOM<T>> (&momenta)) {
    // Catani-Seymour momentum mapping 12;3
    p1 = momenta[pos1];
    p2 = momenta[factPos2];
    const MOM <T> p12{p1 + p2};
    const MOM <T> p3{momenta[factPos3]};
    const T x12_3{dot(p1, p2) / dot(p12, p3)};
    const MOM <T> p12t{p12 - x12_3 * p3};
    const MOM <T> p3t{(T(1.) + x12_3) * p3};
    z1A = spA(p1, p3) / spA(p12, p3);
    z1B = spB(p1, p3) / spB(p12, p3);
    z2A = spA(p2, p3) / spA(p12, p3);
    z2B = spB(p2, p3) / spB(p12, p3);
    factMomenta[pos1] = p12t;
    factMomenta[factPos2] = p3t;
    for (unsigned int i{0}; i < factMul + 1; ++i) {
        if ((i != pos1) && (i != factPos2)) {
            factMomenta[i > pos1 ? i - 1 : i] = momenta[i];
        }
    }
    printMomenta(factMomenta, verbose);
}

template<typename T, unsigned int mul>
void CollinearLimit<T, mul>::init(const int firstCollinearLeg, const int (&helicity)[mul],
                                  const vector <MOM<T>> (&momenta)) {
    setFactorisationChannel(firstCollinearLeg);
    setHelicities(helicity);
    setMomenta(momenta);
}

string helToStr(const int h) {
    return h == 1 ? string("+") : string("-");
}

// template for general splitting function, general i||j limit, returns value of limit
// internalLeg denotes leg position i, the first collinear leg of i||j in the ordered amplitude
template<typename T, unsigned int mul>
template<class redAmpC>
EpsTriplet <T> CollinearLimit<T, mul>::eval(redAmpC &redAmpClass) {
    redAmpClass.setMomenta(factMomenta);

    redAmpClass.setHelicity(factHelicity_p);
    EpsTriplet <T> factAmp_p{redAmpClass.evalAmp()};
    print("factAmp h+", factAmp_p, verbose);

    redAmpClass.setHelicity(factHelicity_m);
    EpsTriplet <T> factAmp_m{redAmpClass.evalAmp()};
    print("factAmp h-", factAmp_p, verbose);

    const complex <T> split_m{splitFuncTree_ggg_hel(-1)};
    print("split -", split_m, verbose);

    const complex <T> split_p{splitFuncTree_ggg_hel(1)};
    print("split +", split_p, verbose);

    return {factAmp_p * split_m + factAmp_m * split_p};
}

template<typename T, unsigned int mul>
const complex <T> CollinearLimit<T, mul>::elementSplit(const int h1_, const int h2_) {

    const complex <T> sol{conj(splitFuncTree_ggg_hel(h1_)) * splitFuncTree_ggg_hel(h2_)};
    print("M_split^(" + helToStr(h1_) + helToStr(h2_) + ")", sol, verbose);
    return sol;
}

// Does (0,1) -> (-1,+1)
const int intToHel(const unsigned int i) {
    return i == 1 ? +1 : -1;
}

template<typename T, unsigned int mul>
template<class redAmpC>
EpsQuintuplet<T> CollinearLimit<T, mul>::spinCorrelate(redAmpC &redAmpClass) {
    redAmpClass.setMomenta(factMomenta);

    redAmpClass.setHelicity(factHelicity_p);
    const EpsTriplet <T> factAmp_p{redAmpClass.evalAmp()};
    print("factAmp h+", factAmp_p, verbose);

    redAmpClass.setHelicity(factHelicity_m);
    const EpsTriplet <T> factAmp_m{redAmpClass.evalAmp()};
    print("factAmp h-", factAmp_m, verbose);

    const EpsTriplet <T> ampVec[]{factAmp_p, factAmp_m};

    const unsigned int spnLen{2};
    complex <T> spinMatrix[spnLen][spnLen];
    for (unsigned int i{0}; i < spnLen; ++i) {
        for (unsigned int j{0}; j < spnLen; ++j) {
            spinMatrix[i][j] = elementSplit(intToHel(i), intToHel(j));
        }
    }

    EpsQuintuplet<T> amp2;
    for (unsigned int i{0}; i < spnLen; ++i) {
        for (unsigned int j{0}; j < spnLen; ++j) {
            amp2 += multiplyEpsTriplets(conj(ampVec[i]), ampVec[j]) * spinMatrix[i][j];
        }
    }

    return amp2;
}

template<typename T, unsigned int m, unsigned int p>
complex <T>
kroneckerProduct(const T (&colourMatrix)[m][m], const complex<T> (&spinMatrix)[p][p], const unsigned int i,
                 const unsigned int j) {
    const unsigned int colI = floor(static_cast<float>(i) / static_cast<float>(p));
    const unsigned int colJ = floor(static_cast<float>(j) / static_cast<float>(p));
    const unsigned int spnI{i % p};
    const unsigned int spnJ{j % p};
//    print("i ", i);
//    print("j ", j);
//    print("ci", colI);
//    print("cj", colJ);
//    print("si", spnI);
//    print("sj", spnJ);
    return colourMatrix[colI][colJ] * spinMatrix[spnI][spnJ];
}

// Not working
template<typename T, unsigned int mul>
template<class redAmpC>
EpsQuintuplet<T> CollinearLimit<T, mul>::spinCorrelateAlt(redAmpC &redAmpClass) {
    redAmpClass.setMomenta(factMomenta);

    const unsigned int orderZero[]{0, 1, 2, 3, 4};
    const unsigned int orderOne[]{0, 2, 1, 3, 4};

    redAmpClass.setHelicity(factHelicity_p);

    redAmpClass.setPrimOrder(orderZero);
    const EpsTriplet <T> factAmp_p_0{redAmpClass.evalAmp()};
    print("factAmp h+ o0", factAmp_p_0, verbose);

    redAmpClass.setPrimOrder(orderOne);
    const EpsTriplet <T> factAmp_p_1{redAmpClass.evalAmp()};
    print("factAmp h+ o1", factAmp_p_1, verbose);

    redAmpClass.setHelicity(factHelicity_m);

    redAmpClass.setPrimOrder(orderZero);
    const EpsTriplet <T> factAmp_m_0{redAmpClass.evalAmp()};
    print("factAmp h- o0", factAmp_m_0, verbose);

    redAmpClass.setPrimOrder(orderOne);
    const EpsTriplet <T> factAmp_m_1{redAmpClass.evalAmp()};
    print("factAmp h- o1", factAmp_m_1, verbose);

    const unsigned int len{4};
    const EpsTriplet <T> ampVec[]{
            factAmp_p_0,
            factAmp_m_0,
            factAmp_p_1,
            factAmp_m_1
    };

    const unsigned int spnLen{2};
    complex <T> spinMatrix[spnLen][spnLen];
    for (unsigned int i{0}; i < spnLen; ++i) {
        for (unsigned int j{0}; j < spnLen; ++j) {
            spinMatrix[i][j] = elementSplit(intToHel(i), intToHel(j));
        }
    }

    const T colNorm{(-1 + Nc2) / (8. * Nc2)};
    const unsigned int colMatOrder[]{0, 1, 0};
    const T colMatEl[]{-2 * Nc2 + Nc4, -2 * Nc2};

    const unsigned int colLen{2};
    T colourMatrix[colLen][colLen];
    for (unsigned int i{0}; i < colLen; ++i) {
        for (unsigned int j{0}; j < colLen; ++j) {
            colourMatrix[i][j] = redAmpClass.colMat(colLen, colMatEl, colMatOrder, i, j);
        }
    }

    EpsQuintuplet<T> amp2;
    for (unsigned int i{0}; i < len; ++i) {
        for (unsigned int j{0}; j < len; ++j) {
            amp2 += multiplyEpsTriplets(conj(ampVec[i]), ampVec[j]) * kroneckerProduct(colourMatrix, spinMatrix, i, j);
        }
    }

    return Nf2<T> * colNorm * amp2;
}

template<typename T, unsigned int mul>
const complex <T>
CollinearLimit<T, mul>::splitFuncTree_ggg_hel(const int hP) {
    switch (hP) {
        case +1:
            switch (h1) {
                case +1:
                    switch (h2) {
                        case +1:
                            return splitFuncTree_ggg_ppp();
                        case -1:
                            return splitFuncTree_ggg_ppm();
                    }
                case -1:
                    switch (h2) {
                        case +1:
                            return splitFuncTree_ggg_pmp();
                        case -1:
                            return splitFuncTree_ggg_pmm();
                    }
            }
        case -1:
            switch (h1) {
                case +1:
                    switch (h2) {
                        case +1:
                            return splitFuncTree_ggg_mpp();
                        case -1:
                            return splitFuncTree_ggg_mpm();
                    }
                case -1:
                    switch (h2) {
                        case +1:
                            return splitFuncTree_ggg_mmp();
                        case -1:
                            return splitFuncTree_ggg_mmm();
                    }
            }
    }
    cout << "Warning: undefined helicity configuration. Returning zero." << endl;
    return T(0.);
}

// -++ (P,p1,p2) g->gg splitting function at tree level
template<typename T, unsigned int mul>
const complex <T> CollinearLimit<T, mul>::splitFuncTree_ggg_mpp() {
    return T(1.) / spA(p1, p2) / z1A / z2A;
}

// +-+ (P,p1,p2) g->gg splitting function at tree level
template<typename T, unsigned int mul>
const complex <T> CollinearLimit<T, mul>::splitFuncTree_ggg_pmp() {
    const complex <T> z1A3{z1A * z1A * z1A};
    return z1A3 / z2A / spA(p1, p2);
}

// ++- (P,p1,p2) g->gg splitting function at tree level
template<typename T, unsigned int mul>
const complex <T> CollinearLimit<T, mul>::splitFuncTree_ggg_ppm() {
    const complex <T> z2A3{z2A * z2A * z2A};
    return z2A3 / z1A / spA(p1, p2);
}

// +++ (P,p1,p2) g->gg splitting function at tree level
template<typename T, unsigned int mul>
const complex <T> CollinearLimit<T, mul>::splitFuncTree_ggg_ppp() {
    return T(0.);
}

// +-- (P,p1,p2) g->gg splitting function at tree level
template<typename T, unsigned int mul>
const complex <T> CollinearLimit<T, mul>::splitFuncTree_ggg_pmm() {
    return -T(1.) / spB(p1, p2) / z1B / z2B;
}

// --+ (P,p1,p2) g->gg splitting function at tree level
template<typename T, unsigned int mul>
const complex <T> CollinearLimit<T, mul>::splitFuncTree_ggg_mmp() {
    const complex <T> z2B3{z2B * z2B * z2B};
    return -z2B3 / z1B / spB(p1, p2);
}

// -+- (P,p1,p2) g->gg splitting function at tree level
template<typename T, unsigned int mul>
const complex <T> CollinearLimit<T, mul>::splitFuncTree_ggg_mpm() {
    const complex <T> z1B3{z1B * z1B * z1B};
    return -z1B3 / z2B / spB(p1, p2);
}

// --- (P,p1,p2) g->gg splitting function at tree level
template<typename T, unsigned int mul>
const complex <T> CollinearLimit<T, mul>::splitFuncTree_ggg_mmm() {
    return T(0.);
}

// test primitive amplitude ratio to (reduced amp * splitting function) for 1, or amp for 0,
// with PS point moving into harder i||j limit
template<typename T, typename F, unsigned int mul>
void scanAmp(const F func, const T lim, const T step, const int (&helicity)[mul], const string name = "amp/lim",
             const bool verbose = false) {
    for (T p{T(1.)}; p < lim; p += step) {
        const T lambda{pow(10., -p)};
        if (verbose) {
            cout << endl << "lambda=" << lambda << ":" << endl;
        }
        const auto sol{func(helicity, lambda, verbose)};
        cout << name << "=" << sol << endl;
    }
}

template<typename T, typename F, unsigned int mul>
void
scanAmp2(const F func, const T lim, const T step, const int (&helicity)[mul], const Basis basis,
         const bool verbose = false, const string name = "|amp|^2/|lim|^2") {
    for (T p{T(1.)}; p < lim; p += step) {
        const T lambda{pow(10., -p)};
        if (verbose) {
            cout << endl << "lambda=" << lambda << ":" << endl;
        }
        const auto sol{func(helicity, lambda, basis, verbose)};
        cout << name << "=" << sol << endl;
    }
}

// template for general primitive amplitude, returns ratio of amp to limit at general PS point
template<typename T, unsigned int mul, class ampC, class redAmpC>
complex <T>
getCollinearLimitAmpRatio(const vector <MOM<T>> &momenta, const int (&helicity)[mul], ampC &ampClass,
                          redAmpC &redAmpClass, const int internalLeg, const bool verbose = false) {
    if (verbose) {
        testCollinearPhaseSpacePoint(momenta, mul);
    }

    printHelicity("helicity", helicity, verbose);

    ampClass.setMomenta(momenta);
    ampClass.setHelicity(helicity);
    EpsTriplet <T> amp{ampClass.evalAmp()};
    print("amp", amp, verbose);

    CollinearLimit<T, mul> colLim(verbose);
    colLim.init(internalLeg, helicity, momenta);

    const T sij{2 * dot(momenta[colLim.get_pos1()], momenta[colLim.get_pos2()])};
    print("s" + to_string(colLim.get_pos1()) + to_string(colLim.get_pos2()), sij, !verbose);

    const EpsTriplet <T> limit{colLim.eval(redAmpClass)};
    print("limit", limit, verbose);

    return amp.get0() / limit.get0();
}

// template for general || gA primitive amplitude, returns amp (use when should be constant/order lambda)
template<typename T, unsigned int mul, class ampC>
complex <T>
getCollinearLimitAmp(vector <MOM<T>> &momenta, const int (&helicity)[mul], ampC &ampClass, const int internalLeg,
                     const bool verbose = false) {
    if (verbose) {
        testCollinearPhaseSpacePoint(momenta, mul);
    }
    printHelicity("helicity", helicity, verbose);

    ampClass.setMomenta(momenta);
    ampClass.setHelicity(helicity);
    EpsTriplet <T> amp{ampClass.evalAmp()};
    print("amp", amp, verbose);

    const T sij{2 * dot(momenta[internalLeg], momenta[(internalLeg + 1) % mul])};
    print("s" + to_string(internalLeg) + to_string((internalLeg + 1) % mul), sij, !verbose);

    return amp.get0();
}

#endif //NJET_EXAMPLES_COLLINEAR_H
