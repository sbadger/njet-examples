#ifndef NJET_EXAMPLES_COMMON_H
#define NJET_EXAMPLES_COMMON_H

#include "ngluon2/Model.h"

#include <iostream>
#include <vector>
#include <complex>
#include <string>
#include <cassert>

using std::cout;
using std::endl;
using std::vector;
using std::complex;
using std::string;
using std::to_string;
using std::abs;

const Flavour<double> A{StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C())};
const Flavour<double> g{StandardModel::G()};

// light quark charge sum
// 2 * up-type-quark-charge + 3 * down-type-quark-charge
// 2 * std::pow(2 / 3, 2) + 3 * std::pow(-1 / 3, 2)
template<typename T>
const T Nf{11. / 9.};

template<typename T>
const T Nf2{Nf<T> * Nf<T>};

const int Nc{3};
const int Nc2{Nc * Nc};
const int Nc3{Nc2 * Nc};
const int Nc4{Nc3 * Nc};
const int Nc6{Nc4 * Nc2};

enum Basis {
    reflectedFundamental, fundamental
};

template<typename T>
T mod_sq(complex <T> n) {
    return real(conj(n) * n);
}

template<typename T>
void print(const string &name, const T &datum, const bool verbose = true) {
    if (verbose) {
        cout << name << "=" << datum << endl;
    }
}

template<typename AT, unsigned int len>
string genArrStr(const AT (&array)[len]) {
    string arrStr;
    for (auto el: array) {
        arrStr.append(to_string(el));
    }
    return arrStr;
}

template<unsigned int mul>
string genHelStr(const int (&helInt)[mul]) {
    string helStr;
    for (auto hel: helInt) {
        helStr.append(hel == 1 ? "+" : "-");
    }
    return helStr;
}

template<unsigned int mul>
void printHelicity(const string &name, const int (&helInt)[mul], const bool verbose = true) {
    print(name, genHelStr(helInt), verbose);
}


template<typename T>
void printMomenta(const vector <MOM<T>> momenta, const bool verbose = false) {
    if (verbose) {
        int i{0};
        for (auto mom: momenta) {
            cout << 'p' << i << '=' << mom << endl;
            ++i;
        }
    }
}

// test that phase space point has massless momenta and that momentum conservation is obeyed
template<typename T>
void checkPhaseSpacePoint(const vector <MOM<T>> &momenta, const bool verbose = false) {
    T zero{pow(10, -10)};
    MOM <T> sum;
    int i{0};
    for (auto momentum: momenta) {
        sum += momentum;
        print("m" + to_string(i), momentum.mass(), verbose);
        assert(abs(momentum.mass()) <= zero);
        ++i;
    }
    print("momentum sum", sum, verbose);
    assert(sum.x0 <= zero);
    assert(sum.x1 <= zero);
    assert(sum.x2 <= zero);
    assert(sum.x3 <= zero);
}

#endif //NJET_EXAMPLES_COMMON_H
