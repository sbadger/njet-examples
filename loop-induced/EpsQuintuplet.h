#ifndef NJET_EXAMPLES_EPSQUINTUPLET_H
#define NJET_EXAMPLES_EPSQUINTUPLET_H

#include <complex>
#include <iostream>

// functions for EpsTriplet -> EpsQuintuplet

template <typename T>
const EpsQuintuplet<T> sq_amp(const EpsTriplet<T> amp)
{
    return multiplyEpsTriplets(conj(amp), amp);
}

template <typename T>
const EpsQuintuplet<T> multiplyEpsTriplets(const EpsTriplet<T> a, const EpsTriplet<T> b)
{
    return EpsQuintuplet<T>(
        a.get0() * b.get0(),
        a.get1() * b.get0() + a.get0() * b.get1(),
        a.get1() * b.get1() + a.get2() * b.get0() + a.get0() * b.get2(),
        a.get2() * b.get1() + a.get1() * b.get2(),
        a.get2() * b.get2());
}

#endif //NJET_EXAMPLES_EPSQUINTUPLET_H
