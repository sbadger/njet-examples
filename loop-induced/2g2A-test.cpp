#include <iostream>
#include <vector>
#include <complex>

#include "tools/PhaseSpace.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

#include "common.h"
#include "phaseSpace.h"
#include "2g2A.h"
#include "EpsQuintuplet.h"

using std::cout;
using std::endl;
using std::vector;
using std::complex;

template<typename T>
void test(const bool verbose) {
    const int hel[]{-1, -1, +1, +1};

    const vector <MOM<T>> mom{getMasslessPhaseSpacePoint<T>(4, verbose)};

    const complex <T> res = complex<T>(0., 4.) /
        (xspA(mom[0], mom[1]) * xspA(mom[1], mom[2]) * xspA(mom[2], mom[3]) *
         xspA(mom[3], mom[0])) * (+S(mom[0] + mom[1]) * S(mom[1] + mom[2])
         );


    const complex <T> res2 = -complex<T>(0., 4.) /
        (xspA(mom[1], mom[2]) * xspA(mom[2], mom[3]) * xspA(mom[1], mom[3])) *
        (+xspA(mom[0], mom[1]) * xspA(mom[0], mom[3]) * xspB(mom[1], mom[3])
        );

    Amp_2g2A<T> amp_2g2A;
    amp_2g2A.setMomenta(mom);
    amp_2g2A.setHelicity(hel);
    const EpsTriplet <T> amp = amp_2g2A.evalAmp();

    cout << endl << "all plus cross check:     " << res << endl;
    cout << "single minus cross check: " << res2 << endl;
    cout << "A1.eps0/A1(++++) =        " << amp.get0() / res << endl;
    cout << "A1.eps0/A1(-+++) =        " << amp.get0() / res2 << endl;

    cout << endl << "A(" << genHelStr(hel) << ") = " << amp << endl << endl;

    const EpsQuintuplet<T> amp2{sq_amp(amp)};

    cout << "|A^2| = " << amp2 << endl;

    cout << endl;
}

int main(int argc, char **argv) {
    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(8);

    const bool verbose{argc == 2 ? (string(argv[1]) == "v") : false};

    test<double>(verbose);

    return 1;
}
