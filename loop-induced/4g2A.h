#ifndef NJET_EXAMPLES_4G2A_H
#define NJET_EXAMPLES_4G2A_H

#include <vector>
#include <complex>

#include "tools/PhaseSpace.h"

#include "amp-gA.h"
#include "common.h"

using std::vector;
using std::complex;
using std::conj;
using std::real;

template<typename T>
class Amp_4g2A : public Amp_gA<T, 6> {
private:
    static const unsigned int mul{6};
    static const unsigned int num{20};
    const Flavour<double> flavours[mul]{g, g, g, g, A, A};
    const unsigned int orders[num][mul]{
            {4, 5, 0, 1, 2, 3,},
            {4, 5, 1, 2, 3, 0,},
            {4, 5, 2, 3, 0, 1,},
            {4, 5, 3, 0, 1, 2,},
            {4, 0, 5, 1, 2, 3,},
            {4, 1, 5, 2, 3, 0,},
            {4, 2, 5, 3, 0, 1,},
            {4, 3, 5, 0, 1, 2,},
            {4, 0, 1, 5, 2, 3,},
            {4, 1, 2, 5, 3, 0,},
            {4, 2, 3, 5, 0, 1,},
            {4, 3, 0, 5, 1, 2,},
            {4, 0, 1, 2, 5, 3,},
            {4, 1, 2, 3, 5, 0,},
            {4, 2, 3, 0, 5, 1,},
            {4, 3, 0, 1, 5, 2,},
            {4, 0, 1, 2, 3, 5,},
            {4, 1, 2, 3, 0, 5,},
            {4, 2, 3, 0, 1, 5,},
            {4, 3, 0, 1, 2, 5,},
    };

    const EpsQuintuplet<T> evalAmp2_rf();

    const EpsQuintuplet<T> evalAmp2_f();

public:
    const EpsTriplet <T> evalAmp();

    const EpsQuintuplet<T> evalAmp2(const Basis basis);

    Amp_4g2A();
};

// Initialise amp (class constructor)
template<typename T>
Amp_4g2A<T>::Amp_4g2A() {
    this->setProcess(mul, flavours);
    this->setLoopInduced();
}

// Kinematic part of primitive amplitude
template<typename T>
const EpsTriplet <T> Amp_4g2A<T>::evalAmp() {
    return Amp_gA<T, mul>::evalAmp(orders);
}

// using (reflected) fundamental basis
template<typename T>
const EpsQuintuplet<T> Amp_4g2A<T>::evalAmp2_rf() {
    const unsigned int orderZero[]{0, 1, 2, 3, 4, 5};
    this->setPrimOrder(orderZero);
    EpsTriplet <T> A012345{this->evalAmp()};

    const unsigned int orderOne[]{0, 1, 3, 2, 4, 5};
    this->setPrimOrder(orderOne);
    EpsTriplet <T> A013245{this->evalAmp()};

    const unsigned int orderTwo[]{0, 2, 1, 3, 4, 5};
    this->setPrimOrder(orderTwo);
    EpsTriplet <T> A021345{this->evalAmp()};

    // vector of partial amplitudes
    const EpsTriplet <T> ampVec[]{
            A013245,
            A021345,
            A012345,
    };

    // elements of colour matrix
    const T colMatEl[]{
            6 - 2 * Nc2 + Nc4,
            6 - 2 * Nc2,
    };

    // colour matrix (len x len) data; just the upper triangle (without diagonal) since symmetric
    const unsigned int colMatOrder[]{0, 1, 1, 0, 1, 0};

    const EpsQuintuplet<T> amp2{this->evalColourSum(ampVec, colMatEl, colMatOrder)};

    const T colNorm{(-1. + Nc2) / (8. * Nc2)};
    return Nf2<T> * colNorm * amp2;
}

// using fundamental basis
template<typename T>
const EpsQuintuplet<T> Amp_4g2A<T>::evalAmp2_f() {
    const unsigned int orderZero[]{0, 1, 2, 3, 4, 5};
    this->setPrimOrder(orderZero);
    EpsTriplet <T> A012345{this->evalAmp()};
//    cout << A012345.get0() << endl;

    const unsigned int order1[]{0, 1, 3, 2, 4, 5};
    this->setPrimOrder(order1);
    EpsTriplet <T> A013245{this->evalAmp()};
//    cout << A013245.get0() << endl;

    const unsigned int order2[]{0, 2, 1, 3, 4, 5};
    this->setPrimOrder(order2);
    EpsTriplet <T> A021345{this->evalAmp()};
//    cout << A021345.get0() << endl;

    // define other partial amplitudes exploiting reflection (and cyclicity)
    const EpsTriplet <T> A023145{A013245};
    const EpsTriplet <T> A031245{A021345};
    const EpsTriplet <T> A032145{A012345};

    // vector of partial amplitudes
    const EpsTriplet <T> ampVec[]{
            A012345,
            A013245,
            A021345,
            A023145,
            A031245,
            A032145
    };

    // elements of colour matrix
    const T colMatEl[]{
            3 - 3 * Nc2 + Nc4,
            3 - Nc2,
            3 + Nc2
    };

    // colour matrix (len x len) data; just the upper triangle (without diagonal) since symmetric
    const unsigned int colMatOrder[]{
            0, 1, 1, 1, 1, 2,
            0, 1, 2, 1, 1,
            0, 1, 2, 1,
            0, 1, 1,
            0, 1,
            0
    };

    const EpsQuintuplet<T> amp2{this->evalColourSum(ampVec, colMatEl, colMatOrder)};

    const T colNorm{(-1. + Nc2) / (16. * Nc2)};

    return Nf2<T> * colNorm * amp2;
}

template<typename T>
const EpsQuintuplet<T> Amp_4g2A<T>::evalAmp2(const Basis basis) {
    switch (basis) {
        case fundamental:
            return evalAmp2_f();
        case reflectedFundamental:
            return evalAmp2_rf();
        default:
            cout << "Warning! Unrecognised colour basis. Returning zero." << endl;
            return EpsQuintuplet<T>();
    }
}

#endif //NJET_EXAMPLES_4G2A_H
