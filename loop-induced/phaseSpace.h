#ifndef NJET_EXAMPLES_PHASESPACE_H
#define NJET_EXAMPLES_PHASESPACE_H

#include <iostream>
#include <vector>
#include <cassert>
#include <cmath>

#include "tools/PhaseSpace.h"
#include "ngluon2/Model.h"

#include "common.h"

using std::cout;
using std::endl;
using std::vector;
using std::sqrt;

template<typename T>
const vector <MOM<T>>
getMasslessPhaseSpacePoint(const int mul, const bool verbose = false, const int rseed = 1, const double sqrtS = 1.) {
    Flavour<double> flavours[mul];
    for (auto flavour: flavours) {
        flavour = g;
    }

    PhaseSpace <T> ps(mul, flavours, rseed, sqrtS);
    const vector <MOM<T>> momenta{ps.getPSpoint()};

    cout << endl;
    if (verbose) {
        ps.showPSpoint();
    }

    return momenta;
}

// BROKEN - needs complex momenta
// hard coded single collinear phase-space point for 2->2
// collinear in 0||1 (s12->0) as theta -> 0
template<typename T>
vector <MOM<T>> getCollinearPhaseSpacePoint4(const T theta, const bool verbose = false) {
    const T sqrtS{1.};

    const T ct{cos(theta)};
    const T st{sin(theta)};

    const T cp{-1};
    print("cos(phi)", cp, verbose);
    assert(-1. <= cp);
    assert(cp <= 1.);
    const T sp{sqrt(T(1.) - cp * cp)};
    print("sin(phi)", sp, verbose);
    assert(-1. <= sp);
    assert(sp <= 1.);

    vector <MOM<T>> momenta(4);
    momenta[0] = sqrtS / T(2.) * MOM<T>(1., 0., 0., 1.);
    momenta[1] = sqrtS / T(2.) * MOM<T>(1, 0, -st, ct);
    momenta[2] = sqrtS / T(2.) * MOM<T>(-1, sp, -(cp * st), cp * ct);
    momenta[3] = -(momenta[0] + momenta[1] + momenta[2]);

    checkPhaseSpacePoint(momenta, verbose);
    printMomenta(momenta, verbose);

    return momenta;
}

// hard coded single collinear phase-space point for 2->3
// collinear in 3||4 (s34->0)
template<typename T>
vector <MOM<T>> getCollinearPhaseSpacePoint5(const T lambda, const bool verbose = false) {
    const T sqrtS{1.};
    vector <MOM<T>> momenta(5);

    const T y1{lambda};
    assert((0. <= y1) && (y1 <= 1.));
    const T y2{T(0.2)};
    assert((0. <= y2) && (y2 <= 1.));
    assert((0. <= y1 + y2) && (y1 + y2 <= 1.));

    const T x1{T(1.) - y1};
    assert((0. <= x1) && (x1 <= 1.));
    const T x2{T(1.) - y2};
    assert((0. <= x2) && (x2 <= 1.));
    assert((0. <= 2 - x1 - x2) && (2 - x1 - x2 <= 1.));

    const T cosBeta{T(1.) + T(2.) * (T(1.) - x1 - x2) / x1 / x2};
    assert((-1. <= cosBeta) && (cosBeta <= 1.));
    const T sinBeta{sqrt(T(1.) - pow(cosBeta, 2.))};
    assert((-1. <= sinBeta) && (sinBeta <= 1.));

    const T theta{M_PI / T(2.)};
    assert((0. <= theta) && (theta <= M_PI));
    const T cosTheta{cos(theta)};
    assert((-1. <= cosTheta) && (cosTheta <= 1.));
    const T sinTheta{sin(theta)};
    assert((-1. <= sinTheta) && (sinTheta <= 1.));

    const T alpha{M_PI / T(2.)};
    assert((0. <= alpha) && (alpha <= M_PI));
    const T cosAlpha{cos(alpha)};
    assert((-1. <= cosAlpha) && (cosAlpha <= 1.));
    const T sinAlpha{sin(alpha)};
    assert((-1. <= sinAlpha) && (sinAlpha <= 1.));

    momenta[0] = sqrtS / T(2.) * MOM<T>(-1., 0., 0., -1.);
    momenta[1] = sqrtS / T(2.) * MOM<T>(-1., 0., 0., 1.);
    momenta[2] = x1 * sqrtS / T(2.) * MOM<T>(1., sinTheta, 0., cosTheta);
    momenta[3] = x2 * sqrtS / T(2.) * MOM<T>(1., cosAlpha * cosTheta * sinBeta + cosBeta * sinTheta, sinAlpha * sinBeta,
                                             cosBeta * cosTheta - cosAlpha * sinBeta * sinTheta);
    momenta[4] = -(momenta[0] + momenta[1] + momenta[2] + momenta[3]);

    checkPhaseSpacePoint(momenta, verbose);
    printMomenta(momenta, verbose);

    return momenta;
}

// To check that sij scales correctly with lambda (and other sij do not)
template<typename T>
void testCollinearPhaseSpacePoint(const vector <MOM<T>> &mom, const int mul) {
    for (int i{0}; i < mul - 1; ++i) {
        for (int j{i + 1}; j < mul; ++j) {
            const T s{2 * dot(mom[i], mom[j])};
            cout << "s" << i << j << "=" << ((s >= 0) ? " " : "") << s << "   ";
        }
        if (i < mul - 2) {
            cout << endl;
        }
    }
    cout << endl;
}

// To check that collinear sij scales correctly with lambda (and other sij do not)
template<typename T>
void testPhaseSpacePoint5(const T lambda) {
    vector <MOM<T>> mom = getCollinearPhaseSpacePoint5<T>(lambda);
    cout << endl << "lambda = " << lambda << ":" << endl;
    testCollinearPhaseSpacePoint(mom, 5);
}

// hard coded single collinear phase-space point for 2->4, 0||1
template<typename T>
vector <MOM<T>> getCollinear01PhaseSpacePoint6(const T lambda, const bool verbose = false, const T sqrtS = 1e3) {
    const int mul{6};
    std::vector <MOM<T>> momenta(mul);

    const T E2{T(0.12) * sqrtS};
    const T E3{T(0.37) * sqrtS};

    const T theta1{M_PI / T(3.)};
    const T theta2{lambda + theta1};
    const T ct1{cos(theta1)};
    const T ct2{cos(theta2)};
    const T st1{sin(theta1)};
    const T st2{sin(theta2)};

    const T phi1{M_PI / T(2.)};
    const T phi2{M_PI / T(2.)};
    const T cp1{cos(phi1)};
    const T cp2{cos(phi2)};
    const T sp1{sin(phi1)};
    const T sp2{sin(phi2)};

    const T E4{
            -T(0.5) * (
                    -sqrtS * sqrtS + T(2.) * sqrtS * E2 + T(2.) * E2 * st1 * sp1 * E3 * sp2 * st2 +
                    2 * E2 * st1 * cp1 * E3 * cp2 * st2 + T(2.) * E2 * ct1 * E3 * ct2 + T(2.) * sqrtS * E3 -
                    T(2.) * E2 * E3
            ) / (
                    T(2.) * E2 * st1 * sp1 + T(2.) * E3 * sp2 * st2 + T(2.) * E2 * st1 * cp1 + T(2.) * E3 * cp2 * st2 +
                    E2 * ct1 + E3 * ct2 + T(3.) * sqrtS - T(3.) * E2 - T(3.) * E3
            )};

    momenta[4] = MOM<T>(sqrtS / 2, 0., 0., -sqrtS / 2);
    momenta[5] = MOM<T>(sqrtS / 2, 0., 0., sqrtS / 2);
    momenta[0] = -E2 * MOM<T>(1., sp1 * st1, ct1, cp1 * st1);
    momenta[1] = -E3 * MOM<T>(1., sp2 * st2, ct2, cp2 * st2);
    momenta[2] = -E4 * MOM<T>(3., 2., 1., 2.);
    momenta[3] = -momenta[0] - momenta[1] - momenta[2] - momenta[4] - momenta[5];

    checkPhaseSpacePoint(momenta, verbose);

    if (verbose) {
        cout << endl;
        for (int i{0}; i < mul; ++i) {
            cout << 'p' << i << '=' << momenta[i] << endl;
        }
    }

    return momenta;
}

// template for hard coded single collinear phase-space point for 2->4
template<typename T>
vector <MOM<T>>
getCollinearPhaseSpacePoint6(const T alpha, const T beta, const T gamma, const bool verbose = false,
                             const T sqrtS = 1) {
    const int mul{6};
    std::vector <MOM<T>> momenta(mul);

    const T x1{0.12};
    const T x2{0.37};

    const T ca{cos(alpha)};
    const T cb{cos(beta)};
    const T cg{cos(gamma)};

    const T sa{sin(alpha)};
    const T sb{sin(beta)};
    const T sg{sin(gamma)};

    const T x3{
            (4 - 4 * x1 + x1 * x1 - ca * ca * x1 * x1 - sa * sa * x1 * x1 - 4 * x2 + 2 * x1 * x2 -
             2 * ca * ca * cb * x1 * x2 - 2 * sa * sa * x1 * x2 + x2 * x2 - ca * ca * cb * cb * x2 * x2 -
             sa * sa * x2 * x2 -
             ca * ca * sb * sb * x2 * x2) / (2 * (2 - x1 + ca * ca * cb * x1 + cg * sa * sa * x1 -
                                                  ca * sa * sb * sg * x1 - x2 + ca * ca * cb * cb * x2 +
                                                  cg * sa * sa * x2 + ca * ca * cg * sb * sb * x2))
    };

    momenta[0] = -sqrtS / T(2.) * MOM<T>(1., 0., 0., 1.);
    momenta[1] = -sqrtS / T(2.) * MOM<T>(1., 0., 0., -1.);
    momenta[2] = x1 * sqrtS / T(2.) * MOM<T>(1., 0., -sa, ca);
    momenta[3] = x2 * sqrtS / T(2.) * MOM<T>(1., ca * sb, -sa, ca * cb);
    momenta[4] = x3 * sqrtS / T(2.) * MOM<T>(1., ca * cg * sb + sa * sg, -(cg * sa) + ca * sb * sg, ca * cb);
    momenta[5] = -(momenta[0] + momenta[1] + momenta[2] + momenta[3] + momenta[4]);

    checkPhaseSpacePoint(momenta, verbose);

    if (verbose) {
        for (int i{0}; i < mul; ++i) {
            cout << 'p' << i << '=' << momenta[i] << endl;
        }
    }

    return momenta;
}

// hard coded single collinear phase-space point for 2->4, 2||3
template<typename T>
vector <MOM<T>> getCollinear23PhaseSpacePoint6(const T lambda, const bool verbose = false, const T sqrtS = 1) {
    const T alpha{M_PI / T(3.)};
    const T beta{lambda};
    const T gamma{M_PI / T(5.)};
    return getCollinearPhaseSpacePoint6(alpha, beta, gamma, verbose, sqrtS);
}

// hard coded single collinear phase-space point for 2->4, 3||4
template<typename T>
vector <MOM<T>> getCollinear34PhaseSpacePoint6(const T lambda, const bool verbose = false, const T sqrtS = 1) {
    const T alpha{M_PI / T(3.)};
    const T beta{M_PI / T(5.)};
    const T gamma{lambda};
    return getCollinearPhaseSpacePoint6(alpha, beta, gamma, verbose, sqrtS);
}

// To check that s01 scales correctly with lambda (and other sij do not)
template<typename T>
void testCollinear01PhaseSpacePoint6(const T lambda, const bool verbose = false) {
    const int mul{6};
    vector <MOM<T>> mom = getCollinear01PhaseSpacePoint6<T>(lambda, verbose);
    cout << endl << "lambda=" << lambda << ":" << endl;
    testCollinearPhaseSpacePoint(mom, mul);
}

// To check that s34 scales correctly with lambda (and other sij do not)
template<typename T>
void testCollinear34PhaseSpacePoint6(const T lambda, const bool verbose = false) {
    const int mul{6};
    vector <MOM<T>> mom = getCollinear34PhaseSpacePoint6<T>(lambda, verbose);
    cout << endl << "lambda=" << lambda << ":" << endl;
    testCollinearPhaseSpacePoint(mom, mul);
}

// To check that s23 scales correctly with lambda (and other sij do not)
template<typename T>
void testCollinear23PhaseSpacePoint6(const T lambda, const bool verbose = false) {
    const int mul{6};
    vector <MOM<T>> mom = getCollinear23PhaseSpacePoint6<T>(lambda, verbose);
    cout << endl << "lambda=" << lambda << ":" << endl;
    testCollinearPhaseSpacePoint(mom, mul);
}

// To check that s12 scales correctly with lambda (and other sij do not)
template<typename T>
void testCollinearPhaseSpacePoint4(const T lambda, const bool verbose = false) {
    const int mul{4};
    vector <MOM<T>> mom = getCollinearPhaseSpacePoint4<T>(lambda, verbose);
    cout << endl << "lambda=" << lambda << ":" << endl;
    testCollinearPhaseSpacePoint(mom, mul);
}

#endif //NJET_EXAMPLES_PHASESPACE_H
