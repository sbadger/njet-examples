#ifndef NJET_EXAMPLES_2G2A_H
#define NJET_EXAMPLES_2G2A_H

#include <vector>
#include <complex>

#include "tools/PhaseSpace.h"

#include "amp-gA.h"
#include "common.h"
#include "EpsQuintuplet.h"

using std::vector;
using std::complex;

template<typename T>
class Amp_2g2A : public Amp_gA<T, 4> {
    private:
        static const unsigned int mul{4};
        static const unsigned int num{6};
        const Flavour<double> flavours[mul]{g, g, A, A};
        const unsigned int orders[num][mul]{
            {0, 1, 2, 3},
            {0, 2, 1, 3},
            {1, 0, 2, 3},
            {1, 2, 0, 3},
            {2, 0, 1, 3},
            {2, 1, 0, 3}
        };
    public:
        const EpsTriplet <T> evalAmp();

        const EpsQuintuplet <T> evalAmp2Kin();

        const EpsQuintuplet <T> evalAmp2();

        Amp_2g2A();
};

// Initialise amp (class constructor)
template<typename T>
Amp_2g2A<T>::Amp_2g2A() {
    this->setProcess(mul, flavours);
    this->setLoopInduced();
}

// Kinematic part of primitive amplitude
template<typename T>
const EpsTriplet <T> Amp_2g2A<T>::evalAmp() {
    return Amp_gA<T, mul>::evalAmp(orders);
}

// Just kinematic part of helicity amplitude squared, no quark charge or sum, single quark flavour in loop
// (no charge information about quark, ie. quark charge set to unity)
template<typename T>
const EpsQuintuplet<T> Amp_2g2A<T>::evalAmp2Kin() {
    return sq_amp(this->evalAmp());
}

// Full helicity amplitude squared, including colour sum and sum over quark charges in loop
template<typename T>
const EpsQuintuplet<T> Amp_2g2A<T>::evalAmp2() {
    EpsQuintuplet<T> kin{this->evalAmp2Kin()};
    const T colFac{-0.25 + Nc2/4.};
    return Nf2<T> * colFac * kin;
}

#endif //NJET_EXAMPLES_2G2A_H
