#ifndef NJET_EXAMPLES_AMPLITUDEGLUONPHOTON_H
#define NJET_EXAMPLES_AMPLITUDEGLUONPHOTON_H

#include <vector>

#include "tools/PhaseSpace.h"
#include "ngluon2/NParton2.h"

#include "common.h"
#include "EpsQuintuplet.h"

using std::vector;
using std::swap;

template<typename T, unsigned int mul>
class Amp_gA : public NParton2<T> {
private:
    unsigned int primOrder[mul];

    int helicity[mul];
public:

    template<unsigned int num>
    EpsTriplet <T> evalAmp(const unsigned int (&orders)[num][mul]);

    template<unsigned int n_els, unsigned int n_order>
    T colMat(const unsigned int len, const T (&colMatEl)[n_els], const unsigned int (&colMatOrder)[n_order],
             unsigned int i, unsigned int j);

    template<unsigned int len, unsigned int n_els, unsigned int n_order>
    EpsQuintuplet<T> evalColourSum(const EpsTriplet<T> (&ampVec)[len], const T (&colMatEl)[n_els],
                                   const unsigned int (&colMatOrder)[n_order]);

    void setPrimOrder(const unsigned int (&order)[mul]);

    void setHelicity(const int (&helicity)[mul]);

    const int getHelicity(const unsigned int pos);

    Amp_gA();
};

template<typename T, unsigned int mul>
Amp_gA<T, mul>::Amp_gA() {
    const T MuR{91.188};
    this->setMuR2(MuR * MuR);
    for (unsigned int i{0}; i < mul; ++i) {
        primOrder[i] = i;
    }
}

// eval loop-induced fermion-loop primitive amplitude with external photon(s)
template<typename T, unsigned int mul>
template<unsigned int num>
EpsTriplet <T> Amp_gA<T, mul>::evalAmp(const unsigned int (&orders)[num][mul]) {
    EpsTriplet <T> sol;
    int newOrder[mul];
    for (auto &order: orders) {
        for (unsigned int i{0}; i < mul; ++i) {
            newOrder[i] = primOrder[order[i]];
        }
//        print("order", genArrStr(newOrder));
        this->setOrder(newOrder);
        // 1 = fermion loop primitive
        const EpsTriplet <T> tmp{this->eval(1)};
//        print("subAmp", tmp.get0());
        sol += tmp;
    }
    return sol;
}

// function to return the (i,j)^{th} entry of the colour matrix
template<typename T, unsigned int mul>
template<unsigned int n_els, unsigned int n_order>
T Amp_gA<T, mul>::colMat(const unsigned int len, const T (&colMatEl)[n_els], const unsigned int (&colMatOrder)[n_order],
                         unsigned int i, unsigned int j) {
//    print("i", i);
//    print("j", j);
    // use symmetry to address upper triangle only
    if (i > j) {
        swap(i, j);
    }
    // change (i,j) upper triangle coords to list index
    const unsigned int index{(i * len) - ((i * i - i) / 2) + (j - i)};
//    print("index", index);
    return colMatEl[colMatOrder[index]];
}

// perform the colour sum to obtain the matrix element (up to a normalisation factor)
template<typename T, unsigned int mul>
template<unsigned int len, unsigned int n_els, unsigned int n_order>
EpsQuintuplet<T> Amp_gA<T, mul>::evalColourSum(const EpsTriplet<T> (&ampVec)[len], const T (&colMatEl)[n_els],
                                               const unsigned int (&colMatOrder)[n_order]) {
    EpsQuintuplet<T> amp2;
    for (unsigned int i{0}; i < len; ++i) {
        for (unsigned int j{0}; j < len; ++j) {
//            cout << i << j << " " << this->colMat(len, colMatEl, colMatOrder, i, j) << endl;
            amp2 += multiplyEpsTriplets(conj(ampVec[i]), ampVec[j]) * this->colMat(len, colMatEl, colMatOrder, i, j);
        }
    }
    return amp2;
}

template<typename T, unsigned int mul>
void Amp_gA<T, mul>::setPrimOrder(const unsigned int (&order)[mul]) {
    for (unsigned int i{0}; i < mul; ++i) {
        primOrder[i] = order[i];
    }
}

template<typename T, unsigned int mul>
void Amp_gA<T, mul>::setHelicity(const int (&helicityIn)[mul]) {
    NParton2<T>::setHelicity(helicityIn);
    for (unsigned int i{0}; i < mul; ++i) {
        helicity[i] = helicityIn[i];
    }
}

template<typename T, unsigned int mul>
const int Amp_gA<T, mul>::getHelicity(const unsigned int pos) {
    return helicity[pos];
}

#endif //NJET_EXAMPLES_AMPLITUDEGLUONPHOTON_H
