#include <iostream>
#include <vector>

#include "tools/PhaseSpace.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

#include "common.h"
#include "phaseSpace.h"
#include "amp-gA.h"

using std::cout;
using std::endl;
using std::vector;

template<typename T>
void evalCoeffs(const bool verbose = false) {
    const char line[] = "----------------------------------------------------------------------";

    const Flavour<double> flavours[] = {g, g, g, g};

    vector <MOM<T>> momenta{getMasslessPhaseSpacePoint<T>(4, verbose)};

    const int helicities[][4] = {
        {+1, +1, +1, +1},
        {-1, +1, +1, +1},
        {+1, -1, +1, +1},
        {+1, +1, -1, +1},
        {-1, -1, +1, +1},
        {-1, +1, -1, +1},
        {+1, -1, -1, +1}
    };

    cout << endl;
    cout << line << endl;
    for (int i = 0; i < 7; ++i) {
        printHelicity("helicity", helicities[i]);
        cout << endl;

        NParton2 <T> amp;
        const T MuR{91.188};
        amp.setMuR2(MuR * MuR);
        amp.setProcess(4, flavours);
        amp.setMomenta(momenta);
        amp.setHelicity(helicities[i]);
        if (i < 4) {
            amp.setLoopInduced();
        }
        amp.eval(1);
        amp.printCoefficients();
        cout << line << endl;
    };

    T s{2 * dot(momenta[0], momenta[1])};
    T t{2 * dot(momenta[0], momenta[2])};
    T u{2 * dot(momenta[0], momenta[3])};

    T c44_pppp{-1};

    T c44_mmpp{1};
    T c2_t_mmpp{-s / 2 / t};

    T c44_mppp{-s * t / 2 / u};
    T c32_s_mppp{t * (u - s) / s / u};
    T c32_t_mppp{s * (u - t) / t / u};
    T c22_s_mppp{(t - u) / s / s};
    T c22_t_mppp{(s - u) / t / t};

    cout << "c44   ++++ = " << c44_pppp << endl;
    cout << endl;
    cout << "c44   -+++ = " << c44_mppp << endl;
    cout << "c32_s -+++ = " << c32_s_mppp << endl;
    cout << "c32_t -+++ = " << c32_t_mppp << endl;
    cout << "c22_s -+++ = " << c22_s_mppp << endl;
    cout << "c22_t -+++ = " << c22_t_mppp << endl;
    cout << endl;
    cout << "++-+ = -+++" << endl;
    cout << endl;
    cout << "c44   --++ = " << c44_mmpp << endl;
    cout << "c2_t  --++ = " << c2_t_mmpp << endl;
    cout << endl;
    //cout << "mpmp not done" << endl;
    //cout << endl;
    cout << line << endl;
};

int main(int argc, char **argv) {
    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(8);

    const bool verbose{argc == 2 ? (string(argv[1]) == "v") : false};

    cout << endl;
    evalCoeffs<double>(verbose);

    return 1;
}
