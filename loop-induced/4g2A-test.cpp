#include <iostream>
#include <vector>
#include <cmath>
#include <chrono>

#include "tools/PhaseSpace.h"

#include "4g2A.h"
#include "3g2A.h"
#include "phaseSpace.h"
#include "common.h"
#include "collinear.h"

using std::cout;
using std::endl;
using std::vector;
using std::pow;

template<typename T>
void test(const T lambda, const bool verbose = false) {
    cout << endl;

    print("lambda      ", lambda);

    //    const vector <MOM<T>> momenta{getMasslessPhaseSpacePoint<T>(6, verbose)};
    const vector <MOM<T>> colMom{getCollinear23PhaseSpacePoint6(lambda, verbose)};
    const int helicities[] = {-1, -1, +1, +1, +1, +1};

    printHelicity("helicity    ", helicities);

    Amp_4g2A<T> amp_4g2A;
    //    amp_4g2A.setMomenta(momenta);
    amp_4g2A.setMomenta(colMom);
    amp_4g2A.setHelicity(helicities);


    const EpsTriplet <T> amp{amp_4g2A.evalAmp()};
    print("A           ", amp);

    // print the colour-summed helicity matrix element
    auto start{std::chrono::high_resolution_clock::now()};
    const EpsQuintuplet<T> amp2_rf{amp_4g2A.evalAmp2(reflectedFundamental)};
    auto stop{std::chrono::high_resolution_clock::now()};
    auto duration{std::chrono::duration_cast<std::chrono::milliseconds>(stop - start)};
    print("|A_rf|^2    ", amp2_rf);
    print("time (ms):  ", duration.count());

    // and again for different colour basis - should be the same
    auto start1{std::chrono::high_resolution_clock::now()};
    const EpsQuintuplet<T> amp2_f{amp_4g2A.evalAmp2(fundamental)};
    auto stop1{std::chrono::high_resolution_clock::now()};
    auto duration1{std::chrono::duration_cast<std::chrono::milliseconds>(stop1 - start1)};
    print("|A_f|^2     ", amp2_f);
    print("time (ms):  ", duration1.count());

    print("rf^2/f^2    ", real(amp2_rf.get0() / amp2_f.get0()));

    CollinearLimit<T, 6> colLim(verbose);
    colLim.init(2, helicities, colMom);
    Amp_3g2A<T> amp_3g2A;

    const EpsTriplet <T> lim{colLim.eval(amp_3g2A)};
    print("lim         ", lim);

    print("A/lim       ", real(amp.get0() / lim.get0()));

    cout << "Simple squaring" << endl;
    const EpsQuintuplet<T> lim2{sq_amp(lim)};
    print("|lim_rf|^2     ", lim2);

    cout << "Spin correlation" << endl;
    const EpsQuintuplet<T> lim2_rf{colLim.spinCorrelate(amp_3g2A)};
    print("|lim_rf|^2  ", lim2_rf);

    const EpsQuintuplet<T> lim2_f{colLim.spinCorrelateAlt(amp_3g2A)};
    const EpsQuintuplet<T> lim2_f_h{0.5 * lim2_f};
    print("|lim_f|^2/2 ", lim2_f_h);

    print("rf:ss^2/sc^2", real(lim2.get0() / lim2_rf.get0()));
    print("sc:rf^2/2f^2", real(lim2_rf.get0() / lim2_f_h.get0()));

    cout << endl;
}

int main(int argc, char **argv) {
    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(8);

    const bool verbose{argc == 2 ? (string(argv[1]) == "v") : false};

    test<dd_real>(pow(10, -9), verbose);

    return 1;
}
