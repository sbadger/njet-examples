#include <iostream>

#include "tools/PhaseSpace.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

#include "phaseSpace.h"
#include "common.h"
#include "amp-gA.h"

using std::cout;
using std::endl;
using std::vector;

template<typename T>
EpsTriplet <T> evalAmp_3g1A(const bool verbose = false) {
    const Flavour<double> flavours[]{A, g, g, g};
    const vector <MOM<T>> momenta{getMasslessPhaseSpacePoint<T>(4, verbose)};
    const int helicity[] = {-1, +1, +1, -1};

    const int orders[][4] = {
        {0, 1, 2, 3},
        {1, 0, 2, 3},
        {1, 2, 0, 3}
    };

    return evalPrimitiveAmplitude(flavours, momenta, helicity, orders);
};

int main(int argc, char **argv) {
    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(8);

    const bool verbose{argc == 2 ? (string(argv[1]) == "v") : false};

    EpsTriplet<double> sol{evalAmp_3g1A<double>(verbose)};

    cout << endl << "amp=" << sol << endl << endl;

    return 1;
}
