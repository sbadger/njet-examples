#include <iostream>
#include <vector>

#include "tools/PhaseSpace.h"

#include "3g2A.h"
#include "phaseSpace.h"
#include "common.h"

using std::cout;
using std::endl;
using std::vector;

template<typename T>
void test(const bool verbose) {
    const vector <MOM<T>> momenta{getMasslessPhaseSpacePoint<T>(5, verbose)};
    const int helicities[] = {-1, -1, +1, +1, +1};

    Amp_3g2A<T> amp_3g2A;
    amp_3g2A.setMomenta(momenta);
    amp_3g2A.setHelicity(helicities);

    cout << "A(" << genHelStr(helicities) << ") = " << amp_3g2A.evalAmp() << endl << endl;

    cout << "|A(" << genHelStr(helicities) << ")|^2 = " << amp_3g2A.evalAmp2_f() << endl << endl;
}

int main(int argc, char **argv) {
    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(8);

    const bool verbose{argc == 2 ? (string(argv[1]) == "v") : false};

    test<double>(verbose);

    return 1;
}
