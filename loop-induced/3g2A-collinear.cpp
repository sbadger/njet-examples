// 3||4 limit of 3g2A
// ERROR: Factorised amps currently returning NAN

#include <iostream>
#include <vector>
#include <complex>
#include <cmath>

#include "tools/PhaseSpace.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

#include "amp-gA.h"
#include "3g2A.h"
#include "2g2A.h"
#include "phaseSpace.h"
#include "common.h"
#include "collinear.h"

using std::cout;
using std::endl;
using std::vector;
using std::conj;
using std::complex;
using std::pow;

template<typename T>
complex <T> evalAmp_3g2A_mmppp(const T lambda, const bool verbose = false) {
    const int helicity[]{-1, -1, +1, +1, +1};
    const vector <MOM<T>> momenta{getCollinearPhaseSpacePoint5<T>(lambda, verbose)};
    Amp_3g2A<T> amp_3g2A;
    Amp_2g2A<T> amp_2g2A;
    return getCollinearLimitAmpRatio(momenta, helicity, amp_3g2A, amp_2g2A, 3, verbose);
}

int main(int argc, char **argv) {
    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(8);

    const bool verbose{argc == 2 ? (string(argv[1]) == "v") : false};

    cout << endl << evalAmp_3g2A_mmppp<double>(double(0.5), verbose) << endl << endl;

    //    const dd_real ddLim{10.};
    //    const dd_real ddStep{1.};
    //    scanPrimAmp(evalAmp_3g2A_mmppp<dd_real>, ddLim, ddStep, "amp/lim", verbose);

    return 1;
}
