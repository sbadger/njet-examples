// Test 0||1 limit of ggggAA
// Doesn't work

#include <iostream>
#include <vector>
#include <complex>
#include <cmath>

#include "ngluon2/Initialize.h"
#include "tools/PhaseSpace.h"

#include "3g2A.h"
#include "4g2A.h"
#include "phaseSpace.h"

using std::cout;
using std::endl;
using std::vector;
using std::complex;
using std::pow;

template<typename T>
T checkCollinearLimitAmp2(const T lambda) {
    vector <MOM<T>> mom = getCollinear01PhaseSpacePoint6<T>(lambda);

    const int mul{6};
    const int helicity[mul] = {-1, -1, +1, +1, +1, +1};
//    T amp2_{evalMatrixElement_f6_ggggAA<T>(mom, helicity)};
    T amp2{evalMatrixElement_a2_ggggAA<T>(mom, helicity)};
//    cout << "amp_^2 = " << amp2_ << endl;
//    cout << "amp^2 = " << amp2 << endl;

    const int fact_mul{mul - 1};
    const int fact_helicity_1[fact_mul] = {-1, +1, +1, +1, +1};

    // Catani-Seymour momentum mapping 12;3
    const MOM <T> p1{mom[0]};
    const MOM <T> p2{mom[1]};
    const MOM <T> p12{p1 + p2};
    const MOM <T> p3{mom[2]};
    const T s12{S(p12)};
    const T x12_3{s12 / T(2.) / dot(p12, p3)};
    const MOM <T> p12t{p12 - x12_3 * p3};
    const MOM <T> p3t{(T(1.) + x12_3) * p3};
    vector <MOM<T>> fact_mom(fact_mul);
    fact_mom[0] = p12t;
    fact_mom[1] = p3t;
    fact_mom[2] = mom[3];
    fact_mom[3] = mom[4];
    fact_mom[4] = mom[5];

    T fact_amp2_1{evalMatrixElement_gggAA<T>(fact_mom, fact_helicity_1)};
//    cout << "fact_amp^2 = " << fact_amp2_1 << endl;

    const complex <T> z1A{spA(p1, p3) / spA(p12, p3)};
    const complex <T> z2A{spA(p2, p3) / spA(p12, p3)};
    // gluon splitting amplitude at tree level for +-- (P,p1,p2)
    const complex <T> split{spB(p1, p2) / z1A / z2A};

//    cout << "split = " << split << endl;

    const T s01{2 * dot(mom[0], mom[1])};
    const T limit{real(fact_amp2_1 * conj(split) * split / s01 / s01)};

    return amp2 / limit;
}

template<typename T>
complex <T> checkCollinearLimitPrimAmp(const T lambda) {
    vector <MOM<T>> mom = getCollinear01PhaseSpacePoint6<T>(lambda);

    const int mul{6};
    const int helicity[mul] = {-1, -1, +1, +1, +1, +1};
    complex <T> amp{evalPrimitiveAmplitude_ggggAA<T>(mom, helicity)};
//    cout << "amp = " << amp << endl;

    const int fact_mul{mul - 1};
    const int fact_helicity_1[fact_mul] = {-1, +1, +1, +1, +1};

    // Catani-Seymour momentum mapping 12;3
    const MOM <T> p1{mom[0]};
    const MOM <T> p2{mom[1]};
    const MOM <T> p12{p1 + p2};
    const MOM <T> p3{mom[2]};
    const T s12{S(p12)};
    const T x12_3{s12 / T(2.) / dot(p12, p3)};
    const MOM <T> p12t{p12 - x12_3 * p3};
    const MOM <T> p3t{(T(1.) + x12_3) * p3};
    vector <MOM<T>> fact_mom(fact_mul);
    fact_mom[0] = p12t;
    fact_mom[1] = p3t;
    fact_mom[2] = mom[3];
    fact_mom[3] = mom[4];
    fact_mom[4] = mom[5];

    complex <T> fact_amp_1{evalAmplitude_gggAA<T>(fact_mom, fact_helicity_1)};
//    cout << "fact_amp = " << fact_amp_1 << endl;

    const complex <T> z1A{spA(p1, p3) / spA(p12, p3)};
    const complex <T> z2A{spA(p2, p3) / spA(p12, p3)};
    // gluon splitting amplitude at tree level for +-- (P,p1,p2)
    const complex <T> split{spB(p1, p2) / z1A / z2A};

//    cout << "split = " << split << endl;

    const T s01{2 * dot(mom[0], mom[1])};
    const complex <T> limit{fact_amp_1 * split / s01};

    return amp / limit;
}

template<typename T>
void approachCollinearPrimAmp() {
    //    cout << endl;
    for (int p{1}; p < 5; ++p) {
        T lambda{pow(10, -p)};
        testCollinear01PhaseSpacePoint6<T>(lambda);
        complex<T> ratio{checkCollinearLimitPrimAmp<T>(lambda)};
//        cout << "lambda=" << lambda << "   ratio=" << ratio << endl;
        cout << "ratio=" << ratio << endl;
    }
    cout << endl;
}

template<typename T>
void approachCollinearAmp2() {
    //    cout << endl;
    for (int p{1}; p < 5; ++p) {
        T lambda{pow(10, -p)};
        testCollinear01PhaseSpacePoint6<T>(lambda);
        T ratio{checkCollinearLimitAmp2<T>(lambda)};
//        cout << "lambda=" << lambda << "   ratio=" << ratio << endl;
        cout << "ratio=" << ratio << endl;
    }
    cout << endl;
}

int main(int argc, char **argv) {
    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(16);

//    cout << endl << "64 digit precision. 0||1 <=> lambda->0:" << endl;
//    for (int p = 1; p < 64; p += 30) {
//        testCollinear01PhaseSpacePoint6<qd_real>(pow(10., -p));
//    }

    cout << endl << "16 digit precision. 1||0 <=> lambda->0:" << endl;
    approachCollinearPrimAmp<double>();

    return 1;
}
