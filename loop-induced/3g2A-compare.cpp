#include <iostream>
#include <vector>

#include "tools/PhaseSpace.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

#include "amp-gA.h"
#include "common.h"
#include "phaseSpace.h"

using std::cout;
using std::endl;
using std::vector;

template<typename T>
void compare(const bool verbose = false) {
    const int mul = 5;

    const Flavour<double> flavoursA[] = {g, g, g, A, A};
    const Flavour<double> flavoursG[] = {g, g, g, g, g};
    const int helicity[] = {-1, -1, +1, +1, +1};

    const vector <MOM<T>> momenta{getMasslessPhaseSpacePoint<T>(mul, verbose)};

    const int num = 12;
    const int orders[num][mul] = {
        {0, 1, 2, 3, 4},
        {0, 1, 2, 4, 3},
        {0, 1, 3, 2, 4},
        {0, 3, 1, 2, 4},
        {0, 1, 4, 2, 3},
        {0, 4, 1, 2, 3},
        {0, 1, 3, 4, 2},
        {0, 4, 3, 1, 2},
        {0, 1, 4, 3, 2},
        {0, 3, 4, 1, 2},
        {0, 3, 1, 4, 2},
        {0, 4, 1, 3, 2},
    };

    int ordersF[num][mul];

    for (int n = 0; n < num; ++n) {
        for (int m = 0; m < mul; ++m) {
            if (orders[n][m] == 0) {
                ordersF[n][m] = 2;
            } else if (orders[n][m] == 2) {
                ordersF[n][m] = 0;
            } else {
                ordersF[n][m] = orders[n][m];
            }
        }
    }

    EpsTriplet<T> ampA{evalPrimitiveAmplitude(flavoursA, momenta, helicity, orders, true)};
    EpsTriplet<T> ampAF{evalPrimitiveAmplitude(flavoursA, momenta, helicity, ordersF, true)};
    EpsTriplet<T> ampG{evalPrimitiveAmplitude(flavoursG, momenta, helicity, orders, false)};
    EpsTriplet<T> ampGF{evalPrimitiveAmplitude(flavoursG, momenta, helicity, ordersF, false)};

    cout << endl << "A(" << genHelStr(helicity) << ")" << endl;

    cout << "gggAA            " << ampA << endl;
    cout << "gggAA -F         " << -ampAF << endl;
    cout << "2 * sum ggggg    " << 2 * ampG << endl;
    cout << "2 * sum ggggg -F " << -2 * ampGF << endl << endl;

    cout << "Should have (ggg** == ggg** -F) and (2 * sum ggggg == gggAA) by 9905283" << endl << endl;
}

int main(int argc, char **argv) {
    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(8);

    const bool verbose{argc == 2 ? (string(argv[1]) == "v") : false};

    compare<double>(verbose);

    return 1;
}
