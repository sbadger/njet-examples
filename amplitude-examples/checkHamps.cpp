#include <iostream>
#include <getopt.h>
#include <cstdio>

#include "ngluon2/Model.h"
#include "tools/PhaseSpace.h"
#include "chsums/0q6gH.h"
#include "analytic/0q6gH-analytic.h"

using std::cout;
using std::endl;

int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  const int nn = 7;
  Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::Higgs()
  };

  PhaseSpace<double> ps(nn, flavours);

  Amp0q6gH<double> amp(1.);
  Amp0q6gH_a<double> ampA(1.);

  for (int i=0; i<5; i++) {
    ps.getPSpoint();
    ps.showPSpoint();

    amp.setMomenta(ps.Mom());
    cout << amp.born() << endl;

    ampA.setMomenta(ps.Mom());
    cout << ampA.born() << endl;
  }

  for (int i=0; i < 1000; i++) {
    ampA.setMomenta(ps.getPSpoint());
    ampA.born();
  }

  return 1;
}
