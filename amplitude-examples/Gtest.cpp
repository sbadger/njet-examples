#include <iostream>
#include <getopt.h>
#include <cstdio>

#include "tools/PhaseSpace.h"
#include "ngluon2/Initialize.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

using std::cout;
using std::endl;

void check4g() {

  cout << "4g amp checks" << endl;

  const int nn = 4;
  Flavour<double> flavours[] = {
    StandardModel::u(),
    StandardModel::ubar(),
    StandardModel::G(),
    StandardModel::G()
  };

  NParton2<double> amp;
  amp.setProcess(nn, flavours);

  const double MuR = 91.188;
  amp.setMuR2(MuR*MuR);

  const int hlist[4][4] = {
    {+1,-1,+1,-1},
  };

  MOM<double> PMpt[4] = {
    MOM<double>( 3.46507810831599451,  3.39664787927027714,  0.09208348768108298,  0.67902143727276213),
    MOM<double>( 2.33822158280808101, -0.41353652711221755,  2.18459244064649740,  0.72375657463243367),
    MOM<double>(-1.70876612243953088, -0.56836846175583105, -0.64843643562837180,  1.47525222990101347),
    MOM<double>(-4.09453356868454464, -2.41474289040222854, -1.62823949269920858, -2.87803024180620926),
  };

  std::vector<MOM<double> > PMmom;
  PMmom.insert(PMmom.end(), &PMpt[0], &PMpt[5]);

  amp.setMomenta(PMmom);
  const int ord[][4] = {
    {0,1,2,3},
//    {0,1,3,2},
//    {0,2,1,3},
    {0,3,1,2},
    {0,3,2,1},
//    {0,3,2,1}
  };

  for (int h=0; h<1; h++) {
    for (int i=0; i<4; i++) {
      cout << hlist[h][i] << " ";
    }
    cout << endl;
    amp.setHelicity(hlist[h]);
    for (int o=0; o<3; o++) {
      amp.setOrder(ord[o]);
      amp.eval(0);
      amp.printCoefficients();
    }
  }

  return;
}

void check5g() {

  cout << "5g amp checks" << endl;

  const int nn = 5;
  Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };

  NParton2<double> amp;
  amp.setProcess(nn, flavours);

  const double MuR = 91.188;
  amp.setMuR2(MuR*MuR);

  const int hlist[4][5] = {
    {+1,+1,+1,+1,+1},
    {-1,+1,+1,+1,+1},
    {-1,-1,+1,+1,+1},
    {-1,+1,-1,+1,+1},
  };

  MOM<double> PMpt[5] = {
    MOM<double>( 1.01253589148559857,  0.70940551259348613,  0.58370834601793628,  0.42574325248618352),
    MOM<double>( 5.08307559975846367, -0.84536120413115323, -5.00141422248981307,  0.32996630507736773),
    MOM<double>(-2.34316440682886203, -0.497649668590323364, 1.60232555861972120, -1.63563964520782895),
    MOM<double>(-1.99211785979492488,  1.23607685944128500,  1.55108166223856056, -0.18652946666665207),
    MOM<double>(-1.76032922462027534, -0.602471499313294539, 1.26429865561359503,  1.06645955431092977),
  };

  std::vector<MOM<double> > PMmom;
  PMmom.insert(PMmom.end(), &PMpt[0], &PMpt[6]);

  amp.setMomenta(PMmom);

  for (int h=0; h<4; h++) {
    for (int i=0; i<5; i++) {
      cout << hlist[h][i] << " ";
    }
    cout << endl;
    amp.setHelicity(hlist[h]);
    amp.eval(0);
    amp.printCoefficients();
  }

  return;
}

void check6g() {

  cout << "6g amp checks" << endl;

  const int nn = 6;
  Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };

  NParton2<double> amp;
  amp.setProcess(nn, flavours);

  const double MuR = 91.188;
  amp.setMuR2(MuR*MuR);

  const int hlist[8][6] = {
    {+1,+1,+1,+1,+1,+1},
    {-1,+1,+1,+1,+1,+1},
    {-1,-1,+1,+1,+1,+1},
    {-1,+1,-1,+1,+1,+1},
    {-1,+1,+1,-1,+1,+1},
    {-1,-1,-1,+1,+1,+1},
    {-1,+1,-1,-1,+1,+1},
    {-1,+1,-1,+1,-1,+1},
  };

  MOM<double> PMpt[6] = {
    MOM<double>(  20.366796084954325, -8.9029937397062567,-18.219259638185437 ,  1.8978049091645588),
    MOM<double>(  7.4339564141767147, -4.2255756802005397, -6.1007761073897945,  0.4344525592958229),
    MOM<double>(  5.199772739628204 , -1.4276177534052732, -4.721887402103609 ,  1.6441786568976973),
    MOM<double>(-20.236865568084545 ,  7.8994557994106646, 18.440584359804413 , -2.6597320471844358),
    MOM<double>(- 5.980304013689719 ,  3.7088425252795359,  4.686506651534028 ,  0.2125526380894671),
    MOM<double>(- 6.7833556569849803,  2.9478888486218691,  5.9148321363403994, -1.5292567162631103),
  };

  std::vector<MOM<double> > PMmom;
  PMmom.insert(PMmom.end(), &PMpt[0], &PMpt[7]);

  amp.setMomenta(PMmom);

  for (int h=0; h<8; h++) {
    cout << "helicity: ";
    for (int i=0; i<6; i++) {
      cout << hlist[h][i] << " ";
    }
    cout << endl;
    amp.setHelicity(hlist[h]);
    amp.eval(0);
    amp.printCoefficients();
  }

  return;
}



int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  check4g();
//  check5g();
//  check6g();

  return 1;
}
