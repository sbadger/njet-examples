#include <iostream>
#include <getopt.h>
#include <cstdio>

#include "tools/PhaseSpace.h"
#include "ngluon2/Initialize.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

using std::cout;
using std::endl;
using std::complex;

void checkGU(double mt, const int *helicity) {
  const int nn = 4;
  double mH = 1.2;
  StandardModel::setHmass(mH);

  Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::TopHiggs(),
  };

  MOM<double> GUpt[4] = {
    MOM<double>( 8.8317608663278469 , 2.0000000000000000, 5.0000000000000000,  7.0000000000000000),
    MOM<double>( 10.8166538263919679, 4.0000000000000000, 1.0000000000000000, 10.0000000000000000),
    MOM<double>(-4.0650794167490064 ,-2.3469746955372458,-2.3469746955372458, -2.3469746955372458),
    MOM<double>(-15.5833352759708083,-3.6530253044627542,-3.6530253044627542,-14.6530253044627542),
  };
  std::vector<MOM<double> > ps;
  ps.insert(ps.end(), &GUpt[0], &GUpt[4]);

  NParton2<double> amp;
  amp.setProcess(nn, flavours);
  amp.setLoopInduced();

  const double MuR = mt;
  amp.setMuR2(MuR*MuR);

  amp.setHeavyQuarkMass(mt);

  int perms[][4] = {
    {3,0,1,2},
    {3,1,2,0},
    {3,2,0,1},
  };

//  const int helicity[] = {+1,+1,+1,+1};

  EpsTriplet<double> part,sum;
  for (int p=0; p<3; p++) {
    amp.setOrder(perms[p]);
    amp.setHelicity(helicity);
    amp.setMomenta(ps);
    amp.eval(2);
    amp.printCoefficients();
    cout << amp.getcc(1) << endl;
    cout << amp.getcc(2) << endl;
    cout << amp.getcc(3) << endl;
    cout << amp.getrat() << endl;
    sum += amp.getcc(1)+amp.getcc(2)+amp.getcc(3)+(-amp.getrat());
//    sum += -amp.getcc(2);
//    sum += (-amp.getrat());
  }
  cout << "A(1,2,3,H) = " << sum.get0() << endl;

  // +++H from Rozowsky hep-ph/9709423
//  complex<double> phase = 1./(xspA(ps[0], ps[1])*xspA(ps[1], ps[2])*xspA(ps[2], ps[0]));
//  double s = S(ps[0]+ps[1]);
//  double t = S(ps[1]+ps[2]);
//  double u = S(ps[0]+ps[2]);

//  cout << "C4[1|2|3|H] = " << -mt/sqrt(2.)*i_*s*t*(4.*mt*mt-mH*mH)*phase*0.5 << endl;
//  cout << "C4[2|3|1|H] = " << -mt/sqrt(2.)*i_*u*t*(4.*mt*mt-mH*mH)*phase*0.5 << endl;
//  cout << "C4[3|1|2|H] = " << -mt/sqrt(2.)*i_*s*u*(4.*mt*mt-mH*mH)*phase*0.5 << endl;
//  cout << "C3[1|2|3H] + C3[1|2|H3] = " << -mt/sqrt(2.)*i_*s*t*(4.*mt*mt-mH*mH)*phase*(t-u)/(t*(s-mH*mH)) << endl;
//  cout << "C3[1|2|3H] + C3[1|2|H3] = " << -mt/sqrt(2.)*i_*s*t*(4.*mt*mt-mH*mH)*phase*(s-u)/(s*(t-mH*mH)) << endl;

  Flavour<double> flavoursEffTree[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::Higgs(),
  };

  NParton2<double> ampEffTree;
  ampEffTree.setProcess(nn, flavoursEffTree);

  complex<double> tree;
  for (int p=0; p<3; p++) {
    ampEffTree.setOrder(perms[p]);
    ampEffTree.setHelicity(helicity);
    ampEffTree.setMomenta(ps);
    tree += ampEffTree.evalTree();
  }
  cout << "# " << tree << endl;
  cout << "# +++ " << -0.5*i_*pow(mH,4)/(xspA(ps[0], ps[1])*xspA(ps[1], ps[2])*xspA(ps[2], ps[3])) << endl;
//  cout << "# ++- " << 0.5*i_*pow(xspB(ps[1],ps[2]),4)/(xspB(ps[1], ps[2])*xspB(ps[2], ps[3])*xspB(ps[3], ps[1])) << endl;
//  cout << "# +-+ " << 0.5*i_*pow(xspB(ps[3],ps[1]),4)/(xspB(ps[1], ps[2])*xspB(ps[2], ps[3])*xspB(ps[3], ps[1])) << endl;

  cout << "### [" << mt << ", " << abs(sum.get0()) << "],"<< endl;
}


template <typename T>
void check(double mt) {
  const int nn = 4;
  double mH = 1.;
  StandardModel::setHmass(mH);

  Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::TopHiggs(),
  };

  PhaseSpace<T> ps(nn, flavours, 1, 1.4e4);
  ps.getPSpoint();
  ps.showPSpoint();

  NParton2<T> amp;
  amp.setProcess(nn, flavours);
  amp.setLoopInduced();

  const T MuR = mt;
  amp.setMuR2(MuR*MuR);

  amp.setHeavyQuarkMass(mt);

  int perms[][4] = {
    {0,1,2,3},
    {0,2,3,1},
    {0,3,1,2},
  };

  const int helicity[][4] = {
    {+1,+1,+1,+1},
    {-1,+1,+1,+1},
    {+1,-1,+1,+1},
    {+1,+1,-1,+1},
    {-1,-1,+1,+1},
    {-1,+1,-1,+1},
    {+1,-1,-1,+1},
    {-1,-1,-1,+1},
  };

  Flavour<double> flavoursEffTree[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::Higgs(),
  };

  NParton2<T> ampEffTree;
  ampEffTree.setProcess(nn, flavoursEffTree);
  int permsEff[][4] = {
    {1,2,3,0},
    {1,3,2,0},
    {3,1,2,0},
  };

  T sqamp = 0.;
  for (int h=0; h<8; h++) {
    EpsTriplet<T> hampbox;
    EpsTriplet<T> hamptri;
    EpsTriplet<T> hamprat;
    EpsTriplet<T> hamp;
    for (int p=0; p<3; p++) {
      amp.setOrder(perms[p]);
      amp.setHelicity(helicity[h]);
      amp.setMomenta(ps.Mom());
      amp.eval(2);
      hampbox += amp.getcc(1);
      hamptri += amp.getcc(2);
      hamprat += amp.getrat();
      hamp += amp.getres();
    }
    //      cout << hampbox << endl;
    //      cout << hamptri << endl;
    //      cout << hamprat << endl;
    //      cout << hamp << endl;
    sqamp += real(conj(hamp.get0())*hamp.get0());
  }
  cout << "|A(1,2,3,H)|^2 = " << sqamp << endl;

  T sqtree = 0.;
  for (int h=0; h<8; h++) {
    complex<T> tree;
    for (int p=0; p<3; p++) {
      ampEffTree.setOrder(permsEff[p]);
      ampEffTree.setHelicity(helicity[h]);
      ampEffTree.setMomenta(ps.Mom());
      tree += ampEffTree.evalTree();
    }
    if (h==0) {
      cout << h << " A0eff = " << tree << endl;
      cout << " -I/2*mH^4/<i i+1> = " << -0.5*i_*pow(mH,4)/(xspA(ps[0], ps[1])*xspA(ps[1], ps[2])*xspA(ps[2], ps[0])) << endl;
    }
    sqtree += real(tree*conj(tree));
  }
  cout << "|A0eff|^2            = " << sqtree << endl;
  cout << "|A0eff|^2/|A1full*mt^2|^2 = " << sqtree/sqamp*pow(mt,-4) << endl;
  cout << "### [" << mt << ", " <<  sqtree/sqamp*pow(mt,-4)/mH << ", " << mH*mH/4./mt/mt << "],"<< endl;
}

void checkMG5()
{
  double mH = 125.;
  StandardModel::setHmass(mH);

  MOM<double> MG5mom[][4] = {
    {
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,   -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,    0.500000000000000E+03),
      MOM<double>(0.492187500000000E+03,    0.109191092499398E+03,    0.437880308402370E+03,   -0.196434915400709E+03),
      MOM<double>(0.507812500000000E+03,   -0.109191092499398E+03,   -0.437880308402370E+03,    0.196434915400709E+03),
    },{
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,   -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,    0.500000000000000E+03),
      MOM<double>(0.492187500000000E+03,   -0.277510576031119E+03,   -0.384684857748014E+03,    0.131354389216792E+03),
      MOM<double>(0.507812500000000E+03,    0.277510576031119E+03,    0.384684857748014E+03,   -0.131354389216792E+03),
    },{
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,   -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,    0.500000000000000E+03),
      MOM<double>(0.492187499999999E+03,   -0.236861325514177E+03,    0.276089630897517E+03,   -0.331541495657465E+03),
      MOM<double>(0.507812500000001E+03,    0.236861325514177E+03,   -0.276089630897517E+03,    0.331541495657465E+03),
    },{
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,   -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,    0.500000000000000E+03),
      MOM<double>(0.492187500000000E+03,    0.443987787898762E+03,    0.595346920400478E+02,   -0.203909293061180E+03),
      MOM<double>(0.507812500000000E+03,   -0.443987787898762E+03,   -0.595346920400478E+02,    0.203909293061180E+03),
    }
  };

  double mt = 173.;
//  double MG5res[] = {1.1755398520541811E-003, 9.5683498217621573E-004, 2.5985570647677108E-003, 1.2110252292724436E-003}; // mt=173.
  double MG5res[] = {9.9684283395507346E-007, 1.0196247969704364E-006, 8.6798383224316516E-007, 9.9319987861272061E-007}; // +++ mt=173.
//  double mt = 1e4;
//  double MG5res[] =  {2.1753792107253658E-003, 1.8662399956152649E-003, 3.9688788511454470E-003, 2.2242591406133397E-003}; // mt=1e4
//  double MG5res[] = {8.8249436681937094E-004, 7.9882129213011232E-004, 1.3582113427957616E-003, 8.9565385668281101E-004}; // +++ mt=1e4
//  mt = 9e2;

  Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::TopHiggs(),
  };

  NParton2<double> amp;
  amp.setProcess(4, flavours);
  amp.setLoopInduced();

  const double MuR = mt;
  amp.setMuR2(MuR*MuR);

  amp.setHeavyQuarkMass(mt);

  int perms[][4] = {
    {0,1,2,3},
    {0,1,3,2},
    {0,3,1,2},
  };

  const int helicity[][4] = {
    {+1,+1,+1,+1},
    {-1,+1,+1,+1},
    {+1,-1,+1,+1},
    {+1,+1,-1,+1},
    {-1,-1,+1,+1},
    {-1,+1,-1,+1},
    {+1,-1,-1,+1},
    {-1,-1,-1,+1},
  };

  Flavour<double> flavoursEffTree[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::Higgs(),
  };

  NParton2<double> ampEffTree;
  ampEffTree.setProcess(4, flavoursEffTree);
  int permsEff[][4] = {
    {1,2,3,0},
    {1,3,2,0},
    {3,1,2,0},
  };

  for (int pt=0; pt<4; pt++) {
    std::vector<MOM<double> > ps;
    ps.insert(ps.end(), &MG5mom[pt][0], &MG5mom[pt][4]);

    double sqamp = 0.;
    for (int h=0; h<1; h++) {
      EpsTriplet<double> hampbox;
      EpsTriplet<double> hamptri;
      EpsTriplet<double> hamprat;
      EpsTriplet<double> hamp;
      for (int p=0; p<3; p++) {
        amp.setOrder(perms[p]);
        amp.setHelicity(helicity[h]);
        amp.setMomenta(ps);
        amp.eval(2);
        hampbox += amp.getcc(1);
        hamptri += amp.getcc(2);
        hamprat += amp.getrat();
        hamp += amp.getres();
        cout << amp.getres() << " +- " << amp.geterr() << endl;
      }
//      cout << hampbox << endl;
//      cout << hamptri << endl;
//      cout << hamprat << endl;
//      cout << hamp << endl;
      sqamp += real(conj(hamp.get0())*hamp.get0());
    }
    cout << "|A(1,2,3,H)|^2 = " << sqamp << endl;
    cout << "ratio to MG5   = " << sqamp/MG5res[pt] << endl;

    double sqtree = 0.;
    for (int h=0; h<1; h++) {
    complex<double> tree;
      for (int p=0; p<3; p++) {
        ampEffTree.setOrder(permsEff[p]);
        ampEffTree.setHelicity(helicity[h]);
        ampEffTree.setMomenta(ps);
        tree += ampEffTree.evalTree();
      }
      sqtree += real(tree*conj(tree));
      if (h==0) {
        cout << h << " A0eff = " << tree << endl;
        cout << " -I/2*mH^4/<i i+1> = " << -0.5*i_*pow(mH,4)/(xspA(ps[0], ps[1])*xspA(ps[1], ps[2])*xspA(ps[2], ps[0])) << endl;
      }
    }
    cout << "|A0eff|^2            = " << sqtree << endl;
    cout << "|A0eff|^2/MG5        = " << sqtree/MG5res[pt] << endl;
    cout << "|A0eff|^2/|A1full*mt^2|^2 = " << sqtree/sqamp*pow(mt,-4) << endl;
  }

}

void checkMG5qq()
{
  double mH = 125.;
  StandardModel::setHmass(mH);

  MOM<double> MG5mom[][4] = {
    {
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,   -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,    0.500000000000000E+03),
      MOM<double>(0.492187500000000E+03,    0.109191092499398E+03,    0.437880308402370E+03,   -0.196434915400709E+03),
      MOM<double>(0.507812500000000E+03,   -0.109191092499398E+03,   -0.437880308402370E+03,    0.196434915400709E+03),
    },{
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,   -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,    0.500000000000000E+03),
      MOM<double>(0.492187500000000E+03,   -0.277510576031119E+03,   -0.384684857748014E+03,    0.131354389216792E+03),
      MOM<double>(0.507812500000000E+03,    0.277510576031119E+03,    0.384684857748014E+03,   -0.131354389216792E+03),
    },{
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,   -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,    0.500000000000000E+03),
      MOM<double>(0.492187499999999E+03,   -0.236861325514177E+03,    0.276089630897517E+03,   -0.331541495657465E+03),
      MOM<double>(0.507812500000001E+03,    0.236861325514177E+03,   -0.276089630897517E+03,    0.331541495657465E+03),
    },{
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,   -0.500000000000000E+03),
      MOM<double>(-0.500000000000000E+03,    0.000000000000000E+00,    0.000000000000000E+00,    0.500000000000000E+03),
      MOM<double>(0.492187500000000E+03,    0.443987787898762E+03,    0.595346920400478E+02,   -0.203909293061180E+03),
      MOM<double>(0.507812500000000E+03,   -0.443987787898762E+03,   -0.595346920400478E+02,    0.203909293061180E+03),
    }
  };

  double mt = 173.;
  double MG5res[] = {7.6493921713826859E-005, 7.0683318729241961E-005, 9.5923634880645705E-005, 7.7308971159346652E-005};

  Flavour<double> flavours[] = {
    StandardModel::ubar(),
    StandardModel::u(),
    StandardModel::G(),
    StandardModel::TopHiggs(),
  };

  NParton2<double> amp;
  amp.setProcess(4, flavours);
  amp.setLoopInduced();

  const double MuR = mt;
  amp.setMuR2(MuR*MuR);

  amp.setHeavyQuarkMass(mt);

  int perms[][4] = {
    {0,1,2,3},
    {0,1,3,2},
  };

  const int helicity[][4] = {
    {-1,+1,+1,+1},
    {+1,-1,+1,+1},
    {-1,+1,-1,+1},
    {+1,-1,-1,+1},
  };

  Flavour<double> flavoursEffTree[] = {
    StandardModel::ubar(),
    StandardModel::u(),
    StandardModel::G(),
    StandardModel::Higgs(),
  };

  NParton2<double> ampEffTree;
  ampEffTree.setProcess(4, flavoursEffTree);
  int permsEff[][4] = {
    {1,2,3,0},
    {1,3,2,0}
  };

  for (int pt=0; pt<4; pt++) {
    std::vector<MOM<double> > ps;
    ps.insert(ps.end(), &MG5mom[pt][0], &MG5mom[pt][4]);

    double sqamp = 0.;
    for (int h=0; h<4; h++) {
      EpsTriplet<double> hampbox;
      EpsTriplet<double> hamptri;
      EpsTriplet<double> hamprat;
      EpsTriplet<double> hamp;
      for (int p=0; p<2; p++) {
        amp.setOrder(perms[p]);
        amp.setHelicity(helicity[h]);
        amp.setMomenta(ps);
        amp.eval(2);
        hampbox += amp.getcc(1);
        hamptri += amp.getcc(2);
        hamprat += amp.getrat();
        hamp += amp.getres();
        cout << amp.getres() << " +- " << amp.geterr() << endl;
      }
//      cout << hampbox << endl;
//      cout << hamptri << endl;
//      cout << hamprat << endl;
//      cout << hamp << endl;
      sqamp += real(conj(hamp.get0())*hamp.get0());
    }
    cout << "|A(1,2,3,H)|^2 = " << sqamp << endl;
    cout << "ratio to MG5   = " << sqamp/MG5res[pt] << endl;

    double sqtree = 0.;
    for (int h=0; h<4; h++) {
    complex<double> tree;
      for (int p=0; p<2; p++) {
        ampEffTree.setOrder(permsEff[p]);
        ampEffTree.setHelicity(helicity[h]);
        ampEffTree.setMomenta(ps);
        tree += ampEffTree.evalTree();
      }
      sqtree += real(tree*conj(tree));
    }
    cout << "|A0eff|^2            = " << sqtree << endl;
    cout << "|A0eff|^2/MG5        = " << sqtree/MG5res[pt] << endl;
    cout << "|A0eff|^2/|A1full*mt^2|^2 = " << sqtree/sqamp*pow(mt,-4) << endl;
  }

}

int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  checkMG5();
  checkMG5qq();
  exit(0);

//  const int hel[4] = {+1,+1,+1,+1};
//  checkGU(1.75, hel);
//  exit(0);

//  const int hel[4] = {+1,+1,+1,+1};
//  for (int mt=0; mt<100; mt++) {
//    check<dd_real>(125.1*pow(1.5,mt));
//  }

  return 1;
}
