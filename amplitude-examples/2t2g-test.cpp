#include <iostream>
#include <getopt.h>
#include <cstdio>

#include "tools/PhaseSpace.h"
#include "ngluon2/Initialize.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

using std::cout;
using std::endl;

int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  const int nn = 4;
  Flavour<double> flavours[] = {
    StandardModel::t(173.).C(),
    StandardModel::t(173.),
    StandardModel::G(),
    StandardModel::G()
  };
  PhaseSpace<double> ps(nn, flavours);
  ps.getPSpoint();
  ps.showPSpoint();

  NParton2<double> amp;
  amp.setProcess(nn, flavours);

  const double MuR = 173.;
  amp.setMuR2(MuR*MuR);

  const int helicity[] = {+1,+1,+1,+1};
  amp.setHelicity(helicity);
  amp.setMomenta(ps.Mom());
  amp.eval(0);
  amp.printCoefficients();

  return 1;
}
