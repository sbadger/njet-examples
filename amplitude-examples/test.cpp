#include <iostream>
#include <getopt.h>
#include <cstdio>

#include "tools/PhaseSpace.h"
#include "ngluon2/Initialize.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

using std::cout;
using std::endl;

int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  const int nn = 6;
  Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };
  PhaseSpace<double> ps(nn, flavours);
  ps.getPSpoint();
  ps.showPSpoint();

  NParton2<double> amp;
  amp.setProcess(nn, flavours);

  const double MuR = 91.188;
  amp.setMuR2(MuR*MuR);

  const int helicity[] = {-1,-1,+1,+1,+1,+1};
  amp.setHelicity(helicity);
  amp.setMomenta(ps.Mom());

  cout << amp.evalTree() << endl;
  cout <<
    i_*pow(xspA(ps[0], ps[1]),4)/(xspA(ps[0], ps[1])*xspA(ps[1], ps[2])*xspA(ps[2], ps[3])*xspA(ps[3], ps[4])*xspA(ps[4], ps[5])*xspA(ps[5], ps[0]))
    << endl;
  // 0 = mixed / gluon primitive
  cout << amp.eval(0) << endl;
  // 1 = fermion loop primitive
  cout << amp.eval(1) << endl;

  return 1;
}
