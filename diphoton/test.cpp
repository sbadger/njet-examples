#include <string>
#include <vector>

#include "chsums/0q2gA.h"
#include "chsums/2q0gA.h"
#include "chsums/0q2gH.h"
#include "chsums/NJetAmp.h"

#include "../loop-induced/phaseSpace.h"

template<typename T>
void test(const bool verbose = false) {
    typedef typename NJetAmp<T>::LoopValue LoopValue;

    const int hel[]{-1, -1, +1, +1};
    const std::vector <MOM<T>> mom{getMasslessPhaseSpacePoint<T>(4, verbose)};

    // A simple example of calling a chsums amp from NJet - 2g1H born
    Amp0q2gH<T> amp0(1.);
    amp0.setHelicity(hel);
    amp0.setMomenta(mom);
    std::cout << "2g1H born " << amp0.born() << std::endl;

    // This example shows how to define the external photon for a photon chsums amp - 2q2A born
    const Flavour<double> ff1{StandardModel::Au(StandardModel::u(), StandardModel::ubar())};
    Amp2q0gAA<T> amp1(ff1, 1.);
    amp1.setHelicity(hel);
    amp1.setMomenta(mom);
    std::cout << "2q2A born " << amp1.born() << std::endl;

    // The new chsums amp I'm adding to NJet
    // A doesn't couple to g, so should be zero (for born)
    // Define dummy external photon flavour - expect warning
    const Flavour<double> ff2{StandardModel::Ax(StandardModel::G(), StandardModel::G())};
    Amp0q2gAA <T> amp2(ff2, 1.);
    amp2.setHelicity(hel);
    amp2.setMomenta(mom);
    std::cout << "2g2A born " << amp2.born() << std::endl;

    // Example of 1-loop call - virt x tree for 2q2A
    const EpsTriplet<T> sol4{amp1.virt()};
    std::cout << "2q2A virt " << sol4 << std::endl;

    // Next, try virtual squared for 2g2A
    const Flavour<double> ff3{StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C())};
    Amp0q2gAA <T> amp3(ff3, 1.);
    amp3.setLoopInduced(true);
    amp3.setHelicity(hel);
    amp3.setMomenta(mom);
    const LoopValue sol3{amp3.virtsq()};
    std::cout << "2g2A virt " << sol3.get0().real() << std::endl;

    std::cout << std::endl;
}

int main(int argc, char **argv) {
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(8);

    const bool verbose{argc == 2 ? (std::string(argv[1]) == "v") : false};

    test<double>(verbose);

    return 1;
}
