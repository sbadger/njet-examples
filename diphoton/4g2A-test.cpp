#include <string>
#include <vector>

#include "chsums/0q4gA.h"

#include "../loop-induced/common.h"
#include "../loop-induced/phaseSpace.h"
#include "../loop-induced/4g2A.h"
#include "../loop-induced/EpsQuintuplet.h"

// virtual squared for 4g2A, nJet implementation vs external implementation

const int mul{6};

template<typename T>
const EpsTriplet <T> extAmp(const std::vector <MOM<T>> &mom, const int (&hel)[mul], const bool &verbose = false) {
    Amp_4g2A<T> amp_class;
    amp_class.setMomenta(mom);
    amp_class.setHelicity(hel);
    EpsTriplet <T> amp{amp_class.evalAmp()};
    return amp;
}

template<typename T>
const EpsQuintuplet<T> extColSum(const std::vector <MOM<T>> &mom, const int (&hel)[mul], const bool &verbose = false) {
    Amp_4g2A<T> amp_class;
    amp_class.setMomenta(mom);
    amp_class.setHelicity(hel);
    return amp_class.evalAmp2(fundamental);
}

template<typename T>
Amp0q4gAA <T> initNJetAmp(const std::vector <MOM<T>> &mom) {
    // A defined in "../loop-induced/common.h"
    Amp0q4gAA <T> amp(A, 1.);
    amp.setMomenta(mom);
    return amp;
}

template<typename T>
T nJetColSum(const std::vector <MOM<T>> &mom, const int (&hel)[mul], const bool &verbose = false) {
    typedef typename NJetAmp<T>::LoopValue LoopValue;
    Amp0q4gAA <T> amp{initNJetAmp(mom)};
    const LoopValue amp2{amp.virtsq(hel)};
    return amp2.get0().real();
}

template<typename T>
T nJetHelSum(const std::vector <MOM<T>> &mom, const bool &verbose = false) {
    typedef typename NJetAmp<T>::LoopValue LoopValue;
    Amp0q4gAA <T> amp{initNJetAmp(mom)};
    const LoopValue amp2{amp.virtsq()};
    return amp2.get0().real();
}

template<typename T>
const EpsQuintuplet<T>
compareColSum(const std::vector <MOM<T>> mom, const int (&hel)[mul], const bool &verbose = false) {
    std::cout << "|A(" << genHelStr(hel) << ")|^2" << std::endl;

    const EpsQuintuplet<T> ext{extColSum<T>(mom, hel, verbose)};
    std::cout << "ext  = " << ext.get0().real() << std::endl;

    const T nJet{nJetColSum<T>(mom, hel, verbose)};
    std::cout << "nJet = " << nJet << std::endl;

    const T ratio{nJet / ext.get0().real()};
    std::cout << "ratio=" << ratio << std::endl;
    std::cout << std::endl;

    return ext;
}

const int h(const unsigned int i) {
    return i == 0 ? -1 : 1;
}

template<typename T>
EpsQuintuplet<T> loopHels(const std::vector <MOM<T>> mom, const bool &verbose = false) {
    EpsQuintuplet<T> sum;
    for (unsigned int i0{0}; i0 < 2; ++i0) {
        for (unsigned int i1{0}; i1 < 2; ++i1) {
            for (unsigned int i2{0}; i2 < 2; ++i2) {
                for (unsigned int i3{0}; i3 < 2; ++i3) {
                    for (unsigned int i4{0}; i4 < 2; ++i4) {
                        for (unsigned int i5{0}; i5 < 2; ++i5) {
                            const int hel[mul]{h(i0), h(i1), h(i2), h(i3), h(i4), h(i5)};
                            sum += compareColSum<T>(mom, hel, verbose);
                        }
                    }
                }
            }
        }
    }
    return sum;
}

template<typename T>
void mainTemplate(const bool &verbose = false) {
    const std::vector <MOM<T>> mom{getMasslessPhaseSpacePoint<T>(mul, verbose)};

    //    // check single helicity amp
    //    const int hel[mul]{1, 1, 1, 1, 1, 1};
    //    std::cout << "Single amp check" << std::endl;
    //    std::cout << "ext  = " << extAmp(mom, hel, verbose).get0() << std::endl;
    //
    //    // requires the next line in line 64 of njet-develop/chsums/0q4gA.cpp
    //    // std::cout << sol.loop.get0() << std::endl;
    //    std::cout << "nJet = " << nJetColSum<T>(mom, hel, verbose) << std::endl << std::endl;

    EpsQuintuplet<T> ext{loopHels<T>(mom, verbose)};
    //
    //    std::cout << "sum_h |A_h|^2" << std::endl;
    //
    //    std::cout << "ext  = " << ext.get0().real() << std::endl;
    //
    //    const T nJet{nJetHelSum<T>(mom, verbose)};
    //    std::cout << "nJet = " << nJet << std::endl;
    //
    //    const T ratio{nJet / ext.get0().real()};
    //    std::cout << "ratio=" << ratio << std::endl << std::endl;
}

int main(int argc, char **argv) {
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(8);

    const bool verbose{argc == 2 ? (std::string(argv[1]) == "v") : false};

    mainTemplate<double>(verbose);

    return 1;
}
