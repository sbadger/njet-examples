#include <string>
#include <vector>

#include "chsums/0q2gA.h"

#include "../loop-induced/phaseSpace.h"
#include "../loop-induced/2g2A.h"
#include "../loop-induced/EpsQuintuplet.h"

// virtual squared for 2g2A, nJet implementation vs external implementation

template<typename T>
const EpsQuintuplet<T> extColSum(const std::vector <MOM<T>> &mom, const int (&hel)[4], const bool &verbose = false) {
    Amp_2g2A<T> amp_class;
    amp_class.setMomenta(mom);
    amp_class.setHelicity(hel);
    return amp_class.evalAmp2();
}

template<typename T>
const EpsQuintuplet<T> extHelSum(const std::vector <MOM<T>> &mom, const bool &verbose = false) {
    EpsQuintuplet<T> sum;
    for (int i0{0}; i0 < 2; ++i0) {
        for (int i1{0}; i1 < 2; ++i1) {
            for (int i2{0}; i2 < 2; ++i2) {
                for (int i3{0}; i3 < 2; ++i3) {
                    const int hel[4]{i0 == 0 ? -1 : 1, i1 == 0 ? -1 : 1, i2 == 0 ? -1 : 1, i3 == 0 ? -1 : 1};
                    sum += extColSum(mom, hel, verbose);
                }
            }
        }
    }
    return sum;
}

template<typename T>
T nJetColSum(const std::vector <MOM<T>> &mom, const int (&hel)[4], const bool &verbose = false) {
    typedef typename NJetAmp<T>::LoopValue LoopValue;
    const Flavour<double> ff3{StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C())};
    Amp0q2gAA <T> amp(ff3, 1.);
    amp.setMomenta(mom);
    const LoopValue amp2{amp.virtsq(hel)};
    return amp2.get0().real();
}

template<typename T>
T nJetHelSum(const std::vector <MOM<T>> &mom, const bool &verbose = false) {
    typedef typename NJetAmp<T>::LoopValue LoopValue;
    const Flavour<double> ff3{StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C())};
    Amp0q2gAA <T> amp(ff3, 1.);
    amp.setMomenta(mom);
    const LoopValue amp2{amp.virtsq()};
    return amp2.get0().real();
}

template<typename T>
void compareColSum(const std::vector <MOM<T>> mom, const int (&hel)[4], const bool &verbose = false) {
    std::cout << "|A(" << genHelStr(hel) << ")|^2" << std::endl;

    const T ext{extColSum<T>(mom, hel, verbose).get0().real()};
    std::cout << "ext  = " << ext << std::endl;

    const T nJet{nJetColSum<T>(mom, hel, verbose)};
    std::cout << "nJet = " << nJet << std::endl;

    const T ratio{nJet / ext};
    std::cout << "ratio=" << ratio << std::endl << std::endl;
}

template<typename T>
void compareHelSum(const std::vector <MOM<T>> mom, const bool &verbose = false) {
    std::cout << "sum_h |A_h|^2" << std::endl;

    const T ext{extHelSum<T>(mom, verbose).get0().real()};
    std::cout << "ext  = " << ext << std::endl;

    const T nJet{nJetHelSum<T>(mom, verbose)};
    std::cout << "nJet = " << nJet << std::endl;

    const T ratio{nJet / ext};
    std::cout << "ratio=" << ratio << std::endl << std::endl;
}

template<typename T>
void loopHels(const std::vector <MOM<T>> mom, const bool &verbose = false) {
    for (int i0{0}; i0 < 2; ++i0) {
        for (int i1{0}; i1 < 2; ++i1) {
            for (int i2{0}; i2 < 2; ++i2) {
                for (int i3{0}; i3 < 2; ++i3) {
                    const int hel[4]{i0 == 0 ? -1 : 1, i1 == 0 ? -1 : 1, i2 == 0 ? -1 : 1, i3 == 0 ? -1 : 1};
                    compareColSum<T>(mom, hel, verbose);
                }
            }
        }
    }
}

template<typename T>
void mainTemplate(const bool &verbose = false) {
    const std::vector <MOM<T>> mom{getMasslessPhaseSpacePoint<T>(4, verbose)};

    loopHels<T>(mom, verbose);

    compareHelSum<T>(mom, verbose);
}

int main(int argc, char **argv) {
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(8);

    const bool verbose{argc == 2 ? (std::string(argv[1]) == "v") : false};

    mainTemplate<double>(verbose);

    return 1;
}
