#include <string>
#include <vector>

#include "chsums/0q2gA.h"

#include "../loop-induced/phaseSpace.h"
#include "../loop-induced/2g2A.h"
#include "../loop-induced/EpsQuintuplet.h"

// virtual squared for 2g2A, nJet implementation vs external implementation

const int hels[8][4] = {
    {+1, -1, -1, +1},
    {+1, -1, +1, +1},
    {-1, +1, -1, +1},
    {-1, +1, +1, +1},
    {+1, -1, -1, -1},
    {+1, -1, +1, -1},
    {-1, +1, -1, -1},
    {-1, +1, +1, -1},
};

template<typename T>
const EpsTriplet <T> extAns(const std::vector <MOM<T>> &mom, const int (&hel)[4], const bool &verbose = false) {
    Amp_2g2A<T> amp_class;
    amp_class.setMomenta(mom);
    amp_class.setHelicity(hel);
    return amp_class.evalAmp();
}

// print helicity amplitudes in 0q2gA.cpp > LoopResult<T> Amp0q2gAA<T>::AF(int p0, int p1) [line 65] as
// std::cout << BaseClass::AFgAAxx(order).loop << std::endl;
template<typename T>
void nJetAns(const std::vector <MOM<T>> &mom, const int (&hel)[4], const bool &verbose = false) {
    const Flavour<double> ff{StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C())};
    Amp0q2gAA <T> amp(ff, 1.);
    amp.setMomenta(mom);
    amp.virtsq(hel);
}

template<typename T>
void compare(const bool &verbose = false) {
    const std::vector <MOM<T>> mom{getMasslessPhaseSpacePoint<T>(4, verbose)};

    for (auto &hel: hels) {
        std::cout << "A(" << genHelStr(hel) << ")" << std::endl;
        const EpsTriplet<T> ext{extAns<T>(mom, hel, verbose)};
        std::cout << "ext  =" << ext << std::endl << "nJet =";
        nJetAns<T>(mom, hel, verbose);
        std::cout << std::endl;
    }

}

int main(int argc, char **argv) {
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(8);

    const bool verbose{argc == 2 ? (std::string(argv[1]) == "v") : false};

    compare<double>(verbose);

    return 1;
}
