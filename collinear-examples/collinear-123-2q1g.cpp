#include "split3.h"

const double DEBUG_TOLL = 10e-8;

#define P_MIN 10
#define P_MAX 11
#define DEFAULT_P 10

#define DO_QBQG 0
#define DO_QBGQ 1

#define DO_QBQG_R 0
#define DO_QBQG_F 0
#define DO_QBQG_L 0
#define DO_QBGQ_LR 1
#define DO_LOOP (DO_QBQG_L || DO_QBQG_R || DO_QBQG_F || DO_QBGQ_LR)

bool DEBUG = true;
int DEBUG_HIDX;


template <typename T>
inline EpsTriplet<T> epsabs(EpsTriplet<T> x)
{
  return EpsTriplet<T>(abs(x.get0()),abs(x.get1()),abs(x.get2()));
}


// generate triple collinear phase-space point for 2->4
template <typename T>
std::vector<MOM<T> > getTripleCollinearPoint6(const double lambda)
{

  StandardModel::setHmass(lambda);
  const Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::Higgs(),
    StandardModel::G()
  };
  PhaseSpace<T> ps(4, flavours, 1, 1e3);
  ps.getPSpoint();
//  ps.showPSpoint();

  const T s123 = S(ps[2]);
  const T x1234 = s123/dot(ps[2],ps[3])*T(0.5);
  MOM<T> p123t = ps[2]-x1234*ps[3];

  cout << "### s123 = " << s123 << endl;

  // map to 2->4 configuration //
  const T z1 = 0.545;
  const T z2 = 0.424;
  const T z3 = 1.-z1-z2;

  MOM<complex<T> > e12 = CMOM(p123t,ps[3]);
  MOM<complex<T> > e21 = CMOM(ps[3],p123t);

  MOM<complex<T> > v1_c = i_*(e12-e21);
  MOM<complex<T> > v2_c = (e12+e21);
  cout << v1_c << endl;
  cout << v2_c << endl;

  MOM<T> v1(real(v1_c.x0), real(v1_c.x1), real(v1_c.x2), real(v1_c.x3));
  MOM<T> v2(real(v2_c.x0), real(v2_c.x1), real(v2_c.x2), real(v2_c.x3));

  T y11 = 0.124*sqrt(s123);
  T y12 = 0.456*sqrt(s123);
  T y21 = 0.294*sqrt(s123);
  T y22 = 0.835*sqrt(s123);

  T y13 = -y11-y12;
  T y23 = -y21-y22;

  MOM<T> kt1 = y11*v1 + y21*v2;
  MOM<T> kt2 = y12*v1 + y22*v2;
  MOM<T> kt3 = y13*v1 + y23*v2;

  T mshift = (S(kt1)/z1+S(kt2)/z2+S(kt3)/z3)*x1234/s123;

  std::vector<MOM<T> >  Mom(6);
  Mom[4] = ps[0];
  Mom[5] = ps[1];
  Mom[0] = z1*p123t + kt1 - S(kt1)/z1*x1234/s123*ps[3];
  Mom[1] = z2*p123t + kt2 - S(kt2)/z2*x1234/s123*ps[3];
  Mom[2] = z3*p123t + kt3 - S(kt3)/z3*x1234/s123*ps[3];
  Mom[3] = (1. + x1234 + mshift)*ps[3];

  MOM<T> mcons;
  for (int i=0; i<6; i++) {
    mcons += Mom[i];
    cout << "p"<<i<<"^2 = "<< S(Mom[i]) << endl;
  }
  cout << mcons << endl;

  return Mom;
}

complex<double> chop(complex<double> a) {
  complex<double> ans(0.,0.);
  if(!(abs(real(a)) < 1e-8)) ans.real(real(a));
  if(!(abs(imag(a)) < 1e-8)) ans.imag(imag(a));
  return ans;
}

complex<dd_real> chop(complex<dd_real> a) {
  complex<dd_real> ans(0.,0.);
  if(!(abs(real(a)) < 1e-14)) ans.real(real(a));
  if(!(abs(imag(a)) < 1e-14)) ans.imag(imag(a));
  return ans;
}

complex<qd_real> chop(complex<qd_real> a) {
  complex<qd_real> ans(0.,0.);
  if(!(abs(real(a)) < 1e-40)) ans.real(real(a));
  if(!(abs(imag(a)) < 1e-40)) ans.imag(imag(a));
  return ans;
}

EpsTriplet<double> chop(EpsTriplet<double> a) { return EpsTriplet<double>(chop(a.get0()), chop(a.get1()), chop(a.get2())); }
EpsTriplet<dd_real> chop(EpsTriplet<dd_real> a) { return EpsTriplet<dd_real>(chop(a.get0()), chop(a.get1()), chop(a.get2())); }
EpsTriplet<qd_real> chop(EpsTriplet<qd_real> a) { return EpsTriplet<qd_real>(chop(a.get0()), chop(a.get1()), chop(a.get2())); }

template <typename T>
EpsTriplet<T> ratio(EpsTriplet<T> a1, EpsTriplet<T> a2)
{
  return EpsTriplet<T>(
    chop(a1.get0())/chop(a2.get0()),
    chop(a1.get1())/chop(a2.get1()),
    chop(a1.get2())/chop(a2.get2()));
}

template <typename T>
void checkTripleCollinear(const Flavour<double>* flavours, const int h[]) {
  const int nn = 6;

  std::vector<MOM<T> > mom(nn);

  NParton2<T> amp;
  amp.setProcess(nn, flavours);

  NParton2<T> fact_amp;
  const Flavour<double> fact_flavours[] = {StandardModel::G(), flavours[3], flavours[4], flavours[5]};
  fact_amp.setProcess(nn-2, fact_flavours);
  std::vector<MOM<T> > fact_mom(nn-2);

//  const_Flavour<double> split_flavours[] = {StandardModel::G(), flavours[0], flavours[1], flavours[2]};
  Split3q<T> split;

  const T MuR = 1e3/7.;
  amp.setMuR2(MuR*MuR);
  fact_amp.setMuR2(MuR*MuR);
  split.setMuR2(MuR*MuR);

  amp.setHelicity(h);
  int fact_h[] = {+1, h[3], h[4], h[5]};
  int split_h[] = {-1, h[0], h[1], h[2]};

  for (int p=P_MIN; p<P_MAX; p++) {
    mom = getTripleCollinearPoint6<T>(pow(10.,-p));
//    for (int i=0; i<5; i++) {
//      for (int j=i+1; j<6; j++) {
//        cout <<"s"<<i<<j<< "  = " <<  S(mom[i]+mom[j]) << endl;
//        for (int k=j+1; k<6; k++) {
//          cout <<"s"<<i<<j<<k<< " = " << S(mom[i]+mom[j]+mom[k]) << endl;
//        }
//      }
//    }
    amp.setMomenta(mom);

#if DO_QBQG_R
    int amp_order[] = {0,5,4,3,2,1};
    amp.setOrder(amp_order);
#endif

    bool FACT_LOOP = true;
    int AMP_TYPE = 0;
    int FACT_AMP_TYPE = 0;
    int SPLIT_TYPE_T = 0;
    int SPLIT_TYPE_L = 0;
    (void)(FACT_LOOP); (void)(AMP_TYPE); (void)(FACT_AMP_TYPE);
    (void)(SPLIT_TYPE_T); (void)(SPLIT_TYPE_L);

#if DO_QBQG
    SPLIT_TYPE_T = Split3q<qd_real>::QBQG;
# if DO_QBQG_R
    FACT_LOOP = false;
    SPLIT_TYPE_L = Split3q<qd_real>::QBQG_R;
# elif DO_QBQG_F
    AMP_TYPE = 1;
    FACT_AMP_TYPE = 1;
    SPLIT_TYPE_L = Split3q<qd_real>::QBQG_F;
# elif DO_QBQG_L
    SPLIT_TYPE_L = Split3q<qd_real>::QBQG_L;
# endif
#elif DO_QBGQ
    SPLIT_TYPE_T = Split3q<qd_real>::QBGQ;
# if DO_QBGQ_LR
    SPLIT_TYPE_L = Split3q<qd_real>::QBGQ_LR;
# endif
#endif

    // Catani-Seymour momentum mapping 123;4
    const MOM<T> p1 = mom[0];
    const MOM<T> p2 = mom[1];
    const MOM<T> p3 = mom[2];
    const MOM<T> p123 = p1+p2+p3;
    const MOM<T> p4 = mom[3];
    const T s123 = S(p123);
    const T x123_4 = s123/T(2.)/dot(p123,p4);
    const MOM<T> p123t = p123 - x123_4*p4;
    const MOM<T> p4t = (1.+x123_4)*p4;
    fact_mom[0] = p123t;
    fact_mom[1] = p4t;
    fact_mom[2] = mom[4];
    fact_mom[3] = mom[5];

    fact_amp.setMomenta(fact_mom);
    split.setMomenta(p123,p1,p2,p3,p4);

    bool amp1nonzero = 0, amp2nonzero = 0;

    complex<T> limittree = T();
    EpsTriplet<T> limitloop;

    fact_h[0] = +1;
    split_h[0] = -1;
    fact_amp.setHelicity(fact_h);
    split.setHelicity(split_h);
    limittree += split.evalTree(SPLIT_TYPE_T)*fact_amp.evalTree();
    if (DEBUG)
      amp1nonzero = abs(split.evalTree(SPLIT_TYPE_T)*fact_amp.evalTree()) > DEBUG_TOLL;
      
#if DO_LOOP
    limitloop += split.eval(SPLIT_TYPE_L)*fact_amp.evalTree();
    if (FACT_LOOP)
      limitloop += split.evalTree(SPLIT_TYPE_T)*fact_amp.eval(FACT_AMP_TYPE);
    if (DEBUG)
      amp1nonzero = abs(split.eval(SPLIT_TYPE_L).get0()*fact_amp.evalTree()) > DEBUG_TOLL;
    if (0 && DEBUG && p == DEFAULT_P && DEBUG_HIDX == 9) {
      std::cerr << "spL(-) AT(+) = " << split.eval(SPLIT_TYPE_L)*fact_amp.evalTree() << std::endl;
      std::cerr << "spT(-) AL(+) = " << split.evalTree(SPLIT_TYPE_T)*fact_amp.eval(FACT_AMP_TYPE) << std::endl;
    }
#endif

    fact_h[0] = -1;
    split_h[0] = +1;
    fact_amp.setHelicity(fact_h);
    split.setHelicity(split_h);
    limittree += split.evalTree(SPLIT_TYPE_T)*fact_amp.evalTree();
    if (DEBUG)
      amp2nonzero = abs(split.evalTree(SPLIT_TYPE_T)*fact_amp.evalTree()) > DEBUG_TOLL;
#if DO_LOOP
    limitloop += split.eval(SPLIT_TYPE_L)*fact_amp.evalTree();
    if (FACT_LOOP)
      limitloop += split.evalTree(SPLIT_TYPE_T)*fact_amp.eval(FACT_AMP_TYPE);
    if (DEBUG)
      amp2nonzero = abs(split.eval(SPLIT_TYPE_L).get0()*fact_amp.evalTree()) > DEBUG_TOLL;
    if (0 && DEBUG && p == DEFAULT_P && DEBUG_HIDX == 9) {
      std::cerr << "spL(+) AT(-) = " << split.eval(SPLIT_TYPE_L)*fact_amp.evalTree() << std::endl;
      std::cerr << "spT(+) AL(-) = " << split.evalTree(SPLIT_TYPE_T)*fact_amp.eval(FACT_AMP_TYPE) << std::endl;
    }
#endif

    const complex<T> tree = amp.evalTree();
#if DO_LOOP
    EpsTriplet<T> loop6 = amp.eval(AMP_TYPE);
    EpsTriplet<T> loop6err = epsabs(amp.geterr());
# if DO_QBGQ_LR
    int amp_order[] = {0,5,4,3,2,1};
    amp.setOrder(amp_order);
    loop6 += amp.eval(AMP_TYPE);
    loop6err += epsabs(amp.geterr());
# endif
#endif

    cout << "## A6-tree     = " << tree << endl;
    cout << "## Sp x A4g    = " << limittree << endl;
    cout << "### tree/limit = " << chop(tree)/chop(limittree) << endl;
#if DO_LOOP
    cout << "## A6          = " << loop6 << endl;
    cout << "## A6-error    = " << loop6err << endl;
    cout << "## Sp x A4     = " << limitloop << endl;
    cout << "## loop6/limit = " << ratio(loop6, limitloop) << endl;
#endif

    if (DEBUG && p == DEFAULT_P) {
#if DO_LOOP
      complex<T> tree = loop6.get0();
      complex<T> limittree = limitloop.get0();
#endif
      if (!(abs(chop(tree)/chop(limittree)-T(1)) < DEBUG_TOLL)
          && !(abs(chop(tree))<DEBUG_TOLL && abs(chop(limittree))<DEBUG_TOLL)) {
        int i = DEBUG_HIDX;
        cerr << "### " << "helicity " << i << " : ";
        for (int k=0; k<6; k++) {
          cerr << std::showpos << h[k] << " ";
        }
        if (abs(chop(tree)/chop(limittree)+T(1)) < DEBUG_TOLL) {
          cerr << " --> Wrong sign!";
        } else {
          if (!(abs(tree) < 1e08 && abs(limittree) < DEBUG_TOLL))
            cerr << " --> Wrong result --> ratio = " << chop(tree)/chop(limittree);
        }
        {
          int sphel1, sphel2 = split.gethidx();
          split_h[0] = -1;
          split.setHelicity(split_h);
          sphel1 = split.gethidx();
          cerr << "### " << "in helicity " << DEBUG_HIDX << " : ";
          cerr << " using";
          if (amp1nonzero)
            cerr << "SpTree" << sphel1;
          if (amp2nonzero)
            cerr << ", SpTree" << sphel2;
          cerr << "\n";
        }
        cerr << endl;
      }
      if (1 && DEBUG) {
        int sphel1, sphel2;
        split_h[0] = -1;
        split.setHelicity(split_h);
        sphel1 = split.gethidx();
        split_h[0] = 1;
        split.setHelicity(split_h);
        sphel2 = split.gethidx();
        cerr << "### " << "in helicity " << DEBUG_HIDX << " : ";
        cerr << " (using";
        if (amp1nonzero)
          cerr << "SpTree" << sphel1;
        if (amp2nonzero)
          cerr << ", SpTree" << sphel2;
        cerr << ")\n";
      }
      
    }
  }

}


  template <typename T>
  std::vector<MOM<T> > getTripleCollinearPoint7(const double lambda)
  {

    StandardModel::setHmass(lambda);
    const Flavour<double> flavours[] = {
      StandardModel::G(),
      StandardModel::G(),
      StandardModel::Higgs(),
      StandardModel::G(),
      StandardModel::G()
    };
    PhaseSpace<T> ps(5, flavours, 1, 1e3);
    ps.getPSpoint();
    //  ps.showPSpoint();

    const T s123 = S(ps[2]);
    const T x1234 = s123/dot(ps[2],ps[3])*T(0.5);
    MOM<T> p123t = ps[2]-x1234*ps[3];

    cout << endl;
    cout << "### s123 = " << s123 << endl;

    // map to 2->4 configuration //
    const T z1 = 0.545;
    const T z2 = 0.424;
    const T z3 = 1.-z1-z2;

    MOM<complex<T> > e12 = CMOM(p123t,ps[3]);
    MOM<complex<T> > e21 = CMOM(ps[3],p123t);

    MOM<complex<T> > v1_c = i_*(e12-e21);
    MOM<complex<T> > v2_c = (e12+e21);

    MOM<T> v1(real(v1_c.x0), real(v1_c.x1), real(v1_c.x2), real(v1_c.x3));
    MOM<T> v2(real(v2_c.x0), real(v2_c.x1), real(v2_c.x2), real(v2_c.x3));

    T y11 = 0.124*sqrt(s123);
    T y12 = 0.456*sqrt(s123);
    T y21 = 0.294*sqrt(s123);
    T y22 = 0.835*sqrt(s123);

    T y13 = -y11-y12;
    T y23 = -y21-y22;

    MOM<T> kt1 = y11*v1 + y21*v2;
    MOM<T> kt2 = y12*v1 + y22*v2;
    MOM<T> kt3 = y13*v1 + y23*v2;

    T mshift = (S(kt1)/z1+S(kt2)/z2+S(kt3)/z3)*x1234/s123;

    std::vector<MOM<T> >  Mom(7);
    Mom[4] = ps[0];
    Mom[5] = ps[1];
    Mom[0] = z1*p123t + kt1 - S(kt1)/z1*x1234/s123*ps[3];
    Mom[1] = z2*p123t + kt2 - S(kt2)/z2*x1234/s123*ps[3];
    Mom[2] = z3*p123t + kt3 - S(kt3)/z3*x1234/s123*ps[3];
    Mom[3] = (1. + x1234 + mshift)*ps[3];
    Mom[6] = ps[4];

    MOM<T> mcons;

    return Mom;
  }



template <typename T>
void checkTripleCollinear7(const Flavour<double>* flavours, const int h[]) {
  const int nn = 7;

  std::vector<MOM<T> > mom(nn);

  NParton2<T> amp;
  amp.setProcess(nn, flavours);

  NParton2<T> fact_amp;
  const Flavour<double> fact_flavours[] = {StandardModel::G(), flavours[3], flavours[4], flavours[5], flavours[6]};
  fact_amp.setProcess(nn-2, fact_flavours);
  std::vector<MOM<T> > fact_mom(nn-2);

//  const Flavour<double> split_flavours[] = {StandardModel::G(), flavours[0], flavours[1], flavours[2]};
  Split3q<T> split;

#if 0
  const T MuR = 1e3/7.;
  amp.setMuR2(MuR*MuR);
  fact_amp.setMuR2(MuR*MuR);
  split.setMuR2(MuR*MuR);
#endif

  amp.setHelicity(h);
  int fact_h[] = {+1, h[3], h[4], h[5], h[6]};
  int split_h[] = {-1, h[0], h[1], h[2]};

  for (int p=10; p<15; p++) {
    mom = getTripleCollinearPoint7<T>(pow(10.,-p));
//    for (int i=0; i<5; i++) {
//      for (int j=i+1; j<6; j++) {
//        cout <<"s"<<i<<j<< "  = " <<  S(mom[i]+mom[j]) << endl;
//        for (int k=j+1; k<6; k++) {
//          cout <<"s"<<i<<j<<k<< " = " << S(mom[i]+mom[j]+mom[k]) << endl;
//        }
//      }
//    }
    amp.setMomenta(mom);
    const T DEBUG_TOLL = 10e-12;

    // Catani-Seymour momentum mapping 123;4
    const MOM<T> p1 = mom[0];
    const MOM<T> p2 = mom[1];
    const MOM<T> p3 = mom[2];
    const MOM<T> p123 = p1+p2+p3;
    const MOM<T> p4 = mom[3];
    const T s123 = S(p123);
    const T x123_4 = s123/T(2.)/dot(p123,p4);
    const MOM<T> p123t = p123 - x123_4*p4;
    const MOM<T> p4t = (1.+x123_4)*p4;
    fact_mom[0] = p123t;
    fact_mom[1] = p4t;
    fact_mom[2] = mom[4];
    fact_mom[3] = mom[5];
    fact_mom[4] = mom[6];

    fact_amp.setMomenta(fact_mom);
    split.setMomenta(p123,p1,p2,p3,p4);

    bool amp1nonzero = 0, amp2nonzero = 0;

    complex<T> limittree = T();
#if 0
    EpsTriplet<T> limitloopG;
    EpsTriplet<T> limitloopF;
#endif
    fact_h[0] = +1;
    split_h[0] = -1;
    fact_amp.setHelicity(fact_h);
    split.setHelicity(split_h);
    limittree += split.evalTree()*fact_amp.evalTree();
    if (DEBUG)
      amp1nonzero = abs(split.evalTree()*fact_amp.evalTree()) > DEBUG_TOLL;
      
#if 0
    limitloopG += split.eval(0)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(0);
    limitloopF += split.eval(1)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(1);
#endif
//    cout << "limitF a = " << limitloopF << endl;

    fact_h[0] = -1;
    split_h[0] = +1;
    fact_amp.setHelicity(fact_h);
    split.setHelicity(split_h);
    limittree += split.evalTree()*fact_amp.evalTree();
    if (DEBUG)
      amp2nonzero = abs(split.evalTree()*fact_amp.evalTree()) > DEBUG_TOLL;
#if 0
    limitloopG += split.eval(0)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(0);
    limitloopF += split.eval(1)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(1);
#endif
//    cout << "limitF a+b = " << limitloopF << endl;

    const complex<T> tree = amp.evalTree();
#if 0
    const EpsTriplet<T> loopG = amp.eval(0);
    const EpsTriplet<T> loopF = amp.eval(1);
#endif

    cout << "## A6g        = " << tree << endl;
    cout << "## Sp x A5g   = " << limittree << endl;
    cout << "### tree/limit = " << chop(tree)/chop(limittree) << endl;
#if 0
    // 0 = mixed / gluon primitive
    cout << "## A7[g]       = " << loopG << endl;
    cout << "## Sp x A5[g]  = " << limitloopG << endl;
    cout << "## loopG/limit = " << ratio(loopG, limitloopG) << endl;
    // 1 = fermion loop primitive
    cout << "## A7[f]       = " << loopF << endl;
    cout << "## Sp x A4[f]  = " << limitloopF << endl;
    cout << "## loopF/limit = " << ratio(loopF, limitloopF) << endl;
#endif

    if (DEBUG && p == 14) {
      if (abs(chop(tree)/chop(limittree)-T(1)) > DEBUG_TOLL
          && !(abs(chop(tree))<DEBUG_TOLL && abs(chop(limittree))<DEBUG_TOLL)) {
        int i = DEBUG_HIDX;
        cerr << "### " << "helicity " << i << " : ";
        for (int k=0; k<4; k++) {
          cerr << std::showpos << h[k] << " ";
        }
        if (abs(chop(tree)/chop(limittree)+T(1)) < DEBUG_TOLL) {
          cerr << " --> Wrong sign!";
        } else {
          cerr << " --> Wrong result --> ratio = " << chop(tree)/chop(limittree);
        }
        {
          int sphel1, sphel2 = split.gethidx();
          split_h[0] = -1;
          split.setHelicity(split_h);
          sphel1 = split.gethidx();
          cerr << " (using";
          if (amp1nonzero)
            cerr << "SpTree" << sphel1;
          if (amp2nonzero)
            cerr << ", SpTree" << sphel2;
          cerr << ")";
        }
        cerr << endl;
      }
    }
  }

}



template <typename T>
void check_SWI()
{
  const T s123 = 100;
  StandardModel::setHmass(to_double(s123));
  const Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };
  PhaseSpace<T> ps(5, flavours, 1, 1e3);
  ps.getPSpoint();

  MOM<T> p[4] = {ps[0]+ps[1], ps[2], ps[3], ps[4]};

  ps.getPSpoint();
  MOM<T> n = ps[1];

  for (int i=0; i<4; ++i)
    std::cout << p[i] << std::endl;

  Split3g<T> sp3g;
  Split3q<T> sp3q;

  sp3g.setMomenta(-p[0],p[1],p[2],p[3],n);
  sp3q.setMomenta(-p[0],p[1],p[2],p[3],n);

  const MOM<T> p123t = -(p[0] - s123/dot(p[0],n)*0.5*n);
  const complex<T> zspA1 = xspA(p[1],n)/xspA(p123t,n);
  const complex<T> zspA2 = xspA(p[2],n)/xspA(p123t,n);
  const complex<T> zspA3 = xspA(p[3],n)/xspA(p123t,n);
  const complex<T> zspB1 = xspB(p[1],n)/xspB(p123t,n);
  const complex<T> zspB2 = xspB(p[2],n)/xspB(p123t,n);
  const complex<T> zspB3 = xspB(p[3],n)/xspB(p123t,n);

  const int GG_NEQ4 = Split3g<T>::NEQ4;
  const int QQ_NEQ4 = Split3q<T>::QBQG_NEQ4;
  const int QG_NEQ4 = Split3q<T>::QBGQ_NEQ4;
  
  {
    std::cout << "==== hel 2 - bq q g ====" << std::endl;
    int hvec[] = {-1,1,-1,-1};
    sp3g.setHelicity(hvec);
    sp3q.setHelicity(hvec);
    std::cout << -sp3g.eval(GG_NEQ4)*zspB2/zspB1 << std::endl;
    std::cout << sp3q.eval(QQ_NEQ4) << std::endl;
  }

  {
    std::cout << "==== hel 4 - bq q g ====" << std::endl;
    int hvec[] = {-1,-1,1,-1};
    sp3g.setHelicity(hvec);
    sp3q.setHelicity(hvec);
    std::cout << sp3q.eval(QQ_NEQ4) << std::endl;
    std::cout << zspB1/zspB2 * sp3g.eval(GG_NEQ4);
  }

  {
    std::cout << "==== hel 11 - bq q g ====" << std::endl;
    int hvec[] = {1,1,-1,1};
    sp3g.setHelicity(hvec);
    sp3q.setHelicity(hvec);
    std::cout << sp3q.eval(QQ_NEQ4) << std::endl;
    std::cout << zspA1/zspA2 * sp3g.eval(GG_NEQ4);
  }

  {
    std::cout << "==== hel 13 - bq q g ====" << std::endl;
    int hvec[] = {1,-1,1,1};
    sp3g.setHelicity(hvec);
    sp3q.setHelicity(hvec);
    std::cout << sp3q.eval(QQ_NEQ4) << std::endl;
    std::cout << -zspA2/zspA1 * sp3g.eval(GG_NEQ4);
  }

  {
    std::cout << "==== hel 3 - bq g q ====" << std::endl;
    int hvec[] = {1,1,-1,-1};
    sp3g.setHelicity(hvec);
    sp3q.setHelicity(hvec);
    std::cout << sp3g.eval(GG_NEQ4) << std::endl;
    std::cout << zspA3/zspA1 * sp3q.eval(QG_NEQ4) + zspA2/zspA1 * sp3q.eval(QQ_NEQ4) << std::endl;
    // std::cout << zspA1/zspA3 * sp3g.eval(GG_NEQ4) -  zspA2/zspA3 * sp3q.eval(QQ_NEQ4) << std::endl;
    // std::cout << sp3q.eval(QG_NEQ4) << std::endl;
  }
  
  {
    std::cout << "==== hel 12 - bq g q ====" << std::endl;
    int hvec[] = {-1,-1,1,1};
    sp3g.setHelicity(hvec);
    sp3q.setHelicity(hvec);
    std::cout << sp3g.eval(GG_NEQ4) << std::endl;
    std::cout << zspB3/zspB1 * sp3q.eval(QG_NEQ4) + zspB2/zspB1 * sp3q.eval(QQ_NEQ4) << std::endl;
    // std::cout << zspA1/zspA3 * sp3g.eval(GG_NEQ4) -  zspA2/zspA3 * sp3q.eval(QQ_NEQ4) << std::endl;
    // std::cout << sp3q.eval(QG_NEQ4) << std::endl;
  }

  {
    std::cout << "==== hel 5 - bq g q ====" << std::endl;
    int hvec[] = {-1,1,-1,1};
    sp3g.setHelicity(hvec);
    sp3q.setHelicity(hvec);
    std::cout << sp3g.eval(GG_NEQ4) << std::endl;
    EpsTriplet<T> tmp = -zspB1/zspB2 * sp3q.eval(QQ_NEQ4);
    int order[] = {0,3,2,1};
    sp3q.setOrder(order);
    tmp += -zspB3/zspB2 * sp3q.eval(QQ_NEQ4);
    std::cout << tmp << std::endl;
    // std::cout << zspA1/zspA3 * sp3g.eval(GG_NEQ4) -  zspA2/zspA3 * sp3q.eval(QQ_NEQ4) << std::endl;
    // std::cout << sp3q.eval(QG_NEQ4) << std::endl;
  }
    
}


std::vector<int> getHelicity(const int n, const int hidx)
{
  std::vector<int> out(n);
  std::bitset<32> hhh(hidx);
  for (int i=0; i<n; i++) {
    out[i] = 2*hhh[i]-1;
  }
  return out;
}

int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

#if DO_QBQG
  const Flavour<double> flavours[] = {
    StandardModel::ubar(),
    StandardModel::u(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };
  const int qbar_idx = 0;
  const int q_idx = 1;
#elif DO_QBGQ
  const Flavour<double> flavours[] = {
    StandardModel::ubar(),
    StandardModel::G(),
    StandardModel::u(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };
  const int qbar_idx = 0;
  const int q_idx = 2;
#endif

#if 0
  for (int i=0; i<64; i++) {
    std::vector<int> hhh = getHelicity(6,i);
    cout << endl << "### " << "helicity " << i << " : ";
    for (int k=0; k<6; k++) {
      cout << std::showpos << hhh[k] << " ";
    }
    cout << endl;
    if (DEBUG)
      DEBUG_HIDX = i;
//    cout << "### limit double prec. (16) digits" << endl;
//    checkTripleCollinear<double>(flavours, hhh.data());
//    cout << "### limit quadruple prec. (32) digits" << endl;
//    checkTripleCollinear<dd_real>(flavours, hhh.data());
    cout << "### limit octuple prec. (64) digits" << endl;
    if (hhh[qbar_idx] == hhh[q_idx])
      continue;
    checkTripleCollinear<qd_real>(flavours, hhh.data());
  }
#endif

#if 1
  check_SWI<qd_real>();
#endif

#if 0
  for (int i=0; i<128; i++) {
    std::vector<int> hhh = getHelicity(7,i);
    cout << endl << "### " << "helicity " << i << " : ";
    for (int k=0; k<7; k++) {
      cout << std::showpos << hhh[k] << " ";
    }
    cout << endl;
    if (DEBUG)
      DEBUG_HIDX = i;
//    cout << "### limit double prec. (16) digits" << endl;
//    checkTripleCollinear<double>(flavours, hhh.data());
//    cout << "### limit quadruple prec. (32) digits" << endl;
//    checkTripleCollinear<dd_real>(flavours, hhh.data());
    cout << "### limit octuple prec. (64) digits" << endl;
    checkTripleCollinear7<qd_real>(flavours, hhh.data());
  }
#endif

  return 1;
}
