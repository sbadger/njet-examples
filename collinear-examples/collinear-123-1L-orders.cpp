#include "split3.h"

#include <fstream>

std::ofstream outf;


// generate triple collinear phase-space point for 2->4
template <typename T>
std::vector<MOM<T> > getTripleCollinearPoint6(const double lambda)
{

  StandardModel::setHmass(lambda);
  const Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::Higgs(),
    StandardModel::G()
  };
  PhaseSpace<T> ps(4, flavours, 1, 1e3);
  ps.getPSpoint();
//  ps.showPSpoint();

  const T s123 = S(ps[2]);
  const T x1234 = s123/dot(ps[2],ps[3])*T(0.5);
  MOM<T> p123t = ps[2]-x1234*ps[3];

  cout << "### s123 = " << s123 << endl;

  // map to 2->4 configuration //
  const T z1 = 0.345;
  const T z2 = 0.424;
  const T z3 = 1.-z1-z2;

  MOM<complex<T> > e12 = CMOM(p123t,ps[3]);
  MOM<complex<T> > e21 = CMOM(ps[3],p123t);

  MOM<complex<T> > v1_c = i_*(e12-e21);
  MOM<complex<T> > v2_c = (e12+e21);
  cout << v1_c << endl;
  cout << v2_c << endl;

  MOM<T> v1(real(v1_c.x0), real(v1_c.x1), real(v1_c.x2), real(v1_c.x3));
  MOM<T> v2(real(v2_c.x0), real(v2_c.x1), real(v2_c.x2), real(v2_c.x3));

  T y11 = 0.324*sqrt(s123);
  T y12 = 0.416*sqrt(s123);
  T y21 = 0.194*sqrt(s123);
  T y22 = 0.535*sqrt(s123);

  T y13 = -y11-y12;
  T y23 = -y21-y22;

  MOM<T> kt1 = y11*v1 + y21*v2;
  MOM<T> kt2 = y12*v1 + y22*v2;
  MOM<T> kt3 = y13*v1 + y23*v2;

  T mshift = (S(kt1)/z1+S(kt2)/z2+S(kt3)/z3)*x1234/s123;

  std::vector<MOM<T> >  Mom(6);
  Mom[4] = ps[0];
  Mom[5] = ps[1];
  Mom[0] = z1*p123t + kt1 - S(kt1)/z1*x1234/s123*ps[3];
  Mom[1] = z2*p123t + kt2 - S(kt2)/z2*x1234/s123*ps[3];
  Mom[2] = z3*p123t + kt3 - S(kt3)/z3*x1234/s123*ps[3];
  Mom[3] = (1. + x1234 + mshift)*ps[3];

  MOM<T> mcons;
  for (int i=0; i<6; i++) {
    mcons += Mom[i];
    cout << "p"<<i<<"^2 = "<< S(Mom[i]) << endl;
  }
  cout << mcons << endl;

  return Mom;
}

complex<double> chop(complex<double> a) {
  complex<double> ans(0.,0.);
  if(abs(real(a)) > 1e-8) ans.real(real(a));
  if(abs(imag(a)) > 1e-8) ans.imag(imag(a));
  return ans;
}

complex<dd_real> chop(complex<dd_real> a) {
  complex<dd_real> ans(0.,0.);
  if(abs(real(a)) > 1e-14) ans.real(real(a));
  if(abs(imag(a)) > 1e-14) ans.imag(imag(a));
  return ans;
}

complex<qd_real> chop(complex<qd_real> a) {
  complex<qd_real> ans(0.,0.);
  if(abs(real(a)) > 1e-40) ans.real(real(a));
  if(abs(imag(a)) > 1e-40) ans.imag(imag(a));
  return ans;
}

EpsTriplet<double> chop(EpsTriplet<double> a) { return EpsTriplet<double>(chop(a.get0()), chop(a.get1()), chop(a.get2())); }
EpsTriplet<dd_real> chop(EpsTriplet<dd_real> a) { return EpsTriplet<dd_real>(chop(a.get0()), chop(a.get1()), chop(a.get2())); }
EpsTriplet<qd_real> chop(EpsTriplet<qd_real> a) { return EpsTriplet<qd_real>(chop(a.get0()), chop(a.get1()), chop(a.get2())); }

template <typename T>
EpsTriplet<T> ratio(EpsTriplet<T> a1, EpsTriplet<T> a2)
{
  return EpsTriplet<T>(
    chop(a1.get0())/chop(a2.get0()),
    chop(a1.get1())/chop(a2.get1()),
    chop(a1.get2())/chop(a2.get2()));
}


const int orders6[][7] = {
// L
  {0,1,2,3,4,5,  0},
  {0,1,2,4,5,3,  1},
  {0,1,2,5,3,4,  2},
  {0,1,3,2,4,5,  3},
  {0,1,3,4,2,5,  4},
  {0,1,3,4,5,2,  5},
  {0,1,4,2,5,3,  6},
  {0,1,4,5,2,3,  7},
  {0,1,4,5,3,2,  8},
  {0,1,5,2,3,4,  9},
  {0,1,5,3,2,4, 10},
  {0,1,5,3,4,2, 11},
  {0,2,1,3,4,5, 12},
  {0,2,1,4,5,3, 13},
  {0,2,1,5,3,4, 14},
  {0,2,3,1,4,5, 15},
  {0,2,3,4,1,5, 16},
  {0,2,3,4,5,1, 17},
  {0,2,4,1,5,3, 18},
  {0,2,4,5,1,3, 19},
  {0,2,4,5,3,1, 20},
  {0,2,5,1,3,4, 21},
  {0,2,5,3,1,4, 22},
  {0,2,5,3,4,1, 23},
  {0,3,1,2,4,5, 24},
  {0,3,1,4,2,5, 25},
  {0,3,1,4,5,2, 26},
  {0,3,2,1,4,5, 27},
  {0,3,2,4,1,5, 28},
  {0,3,2,4,5,1, 29},
  {0,3,4,1,2,5, 30},
  {0,3,4,1,5,2, 31},
  {0,3,4,2,1,5, 32},
  {0,3,4,2,5,1, 33},
  {0,3,4,5,1,2, 34},
  {0,3,4,5,2,1, 35},
  {0,4,1,2,5,3, 36},
  {0,4,1,5,2,3, 37},
  {0,4,1,5,3,2, 38},
  {0,4,2,1,5,3, 39},
  {0,4,2,5,1,3, 40},
  {0,4,2,5,3,1, 41},
  {0,4,5,1,2,3, 42},
  {0,4,5,1,3,2, 43},
  {0,4,5,2,1,3, 44},
  {0,4,5,2,3,1, 45},
  {0,4,5,3,1,2, 46},
  {0,4,5,3,2,1, 47},
  {0,5,1,2,3,4, 48},
  {0,5,1,3,2,4, 49},
  {0,5,1,3,4,2, 50},
  {0,5,2,1,3,4, 51},
  {0,5,2,3,1,4, 52},
  {0,5,2,3,4,1, 53},
  {0,5,3,1,2,4, 54},
  {0,5,3,1,4,2, 55},
  {0,5,3,2,1,4, 56},
  {0,5,3,2,4,1, 57},
  {0,5,3,4,1,2, 58},
  {0,5,3,4,2,1, 59}};


template <typename T>
void checkTripleCollinear(const Flavour<double>* flavours, const int h[],
                          const int order6 [], const int split_order[], int aimin)
{
  const int nn = 6;

  std::vector<MOM<T> > mom(nn);

  NParton2<T> amp;
  amp.setProcess(nn, flavours);

  NParton2<T> fact_amp;
  const Flavour<double> fact_flavours[] = {StandardModel::G(), flavours[3], flavours[4], flavours[5]};
  fact_amp.setProcess(nn-2, fact_flavours);
  std::vector<MOM<T> > fact_mom(nn-2);

//  const Flavour<double> split_flavours[] = {StandardModel::G(), flavours[0], flavours[1], flavours[2]};
  Split3g<T> split;

  const T MuR = 1e3/7.;
  amp.setMuR2(MuR*MuR);
  fact_amp.setMuR2(MuR*MuR);
  split.setMuR2(MuR*MuR);

  amp.setHelicity(h);
  int fact_h[] = {+1, h[3], h[4], h[5]};
  int split_h[] = {-1, h[0], h[1], h[2]};

  for (int p=8; p<=10; p++) {
    mom = getTripleCollinearPoint6<T>(pow(10.,-p));
//    for (int i=0; i<5; i++) {
//      for (int j=i+1; j<6; j++) {
//        cout <<"s"<<i<<j<< "  = " <<  S(mom[i]+mom[j]) << endl;
//        for (int k=j+1; k<6; k++) {
//          cout <<"s"<<i<<j<<k<< " = " << S(mom[i]+mom[j]+mom[k]) << endl;
//        }
//      }
//    }
    amp.setMomenta(mom);
    amp.setOrder(order6);

    // Catani-Seymour momentum mapping 123;4
    const MOM<T> p1 = mom[0];
    const MOM<T> p2 = mom[1];
    const MOM<T> p3 = mom[2];
    const MOM<T> p123 = p1+p2+p3;
    const MOM<T> p4 = mom[3];
    const T s123 = S(p123);
    const T x123_4 = s123/T(2.)/dot(p123,p4);
    const MOM<T> p123t = p123 - x123_4*p4;
    const MOM<T> p4t = (1.+x123_4)*p4;
    fact_mom[0] = p123t;
    fact_mom[1] = p4t;
    fact_mom[2] = mom[4];
    fact_mom[3] = mom[5];

    fact_amp.setMomenta(fact_mom);
    split.setMomenta(p123,p1,p2,p3,p4);

    complex<T> limittree = T();
    EpsTriplet<T> limitloopG;
    EpsTriplet<T> limitloopF;

    int fact_amp_order[] = {0,order6[aimin]-2,order6[aimin+1]-2,order6[aimin+2]-2};

    outf << "orders :" << order6[0] << order6[1] << order6[2] << order6[3] << order6[4] << order6[5];
    outf << " --> " << 0 << order6[aimin]-2 << order6[aimin+1]-2 << order6[aimin+2]-2;
    outf << " x " << split_order[0] << split_order[1] << split_order[2] << split_order[3] << std::endl;
    
    fact_h[0] = +1;
    split_h[0] = -1;
    fact_amp.setHelicity(fact_h);
    split.setHelicity(split_h);
    fact_amp.setOrder(fact_amp_order);
    split.setOrder(split_order);
    limittree += split.evalTree()*fact_amp.evalTree();
    limitloopG += split.eval(0)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(0);
    limitloopF += split.eval(1)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(1);

    cout << "###limit Sp[g](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[0](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout<<") ### " << split.eval(0)*fact_amp.evalTree() << endl;

    cout << "###limit Sp[0](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[g](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout << ") ### " << split.evalTree()*fact_amp.eval(0) << endl;

    cout << "###limit Sp[f](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[0](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout<<") ### " << split.eval(1)*fact_amp.evalTree() << endl;

    cout << "###limit Sp[0](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[f](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout << ") ### " << split.evalTree()*fact_amp.eval(1) << endl;

    fact_h[0] = -1;
    split_h[0] = +1;
    fact_amp.setHelicity(fact_h);
    split.setHelicity(split_h);
    fact_amp.setOrder(fact_amp_order);
    split.setOrder(split_order);
    limittree += split.evalTree()*fact_amp.evalTree();
    limitloopG += split.eval(0)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(0);
    limitloopF += split.eval(1)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(1);

    cout << "###limit Sp[g](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[0](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout<<") ### " << split.eval(0)*fact_amp.evalTree() << endl;

    cout << "###limit Sp[0](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[g](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout << ") ### " << split.evalTree()*fact_amp.eval(0) << endl;

    cout << "###limit Sp[f](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[0](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout<<") ### " << split.eval(1)*fact_amp.evalTree() << endl;

    cout << "###limit Sp[0](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[f](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout << ") ### " << split.evalTree()*fact_amp.eval(1) << endl;

    const complex<T> tree = amp.evalTree();
    const EpsTriplet<T> loopG = amp.eval(0);
    const EpsTriplet<T> loopF = amp.eval(1);

    cout << "## A6g        = " << tree << endl;
    cout << "## Sp x A4g   = " << limittree << endl;
    cout << "### tree/limit = " << chop(tree)/chop(limittree) << endl;
    // 0 = mixed / gluon primitive
    cout << "## A6[g]       = " << loopG << endl;
    cout << "## Sp x A4[g]  = " << limitloopG << endl;
    cout << "## loopG/limit = " << ratio(loopG, limitloopG) << endl;
    outf << "## loopG/limit = " << ratio(loopG, limitloopG) << endl;
    // 1 = fermion loop primitive
    cout << "## A6[f]       = " << loopF << endl;
    cout << "## Sp x A4[f]  = " << limitloopF << endl;
    cout << "## loopF/limit = " << ratio(loopF, limitloopF) << endl;

  }

}



template <typename T>
void checkTripleCollinearSimple(const Flavour<double>* flavours, const int h[])
{
  const int nn = 6;

  std::vector<MOM<T> > mom(nn);

  NParton2<T> amp;
  amp.setProcess(nn, flavours);

  NParton2<T> fact_amp;
  const Flavour<double> fact_flavours[] = {StandardModel::G(), flavours[3], flavours[4], flavours[5]};
  fact_amp.setProcess(nn-2, fact_flavours);
  std::vector<MOM<T> > fact_mom(nn-2);

//  const Flavour<double> split_flavours[] = {StandardModel::G(), flavours[0], flavours[1], flavours[2]};
  Split3g<T> split;

  const T MuR = 1e3/7.;
  amp.setMuR2(MuR*MuR);
  fact_amp.setMuR2(MuR*MuR);
  split.setMuR2(MuR*MuR);

  amp.setHelicity(h);
  int fact_h[] = {+1, h[3], h[4], h[5]};
  int split_h[] = {-1, h[0], h[1], h[2]};

  for (int p=5; p<10; p++) {
    mom = getTripleCollinearPoint6<T>(pow(10.,-p));
//    for (int i=0; i<5; i++) {
//      for (int j=i+1; j<6; j++) {
//        cout <<"s"<<i<<j<< "  = " <<  S(mom[i]+mom[j]) << endl;
//        for (int k=j+1; k<6; k++) {
//          cout <<"s"<<i<<j<<k<< " = " << S(mom[i]+mom[j]+mom[k]) << endl;
//        }
//      }
//    }
    amp.setMomenta(mom);
    int order6[] = {0,1,3,2,4,5};
    amp.setOrder(order6);

    // Catani-Seymour momentum mapping 123;4
    const MOM<T> p1 = mom[0];
    const MOM<T> p2 = mom[1];
    const MOM<T> p3 = mom[3];
    const MOM<T> p123 = p1+p2+p3;
    const MOM<T> p4 = mom[2];
    const T s123 = S(p123);
    const T x123_4 = s123/T(2.)/dot(p123,p4);
    const MOM<T> p123t = p123 - x123_4*p4;
    const MOM<T> p4t = (1.+x123_4)*p4;
    fact_mom[0] = p123t;
    fact_mom[1] = p4t;
    fact_mom[2] = mom[4];
    fact_mom[3] = mom[5];

    fact_amp.setMomenta(fact_mom);
    split.setMomenta(p123,p1,p2,p3,p4);

    complex<T> limittree = T();
    EpsTriplet<T> limitloopG;
    EpsTriplet<T> limitloopF;

    int fact_amp_order[] = {0,1,2,3};
    
    fact_h[0] = +1;
    split_h[0] = -1;
    fact_amp.setHelicity(fact_h);
    split.setHelicity(split_h);
    limittree += split.evalTree()*fact_amp.evalTree();
    limitloopG += split.eval(0)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(0);
    limitloopF += split.eval(1)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(1);

    cout << "###limit Sp[g](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[0](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout<<") ### " << split.eval(0)*fact_amp.evalTree() << endl;

    cout << "###limit Sp[0](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[g](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout << ") ### " << split.evalTree()*fact_amp.eval(0) << endl;

    cout << "###limit Sp[f](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[0](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout<<") ### " << split.eval(1)*fact_amp.evalTree() << endl;

    cout << "###limit Sp[0](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[f](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout << ") ### " << split.evalTree()*fact_amp.eval(1) << endl;

    fact_h[0] = -1;
    split_h[0] = +1;
    fact_amp.setHelicity(fact_h);
    split.setHelicity(split_h);
    limittree += split.evalTree()*fact_amp.evalTree();
    limitloopG += split.eval(0)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(0);
    limitloopF += split.eval(1)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(1);

    cout << "###limit Sp[g](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[0](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout<<") ### " << split.eval(0)*fact_amp.evalTree() << endl;

    cout << "###limit Sp[0](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[g](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout << ") ### " << split.evalTree()*fact_amp.eval(0) << endl;

    cout << "###limit Sp[f](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[0](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout<<") ### " << split.eval(1)*fact_amp.evalTree() << endl;

    cout << "###limit Sp[0](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[f](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout << ") ### " << split.evalTree()*fact_amp.eval(1) << endl;

    const complex<T> tree = amp.evalTree();
    const EpsTriplet<T> loopG = amp.eval(0);
    const EpsTriplet<T> loopF = amp.eval(1);

    cout << "## A6g        = " << tree << endl;
    cout << "## Sp x A4g   = " << limittree << endl;
    cout << "### tree/limit = " << chop(tree)/chop(limittree) << endl;
    // 0 = mixed / gluon primitive
    cout << "## A6[g]       = " << loopG << endl;
    cout << "## Sp x A4[g]  = " << limitloopG << endl;
    cout << "## loopG/limit = " << ratio(loopG, limitloopG) << endl;
    //outf << "## loopG/limit = " << ratio(loopG, limitloopG) << endl;
    // 1 = fermion loop primitive
    cout << "## A6[f]       = " << loopF << endl;
    cout << "## Sp x A4[f]  = " << limitloopF << endl;
    cout << "## loopF/limit = " << ratio(loopF, limitloopF) << endl;

  }

}



std::vector<int> getHelicity(const int n, const int hidx)
{
  std::vector<int> out(n);
  std::bitset<32> hhh(hidx);
  for (int i=0; i<n; i++) {
    out[i] = 2*hhh[i]-1;
  }
  return out;
}


bool have_adjacent_012(const int * order, int n, int * splitorder, int & a4imin)
{
  // assuming order[0] = 0;
  if (order[0] != 0) {
    std::cerr << "Nope!"  << std::endl;
    std::terminate();
  }
  if (order[1]<3 && order[2]<3) {
    splitorder[0] = 0;
    splitorder[1] = order[0]+1;
    splitorder[2] = order[1]+1;
    splitorder[3] = order[2]+1;
    a4imin = 3;
    return true;
  }
  if (order[1]<3 && order[n-1]<3) {
    splitorder[0] = 0;
    splitorder[1] = order[n-1]+1;
    splitorder[2] = order[0]+1;
    splitorder[3] = order[1]+1;
    a4imin = 2;
    return true;
  }
  if (order[n-2]<3 && order[n-1]<3) {
    splitorder[0] = 0;
    splitorder[1] = order[n-2]+1;
    splitorder[2] = order[n-1]+1;
    splitorder[3] = order[0]+1;
    a4imin = 1;
    return true;
  }
  return false;
}


int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  const Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };

  int aimin = -1;
  int splitorder[4];

  outf.open("outorder.dat");
  outf.precision(20);

  for (unsigned oi = 0; oi < sizeof(orders6); ++oi) {

    if (have_adjacent_012(orders6[oi],6,splitorder,aimin)) {
      for (int i=0; i<2*2*2*2*2*2; i++) {
        std::vector<int> hhh = getHelicity(6,i);
        cout << endl << "##limit# " << "helicity " << i << " : ";
        for (int k=0; k<6; k++) {
          cout << std::showpos << hhh[k] << " ";
          outf << std::showpos << hhh[k] << " ";
        }
        cout << endl;
        outf << endl;
        //    cout << "### limit double prec. (16) digits" << endl;
        //    checkTripleCollinear<double>(flavours, hhh.data());
        //    cout << "### limit quadruple prec. (32) digits" << endl;
        //    checkTripleCollinear<dd_real>(flavours, hhh.data());
        cout << "### limit octuple prec. (64) digits" << endl;
        checkTripleCollinear<qd_real>(flavours, hhh.data(), orders6[oi], splitorder, aimin);
      };     
      
    }

  }

  if (false)
  for (int i=0; i<2*2*2*2*2*2; i++) {
    std::vector<int> hhh = getHelicity(6,i);
    cout << endl << "##limit# " << "helicity " << i << " : ";
    for (int k=0; k<6; k++) {
      cout << std::showpos << hhh[k] << " ";
      outf << std::showpos << hhh[k] << " ";
    }
    //checkTripleCollinearSimple<qd_real>(flavours, hhh.data());
  }

  return 1;
}
