#include "split3.h"
#include <stdexcept>

#include <fstream>

inline void myassert(bool condition, const char * msg)
{
  if (condition == false)
    throw std::runtime_error(msg);
}

#if 0
double DiLog(double arg) {
  return gsl_sf_dilog(arg);
}
complex<double> DiLog(complex<double> arg)
{
  gsl_sf_result res_re, res_im;
  gsl_sf_complex_dilog_xy_e(real(arg), imag(arg), &res_re, &res_im);
  complex<double> res(res_re.val, res_im.val);
  return res;
}

dd_real DiLog(dd_real arg)
{
  return static_cast<dd_real>(gsl_sf_dilog(to_double(arg)));
}
complex<dd_real> DiLog(complex<dd_real> arg)
{
  gsl_sf_result res_re, res_im;
  gsl_sf_complex_dilog_xy_e(to_double(real(arg)), to_double(imag(arg)), &res_re, &res_im);
  complex<dd_real> res(static_cast<dd_real>(res_re.val), static_cast<dd_real>(res_im.val));
  return res;
}

qd_real DiLog(qd_real arg)
{
  return static_cast<qd_real>(gsl_sf_dilog(to_double(arg)));
}
complex<qd_real> DiLog(complex<qd_real> arg)
{
  gsl_sf_result res_re, res_im;
  gsl_sf_complex_dilog_xy_e(to_double(real(arg)), to_double(imag(arg)), &res_re, &res_im);
  complex<qd_real> res(static_cast<qd_real>(res_re.val), static_cast<qd_real>(res_im.val));
  return res;
}
#endif // 0

template <typename T>
complex<T> MyDiLog(T x) {
  
  if (abs(x) > 1.) {
    return -MyDiLog(1./x) - Pi<T>::val()*Pi<T>::val()/T(6.) - T(0.5)*logc(-x)*logc(-x);
  }
  if (x > 0.5) {
    return -MyDiLog(T(1.)-x) + Pi<T>::val()*Pi<T>::val()/T(6.) - log(x)*log(T(1.)-x);
  }

  // Bernoulli numbers normalized by 1/(n+1)!
  const T NBern[31] = {
    T(1.), -1./T(4.), 1./T(36.), T(), -1./T(3600.), T(), 1./T(211680.), T(), -1./T(10886400.), T(), 1./T(526901760.), T(),
    -T(691.)/T(16999766784000.), T(), 1./T(1120863744000.), T(), -T(3617.)/T(181400588328960000.), T(),
    T(43867.)/T(97072790126247936000.), T(),
   -T(174611.)/T(16860010916664115200000.), T(), T(77683.)/T(324325300906011525120000.), T(),
   -T(236364091.)/T(42345603418293591736320000000.), T(), T(657931.)/T(5025632054039239458816000000.), T(),
   -T(3392780147.)/T(1098904704936220100064706560000000.), T(), T(1723168255201.)/T(23553499041027242119093102313472000000.)
  };

  complex<T> res;
  for (int k=0; k<31; k++) {
    res += NBern[k]*pow(-log(T(1.)-x),k+1);
  }
  return res;
}

//template complex<dd_real> MyDiLog(dd_real x);
template complex<qd_real> MyDiLog(qd_real x);
template complex<double> MyDiLog(double x);

template <typename T>
Split3Base<T>::Split3Base() : MuR2(4.)
{
  nc = 3;
  nf = 5;

  ord[0] = 0;
  ord[1] = 1;
  ord[2] = 2;
  ord[3] = 3;

  SpTree[0] = &Split3Base::SpTree0;
  SpTree[1] = &Split3Base::SpTree1;
  SpTree[2] = &Split3Base::SpTree2;
  SpTree[3] = &Split3Base::SpTree3;
  SpTree[4] = &Split3Base::SpTree4;
  SpTree[5] = &Split3Base::SpTree5;
  SpTree[6] = &Split3Base::SpTree6;
  SpTree[7] = &Split3Base::SpTree7;
  SpTree[8] = &Split3Base::SpTree8;
  SpTree[9] = &Split3Base::SpTree9;
  SpTree[10] = &Split3Base::SpTree10;
  SpTree[11] = &Split3Base::SpTree11;
  SpTree[12] = &Split3Base::SpTree12;
  SpTree[13] = &Split3Base::SpTree13;
  SpTree[14] = &Split3Base::SpTree14;
  SpTree[15] = &Split3Base::SpTree15;

  SpLoop[0] = &Split3Base::SpLoop0;
  SpLoop[1] = &Split3Base::SpLoop1;
  SpLoop[2] = &Split3Base::SpLoop2;
  SpLoop[3] = &Split3Base::SpLoop3;
  SpLoop[4] = &Split3Base::SpLoop4;
  SpLoop[5] = &Split3Base::SpLoop5;
  SpLoop[6] = &Split3Base::SpLoop6;
  SpLoop[7] = &Split3Base::SpLoop7;
  SpLoop[8] = &Split3Base::SpLoop8;
  SpLoop[9] = &Split3Base::SpLoop9;
  SpLoop[10] = &Split3Base::SpLoop10;
  SpLoop[11] = &Split3Base::SpLoop11;
  SpLoop[12] = &Split3Base::SpLoop12;
  SpLoop[13] = &Split3Base::SpLoop13;
  SpLoop[14] = &Split3Base::SpLoop14;
  SpLoop[15] = &Split3Base::SpLoop15;

}



template <typename T>
void Split3Base<T>::setMomenta_(const MOM<T> P123,
                                     const  MOM<T> p1, const MOM<T> p2, const MOM<T> p3,
                                     const MOM<T> n_)
{
  s123 = S(P123);
  const MOM<T> p123t = P123 - s123/dot(P123,n_)*0.5*n_;

  const MOM<T> pp[4] = {p123t,p1,p2,p3};

  z[1] = dot(p1,n_)/dot(P123,n_);
  z[2] = dot(p2,n_)/dot(P123,n_);
  z[3] = dot(p3,n_)/dot(P123,n_);

  zspA[1] = xspA(p1,n_)/xspA(p123t,n_);
  zspA[2] = xspA(p2,n_)/xspA(p123t,n_);
  zspA[3] = xspA(p3,n_)/xspA(p123t,n_);
  zspB[1] = xspB(p1,n_)/xspB(p123t,n_);
  zspB[2] = xspB(p2,n_)/xspB(p123t,n_);
  zspB[3] = xspB(p3,n_)/xspB(p123t,n_);

  wspA[1] = xspA(p1,p123t)/xspA(n_,p123t);
  wspA[2] = xspA(p2,p123t)/xspA(n_,p123t);
  wspA[3] = xspA(p3,p123t)/xspA(n_,p123t);
  wspB[1] = xspB(p1,p123t)/xspB(n_,p123t);
  wspB[2] = xspB(p2,p123t)/xspB(n_,p123t);
  wspB[3] = xspB(p3,p123t)/xspB(n_,p123t);

  for (int i=0; i<4; ++i) {
    for (int j=i+1; j<4; ++j) {
      sij(i,j) = S(pp[i]+pp[j]);
      sij(j,i) = sij(i,j);
      spA(i,j) = xspA(pp[i],pp[j]);
      spA(j,i) = -spA(i,j);
      spB(i,j) = xspB(pp[i],pp[j]);
      spB(j,i) = -spB(i,j);
    }
  }

  for (int i=1; i<4; ++i) {
    for (int j=1; j<4; ++j) {
      if (j==i)
        continue;
      int k = 6-i-j; // k in {1,2,3} | k != i,j
      r(i,j) = (spA(i,j)*zspA[k])/(spA(j,k)*zspA[i]);
      rcc(i,j) = (spB(i,j)*zspB[k])/(spB(j,k)*zspB[i]);
      rt(i,j) = (spB(i,j)*wspB[k])/(spB(j,k)*wspB[i]);
      rtcc(i,j) = (spA(i,j)*wspA[k])/(spA(j,k)*wspA[i]);
      X(i,j) = zspA[i]*spB(i,j)/spB(j,0);
      Xcc(i,j) = zspB[i]*spA(i,j)/spA(j,0);
    }
  }

}



template <typename T>
void Split3Base<T>::born_spnmatrix(const int h[], StaticMatrix< complex<T> > & spmatrix)
{
  typedef complex<T> Result_t;
    
  Result_t samps123[16];
  Result_t samps132[16];
  int order123[4] = {0,1,2,3};
  int order132[4];
  T sign;
  getCSumOrder(sign, order132);

  StaticMatrix< T > cmatrix;
  getCMatrix(cmatrix);

  const int hels[16][4] = {
    {-1, -1, -1, -1},
    {+1, -1, -1, -1},
    {-1, +1, -1, -1},
    {+1, +1, -1, -1},
    {-1, -1, +1, -1},
    {+1, -1, +1, -1},
    {-1, +1, +1, -1},
    {+1, +1, +1, -1},
    {-1, -1, -1, +1},
    {+1, -1, -1, +1},
    {-1, +1, -1, +1},
    {+1, +1, -1, +1},
    {-1, -1, +1, +1},
    {+1, -1, +1, +1},
    {-1, +1, +1, +1},
    {+1, +1, +1, +1}
  };

  spmatrix(0,0) = spmatrix(0,1) = spmatrix(1,0) = spmatrix(1,1) = T();

  setOrder(order123);
  for (int i=0; i<16; ++i) {
    setHelicity(hels[i]);
    //myassert(hidx == i, "Error: Index mismatch!");
    setOrder(order123);
    samps123[hidx] = evalTree();
    setOrder(order132);
    samps132[hidx] = sign*evalTree();
  }

  // loop over spmatrix entries
  for (int sphidx0 = -1; sphidx0 <= 1; sphidx0 += 2)
    for (int sphidx1 = -1; sphidx1 <= 1; sphidx1 += 2)
      {
        int matidx0 = (sphidx0+1) / 2;
        int matidx1 = (sphidx1+1) / 2;
        
        // loop over the other helicities
        if (!h) {
          for (int h1 = -1; h1 <= 1; h1 += 2)
            for (int h2 = -1; h2 <= 1; h2 += 2)
              for (int h3 = -1; h3 <= 1; h3 += 2) {

                int sphel0[] = {sphidx0,h1,h2,h3};
                int sphel1[] = {sphidx1,h1,h2,h3};

                setOrder(order123);
                setHelicity(sphel0);
                Result_t sp123 = samps123[hidx];

                setOrder(order132);
                Result_t sp132 = samps132[hidx];

                setHelicity(sphel1);
                setOrder(order123);
                Result_t spc123 = std::conj(samps123[hidx]);

                setOrder(order132);
                Result_t spc132 = std::conj(samps132[hidx]);
                
                spmatrix(matidx0, matidx1) += cmatrix(0,0)*sp123*spc123
                  + cmatrix(0,1)*sp132*spc123 + cmatrix(1,0)*sp123*spc132
                  + cmatrix(1,1)*sp132*spc132;
              }
        } else {
          int h1 = h[0], h2 = h[1], h3 = h[2];
            
          int sphel0[] = {sphidx0,h1,h2,h3};
          int sphel1[] = {sphidx1,h1,h2,h3};

          setOrder(order123);
          setHelicity(sphel0);
          Result_t sp123 = samps123[hidx];

          setOrder(order132);
          Result_t sp132 = samps132[hidx];

          setHelicity(sphel1);
          setOrder(order123);
          Result_t spc123 = std::conj(samps123[hidx]);

          setOrder(order132);
          Result_t spc132 = std::conj(samps132[hidx]);
                
          spmatrix(matidx0, matidx1) += cmatrix(0,0)*sp123*spc123
            + cmatrix(0,1)*sp132*spc123 + cmatrix(1,0)*sp123*spc132
            + cmatrix(1,1)*sp132*spc132;            
        }
      }
}


template <typename T>
void Split3g<T>::virt_spnmatrix(const int h[], StaticMatrix< EpsTriplet<T> > & spmatrix)
{
  typedef EpsTriplet<T> Result_t;
  typedef complex<T> TreeResult_t;
    
  TreeResult_t sampst123[16];
  TreeResult_t sampst213[16];
  Result_t sampsl123g[16];
  Result_t sampsl213g[16];
  Result_t sampsl231g[16];
  Result_t sampsl123f[16];
  Result_t sampsl213f[16];
  Result_t sampsl231f[16];
  int ordert123[4] = {0,1,2,3};
  int ordert213[4] = {0,1,3,2};
  int orderl123[4] = {0,1,2,3};
  int orderl213[4] = {0,2,1,3};
  //int orderl231[4] = {0,1,3,2};
  int orderl231[4] = {0,2,3,1};
  T sign = 1;
  //getCSumOrder(sign, order132);

  StaticMatrix<T,6,2> cmatrix
    = {{
      // gluons
      2, 0,
      -2, -2,
      0, 2,
      // fermions
      -2, 0,
      2, 2,
      0, -2
    }};
  //getCMatrix(cmatrix);

  const int hels[16][4] = {
    {-1, -1, -1, -1},
    {+1, -1, -1, -1},
    {-1, +1, -1, -1},
    {+1, +1, -1, -1},
    {-1, -1, +1, -1},
    {+1, -1, +1, -1},
    {-1, +1, +1, -1},
    {+1, +1, +1, -1},
    {-1, -1, -1, +1},
    {+1, -1, -1, +1},
    {-1, +1, -1, +1},
    {+1, +1, -1, +1},
    {-1, -1, +1, +1},
    {+1, -1, +1, +1},
    {-1, +1, +1, +1},
    {+1, +1, +1, +1}
  };

  spmatrix.fill(EpsTriplet<T>());

  // eval tree
  Base::setOrder(ordert123);
  for (int i=0; i<16; ++i) {
    Base::setHelicity(hels[i]);
    Base::setOrder(ordert123);
    sampst123[hidx] = Base::evalTree();
    Base::setOrder(ordert213);
    sampst213[hidx] = sign*Base::evalTree();
  }

  // eval loop
  Base::setOrder(orderl123);
  for (int i=0; i<16; ++i) {
    Base::setHelicity(hels[i]);
    Base::setOrder(orderl123);
    sampsl123g[hidx] = eval(0);
    sampsl123f[hidx] = (nf/nc)*eval(1);
    Base::setOrder(orderl213);
    sampsl213g[hidx] = eval(0);
    sampsl213f[hidx] = (nf/nc)*eval(1);
    Base::setOrder(orderl231);
    sampsl231g[hidx] = eval(0);
    sampsl231f[hidx] = (nf/nc)*eval(1);
  }

  // loop over spmatrix entries
  for (int sphidx0 = -1; sphidx0 <= 1; sphidx0 += 2)
    for (int sphidx1 = -1; sphidx1 <= 1; sphidx1 += 2)
      {
        int matidx0 = (sphidx0+1) / 2;
        int matidx1 = (sphidx1+1) / 2;

        TreeResult_t sptc[2];
        Result_t spl[6];

        if (!h) {
          // loop over the other helicities
          for (int h1 = -1; h1 <= 1; h1 += 2)
            for (int h2 = -1; h2 <= 1; h2 += 2)
              for (int h3 = -1; h3 <= 1; h3 += 2) {

                int sphel0[] = {sphidx0,h1,h2,h3};
                int sphel1[] = {sphidx1,h1,h2,h3};

                Base::setOrder(orderl123);
                Base::setHelicity(sphel0);
                spl[0] = sampsl123g[hidx];
                spl[3] = sampsl123f[hidx];

                Base::setOrder(orderl213);
                spl[1] = sampsl213g[hidx];
                spl[1+3] = sampsl213f[hidx];

                Base::setOrder(orderl231);
                spl[2] = sampsl231g[hidx];
                spl[2+3] = sampsl231f[hidx];

                Base::setHelicity(sphel1);
                Base::setOrder(ordert123);
                sptc[0] = std::conj(sampst123[hidx]);

                Base::setOrder(ordert213);
                sptc[1] = std::conj(sampst213[hidx]);

                for (int i=0; i<6; ++i)
                  for (int j=0; j<2; ++j) {
                    spmatrix(matidx0, matidx1) += T(2)*(nc*nc*nc)*sptc[j]*cmatrix(i,j)*spl[i];
                    //spmatrix(matidx1, matidx0) += (nc*nc*nc)*(sptc[j]*cmatrix(i,j)*spl[i]).conjugate();
                  }
              }
        } else {
          int h1 = h[0], h2 = h[1], h3 = h[2];
          int sphel0[] = {sphidx0,h1,h2,h3};
          int sphel1[] = {sphidx1,h1,h2,h3};

          Base::setOrder(orderl123);
          Base::setHelicity(sphel0);
          spl[0] = sampsl123g[hidx];
          spl[3] = sampsl123f[hidx];

          Base::setOrder(orderl213);
          spl[1] = sampsl213g[hidx];
          spl[1+3] = sampsl213f[hidx];

          Base::setOrder(orderl231);
          spl[2] = sampsl231g[hidx];
          spl[2+3] = sampsl231f[hidx];

          Base::setHelicity(sphel1);
          Base::setOrder(ordert123);
          sptc[0] = std::conj(sampst123[hidx]);

          Base::setOrder(ordert213);
          sptc[1] = std::conj(sampst213[hidx]);

          for (int i=0; i<6; ++i)
            for (int j=0; j<2; ++j) {
              spmatrix(matidx0, matidx1) += T(2)*(nc*nc*nc)*sptc[j]*cmatrix(i,j)*spl[i];
              //spmatrix(matidx1, matidx0) += (nc*nc*nc)*(sptc[j]*cmatrix(i,j)*spl[i]).conjugate();
            }
        }
      }
}


template <typename T>
void Split3q<T>::virt_spnmatrix(const int h[], StaticMatrix< EpsTriplet<T> > & spmatrix)
{
  typedef EpsTriplet<T> Result_t;
  typedef complex<T> TreeResult_t;
    
  TreeResult_t sampst[16][3];
  Result_t sampsl[16][3];
  
  const T v = nc*nc - 1;
  StaticMatrix<T,3,3> cmatrix
    = {{
      v, -1, nc,
      -1, v, nc,
      nc, nc, nc*nc
    }};
  //getCMatrix(cmatrix);

  const int hels[16][4] = {
    {-1, -1, -1, -1},
    {+1, -1, -1, -1},
    {-1, +1, -1, -1},
    {+1, +1, -1, -1},
    {-1, -1, +1, -1},
    {+1, -1, +1, -1},
    {-1, +1, +1, -1},
    {+1, +1, +1, -1},
    {-1, -1, -1, +1},
    {+1, -1, -1, +1},
    {-1, +1, -1, +1},
    {+1, +1, -1, +1},
    {-1, -1, +1, +1},
    {+1, -1, +1, +1},
    {-1, +1, +1, +1},
    {+1, +1, +1, +1}
  };

  spmatrix.fill(EpsTriplet<T>());

  for (int i=0; i<16; ++i) {
    Base::setHelicity(hels[i]);

    Base::setOrder(0,1,2,3);
    sampst[i][0] = Base::evalTree();
    Base::setOrder(0,2,1,3);
    sampst[i][1] = -Base::evalTree();
    sampst[i][2] = T();

    Base::setOrder(0,1,2,3);
    const Result_t P1234 = eval(QBQG_L);
    Base::setOrder(0,2,1,3);
    const Result_t P1243 = -eval(QBQG_L);
    Base::setOrder(0,1,2,3);
    const Result_t P1432 = eval(QBQG_R);
    Base::setOrder(0,2,1,3);
    const Result_t P1342 = -eval(QBQG_R);
    Base::setOrder(0,1,3,2);
    const Result_t P1324p1423 = eval(QBGQ_LR);
    sampsl[i][0] = nc*P1234-P1432/nc;
    sampsl[i][1] = nc*P1243-P1342/nc;
    sampsl[i][2] = P1234+P1243+P1324p1423+P1342+P1432;
    Base::setOrder(0,1,2,3);
    if (nf != 0) {
      const Result_t Q1234 = eval(QBQG_F);
      sampsl[i][0] += nf*(-Q1234);
      Base::setOrder(0,2,1,3);
      const Result_t Q1243 = eval(QBQG_F);
      sampsl[i][1] += nf*(Q1243);
      Base::setOrder(0,1,2,3);
    }
  }

  // loop over spmatrix entries
  for (int sphidx0 = -1; sphidx0 <= 1; sphidx0 += 2)
    for (int sphidx1 = -1; sphidx1 <= 1; sphidx1 += 2)
      {
        int matidx0 = (sphidx0+1) / 2;
        int matidx1 = (sphidx1+1) / 2;

        TreeResult_t sptc[3] = {T(), T(), T()};
        Result_t spl[3];

        if (!h) {
          // loop over the other helicities
          for (int h1 = -1; h1 <= 1; h1 += 2)
            for (int h2 = -1; h2 <= 1; h2 += 2)
              for (int h3 = -1; h3 <= 1; h3 += 2) {

                int sphel0[] = {sphidx0,h1,h2,h3};
                int sphel1[] = {sphidx1,h1,h2,h3};

                Base::setHelicity(sphel0);
                spl[0] = sampsl[hidx][0];
                spl[1] = sampsl[hidx][1];
                spl[2] = sampsl[hidx][2];

                Base::setHelicity(sphel1);
                sptc[0] = std::conj(sampst[hidx][0]);
                sptc[1] = std::conj(sampst[hidx][1]);

                for (int i=0; i<3; ++i)
                  for (int j=0; j<3; ++j) {
                    spmatrix(matidx0, matidx1) += (T(1)/T(v))*T(2)*v/nc*sptc[j]*cmatrix(i,j)*spl[i];
                  }
              }
        } else {
          int h1 = h[0], h2 = h[1], h3 = h[2];
          int sphel0[] = {sphidx0,h1,h2,h3};
          int sphel1[] = {sphidx1,h1,h2,h3};

          Base::setHelicity(sphel0);
          spl[0] = sampsl[hidx][0];
          spl[1] = sampsl[hidx][1];
          spl[2] = sampsl[hidx][2];

          Base::setHelicity(sphel1);
          sptc[0] = std::conj(sampst[hidx][0]);
          sptc[1] = std::conj(sampst[hidx][1]);

          for (int i=0; i<3; ++i)
            for (int j=0; j<3; ++j) {
              spmatrix(matidx0, matidx1) += (T(1)/T(v))*T(2)*v/nc*sptc[j]*cmatrix(i,j)*spl[i];
            }
        }
      }
}


// log and polylog functions //
template <typename T>
complex<T> Split3Base<T>::mLog(T s)
{
  T val_re = log(abs(MuR2/s));
  T val_im = s > 0. ? T(Pi<T>::val()) : 0.;
  return complex<T>(val_re, val_im);
}

template <typename T>
complex<T> Split3Base<T>::rLog(T s1, T s2)
{
  T val_re = log(abs(s1/s2));
  T val_im = s2 > 0. ? T(Pi<T>::val()) : 0;
  val_im -= s1 > 0. ? T(Pi<T>::val()) : 0;
  return complex<T>(val_re, val_im);
}

template <typename T>
complex<T> Split3Base<T>::rDiLog(T s1, T s2) {
  T arg = T(1.)-s1/s2;
  T val_re = real(MyDiLog(arg));
  //      complex<T> arg(1.-s1/s2,T());
  //      T val_re = real(DiLog(arg));
  T val_im = -s1/s2 > 0. ? log(1.-s1/s2)*imag(rLog(s1,s2)) : T();
  return complex<T>(val_re, val_im);
}

template <typename T> complex<T> Split3Base<T>::L0hat(T s1, T s2) {
  return rLog(s1,s2);
}
template <typename T> complex<T> Split3Base<T>::L1hat(T s1, T s2) {
  return rLog(s1,s2)/(s1-s2);
}
template <typename T> complex<T> Split3Base<T>::L2hat(T s1, T s2) {
  return rLog(s1,s2)/pow(s1-s2,2) - T(0.5)/(s1-s2)*(1/s1+1/s2);
}
template <typename T> complex<T> Split3Base<T>::L3hat(T s1, T s2) {
  return rLog(s1,s2)/pow(s1-s2,3) - T(0.5)/((s1-s2)*(s1-s2))*(1/s1+1/s2);
}

template <typename T>
EpsTriplet<T> Split3Base<T>::pSS(T s)
{
  complex<T> lll = mLog(s);
  return EpsTriplet<T>(T(0.5)*lll*lll, lll, T(1.));
}

template <typename T>
EpsTriplet<T> Split3Base<T>::pSS1(T s)
{
  complex<T> lll = mLog(s);
  return EpsTriplet<T>(lll, T(1.),T());
}

template <typename T>
EpsTriplet<T> Split3Base<T>::pole(const int * o)
{
  complex<T> l123 = mLog(s123);
  T lz1 = log(z[o[1]]);
  T lz3 = log(z[o[3]]);
  T lz1z3 = lz1+lz3;

  EpsTriplet<T> pZZ(T(0.5)*(-T(2.)*l123*lz1z3 + lz1*lz1 + lz3*lz3), -lz1z3, T());
  return ( - pSS(sij(o[1],o[2])) - pSS(sij(o[2],o[3])) - pZZ);
}

template <typename T>
EpsTriplet<T> Split3Base<T>::poleR(const int * o)
{
  EpsTriplet<T> finite(-(T(7)/T(2)),T(),T());
  return (finite - pSS(sij(o[1],o[2])) - T(3)/T(2)*pSS1(sij(o[1],o[2])) );
}

template <typename T>
EpsTriplet<T> Split3Base<T>::poleR2(const int * o)
{
  EpsTriplet<T> finite(-(T(7)/T(2)),T(),T());
  return (finite - pSS(sij(o[1],o[2])) - pSS(sij(o[2],o[3]))
          - T(3)/T(2)*pSS1(sij(o[1],o[2])) );
}

template <typename T>
EpsTriplet<T> Split3Base<T>::poleF(const int * o)
{
  EpsTriplet<T> finite(-(T(10)/T(9)),T(),T());
  return finite - (T(2)*pSS1(sij(o[1],o[2])))/T(3);
}

template <typename T>
EpsTriplet<T> Split3Base<T>::poleX2(int type, const int * o)
{
  switch (type) {
  case 1:
    {
      EpsTriplet<T> finite((T(10)/T(9)),T(),T());
      return finite + (T(2)*pSS1(sij(o[1],o[3])))/T(3);
      //EpsTriplet<T> finite((T(3)/T(2)),T(),T());
      //return finite + pSS1(sij(o[1],o[3]))/T(3)  + pSS1(s123)/T(3);
    }
  case 2:
    {
      EpsTriplet<T> finite((T(1)/T(9)),T(),T());
      return finite + T(2)*pSS1(s123)/T(3);
    }
  case 3:
    {
      EpsTriplet<T> finite((T(29)/T(18)),T(),T());
      return finite + T(2)*pSS1(s123)/T(3);
    }
  }
  return EpsTriplet<T>();
}

template <typename T>
complex<T> Split3Base<T>::FMHV(const int * o) {
  return (
          T(0.5)*(Pi<T>::val()*Pi<T>::val()/T(3.) + pow(log(z[o[1]]),2) + pow(log(z[o[3]]),2))
          - rLog(sij(o[1],o[2]),s123)*rLog(sij(o[2],o[3]),s123)
          + rLog(sij(o[1],o[2]),s123)*log((1.-z[o[3]])/z[o[1]])
          + rLog(sij(o[2],o[3]),s123)*log((1.-z[o[1]])/z[o[3]])
          + MyDiLog(-z[o[2]]/z[o[3]])
          + MyDiLog(-z[o[2]]/z[o[1]])
          + MyDiLog(-z[o[1]]/(1.-z[o[1]]))
          + MyDiLog(-z[o[3]]/(1.-z[o[3]]))
          - rDiLog(sij(o[1],o[2]),s123*(1-z[o[3]]))
          - rDiLog(sij(o[2],o[3]),s123*(1-z[o[1]]))
          );
}

template <typename T>
complex<T> Split3Base<T>::FNMHV1(const int * o) {
  return (
          T(0.5)*(Pi<T>::val()*Pi<T>::val()/T(3.) - log(z[o[1]])*rLog(sij(o[2],o[3]),s123) - log(z[o[3]])*rLog(sij(o[1],o[2]),s123))
          - log(1-z[o[3]])*log(z[o[1]]*z[o[3]]/(1-z[o[3]]))
          + log(z[o[1]]*z[o[3]])*rLog(sij(o[1],o[2]),s123)
          - log(1-z[o[3]])*rLog(sij(o[1],o[2]),sij(o[2],o[3]))
          );
}

template <typename T>
complex<T> Split3Base<T>::FNMHV2(const int * o) {
#if 0
  return (
          T(0.5)*(Pi<T>::val()*Pi<T>::val()/T(3.) - log(z[o[1]])*rLog(sij(o[2],o[3]),s123) - log(z[o[3]])*rLog(sij(o[1],o[2]),s123))
          - log(1-z[o[1]])*log(z[o[1]]*z[o[3]]/(1-z[o[1]]))
          + log(z[o[1]]*z[o[3]])*rLog(sij(o[2],o[3]),s123)
          - log(1-z[o[1]])*rLog(sij(o[2],o[3]),sij(o[1],o[2]))
          );
#else
  const int order[] = {o[0],o[3],o[2],o[1]};
  return FNMHV1(order);
#endif
}

template <typename T>
complex<T> Split3Base<T>::FNMHV3(const int * o) {
  return (
          - T(0.5)*(Pi<T>::val()*Pi<T>::val()/T(3.) - log(z[o[1]])*rLog(sij(o[2],o[3]),s123) - log(z[o[3]])*rLog(sij(o[1],o[2]),s123))
          - rLog(sij(o[1],o[2]),s123)*rLog(sij(o[2],o[3]),s123)
          );
}

template <typename T>
complex<T> Split3Base<T>::F1mBox(const int * o) {
  return -2.*(
      Pi<T>::val()*Pi<T>::val()/T(6.) + T(0.5)*pow(rLog(sij(o[1],o[2]),sij(o[2],o[3])),2)
        + rDiLog(s123,sij(o[1],o[2]))
        + rDiLog(s123,sij(o[2],o[3]))
      );
}



//{{{ trees
template <typename T>
complex<T> Split3g<T>::SpTree0(int, const int * o)/* {-1, -1, -1, -1} */
{
  return T();
}
template <typename T>
complex<T> Split3g<T>::SpTree1(int, const int * o)/* {1, -1, -1, -1} */
{
  return T(1)/(zspB[o[1]]*zspB[o[3]]*spB(o[1],o[2])*spB(o[2],o[3]));
}
template <typename T>
complex<T> Split3g<T>::SpTree2(int, const int * o)/* {-1, 1, -1, -1} */
{
  // = Sp8(3,2,1)
  const int order[4] = {o[0], o[3], o[2], o[1]};
  return Split3g::SpTree8(0,order);
}
template <typename T>
complex<T> Split3g<T>::SpTree3(int, const int * o)/* {1, 1, -1, -1} */
{
  return - pow(spB(o[1],o[0]),2)/pow(spB(o[2],o[3]),2) * (rt(o[3],o[2])/s123  + pow(X(o[2],o[3]),3)*r(o[1],o[2])*pow(rt(o[1],o[2]),2)*sij(o[2],o[3])/((1-z[o[3]])*z[o[3]]*pow(sij(o[1],o[2]),2)));
}
template <typename T>
complex<T> Split3g<T>::SpTree4(int, const int * o)/* {-1, -1, 1, -1} */
{
  // = conj(Sp11)
  return pow(zspB[o[2]],4)/(zspB[o[1]]*zspB[o[3]]*spB(o[1],o[2])*spB(o[2],o[3]));
}
template <typename T>
complex<T> Split3g<T>::SpTree5(int, const int * o)/* {1, -1, 1, -1} */
{
  return -pow(spB(o[2],o[0]),2)/(pow(spB(o[1],o[3]),2)*sij(o[0],o[1])*sij(o[0],o[3])) * (rt(o[2],o[1])*rt(o[2],o[3])*pow(sij(o[1],o[3]),4)/(sij(o[1],o[2])*s123*sij(o[2],o[3])) + pow(X(o[3],o[2]),2)*z[o[2]]*z[o[3]]*sij(o[0],o[1])*rcc(o[1],o[3])*rcc(o[3],o[1])/(X(o[2],o[3])*(1-z[o[3]])) + pow(X(o[1],o[2]),2)*z[o[1]]*z[o[2]]*sij(o[0],o[3])*rcc(o[1],o[3])*rcc(o[3],o[1])/(X(o[2],o[1])*(1-z[o[1]]))); //d
}
template <typename T>
complex<T> Split3g<T>::SpTree6(int, const int * o)/* {-1, 1, 1, -1} */
{
  // = Sp12(3,2,1)
  const int order[4] = {o[0], o[3], o[2], o[1]};
  return Split3g::SpTree12(0,order);
}
template <typename T>
complex<T> Split3g<T>::SpTree7(int, const int * o)/* {1, 1, 1, -1} */
{
  return pow(zspA[o[3]],3)/(zspA[o[1]]*spA(o[1],o[2])*spA(o[2],o[3]));
}
template <typename T>
complex<T> Split3g<T>::SpTree8(int, const int * o)/* {-1, -1, -1, 1} */
{
  // = conj(Sp7)
  return pow(zspB[o[3]],3)/(zspB[o[1]]*spB(o[1],o[2])*spB(o[2],o[3]));
}
template <typename T>
complex<T> Split3g<T>::SpTree9(int, const int * o)/* {1, -1, -1, 1} */
{
  // = Sp3(3,2,1)
  const int order[4] = {o[0], o[3], o[2], o[1]};
  return Split3g::SpTree3(0,order);
}
template <typename T>
complex<T> Split3g<T>::SpTree10(int, const int * o)/* {-1, 1, -1, 1} */
{
  // = conj(Sp5)
  return -pow(spA(o[2],o[0]),2)/(pow(spA(o[1],o[3]),2)*sij(o[0],o[1])*sij(o[0],o[3])) * (rtcc(o[2],o[1])*rtcc(o[2],o[3])*pow(sij(o[1],o[3]),4)/(sij(o[1],o[2])*s123*sij(o[2],o[3])) + pow(Xcc(o[3],o[2]),2)*z[o[2]]*z[o[3]]*sij(o[0],o[1])*r(o[1],o[3])*r(o[3],o[1])/(Xcc(o[2],o[3])*(1-z[o[3]])) + pow(Xcc(o[1],o[2]),2)*z[o[1]]*z[o[2]]*sij(o[0],o[3])*r(o[1],o[3])*r(o[3],o[1])/(Xcc(o[2],o[1])*(1-z[o[1]])));
}
template <typename T>
complex<T> Split3g<T>::SpTree11(int, const int * o)/* {1, 1, -1, 1} */
{
  return pow(zspA[o[2]],4)/(zspA[o[1]]*zspA[o[3]]*spA(o[1],o[2])*spA(o[2],o[3]));
}
template <typename T>
complex<T> Split3g<T>::SpTree12(int, const int * o)/* {-1, -1, 1, 1} */
{
  // = conj(Sp3)
  return - pow(spA(o[1],o[0]),2)/pow(spA(o[2],o[3]),2) * (rtcc(o[3],o[2])/s123  + pow(Xcc(o[2],o[3]),3)*rcc(o[1],o[2])*pow(rtcc(o[1],o[2]),2)*sij(o[2],o[3])/((1-z[o[3]])*z[o[3]]*pow(sij(o[1],o[2]),2)));
}
template <typename T>
complex<T> Split3g<T>::SpTree13(int, const int * o)/* {1, -1, 1, 1} */
{
  // = Sp7(3,2,1)
  const int order[4] = {o[0], o[3], o[2], o[1]};
  return Split3g::SpTree7(0,order);
}
template <typename T>
complex<T> Split3g<T>::SpTree14(int, const int * o)/* {-1, 1, 1, 1} */
{
  // = conj(Sp1)
  return T(1)/(zspA[o[1]]*zspA[o[3]]*spA(o[1],o[2])*spA(o[2],o[3]));
}
template <typename T>
complex<T> Split3g<T>::SpTree15(int, const int * o)/* {1, 1, 1, 1} */
{
  return T();
}
//}}}


//{{{ loop primitives
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop0(int flav, const int * o)/* {-1, -1, -1, -1} */
{
  // conj(Sp15)
  if (flav == NEQ0) {
    return -(spA(o[1],o[0]) * spA(o[3],o[0]))/( T(3) * spB(o[1],o[2]) * spB(o[2],o[3]) )*(( T(1))/(s123)-(pow(Xcc(o[2],o[3]),2) * rtcc(o[1],o[3]) * (rcc(o[3],o[2]) * sij(o[1],o[2])+rcc(o[1],o[2]) * sij(o[2],o[3])))/(rtcc(o[3],o[1]) * sij(o[1],o[2]) * sij(o[2],o[3])));
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop1(int flav, const int * o)/* {1, -1, -1, -1} */
{
  if (flav == NEQ4) {
    return Split3g::SpTree1(0,o) * (pole(o) + FMHV(o));
  } else if (flav == NEQ0) {
    return T(1)/T(3) * Split3g::SpTree1(0,o) * (z[o[1]] * z[o[2]]+(z[o[1]] * ( T(1)-pow(z[o[2]],2)) * z[o[3]] * z[o[2]])/(( T(1)-z[o[1]])*( T(1)-z[o[2]])*( T(1)-z[o[3]]))+z[o[3]] * z[o[2]]+z[o[1]] * z[o[3]] -(z[o[1]] * z[o[3]])/(X(o[1],o[2]) * X(o[3],o[2]))*((X(o[3],o[2]) * z[o[1]])/( T(1)-z[o[3]])+(X(o[1],o[2]) * z[o[3]])/( T(1)-z[o[1]])+(sij(o[1],o[3]))/(s123)));
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop2(int flav, const int * o)/* {-1, 1, -1, -1} */
{
  // = Sp8(3,2,1)
  const int order[4] = {o[0], o[3], o[2], o[1]};
  return SpLoop8(flav,order);
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop3(int flav, const int * o)/* {1, 1, -1, -1} */
{
  if (flav == NEQ4) {
    return Split3g::SpTree3(0,o)*pole(o)+(-pow(spB(o[1],o[0]),2))/(pow(spB(o[2],o[3]),2))*(  + ( (X(o[2],o[3]) * r(o[1],o[2]) * rt(o[1],o[2]) * sij(o[2],o[3]))/(z[o[3]] * pow(sij(o[1],o[2]),2))*((sij(o[1],o[2]) * pow(z[o[1]]- T(1),3))/(z[o[2]] * sij(o[1],o[0]))+( T(2) * pow(X(o[2],o[3]),2) * rt(o[1],o[2]))/(z[o[3]]- T(1))) +( T(1))/(s123)*((rt(o[1],o[2]) * pow(sij(o[2],o[3]),3))/(sij(o[1],o[2]) * sij(o[1],o[0]) * sij(o[3],o[0]))-rt(o[3],o[2])) ) * FNMHV1(o)  -(X(o[2],o[3]) * r(o[1],o[2]) * rt(o[1],o[2]) * sij(o[2],o[3]) * pow(z[o[1]]- T(1),3))/(z[o[2]] * z[o[3]] * sij(o[1],o[2]) * sij(o[1],o[0])) * FNMHV2(o)  +( T(1))/(s123)*((rt(o[1],o[2]) * pow(sij(o[2],o[3]),3))/(sij(o[1],o[2]) * sij(o[1],o[0]) * sij(o[3],o[0]))+rt(o[3],o[2])) * FNMHV3(o) );
  } else if (flav == NEQ1) {
    return -(pow(spB(o[1],o[0]),2) * sij(o[2],o[3]))/(pow(spB(o[2],o[3]),2) * s123) * L1hat(sij(o[1],o[2]),s123);
  } else if (flav == NEQ0) {
    return  -(pow(spB(o[1],o[0]),2))/( T(3) * pow(spB(o[2],o[3]),2))*( ( T(2) * pow(X(o[2],o[3]),2) * pow(r(o[2],o[1]),2) * pow(sij(o[1],o[3]),2) * sij(o[2],o[3]) * pow(rt(o[1],o[2]),2))/(sij(o[1],o[2])) * L3hat(sij(o[1],o[2]),s123) +(pow(X(o[2],o[3]),2) * r(o[2],o[1]) * sij(o[1],o[3]) * (-sij(o[1],o[2])-r(o[2],o[1]) * sij(o[1],o[3])) * sij(o[2],o[3]) * pow(rt(o[1],o[2]),2))/(sij(o[1],o[2]) * s123) * L2hat(sij(o[1],o[2]),s123)  +(pow(X(o[2],o[3]),2)*( T(3) * r(o[2],o[1]) * sij(o[1],o[3])-sij(o[1],o[2])) * sij(o[2],o[3]) * pow(rt(o[1],o[2]),2))/(sij(o[1],o[2]) * s123) * L1hat(sij(o[1],o[2]),s123) -(pow(X(o[2],o[3]),2) * r(o[1],o[2]) * sij(o[2],o[3]) * pow(rt(o[1],o[2]),2))/(pow(sij(o[1],o[2]),2)) -(pow(X(o[2],o[3]),2) * r(o[2],o[1]) * sij(o[1],o[3]) * sij(o[2],o[3]) * pow(rt(o[1],o[2]),2))/(pow(sij(o[1],o[2]),2) * s123)  +( T(1))/(pow(s123,2))*((X(o[2],o[3]) * r(o[2],o[1]) * pow(rt(o[1],o[2]),2) * rt(o[3],o[2]) * sij(o[1],o[3]) * sij(o[2],o[3]))/( T(2) * sij(o[1],o[2]))-(pow(X(o[2],o[3]),2) * r(o[2],o[1]) * pow(rt(o[1],o[2]),2) * sij(o[1],o[3]) * sij(o[2],o[3]))/(sij(o[1],o[2]))) );
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop4(int flav, const int * o)/* {-1, -1, 1, -1} */
{
  // = conj(Sp11)
  if (flav == NEQ4) {
    return Split3g::SpTree4(0,o) * (pole(o) + FMHV(o));
  } else if (flav == NEQ1) {
    return Split3g::SpTree4(0,o) * (rcc(o[2],o[3]))/(rcc(o[3],o[1]))*((F1mBox(o))/( T(2))-(L1hat(sij(o[1],o[2]),s123)+L1hat(sij(o[2],o[3]),s123)) * sij(o[1],o[3]) );
  } else if (flav == NEQ0) {
    return Split3g::SpTree4(0,o) * (pow(rcc(o[2],o[3]),2) * sij(o[1],o[3]))/( T(3) * pow(rcc(o[3],o[1]),2))*( -( T(2) * L3hat(sij(o[1],o[2]),s123) * pow(sij(o[1],o[3]),2))/(rcc(o[3],o[2])) - T(2) * rcc(o[3],o[2]) * L3hat(sij(o[2],o[3]),s123) * pow(sij(o[1],o[3]),2) - T(3) * L2hat(sij(o[2],o[3]),s123)*( T(2) * sij(o[1],o[2])+ T(3) * sij(o[1],o[3])) +( T(5))/( T(2))*(( T(2))/(s123)+( T(1))/(sij(o[2],o[3]))+( T(1))/(sij(o[1],o[2]))) - T(3) * L2hat(sij(o[1],o[2]),s123)*( T(3) * sij(o[1],o[3])+ T(2) * sij(o[2],o[3])) -( T(1))/( T(2) * s123)*((sij(o[1],o[2]))/(sij(o[2],o[3]))+(sij(o[2],o[3]))/(sij(o[1],o[2]))) +(s123)/(sij(o[1],o[2]) * sij(o[2],o[3])) -( T(3) * F1mBox(o))/(sij(o[1],o[3])) );
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop5(int flav, const int * o)/* {1, -1, 1, -1} */
{
  if (flav == NEQ4) {
    return Split3g::SpTree5(0,o) * pole(o) + (-pow(spB(o[2],o[0]),2))/(pow(spB(o[1],o[3]),2))*( ((X(o[3],o[2]) * rt(o[3],o[1]) * sij(o[1],o[3]) * pow(X(o[1],o[2]),2))/(X(o[2],o[3]) * z[o[3]] * sij(o[1],o[2]) * sij(o[3],o[0])) +(z[o[2]] * z[o[3]] * (rcc(o[1],o[3]) * rcc(o[3],o[1])) * pow(X(o[3],o[2]),2))/(X(o[2],o[3]) * sij(o[3],o[0]) * (z[o[3]]- T(1))) +(pow(X(o[1],o[2]),2) * z[o[2]] * pow(rt(o[3],o[1]),2) * pow(X(o[3],o[2]),2))/(pow(X(o[2],o[3]),2) * z[o[3]] * sij(o[3],o[0]) * (z[o[3]]- T(1))) ) * FNMHV1(o)  + ((z[o[1]] * (rcc(o[1],o[3]) * rcc(o[3],o[1])) * pow(X(o[1],o[2]),2))/(sij(o[1],o[0])) + (X(o[1],o[2]) * rt(o[1],o[3]) * rt(o[3],o[1]) * pow(X(o[3],o[2]),3))/(X(o[2],o[3]) * z[o[1]] * sij(o[2],o[3]) * (z[o[1]]- T(1))) + (X(o[1],o[2]) * pow(z[o[1]],2) * (rcc(o[1],o[3]) * rcc(o[3],o[1])) * X(o[3],o[2]))/(sij(o[1],o[0]) * (z[o[1]]- T(1))) ) * FNMHV2(o) + ( T(1))/(s123)*((rt(o[2],o[1]) * rt(o[2],o[3]) * pow(sij(o[1],o[3]),4))/(sij(o[1],o[2]) * sij(o[2],o[3]) * sij(o[1],o[0]) * sij(o[3],o[0]))+rt(o[1],o[3]) * rt(o[3],o[1])) * FNMHV3(o) );
  } else if (flav == NEQ1) {
    return -(pow(spB(o[2],o[0]),2))/(pow(spB(o[1],o[3]),2) * s123)*( (F1mBox(o))/( T(2))-(L1hat(sij(o[1],o[2]),s123)+L1hat(sij(o[2],o[3]),s123)) * sij(o[1],o[3]));
  } else if (flav == NEQ0) {
    return (-pow(spB(o[2],o[0]),2))/( T(3) * pow(spB(o[1],o[3]),2))*( - T(2) * L3hat(sij(o[2],o[3]),s123) * s123 * sij(o[1],o[3]) * pow(X(o[1],o[2]),2) +L2hat(sij(o[2],o[3]),s123)*( T(3) * X(o[1],o[2]) * s123+sij(o[1],o[3])) * X(o[1],o[2])  - T(2) * pow(X(o[3],o[2]),2) * L3hat(sij(o[1],o[2]),s123) * s123 * sij(o[1],o[3]) +( T(3) * ( T(2) * sij(o[1],o[2])+ T(4) * X(o[1],o[2]) * s123+sij(o[1],o[3])))/(s123 * sij(o[1],o[3])) * L0hat(sij(o[2],o[3]),s123)  +X(o[3],o[2]) * L2hat(sij(o[1],o[2]),s123)*( T(3) * X(o[3],o[2]) * s123+sij(o[1],o[3])) + L1hat(sij(o[2],o[3]),s123)*((sij(o[1],o[3]))/(s123)-( T(6) * pow(X(o[1],o[2]),2) * s123)/(sij(o[1],o[3])))  +L1hat(sij(o[1],o[2]),s123)*((sij(o[1],o[3]))/(s123)-( T(6) * pow(X(o[3],o[2]),2) * s123)/(sij(o[1],o[3]))) +( T(3) * ( T(4) * X(o[3],o[2]) * s123+sij(o[1],o[3])+ T(2) * sij(o[2],o[3])))/(s123 * sij(o[1],o[3])) * L0hat(sij(o[1],o[2]),s123)  +( T(1))/( T(2) * sij(o[1],o[2]) * s123 * sij(o[2],o[3]))*( - T(4) * s123 * sij(o[2],o[3]) * pow(X(o[3],o[2]),2) +(X(o[1],o[2]) * s123 * (- T(3) * sij(o[1],o[2])+ T(2) * s123- T(3) * sij(o[2],o[3]))+( T(5) * sij(o[1],o[2])-sij(o[2],o[3])) * sij(o[2],o[3])) * X(o[3],o[2])  -X(o[1],o[2]) * sij(o[1],o[2]) * (sij(o[1],o[2])+ T(4) * X(o[1],o[2]) * s123- T(5) * sij(o[2],o[3])) ) -( T(3) * rt(o[2],o[1]) * rt(o[2],o[3]))/(s123) * F1mBox(o) );
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop6(int flav, const int * o)/* {-1, 1, 1, -1} */
{
  // = Sp12(3,2,1)
  const int order[4] = {o[0], o[3], o[2], o[1]};
  return SpLoop12(flav,order);
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop7(int flav, const int * o)/* {1, 1, 1, -1} */
{
  if (flav == NEQ4) {
    return Split3g::SpTree7(0,o) * (pole(o) + FMHV(o));
  } else if (flav == NEQ1) {
    return Split3g::SpTree7(0,o) * r(o[3],o[2]) * sij(o[1],o[2]) * L1hat(sij(o[2],o[3]),s123);
  } else if (flav == NEQ0) {
    return Split3g::SpTree7(0,o) * (X(o[1],o[2]) * sij(o[2],o[3])) / ( T(3) * pow(X(o[3],o[2]),3) )*( (X(o[3],o[2]) * z[o[2]])/(sij(o[1],o[2]))*((X(o[1],o[2]))/(z[o[3]])+(X(o[3],o[2]))/(z[o[3]]- T(1))) -X(o[1],o[2]) * X(o[3],o[2]) * L2hat(sij(o[2],o[3]),s123) * sij(o[2],o[3]) + T(2) * X(o[1],o[2]) * rt(o[2],o[3]) * L3hat(sij(o[2],o[3]),s123) * sij(o[1],o[3]) * sij(o[2],o[3])  +( T(1))/(s123)*((sij(o[2],o[3]))/(sij(o[1],o[2]))-( T(1))/( T(2))*(X(o[1],o[2])+ T(2)) * X(o[3],o[2])) );
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop8(int flav, const int * o)/* {-1, -1, -1, 1} */
{
  // = conj(Sp7)
  if (flav == NEQ4) {
    return Split3g::SpTree8(0,o) * (pole(o) + FMHV(o));
  } else if (flav == NEQ1) {
    return Split3g::SpTree8(0,o) * rcc(o[3],o[2]) * sij(o[1],o[2]) * L1hat(sij(o[2],o[3]),s123);
  } else if (flav == NEQ0) {
    return Split3g::SpTree8(0,o) * (Xcc(o[1],o[2]) * sij(o[2],o[3])) / ( T(3) * pow(Xcc(o[3],o[2]),3) )*( (Xcc(o[3],o[2]) * z[o[2]])/(sij(o[1],o[2]))*((Xcc(o[1],o[2]))/(z[o[3]])+(Xcc(o[3],o[2]))/(z[o[3]]- T(1))) -Xcc(o[1],o[2]) * Xcc(o[3],o[2]) * L2hat(sij(o[2],o[3]),s123) * sij(o[2],o[3]) + T(2) * Xcc(o[1],o[2]) * rtcc(o[2],o[3]) * L3hat(sij(o[2],o[3]),s123) * sij(o[1],o[3]) * sij(o[2],o[3])  +( T(1))/(s123)*((sij(o[2],o[3]))/(sij(o[1],o[2]))-( T(1))/( T(2))*(Xcc(o[1],o[2])+ T(2)) * Xcc(o[3],o[2])) );
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop9(int flav, const int * o)/* {1, -1, -1, 1} */
{
  // = Sp3(3,2,1)
  const int order[4] = {o[0], o[3], o[2], o[1]};
  return SpLoop3(flav,order);
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop10(int flav, const int * o)/* {-1, 1, -1, 1} */
{
  // = conj(Sp5)
  if (flav == NEQ4) {
    return Split3g::SpTree10(0,o) * pole(o) + (-pow(spA(o[2],o[0]),2))/(pow(spA(o[1],o[3]),2))*( ((Xcc(o[3],o[2]) * rtcc(o[3],o[1]) * sij(o[1],o[3]) * pow(Xcc(o[1],o[2]),2))/(Xcc(o[2],o[3]) * z[o[3]] * sij(o[1],o[2]) * sij(o[3],o[0])) +(z[o[2]] * z[o[3]] * (r(o[1],o[3]) * r(o[3],o[1])) * pow(Xcc(o[3],o[2]),2))/(Xcc(o[2],o[3]) * sij(o[3],o[0]) * (z[o[3]]- T(1))) +(pow(Xcc(o[1],o[2]),2) * z[o[2]] * pow(rtcc(o[3],o[1]),2) * pow(Xcc(o[3],o[2]),2))/(pow(Xcc(o[2],o[3]),2) * z[o[3]] * sij(o[3],o[0]) * (z[o[3]]- T(1))) ) * FNMHV1(o)  + ((z[o[1]] * (r(o[1],o[3]) * r(o[3],o[1])) * pow(Xcc(o[1],o[2]),2))/(sij(o[1],o[0])) + (Xcc(o[1],o[2]) * rtcc(o[1],o[3]) * rtcc(o[3],o[1]) * pow(Xcc(o[3],o[2]),3))/(Xcc(o[2],o[3]) * z[o[1]] * sij(o[2],o[3]) * (z[o[1]]- T(1))) + (Xcc(o[1],o[2]) * pow(z[o[1]],2) * (r(o[1],o[3]) * r(o[3],o[1])) * Xcc(o[3],o[2]))/(sij(o[1],o[0]) * (z[o[1]]- T(1))) ) * FNMHV2(o) + ( T(1))/(s123)*((rtcc(o[2],o[1]) * rtcc(o[2],o[3]) * pow(sij(o[1],o[3]),4))/(sij(o[1],o[2]) * sij(o[2],o[3]) * sij(o[1],o[0]) * sij(o[3],o[0]))+rtcc(o[1],o[3]) * rtcc(o[3],o[1])) * FNMHV3(o) );
  } else if (flav == NEQ1) {
    return -(pow(spA(o[2],o[0]),2))/(pow(spA(o[1],o[3]),2) * s123)*( (F1mBox(o))/( T(2))-(L1hat(sij(o[1],o[2]),s123)+L1hat(sij(o[2],o[3]),s123)) * sij(o[1],o[3]));
  } else if (flav == NEQ0) {
    return (-pow(spA(o[2],o[0]),2))/( T(3) * pow(spA(o[1],o[3]),2))*( - T(2) * L3hat(sij(o[2],o[3]),s123) * s123 * sij(o[1],o[3]) * pow(Xcc(o[1],o[2]),2) +L2hat(sij(o[2],o[3]),s123)*( T(3) * Xcc(o[1],o[2]) * s123+sij(o[1],o[3])) * Xcc(o[1],o[2])  - T(2) * pow(Xcc(o[3],o[2]),2) * L3hat(sij(o[1],o[2]),s123) * s123 * sij(o[1],o[3]) +( T(3) * ( T(2) * sij(o[1],o[2])+ T(4) * Xcc(o[1],o[2]) * s123+sij(o[1],o[3])))/(s123 * sij(o[1],o[3])) * L0hat(sij(o[2],o[3]),s123)  +Xcc(o[3],o[2]) * L2hat(sij(o[1],o[2]),s123)*( T(3) * Xcc(o[3],o[2]) * s123+sij(o[1],o[3])) + L1hat(sij(o[2],o[3]),s123)*((sij(o[1],o[3]))/(s123)-( T(6) * pow(Xcc(o[1],o[2]),2) * s123)/(sij(o[1],o[3])))  +L1hat(sij(o[1],o[2]),s123)*((sij(o[1],o[3]))/(s123)-( T(6) * pow(Xcc(o[3],o[2]),2) * s123)/(sij(o[1],o[3]))) +( T(3) * ( T(4) * Xcc(o[3],o[2]) * s123+sij(o[1],o[3])+ T(2) * sij(o[2],o[3])))/(s123 * sij(o[1],o[3])) * L0hat(sij(o[1],o[2]),s123)  +( T(1))/( T(2) * sij(o[1],o[2]) * s123 * sij(o[2],o[3]))*( - T(4) * s123 * sij(o[2],o[3]) * pow(Xcc(o[3],o[2]),2) +(Xcc(o[1],o[2]) * s123 * (- T(3) * sij(o[1],o[2])+ T(2) * s123- T(3) * sij(o[2],o[3]))+( T(5) * sij(o[1],o[2])-sij(o[2],o[3])) * sij(o[2],o[3])) * Xcc(o[3],o[2])  -Xcc(o[1],o[2]) * sij(o[1],o[2]) * (sij(o[1],o[2])+ T(4) * Xcc(o[1],o[2]) * s123- T(5) * sij(o[2],o[3])) ) -( T(3) * rtcc(o[2],o[1]) * rtcc(o[2],o[3]))/(s123) * F1mBox(o) );
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop11(int flav, const int * o)/* {1, 1, -1, 1} */
{
  if (flav == NEQ4) {
    return Split3g::SpTree11(0,o) * (pole(o) + FMHV(o));
  } else if (flav == NEQ1) {
    return Split3g::SpTree11(0,o) * (r(o[2],o[3]))/(r(o[3],o[1]))*((F1mBox(o))/( T(2))-(L1hat(sij(o[1],o[2]),s123)+L1hat(sij(o[2],o[3]),s123)) * sij(o[1],o[3]) );
  } else if (flav == NEQ0) {
    return Split3g::SpTree11(0,o) * (pow(r(o[2],o[3]),2) * sij(o[1],o[3]))/( T(3) * pow(r(o[3],o[1]),2))*( -( T(2) * L3hat(sij(o[1],o[2]),s123) * pow(sij(o[1],o[3]),2))/(r(o[3],o[2])) - T(2) * r(o[3],o[2]) * L3hat(sij(o[2],o[3]),s123) * pow(sij(o[1],o[3]),2) - T(3) * L2hat(sij(o[2],o[3]),s123)*( T(2) * sij(o[1],o[2])+ T(3) * sij(o[1],o[3])) +( T(5))/( T(2))*(( T(2))/(s123)+( T(1))/(sij(o[2],o[3]))+( T(1))/(sij(o[1],o[2]))) - T(3) * L2hat(sij(o[1],o[2]),s123)*( T(3) * sij(o[1],o[3])+ T(2) * sij(o[2],o[3])) -( T(1))/( T(2) * s123)*((sij(o[1],o[2]))/(sij(o[2],o[3]))+(sij(o[2],o[3]))/(sij(o[1],o[2]))) +(s123)/(sij(o[1],o[2]) * sij(o[2],o[3])) -( T(3) * F1mBox(o))/(sij(o[1],o[3])) );
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop12(int flav, const int * o)/* {-1, -1, 1, 1} */
{
  // = conj(Sp3)
  if (flav == NEQ4) {
    return Split3g::SpTree12(0,o)*pole(o)+(-pow(spA(o[1],o[0]),2))/(pow(spA(o[2],o[3]),2))*(  + ( (Xcc(o[2],o[3]) * rcc(o[1],o[2]) * rtcc(o[1],o[2]) * sij(o[2],o[3]))/(z[o[3]] * pow(sij(o[1],o[2]),2))*((sij(o[1],o[2]) * pow(z[o[1]]- T(1),3))/(z[o[2]] * sij(o[1],o[0]))+( T(2) * pow(Xcc(o[2],o[3]),2) * rtcc(o[1],o[2]))/(z[o[3]]- T(1))) +( T(1))/(s123)*((rtcc(o[1],o[2]) * pow(sij(o[2],o[3]),3))/(sij(o[1],o[2]) * sij(o[1],o[0]) * sij(o[3],o[0]))-rtcc(o[3],o[2])) ) * FNMHV1(o)  -(Xcc(o[2],o[3]) * rcc(o[1],o[2]) * rtcc(o[1],o[2]) * sij(o[2],o[3]) * pow(z[o[1]]- T(1),3))/(z[o[2]] * z[o[3]] * sij(o[1],o[2]) * sij(o[1],o[0])) * FNMHV2(o)  +( T(1))/(s123)*((rtcc(o[1],o[2]) * pow(sij(o[2],o[3]),3))/(sij(o[1],o[2]) * sij(o[1],o[0]) * sij(o[3],o[0]))+rtcc(o[3],o[2])) * FNMHV3(o) );
  } else if (flav == NEQ1) {
    return -(pow(spA(o[1],o[0]),2) * sij(o[2],o[3]))/(pow(spA(o[2],o[3]),2) * s123) * L1hat(sij(o[1],o[2]),s123);
  } else if (flav == NEQ0) {
    return  -(pow(spA(o[1],o[0]),2))/( T(3) * pow(spA(o[2],o[3]),2))*( ( T(2) * pow(Xcc(o[2],o[3]),2) * pow(rcc(o[2],o[1]),2) * pow(sij(o[1],o[3]),2) * sij(o[2],o[3]) * pow(rtcc(o[1],o[2]),2))/(sij(o[1],o[2])) * L3hat(sij(o[1],o[2]),s123) +(pow(Xcc(o[2],o[3]),2) * rcc(o[2],o[1]) * sij(o[1],o[3]) * (-sij(o[1],o[2])-rcc(o[2],o[1]) * sij(o[1],o[3])) * sij(o[2],o[3]) * pow(rtcc(o[1],o[2]),2))/(sij(o[1],o[2]) * s123) * L2hat(sij(o[1],o[2]),s123)  +(pow(Xcc(o[2],o[3]),2)*( T(3) * rcc(o[2],o[1]) * sij(o[1],o[3])-sij(o[1],o[2])) * sij(o[2],o[3]) * pow(rtcc(o[1],o[2]),2))/(sij(o[1],o[2]) * s123) * L1hat(sij(o[1],o[2]),s123) -(pow(Xcc(o[2],o[3]),2) * rcc(o[1],o[2]) * sij(o[2],o[3]) * pow(rtcc(o[1],o[2]),2))/(pow(sij(o[1],o[2]),2)) -(pow(Xcc(o[2],o[3]),2) * rcc(o[2],o[1]) * sij(o[1],o[3]) * sij(o[2],o[3]) * pow(rtcc(o[1],o[2]),2))/(pow(sij(o[1],o[2]),2) * s123)  +( T(1))/(pow(s123,2))*((Xcc(o[2],o[3]) * rcc(o[2],o[1]) * pow(rtcc(o[1],o[2]),2) * rtcc(o[3],o[2]) * sij(o[1],o[3]) * sij(o[2],o[3]))/( T(2) * sij(o[1],o[2]))-(pow(Xcc(o[2],o[3]),2) * rcc(o[2],o[1]) * pow(rtcc(o[1],o[2]),2) * sij(o[1],o[3]) * sij(o[2],o[3]))/(sij(o[1],o[2]))) );
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop13(int flav, const int * o)/* {1, -1, 1, 1} */
{
  // = Sp7(3,2,1)
  const int order[4] = {o[0], o[3], o[2], o[1]};
  return SpLoop7(flav,order);
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop14(int flav, const int * o)/* {-1, 1, 1, 1} */
{
  // = conj(Sp1)
  if (flav == NEQ4) {
    return Split3g::SpTree14(0,o) * (pole(o) + FMHV(o));
  } else if (flav == NEQ0) {
    return T(1)/T(3) * Split3g::SpTree14(0,o) * (z[o[1]] * z[o[2]]+(z[o[1]] * ( T(1)-pow(z[o[2]],2)) * z[o[3]] * z[o[2]])/(( T(1)-z[o[1]])*( T(1)-z[o[2]])*( T(1)-z[o[3]]))+z[o[3]] * z[o[2]]+z[o[1]] * z[o[3]] -(z[o[1]] * z[o[3]])/(Xcc(o[1],o[2]) * Xcc(o[3],o[2]))*((Xcc(o[3],o[2]) * z[o[1]])/( T(1)-z[o[3]])+(Xcc(o[1],o[2]) * z[o[3]])/( T(1)-z[o[1]])+(sij(o[1],o[3]))/(s123)));
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3g<T>::SpLoop15(int flav, const int * o)/* {1, 1, 1, 1} */
{
  if (flav == NEQ0) {
    return -(spB(o[1],o[0]) * spB(o[3],o[0]))/( T(3) * spA(o[1],o[2]) * spA(o[2],o[3]) )*(( T(1))/(s123)-(pow(X(o[2],o[3]),2) * rt(o[1],o[3]) * (r(o[3],o[2]) * sij(o[1],o[2])+r(o[1],o[2]) * sij(o[2],o[3])))/(rt(o[3],o[1]) * sij(o[1],o[2]) * sij(o[2],o[3])));
  }
  return EpsTriplet<T>();
}
//}}}






//{{{ trees
template <typename T>
complex<T> Split3q<T>::SpTree0(int, const int * o)/* {-1, -1, -1, -1} */
{
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree1(int, const int * o)/* {1, -1, -1, -1} */
{
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree2(int flav, const int * o)/* {-1, 1, -1, -1} */
{
  if (flav == QBQG) {
    // sign
    return -((pow(zspB[o[1]],2)*zspB[o[2]])/(spB(o[1],o[2])*spB(o[2],o[3])*zspB[o[3]]));
  } else if (flav == QBGQ) {
    // sign
    return -pow(zspB[o[1]],2)/(spB(o[1],o[2])*spB(o[2],o[3]));
  }
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree3(int flav, const int * o)/* {1, 1, -1, -1} */
{
  if (flav == QBQG) {
    // sign
    return -((sij(o[1],o[2])/(s123*X(o[3],o[2])*X(o[2],o[1])) + X(o[2],o[1])/((T(1) - z[o[3]])*z[o[3]]))*zspA[o[2]]*zspA[o[3]])/(spA(o[1],o[2])*spB(o[2],o[3])*rt(o[1],o[2]));
  } else if (flav == QBGQ) {
    // sign
    return (pow(spB(o[1],o[0]),2)/(s123*spB(o[1],o[2])*spB(o[2],o[3])));
  }
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree4(int flav, const int * o)/* {-1, -1, 1, -1} */
{
  if (flav == QBQG) {
    return (pow (zspB[o[2]],3)/(spB(o[1],o[2])*spB(o[2],o[3])*zspB[o[3]]));
  }
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree5(int flav, const int * o)/* {1, -1, 1, -1} */
{
  if (flav == QBQG) {
    // sign
    return -(((sij(o[1],o[3])*(rtcc(o[3],o[1])*Xcc(o[3],o[1])))/(s123*z[o[3]]) + (sij(o[2],o[3])*(pow(rcc(o[1],o[3]),2)*rtcc(o[1],o[3]))*pow(z[o[1]],2))/(sij(o[1],o[3])*z[o[3]]) + (sij(o[1],o[2])*(pow(rcc(o[3],o[1]),2)*rtcc(o[3],o[1]))*pow(z[o[3]],2))/(sij(o[1],o[3])*(T(1) - z[o[3]])))*zspA[o[3]])/(spA(o[2],o[0])*spB(o[1],o[3]));
  }
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree6(int flav, const int * o)/* {-1, 1, 1, -1} */
{
  if (flav == QBGQ) {
    // sign
    return -pow(spA(o[3],o[0]),2)/(s123*spA(o[1],o[2])*spA(o[2],o[3]));
  }
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree7(int flav, const int * o)/* {1, 1, 1, -1} */
{
  if (flav == QBGQ) {
    // sign
    return (pow(zspA[o[3]],2)/(spA(o[1],o[2])*spA(o[2],o[3])));
  }
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree8(int flav, const int * o)/* {-1, -1, -1, 1} */
{
  if (flav == QBGQ) {
    return pow(zspB[o[3]],2)/(spB(o[1],o[2])*spB(o[2],o[3]));
  }
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree9(int flav, const int * o)/* {1, -1, -1, 1} */
{
  if (flav == QBGQ) {
    return -(pow(spB(o[3],o[0]),2)/(s123*spB(o[1],o[2])*spB(o[2],o[3])));
  }
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree10(int flav, const int * o)/* {-1, 1, -1, 1} */
{
  if (flav == QBQG) {
    // sign
    return -((((sij(o[1],o[3])*(rt(o[3],o[1])*X(o[3],o[1])))/(s123*z[o[3]]) + (sij(o[2],o[3])*(pow(r(o[1],o[3]),2)*rt(o[1],o[3]))*pow(z[o[1]],2))/(sij(o[1],o[3])*z[o[3]]) + (sij(o[1],o[2])*(pow(r(o[3],o[1]),2)*rt(o[3],o[1]))*pow(z[o[3]],2))/(sij(o[1],o[3])*(T(1) - z[o[3]])))*zspB[o[3]])/(spB(o[2],o[0])*spA(o[1],o[3])));
  }
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree11(int flav, const int * o)/* {1, 1, -1, 1} */
{
  if (flav == QBQG) {
    // sign
    return (pow(zspA[o[2]],3)/(spA(o[1],o[2])*spA(o[2],o[3])*zspA[o[3]]));
  }
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree12(int flav, const int * o)/* {-1, -1, 1, 1} */
{
  if (flav == QBQG) {
    // sign
    return -(((sij(o[1],o[2])/(s123*Xcc(o[3],o[2])*Xcc(o[2],o[1])) + Xcc(o[2],o[1])/((1 - z[o[3]])*z[o[3]]))*zspB[o[2]]*zspB[o[3]])/(spB(o[1],o[2])*spA(o[2],o[3])*rtcc(o[1],o[2])));
  } else if (flav == QBGQ) {
    return pow(spA(o[1],o[0]),2)/(s123*spA(o[1],o[2])*spA(o[2],o[3]));
  }
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree13(int flav, const int * o)/* {1, -1, 1, 1} */
{
  if (flav == QBQG) {
    return -((pow(zspA[o[1]],2)*zspA[o[2]])/(spA(o[1],o[2])*spA(o[2],o[3])*zspA[o[3]]));
  } else if (flav == QBGQ) {
    return -(pow(zspA[o[1]],2)/(spA(o[1],o[2])*spA(o[2],o[3])));
  }
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree14(int, const int * o)/* {-1, 1, 1, 1} */
{
  return T();
}
template <typename T>
complex<T> Split3q<T>::SpTree15(int, const int * o)/* {1, 1, 1, 1} */
{
  return T();
}
//}}}


//{{{ loop primitives
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop0(int flav, const int * o)/* {-1, -1, -1, -1} */
{
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop1(int flav, const int * o)/* {1, -1, -1, -1} */
{
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop2(int flav, const int * o)/* {-1, 1, -1, -1} */
{
  if (flav == QBQG_R) {
    return Split3q::SpTree2(QBQG,o)*(poleR(o) + (-T(1)) * (rcc(o[1],o[2])*(-F1mBox(o)/T(2) - (T(3)*L0hat(sij(o[1],o[2]),s123))/T(2) - (-sij(o[1],o[2]) + s123)/(T(2)*s123) + T(2)*sij(o[2],o[3])*(L1hat(sij(o[1],o[2]),s123) - 1/(T(2)*s123))*rcc(o[1],o[2]) - (pow(sij(o[2],o[3]),2)*(L2hat(sij(o[1],o[2]),s123) - 1/(T(2)*sij(o[1],o[2])*s123))*pow(rcc(o[1],o[2]),2))/T(2)))/rcc(o[1],o[3]));
  } else if (flav == QBQG_F) {
    // sign
    return -Split3q::SpTree2(QBQG,o)*(poleF(o) + (-T(1)) * (rcc(o[1],o[2])*(T(2)*L1hat(sij(o[1],o[2]),s123)*sij(o[1],o[3]) - (L0hat(sij(o[1],o[2]),s123)*sij(o[2],o[3]))/(sij(o[1],o[2]) - s123) + sij(o[2],o[3])/s123 + T(3)*L2hat(sij(o[1],o[2]),s123)*pow(sij(o[2],o[3]),2)*rcc(o[1],o[2]) - (pow(sij(o[2],o[3]),2)*rcc(o[1],o[2]))/(T(2)*sij(o[1],o[2])*s123) - T(2)*L3hat(sij(o[1],o[2]),s123)*pow(sij(o[2],o[3]),3)*pow(rcc(o[1],o[2]),2)))/(T(3)*rcc(o[1],o[3])));
  } else if (flav == QBQG_NEQ4) {
    return (pole(o) + FMHV(o))*Split3q::SpTree2(QBQG,o);
  } else if (flav == QBQG_X) {
    return -T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[2],o[3])*Split3q::SpTree2(QBQG,o)*rcc(o[1],o[2]);
  } else if (flav == QBGQ_NEQ4) {
    return (pole(o) + FMHV(o))*Split3q::SpTree2(QBGQ,o);
  } else if (flav == QBGQ_X) {
    return Split3q::SpTree2(QBGQ,o)*(poleX2(3,o) + (-L0hat(sij(o[1],o[2]),s123)/T(2) + T(4)*L1hat(sij(o[1],o[2]),s123)*sij(o[2],o[3])*rcc(o[1],o[2]) + (pow(sij(o[1],o[3]),2) + T(2)*pow(sij(o[1],o[2]),2)*pow(Xcc(o[1],o[3]),2) - T(2)*pow(s123,2)*pow(Xcc(o[1],o[3]),2))/(T(4)*sij(o[1],o[2])*s123*pow(Xcc(o[1],o[3]),2)) + L2hat(sij(o[1],o[2]),s123)*(pow(sij(o[1],o[3]),2)/(T(2)*pow(Xcc(o[1],o[3]),2)) - (pow(sij(o[1],o[2]),2)*pow(Xcc(o[3],o[2]),2))/(T(2)*pow(Xcc(o[1],o[2]),2))) + (sij(o[1],o[2])*(-T(2) + pow(Xcc(o[3],o[2]),2)))/(T(4)*s123*pow(Xcc(o[1],o[2]),2)) + ((sij(o[1],o[2]) + s123)*Xcc(o[3],o[2]))/(s123*Xcc(o[1],o[2]))));
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop3(int flav, const int * o)/* {1, 1, -1, -1} */
{
  if (flav == QBQG_R) {
    // finite sign
    return (poleR(o))*Split3q::SpTree3(QBQG,o) +(-T(1)) * (r(o[2],o[1])*((T(1)/T(2)) + sij(o[2],o[3])/(T(4)*sij(o[1],o[2])) - (L2hat(sij(o[1],o[2]),s123)*sij(o[2],o[3])*s123)/T(2) + (L1hat(sij(o[1],o[2]),s123)*(-T(3)*s123 - (T(2)*sij(o[2],o[3]))/X(o[3],o[2])))/T(2) - (F1mBox(o)*sij(o[1],o[2]))/(T(2)*s123*r(o[2],o[1])*X(o[3],o[1])*X(o[2],o[1])))*zspA[o[2]]*zspA[o[3]])/(spA(o[1],o[2])*spB(o[2],o[3]));
  } else if (flav == QBQG_F) {
    // pole sign
    return -(poleF(o))*Split3q::SpTree3(QBQG,o) + (T(1)) * (sij(o[1],o[2])*sij(o[2],o[3])*((-T(2)*L3hat(sij(o[1],o[2]),s123)*sij(o[2],o[3]))/X(o[2],o[3]) - L2hat(sij(o[1],o[2]),s123)/X(o[2],o[1]) + T(1)/(T(2)*sij(o[1],o[2])*s123*X(o[2],o[1])) - (T(2)*L1hat(sij(o[1],o[2]),s123))/(s123*X(o[3],o[1])*X(o[2],o[1])) - (T(2)*L0hat(sij(o[1],o[2]),s123))/(sij(o[2],o[3])*s123*X(o[3],o[1])*X(o[2],o[1])))*zspA[o[2]]*zspA[o[3]])/(T(3)*spA(o[1],o[2])*spB(o[2],o[3]));
  } else if (flav == QBQG_NEQ4) {
    // finite sign
    return pole(o)*Split3q::SpTree3(QBQG,o) + T(-1)*(FNMHV3(o)*((pow(sij(o[2],o[3]),3)*pow(spB(o[1],o[0]),2)*rt(o[1],o[2])*zspA[o[1]])/(sij(o[1],o[2])*sij(o[1],o[0])*sij(o[3],o[0])*s123*pow(spB(o[2],o[3]),2)*zspA[o[2]]) + (pow(spB(o[1],o[0]),2)*rt(o[3],o[2])*zspA[o[1]])/(s123*pow(spB(o[2],o[3]),2)*zspA[o[2]]) + (pow(spA(o[2],o[3]),2)*zspA[o[3]])/(s123*spA(o[1],o[0])*spA(o[3],o[0])*zspA[o[2]]) + (pow(spB(o[1],o[0]),2)*zspA[o[3]])/(s123*spB(o[1],o[2])*spB(o[2],o[3])*zspA[o[2]])) + L1hat(sij(o[1],o[2]),s123)*((T(3)*spA(o[2],o[3])*spB(o[1],o[0])*spB(o[2],o[0]))/(s123*spB(o[2],o[3])) - (T(3)*sij(o[1],o[2])*sij(o[2],o[3])*zspA[o[2]]*zspA[o[3]])/(s123*spA(o[1],o[2])*spB(o[2],o[3])*X(o[3],o[2])*X(o[2],o[1]))) - (FNMHV1(o)*(-((pow(zspA[o[3]],3)*pow(zspB[o[1]],2))/(spA(o[3],o[0])*spB(o[1],o[2]))) + pow(zspA[o[2]],2)/(spA(o[1],o[2])*spB(o[3],o[0])*zspB[o[3]])))/(-1 + z[o[3]]) - (FNMHV2(o)*pow(1 - z[o[1]],2))/(spA(o[1],o[0])*spB(o[2],o[3])*zspB[o[3]]));
  } else if (flav == QBQG_X) {
    // sign
    return -(-T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[1],o[2])*sij(o[2],o[3])*zspA[o[2]]*zspA[o[3]])/(s123*spA(o[1],o[2])*spB(o[2],o[3])*X(o[3],o[2])*X(o[2],o[1]));
  } else if (flav == QBGQ_NEQ4) {
    //return zspA[o[1]]/zspA[o[3]] * Base::SpLoop3(Base::NEQ4,o) - zspA[o[2]]/zspA[o[3]] * SpLoop3(QBQG_NEQ4,o);
    return Split3q::SpTree3(QBGQ,o)*(pole(o) + (- (s123*rcc(o[2],o[3])*pow(X(o[3],o[1]),2)*Xcc(o[1],o[3])*FNMHV1(o))/sij(o[1],o[3]) + FNMHV3(o)*(T(1) + (sij(o[2],o[3])*r(o[2],o[3])*X(o[2],o[1])*Xcc(o[1],o[3]))/(sij(o[1],o[0])*z[o[1]])) + (s123*X(o[2],o[1])*FNMHV2(o)*pow(-T(1) + z[o[1]],2))/(sij(o[1],o[0])*z[o[2]])));
  } else if (flav == QBGQ_X) {
    // finite sign
    return Split3q::SpTree3(QBGQ,o)*(poleX2(1,o) + ((T(2)*L0hat(sij(o[1],o[3]),s123))/T(3) - (T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[2],o[3])*X(o[2],o[1]))/X(o[2],o[3])));
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop4(int flav, const int * o)/* {-1, -1, 1, -1} */
{
  if (flav == QBQG_R) {
    return Split3q::SpTree4(QBQG,o)*(poleR(o) +(-T(1)) * ((-(T(3)/T(2)) - F1mBox(o)/T(2) + (T(3)*L0hat(sij(o[1],o[2]),s123))/T(2) - T(3)*L2hat(sij(o[1],o[2]),s123)*pow(sij(o[1],o[2]),2) - T(2)*L1hat(sij(o[1],o[2]),s123)*sij(o[2],o[3]) + (-T(6)*pow(sij(o[1],o[2]),2) + T(2)*sij(o[1],o[2])*sij(o[2],o[3]) + pow(sij(o[2],o[3]),2))/(T(4)*sij(o[1],o[2])*s123) + T(3)*L1hat(sij(o[1],o[2]),s123)*s123 + (L2hat(sij(o[1],o[2]),s123)*(-pow(sij(o[2],o[3]),2) + T(6)*sij(o[1],o[2])*s123))/T(2) + (L1hat(sij(o[2],o[3]),s123)*sij(o[2],o[3]))/pow(rcc(o[1],o[2]),2) + (L2hat(sij(o[2],o[3]),s123)*(-pow(sij(o[1],o[2]),2) - pow(sij(o[2],o[3]),2) + pow(s123,2)))/(T(2)*pow(rcc(o[1],o[2]),2)) - (-pow(sij(o[1],o[2]),2) - T(2)*sij(o[1],o[2])*sij(o[2],o[3]) + pow(sij(o[2],o[3]) + s123,2))/(T(4)*sij(o[2],o[3])*s123*pow(rcc(o[1],o[2]),2)) - (T(2)*L1hat(sij(o[2],o[3]),s123)*sij(o[1],o[3]))/rcc(o[1],o[2]) + sij(o[1],o[3])/(s123*rcc(o[1],o[2])))*pow(rcc(o[1],o[2]),3))/pow(rcc(o[1],o[3]),3));
  } else if (flav == QBQG_F) {
    // sign
    return -Split3q::SpTree4(QBQG,o)*(poleF(o) + (-T(1)) * ((-L0hat(sij(o[1],o[2]),s123) + L3hat(sij(o[1],o[2]),s123)*(pow(sij(o[1],o[3]),3) - pow(sij(o[2],o[3]),3)) + ((sij(o[1],o[3])*sij(o[2],o[3]))/(sij(o[1],o[2])*s123) - ((-sij(o[1],o[3]) + sij(o[2],o[3]))*(sij(o[1],o[2]) + s123))/(sij(o[1],o[2])*s123))/T(2) + (T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[1],o[3]))/pow(rcc(o[1],o[2]),2) - sij(o[1],o[3])/(s123*pow(rcc(o[1],o[2]),2)) - (T(3)*L2hat(sij(o[1],o[2]),s123)*(pow(sij(o[1],o[3]),2) + T(2)*sij(o[1],o[3])*sij(o[2],o[3])))/rcc(o[1],o[2]) + (sij(o[1],o[3])*(T(3)*sij(o[1],o[2]) + sij(o[2],o[3]) + T(5)*s123))/(T(2)*sij(o[1],o[2])*s123*rcc(o[1],o[2])))*pow(rcc(o[1],o[2]),3))/(T(3)*pow(rcc(o[1],o[3]),3)));
  } else if (flav == QBQG_NEQ4) {
    return (pole(o) + FMHV(o))*Split3q::SpTree4(QBQG,o);
  } else if (flav == QBQG_X) {
    return Split3q::SpTree4(QBQG,o)*((-T(3)*F1mBox(o)*rcc(o[1],o[2]))/(T(2)*pow(rcc(o[1],o[3]),2)) - (T(3)*L0hat(sij(o[1],o[2]),s123)*rcc(o[1],o[2]))/pow(rcc(o[1],o[3]),2) - (T(3)*L0hat(sij(o[2],o[3]),s123)*rcc(o[1],o[2]))/pow(rcc(o[1],o[3]),2) - (T(3)*L1hat(sij(o[2],o[3]),s123)*sij(o[1],o[2])*rcc(o[1],o[2]))/pow(rcc(o[1],o[3]),2) - (T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[2],o[3])*rcc(o[1],o[2]))/pow(rcc(o[1],o[3]),2));
  } else if (flav == QBGQ_NEQ4) {
  } else if (flav == QBGQ_X) {
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop5(int flav, const int * o)/* {1, -1, 1, -1} */
{
  if (flav == QBQG_R) {
    // finite sign
    return (poleR(o))*Split3q::SpTree5(QBQG,o) +(-T(1))* (((F1mBox(o)*sij(o[2],o[0])*rt(o[1],o[2])*rt(o[2],o[1]))/(T(2)*s123*X(o[3],o[1])) - (T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[1],o[3])*sij(o[2],o[0])*rt(o[1],o[2])*rt(o[2],o[1]))/(T(2)*s123*X(o[3],o[1])) - (L2hat(sij(o[1],o[2]),s123)*sij(o[1],o[3])*sij(o[2],o[3])*sij(o[2],o[0])*rt(o[1],o[2])*rt(o[2],o[1]))/(T(2)*s123*X(o[3],o[1])) - (T(3)*L0hat(sij(o[2],o[3]),s123)*sij(o[1],o[2])*sij(o[2],o[0])*X(o[1],o[2]))/(T(2)*sij(o[1],o[3])*s123) + (L2hat(sij(o[2],o[3]),s123)*sij(o[1],o[2])*sij(o[2],o[3])*sij(o[2],o[0])*pow(X(o[1],o[2]),2))/(T(2)*sij(o[1],o[3])*X(o[3],o[2])) + L1hat(sij(o[2],o[3]),s123)*(-((sij(o[1],o[2])*sij(o[2],o[0])*X(o[1],o[2]))/sij(o[1],o[3])) + (T(3)*sij(o[2],o[3])*sij(o[2],o[0])*(-sij(o[1],o[2]) + s123)*pow(X(o[1],o[2]),2))/(T(2)*sij(o[1],o[3])*s123*X(o[3],o[2]))) + (L0hat(sij(o[2],o[3]),s123)*sij(o[2],o[0])*(sij(o[2],o[3])*X(o[1],o[2]) + X(o[3],o[2])*(T(3)*sij(o[1],o[2]) + sij(o[2],o[3]) - s123 + T(3)*s123*X(o[1],o[2]))))/(T(2)*sij(o[1],o[3])*s123*X(o[3],o[2])) + (sij(o[2],o[0])*(-(pow(sij(o[1],o[2]),2)*(sij(o[2],o[3]) - T(2)*s123)*pow(X(o[3],o[2]),2)) + T(2)*sij(o[1],o[2])*(sij(o[2],o[3]) - T(2)*s123)*(-sij(o[1],o[2]) + s123)*X(o[3],o[2])*X(o[1],o[2]) + (-(pow(sij(o[1],o[2]),2)*sij(o[2],o[3])) + sij(o[1],o[2])*(sij(o[1],o[2]) + T(2)*sij(o[2],o[3]))*s123 + sij(o[2],o[3])*pow(s123,2))*pow(X(o[1],o[2]),2))*Xcc(o[1],o[3])*(Xcc(o[3],o[1]) + z[o[3]]))/(T(4)*sij(o[1],o[2])*sij(o[1],o[3])*pow(s123,2)*X(o[3],o[2])*Xcc(o[3],o[1])*z[o[1]]))*zspA[o[3]])/(spA(o[2],o[0])*spB(o[1],o[3]));
  } else if (flav == QBQG_F) {
    // pole sign
    return -(poleF(o))*Split3q::SpTree5(QBQG,o) + (sij(o[1],o[3])*sij(o[2],o[0])*(-T(2)*L1hat(sij(o[1],o[2]),s123) - L2hat(sij(o[1],o[2]),s123)*sij(o[1],o[2]) - 1/(T(2)*s123) + T(2)*L3hat(sij(o[1],o[2]),s123)*sij(o[1],o[2])*s123*X(o[3],o[2]) + (T(2)*L1hat(sij(o[1],o[2]),s123)*s123*X(o[1],o[2]))/sij(o[1],o[2]) + (T(2)*L1hat(sij(o[1],o[2]),s123)*sij(o[1],o[2])*pow(X(o[1],o[2]),2))/(s123*X(o[3],o[2])) + (T(2)*L0hat(sij(o[1],o[2]),s123)*(sij(o[1],o[2])*X(o[3],o[2]) + (T(2)*sij(o[1],o[2]) + s123)*X(o[1],o[2])))/(sij(o[1],o[2])*s123))*zspA[o[3]])/(T(3)*sij(o[1],o[2])*spA(o[2],o[0])*spB(o[1],o[3]));
  } else if (flav == QBQG_NEQ4) {
    // finite sign
    return pole(o)*Split3q::SpTree5(QBQG,o) + T(-1)*(- (T(2)*spA(o[1],o[3])*pow(spB(o[2],o[0]),2))/(sij(o[1],o[2])*s123*spB(o[2],o[3])) + FNMHV3(o)*(-((pow(spA(o[1],o[3]),2)*spB(o[2],o[0]))/(sij(o[3],o[0])*s123*spA(o[1],o[0]))) + (spA(o[1],o[2])*spA(o[3],o[0])*pow(spB(o[2],o[0]),3))/(sij(o[1],o[2])*sij(o[3],o[0])*s123*spB(o[2],o[3])) - (pow(spA(o[1],o[3]),2)*spB(o[1],o[2])*zspA[o[1]])/(sij(o[1],o[2])*sij(o[3],o[0])*spA(o[1],o[0]))) - (T(2)*sij(o[1],o[3])*sij(o[2],o[0])*zspA[o[3]])/(sij(o[1],o[2])*s123*spA(o[2],o[0])*spB(o[1],o[3])*X(o[3],o[2])) - (T(2)*sij(o[2],o[0])*X(o[3],o[2])*X(o[1],o[3])*z[o[2]]*zspA[o[3]])/(sij(o[1],o[2])*spA(o[2],o[0])*spB(o[1],o[3])*X(o[2],o[3])*(-1 + z[o[3]])) - (T(2)*sij(o[2],o[0])*X(o[1],o[3])*X(o[1],o[2])*z[o[2]]*zspA[o[3]])/(sij(o[1],o[2])*spA(o[2],o[0])*spB(o[1],o[3])*X(o[2],o[3])*z[o[3]]) + F1mBox(o)*((T(3)*pow(spA(o[1],o[3]),2)*spB(o[1],o[0])*spB(o[2],o[0]))/(T(2)*pow(sij(o[1],o[3]),2)*s123) - (T(3)*sij(o[2],o[0])*zspA[o[3]])/(T(2)*s123*spA(o[2],o[0])*spB(o[1],o[3])*X(o[3],o[1]))) + L1hat(sij(o[1],o[2]),s123)*((-T(3)*pow(spA(o[1],o[3]),2)*spB(o[1],o[0])*spB(o[2],o[0]))/(sij(o[1],o[3])*s123) + (T(3)*sij(o[1],o[3])*sij(o[2],o[0])*zspA[o[3]])/(s123*spA(o[2],o[0])*spB(o[1],o[3])*X(o[3],o[1]))) + L1hat(sij(o[2],o[3]),s123)*((-T(3)*pow(spA(o[1],o[3]),2)*spB(o[1],o[0])*spB(o[2],o[0]))/(sij(o[1],o[3])*s123) + (T(3)*sij(o[1],o[3])*sij(o[2],o[0])*zspA[o[3]])/(s123*spA(o[2],o[0])*spB(o[1],o[3])*X(o[3],o[1]))) + (T(2)*zspA[o[1]]*pow(zspA[o[3]],2)*zspB[o[2]])/(sij(o[1],o[2])*(-1 + z[o[3]])) + FNMHV1(o)*((spB(o[3],o[0])*pow(zspA[o[3]],3)*pow(zspB[o[2]],2))/(sij(o[3],o[0])*spB(o[1],o[2])*(-1 + z[o[3]])) - (spA(o[3],o[0])*pow(zspA[o[1]],2))/(sij(o[3],o[0])*spA(o[1],o[2])*(-1 + z[o[3]])*zspB[o[3]])) + (T(2)*pow(zspA[o[1]],2)*zspB[o[2]])/(spA(o[1],o[2])*spB(o[2],o[3])*zspB[o[3]]) - (spA(o[2],o[3])*FNMHV2(o)*pow(zspA[o[1]],2)*pow(zspB[o[2]],2))/(sij(o[2],o[3])*spA(o[1],o[0])*zspB[o[3]]));
  } else if (flav == QBQG_X) {
    // non-tree sign
    return -T(2)*Split3q::SpTree5(QBQG,o) - (- (T(2)*sij(o[1],o[3])*sij(o[2],o[0])*zspA[o[3]])/(sij(o[1],o[2])*s123*spA(o[2],o[0])*spB(o[1],o[3])*X(o[3],o[2])) - (T(3)*F1mBox(o)*sij(o[2],o[0])*zspA[o[3]])/(T(2)*s123*spA(o[2],o[0])*spB(o[1],o[3])*X(o[3],o[1])) + (T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[1],o[3])*sij(o[2],o[0])*zspA[o[3]])/(s123*spA(o[2],o[0])*spB(o[1],o[3])*X(o[3],o[1])) + (T(3)*L1hat(sij(o[2],o[3]),s123)*sij(o[1],o[3])*sij(o[2],o[0])*zspA[o[3]])/(s123*spA(o[2],o[0])*spB(o[1],o[3])*X(o[3],o[1])) - (T(2)*sij(o[2],o[0])*X(o[3],o[2])*X(o[1],o[3])*z[o[2]]*zspA[o[3]])/(sij(o[1],o[2])*spA(o[2],o[0])*spB(o[1],o[3])*X(o[2],o[3])*(-1 + z[o[3]])) - (T(2)*sij(o[2],o[0])*X(o[1],o[3])*X(o[1],o[2])*z[o[2]]*zspA[o[3]])/(sij(o[1],o[2])*spA(o[2],o[0])*spB(o[1],o[3])*X(o[2],o[3])*z[o[3]]));
  } else if (flav == QBGQ_NEQ4) {
  } else if (flav == QBGQ_X) {
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop6(int flav, const int * o)/* {-1, 1, 1, -1} */
{
  if (flav == QBQG_R) {
  } else if (flav == QBQG_F) {
  } else if (flav == QBQG_NEQ4) {
  } else if (flav == QBQG_X) {
  } else if (flav == QBGQ_NEQ4) {
    return Split3q::SpTree6(QBGQ,o)*(pole(o) +(- (s123*r(o[2],o[1])*X(o[3],o[1])*pow(Xcc(o[1],o[3]),2)*FNMHV2(o))/sij(o[1],o[3]) + FNMHV3(o)*(T(1) + (sij(o[1],o[2])*rcc(o[2],o[1])*X(o[3],o[1])*Xcc(o[2],o[3]))/(sij(o[3],o[0])*z[o[3]])) + (s123*Xcc(o[2],o[3])*FNMHV1(o)*pow(-T(1) + z[o[3]],2))/(sij(o[3],o[0])*z[o[2]])));
  } else if (flav == QBGQ_X) {
    return Split3q::SpTree6(QBGQ,o)*(poleX2(1,o) + ((T(2)*L0hat(sij(o[1],o[3]),s123))/T(3) - (T(3)*L1hat(sij(o[2],o[3]),s123)*sij(o[1],o[2])*Xcc(o[2],o[3]))/Xcc(o[2],o[1])));
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop7(int flav, const int * o)/* {1, 1, 1, -1} */
{
  if (flav == QBQG_R) {
  } else if (flav == QBQG_F) {
  } else if (flav == QBQG_NEQ4) {
  } else if (flav == QBQG_X) {
  } else if (flav == QBGQ_NEQ4) {
    return (pole(o) + FMHV(o))*Split3q::SpTree7(QBGQ,o);
  } else if (flav == QBGQ_X) {
    return Split3q::SpTree7(QBGQ,o)*(poleX2(3,o) + (-L0hat(sij(o[2],o[3]),s123)/T(2) - s123/(T(2)*sij(o[2],o[3])) + T(4)*L1hat(sij(o[2],o[3]),s123)*sij(o[1],o[2])*r(o[3],o[2]) - (sij(o[1],o[2])*r(o[3],o[2]))/sij(o[2],o[3]) - (pow(sij(o[1],o[2]),2)*pow(r(o[3],o[2]),2))/(T(4)*sij(o[2],o[3])*s123) + L2hat(sij(o[2],o[3]),s123)*(-(pow(sij(o[1],o[2]),2)*pow(r(o[3],o[2]),2))/T(2) + pow(sij(o[1],o[3]),2)/(T(2)*pow(X(o[3],o[1]),2))) + pow(sij(o[1],o[3]),2)/(T(4)*sij(o[2],o[3])*s123*pow(X(o[3],o[1]),2))));
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop8(int flav, const int * o)/* {-1, -1, -1, 1} */
{
  if (flav == QBQG_R) {
  } else if (flav == QBQG_F) {
  } else if (flav == QBQG_NEQ4) {
  } else if (flav == QBQG_X) {
  } else if (flav == QBGQ_NEQ4) {
    return (pole(o) + FMHV(o))*Split3q::SpTree8(QBGQ,o);
  } else if (flav == QBGQ_X) {
    return Split3q::SpTree8(QBGQ,o)*(poleX2(3,o) + (-L0hat(sij(o[2],o[3]),s123)/T(2) - s123/(T(2)*sij(o[2],o[3])) + T(4)*L1hat(sij(o[2],o[3]),s123)*sij(o[1],o[2])*rcc(o[3],o[2]) - (sij(o[1],o[2])*rcc(o[3],o[2]))/sij(o[2],o[3]) - (pow(sij(o[1],o[2]),2)*pow(rcc(o[3],o[2]),2))/(T(4)*sij(o[2],o[3])*s123) + L2hat(sij(o[2],o[3]),s123)*(-(pow(sij(o[1],o[2]),2)*pow(rcc(o[3],o[2]),2))/T(2) + pow(sij(o[1],o[3]),2)/(T(2)*pow(Xcc(o[3],o[1]),2))) + pow(sij(o[1],o[3]),2)/(T(4)*sij(o[2],o[3])*s123*pow(Xcc(o[3],o[1]),2))));
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop9(int flav, const int * o)/* {1, -1, -1, 1} */
{
  if (flav == QBQG_R) {
  } else if (flav == QBQG_F) {
  } else if (flav == QBQG_NEQ4) {
  } else if (flav == QBQG_X) {
  } else if (flav == QBGQ_NEQ4) {
    return Split3q::SpTree9(QBGQ,o)*(pole(o) +(- (s123*rcc(o[2],o[1])*pow(X(o[1],o[3]),2)*Xcc(o[3],o[1])*FNMHV2(o))/sij(o[1],o[3]) + FNMHV3(o)*(T(1) + (sij(o[1],o[2])*r(o[2],o[1])*X(o[2],o[3])*Xcc(o[3],o[1]))/(sij(o[3],o[0])*z[o[3]])) + (s123*X(o[2],o[3])*FNMHV1(o)*pow(-T(1) + z[o[3]],2))/(sij(o[3],o[0])*z[o[2]])));
  } else if (flav == QBGQ_X) {
    return Split3q::SpTree9(QBGQ,o)*(poleX2(1,o) + ((T(2)*L0hat(sij(o[1],o[3]),s123))/T(3) - (T(3)*L1hat(sij(o[2],o[3]),s123)*sij(o[1],o[2])*X(o[2],o[3]))/X(o[2],o[1])));
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop10(int flav, const int * o)/* {-1, 1, -1, 1} */
{
  if (flav == QBQG_R) {
    return (poleR(o))*Split3q::SpTree10(QBQG,o) +(-T(1))* (((F1mBox(o)*sij(o[2],o[0])*rtcc(o[1],o[2])*rtcc(o[2],o[1]))/(T(2)*s123*Xcc(o[3],o[1])) - (T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[1],o[3])*sij(o[2],o[0])*rtcc(o[1],o[2])*rtcc(o[2],o[1]))/(T(2)*s123*Xcc(o[3],o[1])) - (L2hat(sij(o[1],o[2]),s123)*sij(o[1],o[3])*sij(o[2],o[3])*sij(o[2],o[0])*rtcc(o[1],o[2])*rtcc(o[2],o[1]))/(T(2)*s123*Xcc(o[3],o[1])) - (T(3)*L0hat(sij(o[2],o[3]),s123)*sij(o[1],o[2])*sij(o[2],o[0])*Xcc(o[1],o[2]))/(T(2)*sij(o[1],o[3])*s123) + (L2hat(sij(o[2],o[3]),s123)*sij(o[1],o[2])*sij(o[2],o[3])*sij(o[2],o[0])*pow(Xcc(o[1],o[2]),2))/(T(2)*sij(o[1],o[3])*Xcc(o[3],o[2])) + L1hat(sij(o[2],o[3]),s123)*(-((sij(o[1],o[2])*sij(o[2],o[0])*Xcc(o[1],o[2]))/sij(o[1],o[3])) + (T(3)*sij(o[2],o[3])*sij(o[2],o[0])*(-sij(o[1],o[2]) + s123)*pow(Xcc(o[1],o[2]),2))/(T(2)*sij(o[1],o[3])*s123*Xcc(o[3],o[2]))) + (L0hat(sij(o[2],o[3]),s123)*sij(o[2],o[0])*(sij(o[2],o[3])*Xcc(o[1],o[2]) + Xcc(o[3],o[2])*(T(3)*sij(o[1],o[2]) + sij(o[2],o[3]) - s123 + T(3)*s123*Xcc(o[1],o[2]))))/(T(2)*sij(o[1],o[3])*s123*Xcc(o[3],o[2])) + (sij(o[2],o[0])*X(o[1],o[3])*(-(pow(sij(o[1],o[2]),2)*(sij(o[2],o[3]) - T(2)*s123)*pow(Xcc(o[3],o[2]),2)) + T(2)*sij(o[1],o[2])*(sij(o[2],o[3]) - T(2)*s123)*(-sij(o[1],o[2]) + s123)*Xcc(o[3],o[2])*Xcc(o[1],o[2]) + (-(pow(sij(o[1],o[2]),2)*sij(o[2],o[3])) + sij(o[1],o[2])*(sij(o[1],o[2]) + T(2)*sij(o[2],o[3]))*s123 + sij(o[2],o[3])*pow(s123,2))*pow(Xcc(o[1],o[2]),2))*(X(o[3],o[1]) + z[o[3]]))/(T(4)*sij(o[1],o[2])*sij(o[1],o[3])*pow(s123,2)*X(o[3],o[1])*Xcc(o[3],o[2])*z[o[1]]))*zspB[o[3]])/(spA(o[1],o[3])*spB(o[2],o[0]));
  } else if (flav == QBQG_F) {
    // sign
    return -(poleF(o))*Split3q::SpTree10(QBQG,o) + (sij(o[1],o[3])*sij(o[2],o[0])*(-T(2)*L1hat(sij(o[1],o[2]),s123) - L2hat(sij(o[1],o[2]),s123)*sij(o[1],o[2]) - 1/(T(2)*s123) + T(2)*L3hat(sij(o[1],o[2]),s123)*sij(o[1],o[2])*s123*Xcc(o[3],o[2]) + (T(2)*L1hat(sij(o[1],o[2]),s123)*s123*Xcc(o[1],o[2]))/sij(o[1],o[2]) + (T(2)*L1hat(sij(o[1],o[2]),s123)*sij(o[1],o[2])*pow(Xcc(o[1],o[2]),2))/(s123*Xcc(o[3],o[2])) + (T(2)*L0hat(sij(o[1],o[2]),s123)*(sij(o[1],o[2])*Xcc(o[3],o[2]) + (T(2)*sij(o[1],o[2]) + s123)*Xcc(o[1],o[2])))/(sij(o[1],o[2])*s123))*zspB[o[3]])/(T(3)*sij(o[1],o[2])*spA(o[1],o[3])*spB(o[2],o[0]));
  } else if (flav == QBQG_NEQ4) {
    return pole(o)*Split3q::SpTree10(QBQG,o) + ((T(2)*pow(spA(o[2],o[0]),2)*spB(o[1],o[3]))/(sij(o[1],o[2])*s123*spA(o[2],o[3])) - (T(2)*zspA[o[2]]*pow(zspB[o[1]],2))/(spA(o[2],o[3])*spB(o[1],o[2])*zspA[o[3]]) + (spB(o[2],o[3])*FNMHV2(o)*pow(zspA[o[2]],2)*pow(zspB[o[1]],2))/(sij(o[2],o[3])*spB(o[1],o[0])*zspA[o[3]]) + FNMHV3(o)*((spA(o[2],o[0])*pow(spB(o[1],o[3]),2))/(sij(o[3],o[0])*s123*spB(o[1],o[0])) - (pow(spA(o[2],o[0]),3)*spB(o[1],o[2])*spB(o[3],o[0]))/(sij(o[1],o[2])*sij(o[3],o[0])*s123*spA(o[2],o[3])) + (spA(o[1],o[2])*pow(spB(o[1],o[3]),2)*zspB[o[1]])/(sij(o[1],o[2])*sij(o[3],o[0])*spB(o[1],o[0]))) + (T(2)*sij(o[1],o[3])*sij(o[2],o[0])*zspB[o[3]])/(sij(o[1],o[2])*s123*spA(o[1],o[3])*spB(o[2],o[0])*Xcc(o[3],o[2])) + (T(2)*sij(o[2],o[0])*Xcc(o[3],o[2])*Xcc(o[1],o[3])*z[o[2]]*zspB[o[3]])/(sij(o[1],o[2])*spA(o[1],o[3])*spB(o[2],o[0])*Xcc(o[2],o[3])*(-1 + z[o[3]])) + (T(2)*sij(o[2],o[0])*Xcc(o[1],o[3])*Xcc(o[1],o[2])*z[o[2]]*zspB[o[3]])/(sij(o[1],o[2])*spA(o[1],o[3])*spB(o[2],o[0])*Xcc(o[2],o[3])*z[o[3]]) - (T(2)*zspA[o[2]]*zspB[o[1]]*pow(zspB[o[3]],2))/(sij(o[1],o[2])*(-1 + z[o[3]])) + F1mBox(o)*((-T(3)*spA(o[1],o[0])*spA(o[2],o[0])*pow(spB(o[1],o[3]),2))/(T(2)*pow(sij(o[1],o[3]),2)*s123) + (T(3)*sij(o[2],o[0])*zspB[o[3]])/(T(2)*s123*spA(o[1],o[3])*spB(o[2],o[0])*Xcc(o[3],o[1]))) + L1hat(sij(o[1],o[2]),s123)*((T(3)*spA(o[1],o[0])*spA(o[2],o[0])*pow(spB(o[1],o[3]),2))/(sij(o[1],o[3])*s123) - (T(3)*sij(o[1],o[3])*sij(o[2],o[0])*zspB[o[3]])/(s123*spA(o[1],o[3])*spB(o[2],o[0])*Xcc(o[3],o[1]))) + L1hat(sij(o[2],o[3]),s123)*((T(3)*spA(o[1],o[0])*spA(o[2],o[0])*pow(spB(o[1],o[3]),2))/(sij(o[1],o[3])*s123) - (T(3)*sij(o[1],o[3])*sij(o[2],o[0])*zspB[o[3]])/(s123*spA(o[1],o[3])*spB(o[2],o[0])*Xcc(o[3],o[1]))) + FNMHV1(o)*((spB(o[3],o[0])*pow(zspB[o[1]],2))/(sij(o[3],o[0])*spB(o[1],o[2])*(-1 + z[o[3]])*zspA[o[3]]) - (spA(o[3],o[0])*pow(zspA[o[2]],2)*pow(zspB[o[3]],3))/(sij(o[3],o[0])*spA(o[1],o[2])*(-1 + z[o[3]]))));
  } else if (flav == QBQG_X) {
    return -T(2)*Split3q::SpTree10(QBQG,o) + (T(2)*sij(o[1],o[3])*sij(o[2],o[0])*zspB[o[3]])/(sij(o[1],o[2])*s123*spA(o[1],o[3])*spB(o[2],o[0])*Xcc(o[3],o[2])) + (T(3)*F1mBox(o)*sij(o[2],o[0])*zspB[o[3]])/(T(2)*s123*spA(o[1],o[3])*spB(o[2],o[0])*Xcc(o[3],o[1])) - (T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[1],o[3])*sij(o[2],o[0])*zspB[o[3]])/(s123*spA(o[1],o[3])*spB(o[2],o[0])*Xcc(o[3],o[1])) - (T(3)*L1hat(sij(o[2],o[3]),s123)*sij(o[1],o[3])*sij(o[2],o[0])*zspB[o[3]])/(s123*spA(o[1],o[3])*spB(o[2],o[0])*Xcc(o[3],o[1])) + (T(2)*sij(o[2],o[0])*Xcc(o[3],o[2])*Xcc(o[1],o[3])*z[o[2]]*zspB[o[3]])/(sij(o[1],o[2])*spA(o[1],o[3])*spB(o[2],o[0])*Xcc(o[2],o[3])*(-1 + z[o[3]])) + (T(2)*sij(o[2],o[0])*Xcc(o[1],o[3])*Xcc(o[1],o[2])*z[o[2]]*zspB[o[3]])/(sij(o[1],o[2])*spA(o[1],o[3])*spB(o[2],o[0])*Xcc(o[2],o[3])*z[o[3]]);
  } else if (flav == QBGQ_NEQ4) {
  } else if (flav == QBGQ_X) {
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop11(int flav, const int * o)/* {1, 1, -1, 1} */
{
  if (flav == QBQG_R) {
    return Split3q::SpTree11(QBQG,o)*(poleR(o) + (-T(1)) * ((-(T(3)/T(2)) - F1mBox(o)/T(2) + (T(3)*L0hat(sij(o[1],o[2]),s123))/T(2) - T(3)*L2hat(sij(o[1],o[2]),s123)*pow(sij(o[1],o[2]),2) - T(2)*L1hat(sij(o[1],o[2]),s123)*sij(o[2],o[3]) + (-T(6)*pow(sij(o[1],o[2]),2) + T(2)*sij(o[1],o[2])*sij(o[2],o[3]) + pow(sij(o[2],o[3]),2))/(T(4)*sij(o[1],o[2])*s123) + T(3)*L1hat(sij(o[1],o[2]),s123)*s123 + (L2hat(sij(o[1],o[2]),s123)*(-pow(sij(o[2],o[3]),2) + T(6)*sij(o[1],o[2])*s123))/T(2) + (L1hat(sij(o[2],o[3]),s123)*sij(o[2],o[3]))/pow(r(o[1],o[2]),2) + (L2hat(sij(o[2],o[3]),s123)*(-pow(sij(o[1],o[2]),2) - pow(sij(o[2],o[3]),2) + pow(s123,2)))/(T(2)*pow(r(o[1],o[2]),2)) - (-pow(sij(o[1],o[2]),2) - T(2)*sij(o[1],o[2])*sij(o[2],o[3]) + pow(sij(o[2],o[3]) + s123,2))/(T(4)*sij(o[2],o[3])*s123*pow(r(o[1],o[2]),2)) - (T(2)*L1hat(sij(o[2],o[3]),s123)*sij(o[1],o[3]))/r(o[1],o[2]) + sij(o[1],o[3])/(s123*r(o[1],o[2])))*pow(r(o[1],o[2]),3))/pow(r(o[1],o[3]),3));
  } else if (flav == QBQG_F) {
    // sign
    return -Split3q::SpTree11(QBQG,o)*(poleF(o) + (-T(1)) * ((-L0hat(sij(o[1],o[2]),s123) + L3hat(sij(o[1],o[2]),s123)*(pow(sij(o[1],o[3]),3) - pow(sij(o[2],o[3]),3)) + ((sij(o[1],o[3])*sij(o[2],o[3]))/(sij(o[1],o[2])*s123) - ((-sij(o[1],o[3]) + sij(o[2],o[3]))*(sij(o[1],o[2]) + s123))/(sij(o[1],o[2])*s123))/T(2) + (T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[1],o[3]))/pow(r(o[1],o[2]),2) - sij(o[1],o[3])/(s123*pow(r(o[1],o[2]),2)) - (T(3)*L2hat(sij(o[1],o[2]),s123)*(pow(sij(o[1],o[3]),2) + T(2)*sij(o[1],o[3])*sij(o[2],o[3])))/r(o[1],o[2]) + (sij(o[1],o[3])*(T(3)*sij(o[1],o[2]) + sij(o[2],o[3]) + T(5)*s123))/(T(2)*sij(o[1],o[2])*s123*r(o[1],o[2])))*pow(r(o[1],o[2]),3))/(T(3)*pow(r(o[1],o[3]),3)));
  } else if (flav == QBQG_NEQ4) {
    return (pole(o) + FMHV(o))*Split3q::SpTree11(QBQG,o);
  } else if (flav == QBQG_X) {
    return Split3q::SpTree11(QBQG,o)*((-T(3)*F1mBox(o)*r(o[1],o[2]))/(T(2)*pow(r(o[1],o[3]),2)) - (T(3)*L0hat(sij(o[1],o[2]),s123)*r(o[1],o[2]))/pow(r(o[1],o[3]),2) - (T(3)*L0hat(sij(o[2],o[3]),s123)*r(o[1],o[2]))/pow(r(o[1],o[3]),2) - (T(3)*L1hat(sij(o[2],o[3]),s123)*sij(o[1],o[2])*r(o[1],o[2]))/pow(r(o[1],o[3]),2) - (T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[2],o[3])*r(o[1],o[2]))/pow(r(o[1],o[3]),2));
  } else if (flav == QBGQ_NEQ4) {
  } else if (flav == QBGQ_X) {
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop12(int flav, const int * o)/* {-1, -1, 1, 1} */
{
  if (flav == QBQG_R) {
    return (poleR(o))*Split3q::SpTree12(QBQG,o) + (-T(1)) * (rcc(o[2],o[1])*((T(1)/T(2)) + sij(o[2],o[3])/(T(4)*sij(o[1],o[2])) - (L2hat(sij(o[1],o[2]),s123)*sij(o[2],o[3])*s123)/T(2) + (L1hat(sij(o[1],o[2]),s123)*(-T(3)*s123 - (T(2)*sij(o[2],o[3]))/Xcc(o[3],o[2])))/T(2) - (F1mBox(o)*sij(o[1],o[2]))/(T(2)*s123*rcc(o[2],o[1])*Xcc(o[3],o[1])*Xcc(o[2],o[1])))*zspB[o[2]]*zspB[o[3]])/(spA(o[2],o[3])*spB(o[1],o[2]));
  } else if (flav == QBQG_F) {
    // sign
    return -(poleF(o))*Split3q::SpTree12(QBQG,o) + (T(1)) * (sij(o[1],o[2])*sij(o[2],o[3])*((-T(2)*L3hat(sij(o[1],o[2]),s123)*sij(o[2],o[3]))/Xcc(o[2],o[3]) - L2hat(sij(o[1],o[2]),s123)/Xcc(o[2],o[1]) + T(1)/(T(2)*sij(o[1],o[2])*s123*Xcc(o[2],o[1])) - (T(2)*L1hat(sij(o[1],o[2]),s123))/(s123*Xcc(o[3],o[1])*Xcc(o[2],o[1])) - (T(2)*L0hat(sij(o[1],o[2]),s123))/(sij(o[2],o[3])*s123*Xcc(o[3],o[1])*Xcc(o[2],o[1])))*zspB[o[2]]*zspB[o[3]])/(T(3)*spA(o[2],o[3])*spB(o[1],o[2]));
  } else if (flav == QBQG_NEQ4) {
    return pole(o)*Split3q::SpTree12(QBQG,o) + ((FNMHV2(o)*pow(1 - z[o[1]],2))/(spA(o[2],o[3])*spB(o[1],o[0])*zspA[o[3]]) + FNMHV3(o)*(-((pow(sij(o[2],o[3]),3)*pow(spA(o[1],o[0]),2)*rtcc(o[1],o[2])*zspB[o[1]])/(sij(o[1],o[2])*sij(o[1],o[0])*sij(o[3],o[0])*s123*pow(spA(o[2],o[3]),2)*zspB[o[2]])) - (pow(spA(o[1],o[0]),2)*rtcc(o[3],o[2])*zspB[o[1]])/(s123*pow(spA(o[2],o[3]),2)*zspB[o[2]]) - (pow(spA(o[1],o[0]),2)*zspB[o[3]])/(s123*spA(o[1],o[2])*spA(o[2],o[3])*zspB[o[2]]) - (pow(spB(o[2],o[3]),2)*zspB[o[3]])/(s123*spB(o[1],o[0])*spB(o[3],o[0])*zspB[o[2]])) + L1hat(sij(o[1],o[2]),s123)*((-T(3)*spA(o[1],o[0])*spA(o[2],o[0])*spB(o[2],o[3]))/(s123*spA(o[2],o[3])) + (T(3)*sij(o[1],o[2])*sij(o[2],o[3])*zspB[o[2]]*zspB[o[3]])/(s123*spA(o[2],o[3])*spB(o[1],o[2])*Xcc(o[3],o[2])*Xcc(o[2],o[1]))) + (FNMHV1(o)*(pow(zspB[o[2]],2)/(spA(o[3],o[0])*spB(o[1],o[2])*zspA[o[3]]) - (pow(zspA[o[1]],2)*pow(zspB[o[3]],3))/(spA(o[1],o[2])*spB(o[3],o[0]))))/(-1 + z[o[3]]));
  } else if (flav == QBQG_X) {
    return (T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[1],o[2])*sij(o[2],o[3])*zspB[o[2]]*zspB[o[3]])/(s123*spA(o[2],o[3])*spB(o[1],o[2])*Xcc(o[3],o[2])*Xcc(o[2],o[1]));
  } else if (flav == QBGQ_NEQ4) {
    return Split3q::SpTree12(QBGQ,o)*(pole(o) +(- (s123*r(o[2],o[3])*X(o[1],o[3])*pow(Xcc(o[3],o[1]),2)*FNMHV1(o))/sij(o[1],o[3]) + FNMHV3(o)*(T(1) + (sij(o[2],o[3])*rcc(o[2],o[3])*X(o[1],o[3])*Xcc(o[2],o[1]))/(sij(o[1],o[0])*z[o[1]])) + (s123*Xcc(o[2],o[1])*FNMHV2(o)*pow(-T(1) + z[o[1]],2))/(sij(o[1],o[0])*z[o[2]])));
  } else if (flav == QBGQ_X) {
    return Split3q::SpTree12(QBGQ,o)*(poleX2(1,o) + ((T(2)*L0hat(sij(o[1],o[3]),s123))/T(3) - (T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[2],o[3])*Xcc(o[2],o[1]))/Xcc(o[2],o[3])));
    //return poleX2(1,o)*Sp("qbgq, 0",List(-1,1,1,-1),List(-t(1,2,3),3,2,1),n);
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop13(int flav, const int * o)/* {1, -1, 1, 1} */
{
  if (flav == QBQG_R) {
    return Split3q::SpTree13(QBQG,o)*(poleR(o) + (-T(1)) * (r(o[1],o[2])*(-F1mBox(o)/T(2) - (T(3)*L0hat(sij(o[1],o[2]),s123))/T(2) - (-sij(o[1],o[2]) + s123)/(T(2)*s123) + T(2)*sij(o[2],o[3])*(L1hat(sij(o[1],o[2]),s123) - 1/(T(2)*s123))*r(o[1],o[2]) - (pow(sij(o[2],o[3]),2)*(L2hat(sij(o[1],o[2]),s123) - 1/(T(2)*sij(o[1],o[2])*s123))*pow(r(o[1],o[2]),2))/T(2)))/r(o[1],o[3]));
  } else if (flav == QBQG_F) {
    // sign
    return -Split3q::SpTree13(QBQG,o)*(poleF(o) + (-T(1)) * (r(o[1],o[2])*(T(2)*L1hat(sij(o[1],o[2]),s123)*sij(o[1],o[3]) - (L0hat(sij(o[1],o[2]),s123)*sij(o[2],o[3]))/(sij(o[1],o[2]) - s123) + sij(o[2],o[3])/s123 + T(3)*L2hat(sij(o[1],o[2]),s123)*pow(sij(o[2],o[3]),2)*r(o[1],o[2]) - (pow(sij(o[2],o[3]),2)*r(o[1],o[2]))/(T(2)*sij(o[1],o[2])*s123) - T(2)*L3hat(sij(o[1],o[2]),s123)*pow(sij(o[2],o[3]),3)*pow(r(o[1],o[2]),2)))/(T(3)*r(o[1],o[3])));
  } else if (flav == QBQG_NEQ4) {
    return (pole(o) + FMHV(o))*Split3q::SpTree13(QBQG,o);
  } else if (flav == QBQG_X) {
    return -T(3)*L1hat(sij(o[1],o[2]),s123)*sij(o[2],o[3])*Split3q::SpTree13(QBQG,o)*r(o[1],o[2]);
  } else if (flav == QBGQ_NEQ4) {
    return (pole(o) + FMHV(o))*Split3q::SpTree13(QBGQ,o);
  } else if (flav == QBGQ_X) {
    return Split3q::SpTree13(QBGQ,o)*(poleX2(3,o) + (-L0hat(sij(o[1],o[2]),s123)/T(2) + T(4)*L1hat(sij(o[1],o[2]),s123)*sij(o[2],o[3])*r(o[1],o[2]) + (pow(sij(o[1],o[3]),2) + T(2)*pow(sij(o[1],o[2]),2)*pow(X(o[1],o[3]),2) - T(2)*pow(s123,2)*pow(X(o[1],o[3]),2))/(T(4)*sij(o[1],o[2])*s123*pow(X(o[1],o[3]),2)) + L2hat(sij(o[1],o[2]),s123)*(pow(sij(o[1],o[3]),2)/(T(2)*pow(X(o[1],o[3]),2)) - (pow(sij(o[1],o[2]),2)*pow(X(o[3],o[2]),2))/(T(2)*pow(X(o[1],o[2]),2))) + (sij(o[1],o[2])*(-T(2) + pow(X(o[3],o[2]),2)))/(T(4)*s123*pow(X(o[1],o[2]),2)) + ((sij(o[1],o[2]) + s123)*X(o[3],o[2]))/(s123*X(o[1],o[2]))));
  }
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop14(int flav, const int * o)/* {-1, 1, 1, 1} */
{
  return EpsTriplet<T>();
}
template <typename T>
EpsTriplet<T> Split3q<T>::SpLoop15(int flav, const int * o)/* {1, 1, 1, 1} */
{
  return EpsTriplet<T>();
}
//}}}





template class Split3Base<double>;
template class Split3Base<dd_real>;
template class Split3Base<qd_real>;

template class Split3g<double>;
template class Split3g<dd_real>;
template class Split3g<qd_real>;

template class Split3q<double>;
template class Split3q<dd_real>;
template class Split3q<qd_real>;
