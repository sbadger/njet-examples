#include <iostream>
#include <getopt.h>
#include <cstdio>

#include "tools/PhaseSpace.h"
#include "ngluon2/Initialize.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

using namespace std;

// hard coded single collinear phase-space point for 2->4
template <typename T>
std::vector<MOM<T> > getCollinearPoint6(const T lambda)
{
  const T sqrtS = 1e3;
  std::vector<MOM<T> >  Mom(6);

  const T E2 = T(0.12)*sqrtS;
  const T E3 = T(0.37)*sqrtS;

  const T theta1 = M_PI/3.;
  const T theta2 = lambda + theta1;
  const T ct1 = cos(theta1);
  const T ct2 = cos(theta2);
  const T st1 = sin(theta1);
  const T st2 = sin(theta2);

  const T phi1 = M_PI/2.;
  const T phi2 = M_PI/2.;
  const T cp1 = cos(phi1);
  const T cp2 = cos(phi2);
  const T sp1 = sin(phi1);
  const T sp2 = sin(phi2);

  const T E4 =
    -T(0.5)*(
        -sqrtS*sqrtS+2.*sqrtS*E2+2.*E2*st1*sp1*E3*sp2*st2+2*E2*st1*cp1*E3*cp2*st2+2.*E2*ct1*E3*ct2+2.*sqrtS*E3-2.*E2*E3
        )/(
        2.*E2*st1*sp1+2.*E3*sp2*st2+2.*E2*st1*cp1+2.*E3*cp2*st2+E2*ct1+E3*ct2+3.*sqrtS-3.*E2-3.*E3
        );

  Mom[4] = MOM<T>(sqrtS/2, 0., 0., -sqrtS/2);
  Mom[5] = MOM<T>(sqrtS/2, 0., 0., sqrtS/2);
  Mom[0] = -E2*MOM<T>(1., sp1*st1, ct1, cp1*st1);
  Mom[1] = -E3*MOM<T>(1., sp2*st2, ct2, cp2*st2);
  Mom[2] = -E4*MOM<T>(3., 2.     , 1. , 2.     );
  Mom[3] = -Mom[0]-Mom[1]-Mom[2]-Mom[4]-Mom[5];

  return Mom;
}

template <typename T>
void checkCollinear(const Flavour<double>* flavours) {
  const int nn = 6;

  std::vector<MOM<T> > mom(nn);
//  MOM<T> total;
//  for (int i=0; i<6; i++) {
//    cout << S(mom[i]) << endl;
//    total += mom[i];
//  }
//  cout << total  << endl;

  NParton2<T> amp;
  amp.setProcess(nn, flavours);

  NParton2<T> fact_amp;
  const Flavour<double> split_flavours[] = {StandardModel::G(), flavours[0], flavours[1]};
  const Flavour<double> fact_flavours[] = {StandardModel::G(), flavours[2], flavours[3], flavours[4], flavours[5]};
  (void)(split_flavours);
  fact_amp.setProcess(nn-1, fact_flavours);
  std::vector<MOM<T> > fact_mom(nn-1);

  const T MuR = 91.188;
  amp.setMuR2(MuR*MuR);

  const int helicity[] = {-1,+1,-1,+1,+1,+1};
  amp.setHelicity(helicity);

  const int fact_helicityA[] = {+1,-1,+1,+1,+1};
  const int fact_helicityB[] = {-1,-1,+1,+1,+1};

  for (int p=0; p<10; p++) {
    mom = getCollinearPoint6<T>(pow(50.,-p));
    amp.setMomenta(mom);

    // Catani-Seymour momentum mapping 12;3
    const MOM<T> p1 = mom[0];
    const MOM<T> p2 = mom[1];
    const MOM<T> p12 = p1+p2;
    const MOM<T> p3 = mom[2];
    const T s12 = S(p12);
    const T x12_3 = s12/T(2.)/dot(p12,p3);
    const MOM<T> p12t = p12 - x12_3*p3;
    const MOM<T> p3t = (1.+x12_3)*p3;
    fact_mom[0] = p12t;
    fact_mom[1] = p3t;
    fact_mom[2] = mom[3];
    fact_mom[3] = mom[4];
    fact_mom[4] = mom[5];

    const T z1 = dot(p1,p3)/dot(p12,p3);
    const T z2 = dot(p2,p3)/dot(p12,p3);

    const T rtz1 = sqrt(z1);
    const T rtz2 = sqrt(z2);

    fact_amp.setMomenta(fact_mom);
    complex<T> limit = T();
    // hard-coded P-;1-,2+
    const complex<T> splitA = rtz2*rtz2*rtz2/rtz1/xspB(p1,p2);
    fact_amp.setHelicity(fact_helicityA);
    limit += splitA*fact_amp.evalTree();
    // hard-coded P+;1-,2+
    const complex<T> splitB = rtz1*rtz1*rtz1/rtz2/xspA(p1,p2);
    fact_amp.setHelicity(fact_helicityB);
    limit += splitB*fact_amp.evalTree();

//    cout << "tree = " << amp.evalTree() << endl;
    cout << "tree/limit = " << amp.evalTree()/limit << endl;
    // 0 = mixed / gluon primitive
//    cout << amp.eval(0) << endl;
    // 1 = fermion loop primitive
//    cout << amp.eval(1) << endl;
  }

}

int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  const Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };

  cout << "double prec. (16) digits" << endl;
  checkCollinear<double>(flavours);
  cout << "quadruple prec. (32) digits" << endl;
  checkCollinear<dd_real>(flavours);
  cout << "octuple prec. (64) digits" << endl;
  checkCollinear<qd_real>(flavours);

  return 1;
}
