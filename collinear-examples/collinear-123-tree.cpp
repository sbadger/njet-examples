#include "split3.h"
#include <cassert>

// hard coded collinear phase-space point for 2->4
template <typename T>
std::vector<MOM<T>>
getTripleCollinearPoint6(const T lambda)
{
    const T sqrtS { 1. };
    const T alpha { static_cast<T>(M_PI) / static_cast<T>(3.) };
    const T beta { lambda };
    const T gamma { lambda };
    const int mul { 6 };
    std::vector<MOM<T>> momenta(6);

    const T x1 { 0.12 };
    const T x2 { 0.37 };

    const T ca { cos(alpha) };
    const T cb { cos(beta) };
    const T cg { cos(gamma) };

    const T sa { sin(alpha) };
    const T sb { sin(beta) };
    const T sg { sin(gamma) };

    // x3 fixes masslessness of p5
    const T x3 {
        (4 - 4 * x1 + x1 * x1 - ca * ca * x1 * x1 - sa * sa * x1 * x1 - 4 * x2 + 2 * x1 * x2 - 2 * ca * ca * cb * x1 * x2 - 2 * sa * sa * x1 * x2 + x2 * x2 - ca * ca * cb * cb * x2 * x2 - sa * sa * x2 * x2 - ca * ca * sb * sb * x2 * x2) / (2 * (2 - x1 + ca * ca * cb * x1 + cg * sa * sa * x1 - ca * sa * sb * sg * x1 - x2 + ca * ca * cb * cb * x2 + cg * sa * sa * x2 + ca * ca * cg * sb * sb * x2))
    };

    // generate momenta recursively
    // incoming momenta are back-to-back
    momenta[0] = -sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), static_cast<T>(0.), static_cast<T>(0.), static_cast<T>(1.));
    momenta[1] = -sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), static_cast<T>(0.), static_cast<T>(0.), -static_cast<T>(1.));
    // p2 = rotate p1 by alpha about x
    momenta[2] = x1 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), static_cast<T>(0.), -sa, ca);
    // p3 = rotate p2 by beta about y
    momenta[3] = x2 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), ca * sb, -sa, ca * cb);
    // p4 = rotate p3 by gamma about z
    momenta[4] = x3 * sqrtS / static_cast<T>(2.) * MOM<T>(static_cast<T>(1.), ca * cg * sb + sa * sg, -(cg * sa) + ca * sb * sg, ca * cb);
    // p5 given by momentum conservation
    momenta[5] = -(momenta[0] + momenta[1] + momenta[2] + momenta[3] + momenta[4]);

    T zero { pow(10, -10) };
    MOM<T> sum;
    for (int i { 0 }; i < mul; ++i) {
        sum += momenta[i];
        assert(abs(momenta[i].mass()) <= zero);
    }
    assert(sum.x0 <= zero);
    assert(sum.x1 <= zero);
    assert(sum.x2 <= zero);
    assert(sum.x3 <= zero);

    for (MOM<T> m : momenta) {
        std::cout << m << '\n';
    }
    std::cout << '\n';

    for (int i { 0 }; i < mul; ++i) {
        for (int j { i + 1 }; j < mul; ++j) {
            const T s { 2 * dot(momenta[i], momenta[j]) };
            std::cout << "s" << i << j << "=" << ((s >= 0) ? " " : "") << s << "  ";
        }
        std::cout << '\n';
    }

    return momenta;
}

complex<double> chop(complex<double> a) {
  complex<double> ans(0.,0.);
  if(abs(real(a)) > 1e-8) ans.real(real(a));
  if(abs(imag(a)) > 1e-8) ans.imag(imag(a));
  return ans;
}

complex<dd_real> chop(complex<dd_real> a) {
  complex<dd_real> ans(0.,0.);
  if(abs(real(a)) > 1e-14) ans.real(real(a));
  if(abs(imag(a)) > 1e-14) ans.imag(imag(a));
  return ans;
}

complex<qd_real> chop(complex<qd_real> a) {
  complex<qd_real> ans(0.,0.);
  if(abs(real(a)) > 1e-40) ans.real(real(a));
  if(abs(imag(a)) > 1e-40) ans.imag(imag(a));
  return ans;
}

EpsTriplet<double> chop(EpsTriplet<double> a) { return EpsTriplet<double>(chop(a.get0()), chop(a.get1()), chop(a.get2())); }
EpsTriplet<dd_real> chop(EpsTriplet<dd_real> a) { return EpsTriplet<dd_real>(chop(a.get0()), chop(a.get1()), chop(a.get2())); }
EpsTriplet<qd_real> chop(EpsTriplet<qd_real> a) { return EpsTriplet<qd_real>(chop(a.get0()), chop(a.get1()), chop(a.get2())); }

template <typename T>
EpsTriplet<T> ratio(EpsTriplet<T> a1, EpsTriplet<T> a2)
{
  return EpsTriplet<T>(
    chop(a1.get0())/chop(a2.get0()),
    chop(a1.get1())/chop(a2.get1()),
    chop(a1.get2())/chop(a2.get2()));
}

template <typename T>
void checkTripleCollinear(const Flavour<double>* flavours, const int h[]) {
  const int nn = 6;

  std::vector<MOM<T> > mom(nn);

  NParton2<T> amp;
  amp.setProcess(nn, flavours);

  NParton2<T> fact_amp;
  const Flavour<double> fact_flavours[] = {StandardModel::G(), flavours[3], flavours[4], flavours[5]};
  fact_amp.setProcess(nn-2, fact_flavours);
  std::vector<MOM<T> > fact_mom(nn-2);

//  const Flavour<double> split_flavours[] = {StandardModel::G(), flavours[0], flavours[1], flavours[2]};
  Split3g<T> split;

  const T MuR = 1e3/7.;
  amp.setMuR2(MuR*MuR);
  fact_amp.setMuR2(MuR*MuR);
  split.setMuR2(MuR*MuR);

  amp.setHelicity(h);
  int fact_h[] = {h[0], h[1], 1, h[5]};
  int split_h[] = {-1, h[2], h[3], h[4]};

  for (int p=16; p<17; p++) {
    mom = getTripleCollinearPoint6<T>(pow(10.,-p));
    amp.setMomenta(mom);

    // Catani-Seymour momentum mapping 123;4
    const MOM<T> p1 = mom[2];
    const MOM<T> p2 = mom[3];
    const MOM<T> p3 = mom[4];
    const MOM<T> p123 = p1+p2+p3;
    const MOM<T> p4 = mom[5];
    const T s123 = S(p123);
    const T x123_4 = s123/T(2.)/dot(p123,p4);
    const MOM<T> p123t = p123 - x123_4*p4;
    const MOM<T> p4t = (1.+x123_4)*p4;
    fact_mom[0] = mom[0];
    fact_mom[1] = mom[1];
    fact_mom[2] = p123t;
    fact_mom[3] = p4t;

    fact_amp.setMomenta(fact_mom);
    split.setMomenta(p123,p1,p2,p3,p4t);

    complex<T> limittree = T();
    fact_h[2] = +1;
    split_h[0] = -1;
    fact_amp.setHelicity(fact_h);
    split.setHelicity(split_h);
    std::complex<T> fact_p {fact_amp.evalTree()};
    std::complex<T> split_m {split.evalTree()};
    limittree += split_m*fact_p;

    cout << "Sp[0](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") = " << split_m << '\n';

    cout << "A[0](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout << ")    = " << fact_p << endl;

    cout << "limit Sp[0](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[0](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout << ") = " << split_m*fact_p << endl;

    fact_h[2] = -1;
    split_h[0] = +1;
    fact_amp.setHelicity(fact_h);
    split.setHelicity(split_h);
    std::complex<T> fact_m {fact_amp.evalTree()};
    std::complex<T> split_p {split.evalTree()};
    limittree += split_p*fact_m;

    cout << "Sp[0](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") = " << split_p << '\n';

    cout << "A[0](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout << ")    = " << fact_m << endl;

    cout << "Sp[0](";
    for (int i=0; i<4; i++) cout << split_h[i] << ",";
    cout << ") x A[0](";
    for (int i=0; i<4; i++) cout << fact_h[i] << ",";
    cout << ") = " << split_p*fact_m << endl;

    const complex<T> tree = amp.evalTree();

    cout << "A6g        = " << tree << endl;
    cout << "Sp x A4g   = " << limittree << endl;
    cout << "tree/limit = " << chop(tree)/chop(limittree) << endl;
  }

}

std::vector<int> getHelicity(const int n, const int hidx)
{
  std::vector<int> out(n);
  std::bitset<32> hhh(hidx);
  for (int i=0; i<n; i++) {
    out[i] = 2*hhh[i]-1;
  }
  return out;
}

int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  const Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };

  for (int i=42; i<43; i++) {
    std::vector<int> hhh = getHelicity(6,i);
    cout << endl << "##limit# " << "helicity " << i << " : ";
    for (int k=0; k<6; k++) {
      cout << hhh[k] << " ";
    }
    cout << endl;
//    cout << "### limit double prec. (16) digits" << endl;
//    checkTripleCollinear<double>(flavours, hhh.data());
//    cout << "### limit quadruple prec. (32) digits" << endl;
//    checkTripleCollinear<dd_real>(flavours, hhh.data());
    cout << "### limit octuple prec. (64) digits" << endl;
    checkTripleCollinear<qd_real>(flavours, hhh.data());
  };

  {

  }

  return 1;
}
