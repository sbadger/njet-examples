// -*- c++ -*-

#ifndef SPLITTREE3_H
#define SPLITTREE3_H

#include <iostream>
#include <getopt.h>
#include <cstdio>
#include <bitset>
//#include <gsl/gsl_sf_result.h>
//#include <gsl/gsl_sf_dilog.h>

#include "tools/PhaseSpace.h"
#include "ngluon2/Initialize.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

#include "chsums/NJetAmp.h"

#ifdef USE_DD
# include <qd/dd_real.h>
#endif
#ifdef USE_QD
# include <qd/qd_real.h>
#endif

using namespace std;


template <typename T> struct Pi;
template<> struct Pi<double> {
  static double val()
  {
    return M_PI;
  }
};
#ifdef USE_DD
template<> struct Pi<dd_real> {
  static dd_real val()
  {
    return dd_real::_pi;
  }
};
#endif
#ifdef USE_QD
template<> struct Pi<qd_real> {
  static qd_real val()
  {
    return qd_real::_pi;
  }
};
#endif


double DiLog(double arg);
complex<double> DiLog(complex<double> arg);

dd_real DiLog(dd_real arg);
complex<dd_real> DiLog(complex<dd_real> arg);

qd_real DiLog(qd_real arg);
complex<qd_real> DiLog(complex<qd_real> arg);


template <typename T, unsigned ROWS = 2, unsigned COLUMNS = 2>
struct StaticMatrix {
  // default constructor

  T & operator() (unsigned i, unsigned j)
  {
    return data_[i*COLUMNS + j];
  }

  const T & operator() (unsigned i, unsigned j) const
  {
    return data_[i*COLUMNS + j];
  }

  void fill(const T & value)
  {
    for (unsigned i=0; i<ROWS*COLUMNS; ++i)
      data_[i] = value;
  }
  
  T data_[ROWS*COLUMNS];
};



template <typename T>
inline
complex<T> logc(T x) {
  return x < 0 ? complex<T>(log(-x),Pi<T>::val()) : log(x);
}

template <typename T>
complex<T> MyDiLog(T x);


template <typename T>
class Split3Base {
  typedef complex<T> (Split3Base::*SplitAmpTree)(int flav, const int * o);
  typedef EpsTriplet<T> (Split3Base::*SplitAmpLoop)(int flav, const int * o);

public:

  static const int default_order[4];

  typedef StaticMatrix<T,2> SPMatrix;
  typedef StaticMatrix<T,2> CMatrix;

  Split3Base();
  virtual ~Split3Base() {}

  void born_spnmatrix(StaticMatrix< complex<T> > & spmatrix)
  {
    born_spnmatrix(0,spmatrix);    
  }
  void born_spnmatrix(const int h[], StaticMatrix< complex<T> >  & spmatrix);

  virtual void virt_spnmatrix(StaticMatrix< EpsTriplet<T> > & spmatrix) = 0;
  virtual void virt_spnmatrix(const int h[],
                              StaticMatrix< EpsTriplet<T> > & spmatrix) = 0;

  void setMomenta(const MOM<T> P123,
                  const  MOM<T> p1, const MOM<T> p2, const MOM<T> p3,
                  const MOM<T> n_)
  {
    p[0] = P123;
    p[1] = p1;
    p[2] = p2;
    p[3] = p3;
    n = n_;
    setMomenta_(p[ord[0]],p[ord[1]],p[ord[2]],p[ord[3]],n);
  }

  void setOrder(const int* ord_) {
    ord[0] = ord_[0];
    ord[1] = ord_[1];
    ord[2] = ord_[2];
    ord[3] = ord_[3];
    setMomenta_(p[ord[0]],p[ord[1]],p[ord[2]],p[ord[3]],n);
    sethidx_();
  }

  void setOrder(int i0, int i1, int i2, int i3) {
    int ord_[4]  = {i0,i1,i2,i3};
    setOrder(ord_);
  }

  void setMuR2(const T mur2) {
    MuR2 = mur2;
  }

  void setHelicity(const int* hels) {
    hel[0] = hels[0];
    hel[1] = hels[1];
    hel[2] = hels[2];
    hel[3] = hels[3];
    sethidx_();
  }

  void setNC(T nc_)
  {
    nc = nc_;
  }

  void setNf(T nf_)
  {
    nf = nf_;
  }

  virtual complex<T> evalTree(int flav = 0)
  {
    return (this->*SpTree[hidx])(flav, default_order);
  }

  virtual EpsTriplet<T> eval(const int primitive) = 0;

  // for debugging
  int gethidx()
  {
    return hidx;
  }

protected:

  SplitAmpTree SpTree[16];
  SplitAmpLoop SpLoop[16];

  void setMomenta_(const MOM<T> P123,
                   const  MOM<T> p1, const MOM<T> p2, const MOM<T> p3,
                   const MOM<T> n_);

  void sethidx_()
  {
    hidx = (hel[ord[0]]+1)/2 + 2*(hel[ord[1]]+1)/2 + 4*(hel[ord[2]]+1)/2 + 8*(hel[ord[3]]+1)/2;
  }

  // log and polylog functions //
  complex<T> mLog(T s);
  static complex<T> rLog(T s1, T s2);
  static complex<T> rDiLog(T s1, T s2);
  static complex<T> L0hat(T s1, T s2);
  static complex<T> L1hat(T s1, T s2);
  static complex<T> L2hat(T s1, T s2);
  static complex<T> L3hat(T s1, T s2);
  EpsTriplet<T> pSS(T s);
  EpsTriplet<T> pSS1(T s);
  
  EpsTriplet<T> pole(const int * o);
  EpsTriplet<T> poleR(const int * o);
  EpsTriplet<T> poleR2(const int * o);
  EpsTriplet<T> poleF(const int * o);
  EpsTriplet<T> poleX2(int type, const int * o);
  complex<T> FMHV(const int * o);
  complex<T> FNMHV1(const int * o);
  complex<T> FNMHV2(const int * o);
  complex<T> FNMHV3(const int * o);
  complex<T> F1mBox(const int * o);

  virtual void getCMatrix(StaticMatrix< T > & cmatrix) = 0;
  virtual void getCSumOrder(T & sign, int order[]) = 0;

  virtual complex<T> SpTree0(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree1(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree2(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree3(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree4(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree5(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree6(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree7(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree8(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree9(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree10(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree11(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree12(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree13(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree14(int flav, const int * ord = default_order) = 0;
  virtual complex<T> SpTree15(int flav, const int * ord = default_order) = 0;

  virtual EpsTriplet<T> SpLoop0(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop1(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop2(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop3(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop4(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop5(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop6(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop7(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop8(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop9(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop10(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop11(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop12(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop13(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop14(int flav, const int * ord = default_order) = 0;
  virtual EpsTriplet<T> SpLoop15(int flav, const int * ord = default_order) = 0;

  MOM<T> p[4];
  MOM<T> n;

  T z[4], y[4];
  T MuR2, s12, s23, s13, s123, nc, nf;
  StaticMatrix<T,4,4> sij;
  StaticMatrix<complex<T>,4,4> spA, spB, r, rt, rcc, rtcc, X, Xcc;
  complex<T> zspA[4], zspB[4], wspA[4], wspB[4], yspA[4], yspB[4];
  int hidx, hel[4], ord[4];

};

template <typename T>
const int Split3Base<T>::default_order[4] = {0,1,2,3};


#define USING_BASE_MEMBERS2 \
  using Base::SpTree; \
  using Base::SpLoop; \
  using Base::p;            \
  using Base::n; \
  using Base::z; \
  using Base::MuR2; \
  using Base::nf; \
  using Base::s123; \
  using Base::zspA; \
  using Base::zspB; \
  using Base::wspA; \
  using Base::wspB; \
  using Base::hidx; \
  using Base::hel; \
  using Base::ord; \
  using Base::pole; \
  using Base::poleR; \
  using Base::poleR2; \
  using Base::poleF; \
  using Base::poleX2; \
  using Base::FMHV; \
  using Base::FNMHV1; \
  using Base::FNMHV2; \
  using Base::FNMHV3; \
  using Base::F1mBox; \
  using Base::L0hat; \
  using Base::L1hat; \
  using Base::L2hat; \
  using Base::L3hat; \
  using Base::nc; \
  using Base::sij; \
  using Base::spA; \
  using Base::spB; \
  using Base::r; \
  using Base::rt; \
  using Base::rcc; \
  using Base::rtcc; \
  using Base::X; \
  using Base::Xcc; \
  using Base::yspA; \
  using Base::yspB



template <typename T>
class Split3g : public Split3Base<T> {
public :

  typedef Split3Base<T> Base;

  using Base::default_order;

  enum FlavoursLoop {GLOOP=0, FLOOP=1, NEQ4, NEQ1, NEQ0,
                     FLAVOURS_LOOP_END};

  Split3g() : Base() {}

  virtual void getCMatrix(StaticMatrix< T > & cmatrix)
  {
    T prefactor = nc*nc;
    StaticMatrix< T > cmatrix_3g = {{prefactor*4, prefactor*2,
                                     prefactor*2, prefactor*4}};
    cmatrix = cmatrix_3g;
  }

  virtual void getCSumOrder(T & sign, int order[])
  {
    sign = 1;
    order[0] = 0;
    order[1] = 1;
    order[2] = 3;
    order[3] = 2;
  }

  virtual EpsTriplet<T> eval(const int primitive) {
    if (primitive == GLOOP) {
      return (this->*SpLoop[hidx])(NEQ4,default_order)
        + T(4)*(this->*SpLoop[hidx])(NEQ1,default_order)
        +(this->*SpLoop[hidx])(NEQ0,default_order);
    } else if (primitive == FLOOP) {
      return (this->*SpLoop[hidx])(NEQ1,default_order)
        + (this->*SpLoop[hidx])(NEQ0,default_order);
    } else if (primitive >= 0 && primitive < FLAVOURS_LOOP_END) {
      return (this->*SpLoop[hidx])(primitive,default_order);
    } else {
      cout << "unknown primitive amplitude type" << endl;
      exit(1);
    }
  }

  virtual void virt_spnmatrix(StaticMatrix< EpsTriplet<T> > & spmatrix)
  {
    virt_spnmatrix(0,spmatrix);
  }
  virtual void virt_spnmatrix(const int h[],
                              StaticMatrix< EpsTriplet<T> > & spmatrix);

  //{{{ trees
  virtual complex<T> SpTree0(int flav, const int * o = default_order);/* {-1, -1, -1, -1} */
  virtual complex<T> SpTree1(int flav, const int * o = default_order);/* {1, -1, -1, -1} */
  virtual complex<T> SpTree2(int flav, const int * o = default_order);/* {-1, 1, -1, -1} */
  virtual complex<T> SpTree3(int flav, const int * o = default_order);/* {1, 1, -1, -1} */
  virtual complex<T> SpTree4(int flav, const int * o = default_order);/* {-1, -1, 1, -1} */
  virtual complex<T> SpTree5(int flav, const int * o = default_order);/* {1, -1, 1, -1} */
  virtual complex<T> SpTree6(int flav, const int * o = default_order);/* {-1, 1, 1, -1} */
  virtual complex<T> SpTree7(int flav, const int * o = default_order);/* {1, 1, 1, -1} */
  virtual complex<T> SpTree8(int flav, const int * o = default_order);/* {-1, -1, -1, 1} */
  virtual complex<T> SpTree9(int flav, const int * o = default_order);/* {1, -1, -1, 1} */
  virtual complex<T> SpTree10(int flav, const int * o = default_order);/* {-1, 1, -1, 1} */
  virtual complex<T> SpTree11(int flav, const int * o = default_order);/* {1, 1, -1, 1} */
  virtual complex<T> SpTree12(int flav, const int * o = default_order);/* {-1, -1, 1, 1} */
  virtual complex<T> SpTree13(int flav, const int * o = default_order);/* {1, -1, 1, 1} */
  virtual complex<T> SpTree14(int flav, const int * o = default_order);/* {-1, 1, 1, 1} */
  virtual complex<T> SpTree15(int flav, const int * o = default_order);/* {1, 1, 1, 1} */
  //}}}

  //{{{ loop primitives
  virtual EpsTriplet<T> SpLoop0(int flav, const int * o = default_order); /* {-1, -1, -1, -1} */
  virtual EpsTriplet<T> SpLoop1(int flav, const int * o = default_order); /* {1, -1, -1, -1} */
  virtual EpsTriplet<T> SpLoop2(int flav, const int * o = default_order); /* {-1, 1, -1, -1} */
  virtual EpsTriplet<T> SpLoop3(int flav, const int * o = default_order); /* {1, 1, -1, -1} */
  virtual EpsTriplet<T> SpLoop4(int flav, const int * o = default_order); /* {-1, -1, 1, -1} */
  virtual EpsTriplet<T> SpLoop5(int flav, const int * o = default_order); /* {1, -1, 1, -1} */
  virtual EpsTriplet<T> SpLoop6(int flav, const int * o = default_order); /* {-1, 1, 1, -1} */
  virtual EpsTriplet<T> SpLoop7(int flav, const int * o = default_order); /* {1, 1, 1, -1} */
  virtual EpsTriplet<T> SpLoop8(int flav, const int * o = default_order); /* {-1, -1, -1, 1} */
  virtual EpsTriplet<T> SpLoop9(int flav, const int * o = default_order); /* {1, -1, -1, 1} */
  virtual EpsTriplet<T> SpLoop10(int flav, const int * o = default_order); /* {-1, 1, -1, 1} */
  virtual EpsTriplet<T> SpLoop11(int flav, const int * o = default_order); /* {1, 1, -1, 1} */
  virtual EpsTriplet<T> SpLoop12(int flav, const int * o = default_order); /* {-1, -1, 1, 1} */
  virtual EpsTriplet<T> SpLoop13(int flav, const int * o = default_order); /* {1, -1, 1, 1} */
  virtual EpsTriplet<T> SpLoop14(int flav, const int * o = default_order); /* {-1, 1, 1, 1} */
  virtual EpsTriplet<T> SpLoop15(int flav, const int * o = default_order); /* {1, 1, 1, 1} */
  //}}}

protected:

  USING_BASE_MEMBERS2;

};



template <typename T>
class Split3q : public Split3g<T> {
public :

  typedef Split3g<T> Base;

  enum FlavoursTree {QBQG, QBGQ};

  enum FlavoursLoop {QBQG_R, QBQG_F, QBQG_NEQ4, QBQG_X, QBQG_L,
                     QBGQ_NEQ4, QBGQ_X, QBGQ_LR,
                     FLAVOURS_LOOP_END};

  using Base::default_order;

  Split3q() : Base() {}

  virtual void getCMatrix(StaticMatrix< T > & cmatrix)
  {
    T nc2 = nc*nc;
    T v = nc2-T(1);
    T prefactor = T(1)/nc;
    StaticMatrix< T > cmatrix_ = {{prefactor*v,     prefactor*T(-1),
                                   prefactor*T(-1), prefactor*v}};
    cmatrix = cmatrix_;
  }

  virtual void getCSumOrder(T & sign, int order[])
  {
    sign = -1;
    order[0] = 0;
    order[1] = 2;
    order[2] = 1;
    order[3] = 3;
  }

  virtual EpsTriplet<T> eval(const int primitive) {
    if (primitive == QBQG_L) {
      return (this->*SpLoop[hidx])(QBQG_NEQ4,default_order)
        - (this->*SpLoop[hidx])(QBQG_R,default_order)
        + (this->*SpLoop[hidx])(QBQG_F,default_order)
        - (this->*SpLoop[hidx])(QBQG_X,default_order);
    } else if (primitive == QBGQ_LR) {
      return (this->*SpLoop[hidx])(QBGQ_NEQ4,default_order)
        + (this->*SpLoop[hidx])(QBGQ_X,default_order);
    } else if (primitive >= 0 && primitive < FLAVOURS_LOOP_END) {
      return (this->*SpLoop[hidx])(primitive,default_order);
    } else {
      cout << "unknown primitive amplitude type" << endl;
      exit(1);
    }
  }

  virtual void virt_spnmatrix(StaticMatrix< EpsTriplet<T> > & spmatrix)
  {
    virt_spnmatrix(0,spmatrix);
  }
  virtual void virt_spnmatrix(const int h[],
                              StaticMatrix< EpsTriplet<T> > & spmatrix);

  //{{{ trees
  virtual complex<T> SpTree0(int flav, const int * o = default_order);/* {-1, -1, -1, -1} */
  virtual complex<T> SpTree1(int flav, const int * o = default_order);/* {1, -1, -1, -1} */
  virtual complex<T> SpTree2(int flav, const int * o = default_order);/* {-1, 1, -1, -1} */
  virtual complex<T> SpTree3(int flav, const int * o = default_order);/* {1, 1, -1, -1} */
  virtual complex<T> SpTree4(int flav, const int * o = default_order);/* {-1, -1, 1, -1} */
  virtual complex<T> SpTree5(int flav, const int * o = default_order);/* {1, -1, 1, -1} */
  virtual complex<T> SpTree6(int flav, const int * o = default_order);/* {-1, 1, 1, -1} */
  virtual complex<T> SpTree7(int flav, const int * o = default_order);/* {1, 1, 1, -1} */
  virtual complex<T> SpTree8(int flav, const int * o = default_order);/* {-1, -1, -1, 1} */
  virtual complex<T> SpTree9(int flav, const int * o = default_order);/* {1, -1, -1, 1} */
  virtual complex<T> SpTree10(int flav, const int * o = default_order);/* {-1, 1, -1, 1} */
  virtual complex<T> SpTree11(int flav, const int * o = default_order);/* {1, 1, -1, 1} */
  virtual complex<T> SpTree12(int flav, const int * o = default_order);/* {-1, -1, 1, 1} */
  virtual complex<T> SpTree13(int flav, const int * o = default_order);/* {1, -1, 1, 1} */
  virtual complex<T> SpTree14(int flav, const int * o = default_order);/* {-1, 1, 1, 1} */
  virtual complex<T> SpTree15(int flav, const int * o = default_order);/* {1, 1, 1, 1} */
  //}}}

  //{{{ loop primitives
  virtual EpsTriplet<T> SpLoop0(int flav, const int * o = default_order); /* {-1, -1, -1, -1} */
  virtual EpsTriplet<T> SpLoop1(int flav, const int * o = default_order); /* {1, -1, -1, -1} */
  virtual EpsTriplet<T> SpLoop2(int flav, const int * o = default_order); /* {-1, 1, -1, -1} */
  virtual EpsTriplet<T> SpLoop3(int flav, const int * o = default_order); /* {1, 1, -1, -1} */
  virtual EpsTriplet<T> SpLoop4(int flav, const int * o = default_order); /* {-1, -1, 1, -1} */
  virtual EpsTriplet<T> SpLoop5(int flav, const int * o = default_order); /* {1, -1, 1, -1} */
  virtual EpsTriplet<T> SpLoop6(int flav, const int * o = default_order); /* {-1, 1, 1, -1} */
  virtual EpsTriplet<T> SpLoop7(int flav, const int * o = default_order); /* {1, 1, 1, -1} */
  virtual EpsTriplet<T> SpLoop8(int flav, const int * o = default_order); /* {-1, -1, -1, 1} */
  virtual EpsTriplet<T> SpLoop9(int flav, const int * o = default_order); /* {1, -1, -1, 1} */
  virtual EpsTriplet<T> SpLoop10(int flav, const int * o = default_order); /* {-1, 1, -1, 1} */
  virtual EpsTriplet<T> SpLoop11(int flav, const int * o = default_order); /* {1, 1, -1, 1} */
  virtual EpsTriplet<T> SpLoop12(int flav, const int * o = default_order); /* {-1, -1, 1, 1} */
  virtual EpsTriplet<T> SpLoop13(int flav, const int * o = default_order); /* {1, -1, 1, 1} */
  virtual EpsTriplet<T> SpLoop14(int flav, const int * o = default_order); /* {-1, 1, 1, 1} */
  virtual EpsTriplet<T> SpLoop15(int flav, const int * o = default_order); /* {1, 1, 1, 1} */
  //}}}

protected:

  USING_BASE_MEMBERS2;

};



#endif // SPLITTREE3_H
