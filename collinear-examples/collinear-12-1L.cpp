#include <iostream>
#include <getopt.h>
#include <cstdio>
#include <bitset>

#include "tools/PhaseSpace.h"
#include "ngluon2/Initialize.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

using namespace std;

template <typename T>
class Split2
{
    typedef complex<T> (Split2::*SplitAmpTree)(const int* ord);
    typedef EpsTriplet<T> (Split2::*SplitAmpLoop)(const int* ord);

  public:

    Split2() : MuR2(4.)
    {
      ord[0] = 0;
      ord[1] = 1;
      ord[2] = 2;

      SpTree[0] = &Split2::SpTree0;
      SpTree[1] = &Split2::SpTree1;
      SpTree[2] = &Split2::SpTree2;
      SpTree[3] = &Split2::SpTree3;
      SpTree[4] = &Split2::SpTree4;
      SpTree[5] = &Split2::SpTree5;
      SpTree[6] = &Split2::SpTree6;
      SpTree[7] = &Split2::SpTree7;

      SpLoopG[0] = &Split2::SpLoopG0;
      SpLoopG[1] = &Split2::SpLoopG1;
      SpLoopG[2] = &Split2::SpLoopG2;
      SpLoopG[3] = &Split2::SpLoopG3;
      SpLoopG[4] = &Split2::SpLoopG4;
      SpLoopG[5] = &Split2::SpLoopG5;
      SpLoopG[6] = &Split2::SpLoopG6;
      SpLoopG[7] = &Split2::SpLoopG7;

      SpLoopF[0] = &Split2::SpLoopF0;
      SpLoopF[1] = &Split2::SpLoopF1;
      SpLoopF[2] = &Split2::SpLoopF2;
      SpLoopF[3] = &Split2::SpLoopF3;
      SpLoopF[4] = &Split2::SpLoopF4;
      SpLoopF[5] = &Split2::SpLoopF5;
      SpLoopF[6] = &Split2::SpLoopF6;
      SpLoopF[7] = &Split2::SpLoopF7;

    }

    void setMomenta(const MOM<T> P12, const  MOM<T> p1, const MOM<T> p2, const MOM<T> n) {
      z[1] = dot(p1,n)/dot(P12,n);
      z[2] = dot(p2,n)/dot(P12,n);
      a12 = xspA(p1,p2);
      b12 = xspB(p1,p2);
      s12 = S(P12);
      rtz[1] = sqrt(z[1]);
      rtz[2] = sqrt(z[2]);

    }

    void setOrder(const int* ord_) {
      ord[0] = ord_[0];
      ord[1] = ord_[1];
      ord[2] = ord_[2];
    }

    void setMuR2(const T mur2) {
      MuR2 = mur2;
    }

    void setHelicity(const int* hels) {
      hidx = (hels[0]+1)/2 + 2*(hels[1]+1)/2 + 4*(hels[2]+1)/2;
    }

    complex<T> evalTree() {
      return (this->*SpTree[hidx])(ord);
    }

    EpsTriplet<T> eval(const int primitive) {
      if (primitive == 0) {
        return (this->*SpLoopG[hidx])(ord);
      }
      else if (primitive == 1) {
        return (this->*SpLoopF[hidx])(ord);
      }
      else {
        cout << "unknown primtiive amplitude type" << endl;
        exit(0);
      }
    }

  protected:

    SplitAmpTree SpTree[8];
    SplitAmpLoop SpLoopG[8];
    SplitAmpLoop SpLoopF[8];

    complex<T> SpTree0(const int* p) /* -1 -1 -1 */
    {
      return T();
    }
    complex<T> SpTree1(const int* p) /* +1 -1 -1 */
    {
      return -T(1.)/(rtz[p[1]]*rtz[p[2]]*b12);
    }
    complex<T> SpTree2(const int* p) /* -1 +1 -1 */
    {
      return -rtz[p[1]]*rtz[p[1]]*rtz[p[1]]/(rtz[p[2]]*b12);
    }
    complex<T> SpTree3(const int* p) /* +1 +1 -1 */
    {
      return rtz[p[2]]*rtz[p[2]]*rtz[p[2]]/(rtz[p[1]]*a12);
    }
    complex<T> SpTree4(const int* p) /* -1 -1 +1 */
    {
      return -rtz[p[2]]*rtz[p[2]]*rtz[p[2]]/(rtz[p[1]]*b12);
    }
    complex<T> SpTree5(const int* p) /* +1 -1 +1 */
    {
      return rtz[p[1]]*rtz[p[1]]*rtz[p[1]]/(rtz[p[2]]*a12);
    }
    complex<T> SpTree6(const int* p) /* -1 +1 +1 */
    {
      return T(1.)/(rtz[p[1]]*rtz[p[2]]*a12);
    }
    complex<T> SpTree7(const int* p) /* +1 +1 +1 */
    {
      return T();
    }

    EpsTriplet<T> poles() {
      complex<T> lll = log(abs(MuR2/s12/z[1]/z[2]));
      if (s12>0.) lll += complex<T>(T(),T(M_PI));
      T lz1 = log(z[1]);
      T lz2 = log(z[2]);
      return EpsTriplet<T>(-lll*lll*T(0.5) - M_PI*M_PI/T(6.) + T(2.)*lz1*lz2, -lll, -T(1.));
    }

    EpsTriplet<T> SpLoopG0(const int* p) /* -1 -1 -1 */
    {
      return 1./T(3.)*rtz[1]*rtz[2]*a12/b12/b12;
    }
    EpsTriplet<T> SpLoopG1(const int* p) /* +1 -1 -1 */
    {
      return SpTree1(ord)*(poles() + (1./T(3.)*z[1]*z[2]));
    }
    EpsTriplet<T> SpLoopG2(const int* p) /* -1 +1 -1 */
    {
      return SpTree2(ord)*(poles());
    }
    EpsTriplet<T> SpLoopG3(const int* p) /* +1 +1 -1 */
    {
      return SpTree3(ord)*(poles());
    }
    EpsTriplet<T> SpLoopG4(const int* p) /* -1 -1 +1 */
    {
      return SpTree4(ord)*(poles());
    }
    EpsTriplet<T> SpLoopG5(const int* p) /* +1 -1 +1 */
    {
      return SpTree5(ord)*(poles());
    }
    EpsTriplet<T> SpLoopG6(const int* p) /* -1 +1 +1 */
    {
      return SpTree6(ord)*(poles() + (1./T(3.)*z[1]*z[2]));
    }
    EpsTriplet<T> SpLoopG7(const int* p) /* +1 +1 +1 */
    {
      return -1./T(3.)*rtz[1]*rtz[2]*b12/a12/a12;
    }

    EpsTriplet<T> SpLoopF0(const int* p) /* -1 -1 -1 */
    {
      return SpLoopG0(p);
    }
    EpsTriplet<T> SpLoopF1(const int* p) /* +1 -1 -1 */
    {
      return EpsTriplet<T>(SpTree1(p)/T(3.)*z[1]*z[2],T(),T());
    }
    EpsTriplet<T> SpLoopF2(const int* p) /* -1 +1 -1 */
    {
      return EpsTriplet<T>();
    }
    EpsTriplet<T> SpLoopF3(const int* p) /* +1 +1 -1 */
    {
      return EpsTriplet<T>();
    }
    EpsTriplet<T> SpLoopF4(const int* p) /* -1 -1 +1 */
    {
      return EpsTriplet<T>();
    }
    EpsTriplet<T> SpLoopF5(const int* p) /* +1 -1 +1 */
    {
      return EpsTriplet<T>();
    }
    EpsTriplet<T> SpLoopF6(const int* p) /* -1 +1 +1 */
    {
      return EpsTriplet<T>(SpTree6(p)/T(3.)*z[1]*z[2],T(),T());
    }
    EpsTriplet<T> SpLoopF7(const int* p) /* +1 +1 +1 */
    {
      return SpLoopG7(p);
    }

    T z[3], rtz[3], MuR2, s12;
    complex<T> a12, b12;
    int hidx, ord[3];

};

// generate single collinear phase-space point for 2->4
template <typename T>
std::vector<MOM<T> > getCollinearPoint6(const double lambda)
{

  StandardModel::setHmass(lambda);
  const Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::Higgs(),
    StandardModel::G(),
    StandardModel::G()
  };
  PhaseSpace<T> ps(5, flavours, 1, 1e3);
  ps.getPSpoint();
//  ps.showPSpoint();

  const T s12 = S(ps[2]);
  const T x123 = s12/dot(ps[2],ps[3])*T(0.5);
  cout << "### s12 = " << s12 << endl;

  // map to 2->4 configuration //
  const T z = 0.232;
  MOM<T> p12t = ps[2]-x123*ps[3];
  MOM<complex<T> > kt_c = i_*sqrt(T(0.25)*z*(1.-z)*x123)*(CMOM(p12t,ps[3]) - CMOM(ps[3],p12t));
  MOM<T> kt(real(kt_c.x0), real(kt_c.x1), real(kt_c.x2), real(kt_c.x3));

  std::vector<MOM<T> >  Mom(6);
  Mom[4] = ps[0];
  Mom[5] = ps[1];
  Mom[0] = z*p12t + kt + (1.-z)*x123*ps[3];
  Mom[1] = (1.-z)*p12t - kt + z*x123*ps[3];
  Mom[2] = ps[3];
  Mom[3] = ps[4];

//  MOM<T> mcons;
//  for (int i=0; i<6; i++) {
//    mcons += Mom[i];
//    cout << "p"<<i<<"^2 = "<< S(Mom[i]) << endl;
//  }
//  cout << mcons << endl;

  return Mom;
}

complex<double> chop(complex<double> a) {
  complex<double> ans(0.,0.);
  if(abs(real(a)) > 1e-10) ans.real(real(a));
  if(abs(imag(a)) > 1e-10) ans.imag(imag(a));
  return ans;
}

complex<dd_real> chop(complex<dd_real> a) {
  complex<dd_real> ans(0.,0.);
  if(abs(real(a)) > 1e-20) ans.real(real(a));
  if(abs(imag(a)) > 1e-20) ans.imag(imag(a));
  return ans;
}

complex<qd_real> chop(complex<qd_real> a) {
  complex<qd_real> ans(0.,0.);
  if(abs(real(a)) > 1e-50) ans.real(real(a));
  if(abs(imag(a)) > 1e-50) ans.imag(imag(a));
  return ans;
}

EpsTriplet<double> chop(EpsTriplet<double> a) { return EpsTriplet<double>(chop(a.get0()), chop(a.get1()), chop(a.get2())); }
EpsTriplet<dd_real> chop(EpsTriplet<dd_real> a) { return EpsTriplet<dd_real>(chop(a.get0()), chop(a.get1()), chop(a.get2())); }
EpsTriplet<qd_real> chop(EpsTriplet<qd_real> a) { return EpsTriplet<qd_real>(chop(a.get0()), chop(a.get1()), chop(a.get2())); }

template <typename T>
EpsTriplet<T> ratio(EpsTriplet<T> a1, EpsTriplet<T> a2)
{
  return EpsTriplet<T>(
    chop(a1.get0())/chop(a2.get0()),
    chop(a1.get1())/chop(a2.get1()),
    chop(a1.get2())/chop(a2.get2()));
}

template <typename T>
void checkCollinear(const Flavour<double>* flavours, const int h[]) {
  const int nn = 6;

  std::vector<MOM<T> > mom(nn);

  NParton2<T> amp;
  amp.setProcess(nn, flavours);

  NParton2<T> fact_amp;
  const Flavour<double> fact_flavours[] = {StandardModel::G(), flavours[2], flavours[3], flavours[4], flavours[5]};
  fact_amp.setProcess(nn-1, fact_flavours);
  std::vector<MOM<T> > fact_mom(nn-1);

//  const Flavour<double> split_flavours[] = {StandardModel::G(), flavours[0], flavours[1]};
  Split2<T> split;

  const T MuR = 1e3/2.;
  amp.setMuR2(MuR*MuR);
  fact_amp.setMuR2(MuR*MuR);
  split.setMuR2(MuR*MuR);

  amp.setHelicity(h);
  int fact_h[] = {+1, h[2], h[3], h[4], h[5]};
  int split_h[] = {-1, h[0], h[1]};

  for (int p=10; p<15; p++) {
    mom = getCollinearPoint6<T>(pow(3.,-p));
//    for (int i=0; i<5; i++) {
//      for (int j=i+1; j<6; j++) {
//        cout <<"s"<<i<<j<< "  = " <<  S(mom[i]+mom[j]) << endl;
//        for (int k=j+1; k<6; k++) {
//          cout <<"s"<<i<<j<<k<< " = " << S(mom[i]+mom[j]+mom[k]) << endl;
//        }
//      }
//    }
    amp.setMomenta(mom);

    // Catani-Seymour momentum mapping 12;3
    const MOM<T> p1 = mom[0];
    const MOM<T> p2 = mom[1];
    const MOM<T> p12 = p1+p2;
    const MOM<T> p3 = mom[2];
    const T s12 = S(p12);
    const T x12_3 = s12/T(2.)/dot(p12,p3);
    const MOM<T> p12t = p12 - x12_3*p3;
    const MOM<T> p3t = (1.+x12_3)*p3;
    fact_mom[0] = p12t;
    fact_mom[1] = p3t;
    fact_mom[2] = mom[3];
    fact_mom[3] = mom[4];
    fact_mom[4] = mom[5];

    fact_amp.setMomenta(fact_mom);
    split.setMomenta(p12,p1,p2,p3);

    complex<T> limittree = T();
    EpsTriplet<T> limitloopG;
    EpsTriplet<T> limitloopF;
    fact_h[0] = +1;
    split_h[0] = -1;
    fact_amp.setHelicity(fact_h);
    split.setHelicity(split_h);
    limittree += split.evalTree()*fact_amp.evalTree();
    limitloopG += split.eval(0)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(0);
    limitloopF += split.eval(1)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(1);
//    cout << "limitF a = " << limitloopF << endl;

    fact_h[0] = -1;
    split_h[0] = +1;
    fact_amp.setHelicity(fact_h);
    split.setHelicity(split_h);
    limittree += split.evalTree()*fact_amp.evalTree();
    limitloopG += split.eval(0)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(0);
    limitloopF += split.eval(1)*fact_amp.evalTree() + split.evalTree()*fact_amp.eval(1);
//    cout << "limitF a+b = " << limitloopF << endl;

    const complex<T> tree = amp.evalTree();
    const EpsTriplet<T> loopG = amp.eval(0);
    const EpsTriplet<T> loopF = amp.eval(1);

    cout << "### A6g        = " << tree << endl;
    cout << "### Sp x A5g   = " << limittree << endl;
    cout << "### tree/limit = " << chop(tree)/chop(limittree) << endl;
    // 0 = mixed / gluon primitive
    cout << "### A6[g]       = " << loopG << endl;
    cout << "### Sp x A5[g]  = " << limitloopG << endl;
    cout << "### loopG/limit = " << ratio(loopG, limitloopG) << endl;
    // 1 = fermion loop primitive
    cout << "## A6[f]       = " << loopF << endl;
    cout << "## Sp x A5[f]  = " << limitloopF << endl;
    cout << "### loopF/limit = " << ratio(loopF, limitloopF) << endl;
  }

}

std::vector<int> getHelicity(const int n, const int hidx)
{
  std::vector<int> out(n);
  std::bitset<32> hhh(hidx);
  for (int i=0; i<n; i++) {
    out[i] = 2*hhh[i]-1;
  }
  return out;
}

int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  const Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };

  for (int i=0; i<64; i++) {
    std::vector<int> hhh = getHelicity(6,i);
    cout << endl << "#limit# " << "helicity " << i << " : ";
    for (int k=0; k<6; k++) {
      cout << std::showpos << hhh[k] << " ";
    }
    cout << endl;
//    cout << "### limit double prec. (16) digits" << endl;
//    checkCollinear<double>(flavours, hhh.data());
    cout << "### limit quadruple prec. (32) digits" << endl;
    checkCollinear<dd_real>(flavours, hhh.data());
//    cout << "### limit octuple prec. (64) digits" << endl;
//    checkCollinear<qd_real>(flavours, hhh.data());
  };

  return 1;
}
