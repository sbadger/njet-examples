#include <fstream>

#include "split3.h"

#include "chsums/0q4g.h"
#include "chsums/0q5g.h"
#include "chsums/0q6g.h"
#include "chsums/0q7g.h"
#include "chsums/2q5g.h"
#include "chsums/2q4g.h"

#ifndef NNPSP
#define NNPSP 
#endif

namespace {

  bool DBG_PRINT = true;//false;

  // generate triple collinear phase-space point for 2->4
  template <typename T>
  std::vector<MOM<T> > getTripleCollinearPoint6(const double lambda)
  {

    StandardModel::setHmass(lambda);
    const Flavour<double> flavours[] = {
      StandardModel::G(),
      StandardModel::G(),
      StandardModel::Higgs(),
      StandardModel::G()
    };
    PhaseSpace<T> ps(4, flavours, 1, 1e3);
    //for (int i = 0; i<NNPSP; ++i)
      ps.getPSpoint();
    //  ps.showPSpoint();

    const T s123 = S(ps[2]);
    const T x1234 = s123/dot(ps[2],ps[3])*T(0.5);
    MOM<T> p123t = ps[2]-x1234*ps[3];

    cout << endl;
    cout << "### s123 = " << s123 << endl;

    // map to 2->4 configuration //
    const T z1 = 0.545;
    const T z2 = 0.424;
    const T z3 = 1.-z1-z2;

    MOM<complex<T> > e12 = CMOM(p123t,ps[3]);
    MOM<complex<T> > e21 = CMOM(ps[3],p123t);

    MOM<complex<T> > v1_c = i_*(e12-e21);
    MOM<complex<T> > v2_c = (e12+e21);
    if (DBG_PRINT) {
      cout << v1_c << endl;
      cout << v2_c << endl;
    }

    MOM<T> v1(real(v1_c.x0), real(v1_c.x1), real(v1_c.x2), real(v1_c.x3));
    MOM<T> v2(real(v2_c.x0), real(v2_c.x1), real(v2_c.x2), real(v2_c.x3));

    T y11 = 0.124*sqrt(s123);
    T y12 = 0.456*sqrt(s123);
    T y21 = 0.294*sqrt(s123);
    T y22 = 0.835*sqrt(s123);

    T y13 = -y11-y12;
    T y23 = -y21-y22;

    MOM<T> kt1 = y11*v1 + y21*v2;
    MOM<T> kt2 = y12*v1 + y22*v2;
    MOM<T> kt3 = y13*v1 + y23*v2;

    T mshift = (S(kt1)/z1+S(kt2)/z2+S(kt3)/z3)*x1234/s123;

    std::vector<MOM<T> >  Mom(6);
    Mom[4] = ps[0];
    Mom[5] = ps[1];
    Mom[0] = z1*p123t + kt1 - S(kt1)/z1*x1234/s123*ps[3];
    Mom[1] = z2*p123t + kt2 - S(kt2)/z2*x1234/s123*ps[3];
    Mom[2] = z3*p123t + kt3 - S(kt3)/z3*x1234/s123*ps[3];
    Mom[3] = (1. + x1234 + mshift)*ps[3];
    // Mom[6] = ps[4];

    MOM<T> mcons;
    if (DBG_PRINT) {
      for (int i=0; i<6; i++) {
        mcons += Mom[i];
        cout << "p"<<i<<"^2 = "<< S(Mom[i]) << endl;
      }
      cout << mcons << endl;
    }

    return Mom;
  }


  
  template <typename T>
  std::vector<MOM<T> > getTripleCollinearPoint7(const double lambda)
  {

    StandardModel::setHmass(lambda);
    const Flavour<double> flavours[] = {
      StandardModel::G(),
      StandardModel::G(),
      StandardModel::Higgs(),
      StandardModel::G(),
      StandardModel::G()
    };
    PhaseSpace<T> ps(5, flavours, 1, 1e3);
    ps.getPSpoint();
    //  ps.showPSpoint();

    const T s123 = S(ps[2]);
    const T x1234 = s123/dot(ps[2],ps[3])*T(0.5);
    MOM<T> p123t = ps[2]-x1234*ps[3];

    cout << endl;
    cout << "### s123 = " << s123 << endl;

    // map to 2->4 configuration //
    const T z1 = 0.545;
    const T z2 = 0.424;
    const T z3 = 1.-z1-z2;

    MOM<complex<T> > e12 = CMOM(p123t,ps[3]);
    MOM<complex<T> > e21 = CMOM(ps[3],p123t);

    MOM<complex<T> > v1_c = i_*(e12-e21);
    MOM<complex<T> > v2_c = (e12+e21);
    if (DBG_PRINT) {
      cout << v1_c << endl;
      cout << v2_c << endl;
    }

    MOM<T> v1(real(v1_c.x0), real(v1_c.x1), real(v1_c.x2), real(v1_c.x3));
    MOM<T> v2(real(v2_c.x0), real(v2_c.x1), real(v2_c.x2), real(v2_c.x3));

    T y11 = 0.124*sqrt(s123);
    T y12 = 0.456*sqrt(s123);
    T y21 = 0.294*sqrt(s123);
    T y22 = 0.835*sqrt(s123);

    T y13 = -y11-y12;
    T y23 = -y21-y22;

    MOM<T> kt1 = y11*v1 + y21*v2;
    MOM<T> kt2 = y12*v1 + y22*v2;
    MOM<T> kt3 = y13*v1 + y23*v2;

    T mshift = (S(kt1)/z1+S(kt2)/z2+S(kt3)/z3)*x1234/s123;

    std::vector<MOM<T> >  Mom(7);
    Mom[4] = ps[0];
    Mom[5] = ps[1];
    Mom[0] = z1*p123t + kt1 - S(kt1)/z1*x1234/s123*ps[3];
    Mom[1] = z2*p123t + kt2 - S(kt2)/z2*x1234/s123*ps[3];
    Mom[2] = z3*p123t + kt3 - S(kt3)/z3*x1234/s123*ps[3];
    Mom[3] = (1. + x1234 + mshift)*ps[3];
    Mom[6] = ps[4];

    MOM<T> mcons;
    if (DBG_PRINT) {
      for (int i=0; i<7; i++) {
        mcons += Mom[i];
        cout << "p"<<i<<"^2 = "<< S(Mom[i]) << endl;
      }
      cout << mcons << endl;
    }

    return Mom;
  }


  template <typename T>
  complex<T> my_scale_test(const EpsTriplet<T> & ain, const EpsTriplet<T> & bin, int i)
  {
    complex<T> a, b;
    switch (i) {
    case 0:
      a = ain.get0();
      b = bin.get0();
      break;
    case 1:
      a = ain.get1();
      b = bin.get1();
      break;
    case 2:
      a = ain.get2();
      b = bin.get2();
      break;
    }
    a /= ain.get2();
    b /= bin.get2();
    return T(2)*(a-b)/(a+b);
  }

  template <typename T>
  void checkTripleCollinear6g() {
    const int nn = 6;

    std::vector<MOM<T> > mom(nn);
    std::vector<MOM<T> > fact_mom(nn-2);

#define MAKE_PLOTS  1
#if MAKE_PLOTS
    std::ofstream outlimt;
    std::ofstream outampt;
    outampt.open("ampt.out");
    outlimt.open("limt.out");
    outampt.setf(std::ios_base::scientific, std::ios_base::floatfield);
    outlimt.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(30);
    outampt.precision(30);
    outlimt.precision(30);
#endif // MAKE_PLOTS

#define DO_VIRT 1

#if MAKE_PLOTS    
#if DO_VIRT    
    std::ofstream outlim;
    std::ofstream outamp;
    outamp.open("amp.out");
    outlim.open("lim.out");
    outamp.setf(std::ios_base::scientific, std::ios_base::floatfield);
    outlim.setf(std::ios_base::scientific, std::ios_base::floatfield);
    outamp.precision(20);
    outlim.precision(20);
#endif // DO_VIRT
#endif // MAKE_PLOTS

    for (double p=3; p<28; p += 0.5) {
      mom = getTripleCollinearPoint6<T>(pow(10.,-double(p)/double(2)));

      // Catani-Seymour momentum mapping 123;4
      const MOM<T> p1 = mom[0];
      const MOM<T> p2 = mom[1];
      const MOM<T> p3 = mom[2];
      const MOM<T> p123 = p1+p2+p3;
      const MOM<T> p4 = mom[3];
      const T s123 = S(p123);
      const T x123_4 = s123/T(2.)/dot(p123,p4);
      const MOM<T> p123t = p123 - x123_4*p4;
      const MOM<T> p4t = (1.+x123_4)*p4;
      fact_mom[0] = p123t;
      fact_mom[1] = p4t;
      fact_mom[2] = mom[4];
      fact_mom[3] = mom[5];

      {
        //NJetAm
        const T MuR = 1e3/7.;
        Amp0q6g<T> amp6(1.);
        Amp0q4g<T> amp4(1.);
        Split3g<T> split3;
        amp6.setNf(5);
        amp4.setNf(5);
        split3.setNf(5);
        amp6.setMuR2(MuR*MuR);
        amp4.setMuR2(MuR*MuR);
        split3.setMuR2(MuR*MuR);
        // amp6.setNc(1000);
        // amp4.setNc(1000);
        // split3.setNC(1000);
      
        amp6.setMomenta(mom);
        amp4.setMomenta(fact_mom);
        split3.setMomenta(p123,p1,p2,p3,p4);

        std::cout << "Tree-level:" << std::endl;
        std::cout << "-----------" << std::endl;
        
        complex<T> amp4spmat[4];
        StaticMatrix< complex<T> > sp3mat;
        amp4.born_spnmatrix(0,amp4spmat);
        split3.born_spnmatrix(sp3mat);
        complex<T> tlimit =  ( sp3mat(0,0) * amp4spmat[3] + sp3mat(0,1) * amp4spmat[2]
                               + sp3mat(1,0) * amp4spmat[1] + sp3mat(1,1) * amp4spmat[0] );

        std::cout << "amp4 = " <<  amp4.born() << endl;
        std::cout << "sp3mat = \n|" << sp3mat(1,1) << ", " << sp3mat(1,0) << "|\n|" << sp3mat(0,1) << ", " << sp3mat(0,0) << "|" << std::endl;
        std::cout << "amp4spmat = \n|" << amp4spmat[0] << ", " << amp4spmat[1] << "|\n|" << amp4spmat[2] << ", " << amp4spmat[3] << "|" << std::endl;
        std::cout << "amp6 = " <<  amp6.born() << endl;
        std::cout << "Ratio = " << amp6.born() / tlimit << std::endl;

#if MAKE_PLOTS        
        outampt << amp6.born() << std::endl;
        outlimt << tlimit << std::endl;
#endif // MAKE_PLOTS

#if DO_VIRT
        std::cout << "One-loop:" << std::endl;
        std::cout << "---------" << std::endl;

        EpsTriplet<T> amp4spmatv[4];
        StaticMatrix< EpsTriplet<T> > sp3matv;
        amp4.virt_spnmatrix(0,amp4spmatv);
        split3.virt_spnmatrix(sp3matv);
        EpsTriplet<T> amp6v = amp6.virt();
        EpsTriplet<T> llimit =  (sp3mat(0,0) * amp4spmatv[3] + sp3mat(0,1) * amp4spmatv[2]
                                 + sp3mat(1,0) * amp4spmatv[1] + sp3mat(1,1) * amp4spmatv[0]
                                 + sp3matv(0,0) * amp4spmat[3] + sp3matv(0,1) * amp4spmat[2]
                                 + sp3matv(1,0) * amp4spmat[1] + sp3matv(1,1) * amp4spmat[0]).real();

        std::cout << "amp4 = " <<  amp4.virt() << endl;
        std::cout << "sp3mat = \n|" << sp3matv(1,1) << ", " << sp3matv(1,0) << "|\n|" << sp3matv(0,1) << ", " << sp3matv(0,0) << "|" << std::endl;
        std::cout << "amp4spmatv = \n|" << amp4spmatv[0] << ", " << amp4spmatv[1] << "|\n|" << amp4spmatv[2] << ", " << amp4spmatv[3] << "|" << std::endl;
        std::cout << "amp6 = " <<  amp6v << endl;
        std::cout << "Ratio (finite) = " << amp6v.get0() / llimit.get0() << std::endl;
        std::cout << "Ratio (pole 1) = " << amp6v.get1() / llimit.get1() << std::endl;
        std::cout << "Ratio (pole 2) = " << amp6v.get2() / llimit.get2() << std::endl;
        std::cout << "amp6 = " <<  amp6v/amp6.born() << endl;

#if MAKE_PLOTS        
        outamp << amp6v.get0() << ",,," << amp6v.get1() << ",,," << amp6v.get2() << std::endl;
        outlim << llimit.get0() << ",,," << llimit.get1() << ",,," << llimit.get2() << std::endl;
#endif // MAKE_PLOTS

#endif // DO_VIRT
      }

    }

  }

#define xstr(s) str(s)
#define str(s) #s

  template <typename T>
  void checkTripleCollinear6q() {
    const int nn = 6;

    std::vector<MOM<T> > mom(nn);
    std::vector<MOM<T> > fact_mom(nn-2);

#undef MAKE_PLOTS
#define MAKE_PLOTS  1
#if MAKE_PLOTS
    std::ofstream outlimt;
    std::ofstream outampt;
    outampt.open("ampqt" xstr(NNPSP) ".out");
    outlimt.open("limqt" xstr(NNPSP) ".out");
    outampt.setf(std::ios_base::scientific, std::ios_base::floatfield);
    outlimt.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(30);
    outampt.precision(30);
    outlimt.precision(30);
#endif // MAKE_PLOTS

#define DO_VIRT 1


    cout.precision(30);
    cerr.precision(30);
#if MAKE_PLOTS    
#if DO_VIRT    
    std::ofstream outlim;
    std::ofstream outamp;
    std::ofstream outlim_a;
    std::ofstream outlim_s;
    outamp.open("ampq"  xstr(NNPSP) ".out");
    outlim.open("limq"  xstr(NNPSP) ".out");
    outlim_s.open("ampq"  xstr(NNPSP) "_s.out");
    outlim_a.open("limq"  xstr(NNPSP) "_a.out");
    outamp.setf(std::ios_base::scientific, std::ios_base::floatfield);
    outlim.setf(std::ios_base::scientific, std::ios_base::floatfield);
    outamp.precision(20);
    outlim.precision(20);
#endif // DO_VIRT
#endif // MAKE_PLOTS

    for (double p=3; p<24; p += 0.5) {
      mom = getTripleCollinearPoint6<T>(pow(10.,-double(p)/double(2)));

      // Catani-Seymour momentum mapping 123;4
      const MOM<T> p1 = mom[0];
      const MOM<T> p2 = mom[1];
      const MOM<T> p3 = mom[2];
      const MOM<T> p123 = p1+p2+p3;
      const MOM<T> p4 = mom[3];
      const T s123 = S(p123);
      const T x123_4 = s123/T(2.)/dot(p123,p4);
      const MOM<T> p123t = p123 - x123_4*p4;
      const MOM<T> p4t = (1.+x123_4)*p4;
      fact_mom[0] = p123t;
      fact_mom[1] = p4t;
      fact_mom[2] = mom[4];
      fact_mom[3] = mom[5];

      {
        //NJetAm
        const T MuR = 1e3/7.;
        const T nf = 5;
        Amp2q4g<T> amp6(1.);
        Amp0q4g<T> amp4(1.);
        Split3q<T> split3;
        amp6.setNf(nf);
        amp4.setNf(nf);
        split3.setNf(nf);
        amp6.setMuR2(MuR*MuR);
        amp4.setMuR2(MuR*MuR);
        split3.setMuR2(MuR*MuR);
        // amp6.setNc(1000);
        // amp4.setNc(1000);
        // split3.setNC(1000);
      
        amp6.setMomenta(mom);
        amp4.setMomenta(fact_mom);
        split3.setMomenta(p123,p1,p2,p3,p4);

        std::cout << "Tree-level:" << std::endl;
        std::cout << "-----------" << std::endl;
        
        complex<T> amp4spmat[4];
        StaticMatrix< complex<T> > sp3mat;
        amp4.born_spnmatrix(0,amp4spmat);
        split3.born_spnmatrix(sp3mat);
        complex<T> tlimit =  ( sp3mat(0,0) * amp4spmat[3] + sp3mat(0,1) * amp4spmat[2]
                               + sp3mat(1,0) * amp4spmat[1] + sp3mat(1,1) * amp4spmat[0] );

        std::cout << "amp4 = " <<  amp4.born() << endl;
        std::cout << "sp3mat = \n|" << sp3mat(1,1) << ", " << sp3mat(1,0) << "|\n|" << sp3mat(0,1) << ", " << sp3mat(0,0) << "|" << std::endl;
        std::cout << "amp4spmat = \n|" << amp4spmat[0] << ", " << amp4spmat[1] << "|\n|" << amp4spmat[2] << ", " << amp4spmat[3] << "|" << std::endl;
        std::cout << "amp6 = " <<  amp6.born() << endl;
        std::cout << "Ratio = " << amp6.born() / tlimit << std::endl;

#if MAKE_PLOTS        
        outampt << amp6.born() << std::endl;
        outlimt << tlimit << std::endl;
#endif // MAKE_PLOTS

#if DO_VIRT
        std::cout << "One-loop:" << std::endl;
        std::cout << "---------" << std::endl;

        EpsTriplet<T> amp4spmatv[4];
        StaticMatrix< EpsTriplet<T> > sp3matv;
        amp4.virt_spnmatrix(0,amp4spmatv);
        split3.virt_spnmatrix(sp3matv);
        EpsTriplet<T> amp6v = amp6.virt();
        EpsTriplet<T> llimit =  (sp3mat(0,0) * amp4spmatv[3] + sp3mat(0,1) * amp4spmatv[2]
                                 + sp3mat(1,0) * amp4spmatv[1] + sp3mat(1,1) * amp4spmatv[0]
                                 + sp3matv(0,0) * amp4spmat[3] + sp3matv(0,1) * amp4spmat[2]
                                 + sp3matv(1,0) * amp4spmat[1] + sp3matv(1,1) * amp4spmat[0]).real();
        EpsTriplet<T> llimit_s =  (sp3matv(0,0) * amp4spmat[3] + sp3matv(0,1) * amp4spmat[2]
                                 + sp3matv(1,0) * amp4spmat[1] + sp3matv(1,1) * amp4spmat[0]).real();
        EpsTriplet<T> llimit_a =  (sp3mat(0,0) * amp4spmatv[3] + sp3mat(0,1) * amp4spmatv[2]
                                 + sp3mat(1,0) * amp4spmatv[1] + sp3mat(1,1) * amp4spmatv[0]).real();

        std::cout << "amp4 = " <<  amp4.virt() << endl;
        std::cout << "sp3mat = \n|" << sp3matv(1,1) << ", " << sp3matv(1,0) << "|\n|" << sp3matv(0,1) << ", " << sp3matv(0,0) << "|" << std::endl;
        std::cout << "amp4spmatv = \n|" << amp4spmatv[0] << ", " << amp4spmatv[1] << "|\n|" << amp4spmatv[2] << ", " << amp4spmatv[3] << "|" << std::endl;
        std::cout << "amp6 = " <<  amp6v << endl;
        std::cout << "amp6/born = " <<  amp6v/amp6.born() << endl;
        std::cout << "Ratio (finite) = " << amp6v.get0() / llimit.get0() << std::endl;
        std::cout << "Ratio (pole 1) = " << amp6v.get1() / llimit.get1() << std::endl;
        std::cout << "Ratio (pole 2) = " << amp6v.get2() / llimit.get2() << std::endl;

#if MAKE_PLOTS        
        outamp << amp6v.get0() << ",,," << amp6v.get1() << ",,," << amp6v.get2() << std::endl;
        outlim << llimit.get0() << ",,," << llimit.get1() << ",,," << llimit.get2() << std::endl;
        outlim_s << llimit_s.get0() << ",,," << llimit_s.get1() << ",,," << llimit_s.get2() << std::endl;
        outlim_a << llimit_a.get0() << ",,," << llimit_a.get1() << ",,," << llimit_a.get2() << std::endl;
#endif // MAKE_PLOTS

#endif // DO_VIRT
      }

    }

  }


    template <typename T>
    void checkTripleCollinear7g() {
    const int nn = 7;

    std::vector<MOM<T> > mom(nn);
    std::vector<MOM<T> > fact_mom(nn-2);

    for (double p=9; p<11; p += 0.5) {
      mom = getTripleCollinearPoint7<T>(pow(10.,-double(p)/double(2)));

      // Catani-Seymour momentum mapping 123;4
      const MOM<T> p1 = mom[0];
      const MOM<T> p2 = mom[1];
      const MOM<T> p3 = mom[2];
      const MOM<T> p123 = p1+p2+p3;
      const MOM<T> p4 = mom[3];
      const T s123 = S(p123);
      const T x123_4 = s123/T(2.)/dot(p123,p4);
      const MOM<T> p123t = p123 - x123_4*p4;
      const MOM<T> p4t = (1.+x123_4)*p4;
      fact_mom[0] = p123t;
      fact_mom[1] = p4t;
      fact_mom[2] = mom[4];
      fact_mom[3] = mom[5];
      fact_mom[4] = mom[6];

      {
        //NJetAm
        const T MuR = 1e3/7.;
        Amp0q7g<T> amp7(1.);
        Amp0q5g<T> amp5(1.);
        Split3g<T> split3;

        const T Nc = 3;

#undef MAKE_PLOTS
#define MAKE_PLOTS  0
#if MAKE_PLOTS
        std::ofstream outlimt;
        std::ofstream outampt;
        outampt.open("ampt7.out");
        outlimt.open("limt7.out");
        outampt.setf(std::ios_base::scientific, std::ios_base::floatfield);
        outlimt.setf(std::ios_base::scientific, std::ios_base::floatfield);
        cout.precision(30);
        outampt.precision(30);
        outlimt.precision(30);
#endif // MAKE_PLOTS

#undef DO_VIRT
#define DO_VIRT 1

#if MAKE_PLOTS    
#if DO_VIRT    
        std::ofstream outlim;
        std::ofstream outamp;
        outamp.open("amp7.out");
        outlim.open("lim7.out");
        outamp.setf(std::ios_base::scientific, std::ios_base::floatfield);
        outlim.setf(std::ios_base::scientific, std::ios_base::floatfield);
        outamp.precision(20);
        outlim.precision(20);
#endif // DO_VIRT
#endif // MAKE_PLOTS

        amp7.setNc(Nc);
        amp5.setNc(Nc);
        split3.setNC(Nc);
        amp5.setNf(5);
        amp7.setNf(5);
        split3.setNf(5);
        amp7.setMuR2(MuR*MuR);
        amp5.setMuR2(MuR*MuR);
        split3.setMuR2(MuR*MuR);
        amp7.setMomenta(mom);
        amp5.setMomenta(fact_mom);
        split3.setMomenta(p123,p1,p2,p3,p4);

        complex<T> amp5spmat[4];
        StaticMatrix< complex<T> > sp3mat;
        std::cout << "amp5 = " <<  amp5.born() << endl;
        amp5.born_spnmatrix(0,amp5spmat);
        split3.born_spnmatrix(sp3mat);

        std::cout << "sp3mat = \n|" << sp3mat(1,1) << ", " << sp3mat(1,0) << "|\n|" << sp3mat(0,1) << ", " << sp3mat(0,0) << "|" << std::endl;
        std::cout << "amp5spmat = \n|" << amp5spmat[0] << ", " << amp5spmat[1] << "|\n|" << amp5spmat[2] << ", " << amp5spmat[3] << "|" << std::endl;
        std::cout << "amp7 = " <<  amp7.born() << endl;
        complex<T> tlimit = sp3mat(0,0) * amp5spmat[0] + sp3mat(0,1) * amp5spmat[1]
          + sp3mat(1,0) * amp5spmat[2] + sp3mat(1,1) * amp5spmat[3];
        std::cout << "limit = " << tlimit << endl;
        std::cout << "Ratio = " << amp7.born()/tlimit;

#if MAKE_PLOTS        
        outampt << amp7.born() << std::endl;
        outlimt << tlimit << std::endl;
#endif // MAKE_PLOTS


#if DO_VIRT
        std::cout << "One-loop:" << std::endl;
        std::cout << "---------" << std::endl;

        EpsTriplet<T> amp5spmatv[4];
        StaticMatrix< EpsTriplet<T> > sp3matv;
        amp5.virt_spnmatrix(0,amp5spmatv);
        split3.virt_spnmatrix(sp3matv);
        EpsTriplet<T> llimit =  (sp3mat(0,0) * amp5spmatv[3] + sp3mat(0,1) * amp5spmatv[2]
                                 + sp3mat(1,0) * amp5spmatv[1] + sp3mat(1,1) * amp5spmatv[0]
                                 + sp3matv(0,0) * amp5spmat[3] + sp3matv(0,1) * amp5spmat[2]
                                 + sp3matv(1,0) * amp5spmat[1] + sp3matv(1,1) * amp5spmat[0]).real();

        std::cout << "amp5 = " <<  amp5.virt() << endl;
        std::cout << "sp3mat = \n|" << sp3matv(1,1) << ", " << sp3matv(1,0) << "|\n|" << sp3matv(0,1) << ", " << sp3matv(0,0) << "|" << std::endl;
        std::cout << "amp5spmatv = \n|" << amp5spmatv[0] << ", " << amp5spmatv[1] << "|\n|" << amp5spmatv[2] << ", " << amp5spmatv[3] << "|" << std::endl;
        EpsTriplet<T> amp7v = amp7.virt();
        std::cout << "amp7 = " <<  amp7v << endl;
        std::cout << "Ratio (finite) = " << amp7v.get0() / llimit.get0() << std::endl;
        std::cout << "Ratio (pole 1) = " << amp7v.get1() / llimit.get1() << std::endl;
        std::cout << "Ratio (pole 2) = " << amp7v.get2() / llimit.get2() << std::endl;

#if MAKE_PLOTS        
        outamp << amp7v.get0() << ",,," << amp7v.get1() << ",,," << amp7v.get2() << std::endl;
        outlim << llimit.get0() << ",,," << llimit.get1() << ",,," << llimit.get2() << std::endl;
#endif // MAKE_PLOTS
#endif // DO_VIRT        
      
      }


    }

  }


    template <typename T>
    void checkTripleCollinear2q5g() {
      const int nn = 7;

      std::vector<MOM<T> > mom(nn);
      std::vector<MOM<T> > fact_mom(nn-2);

      for (int p=10; p<15; p++) {
        mom = getTripleCollinearPoint7<T>(pow(10.,-p));

        // Catani-Seymour momentum mapping 123;4
        const MOM<T> p1 = mom[0];
        const MOM<T> p2 = mom[1];
        const MOM<T> p3 = mom[2];
        const MOM<T> p123 = p1+p2+p3;
        const MOM<T> p4 = mom[3];
        const T s123 = S(p123);
        const T x123_4 = s123/T(2.)/dot(p123,p4);
        const MOM<T> p123t = p123 - x123_4*p4;
        const MOM<T> p4t = (1.+x123_4)*p4;
        fact_mom[0] = p123t;
        fact_mom[1] = p4t;
        fact_mom[2] = mom[4];
        fact_mom[3] = mom[5];
        fact_mom[4] = mom[6];

        {
          //NJetAm
          Amp2q5g<T> amp7(1.);
          Amp0q5g<T> amp5(1.);
          Split3q<T> split3;
      
          amp7.setMomenta(mom);
          amp5.setMomenta(fact_mom);
          split3.setMomenta(p123,p1,p2,p3,p4);

          complex<T> amp5spmat[4];
          StaticMatrix< complex<T> > sp3mat;
          std::cout << "amp5 = " <<  amp5.born() << endl;
          amp5.born_spnmatrix(0,amp5spmat);
          split3.born_spnmatrix(sp3mat);

          std::cout << "sp3mat = \n|" << sp3mat(1,1) << ", " << sp3mat(1,0) << "|\n|" << sp3mat(0,1) << ", " << sp3mat(0,0) << "|" << std::endl;
          std::cout << "amp5spmat = \n|" << amp5spmat[0] << ", " << amp5spmat[1] << "|\n|" << amp5spmat[2] << ", " << amp5spmat[3] << "|" << std::endl;
          std::cout << "amp7 = " <<  amp7.born() << endl;
          complex<T> limit = sp3mat(0,0) * amp5spmat[0] + sp3mat(0,1) * amp5spmat[1]
            + sp3mat(1,0) * amp5spmat[2] + sp3mat(1,1) * amp5spmat[3];
          std::cout << "limit = " << limit << endl;
          std::cout << "Ratio = " << amp7.born()/limit;
      
        }


      }

    }

  void get_helicity(int h, int n, int * hh)
  {
    int mask = 1;
    for (int i=0; i<n; ++i) {
      hh[i] = 2*(h & mask)-1;
      h >>= 1;
    }
  }

} // namepsace


int main()
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  cout << "===========================" << endl;
  cout << "Checking 6g --> 4g * Split3" << endl;
  cout << "===========================" << endl;
  //checkTripleCollinear6g<qd_real>();
  //checkTripleCollinear6g<dd_real>();
  //checkTripleCollinear6g<double>();

  cout << endl << endl;

  cout << "===============================================" << endl;
  cout << "Checking qbar q + 4g --> (qbar q + 4g) * Split3" << endl;
  cout << "===============================================" << endl;
  //checkTripleCollinear6q<qd_real>();
  //checkTripleCollinear6q<dd_real>();
  //checkTripleCollinear6q<double>();

  cout << endl << endl;

  cout << "===========================" << endl;
  cout << "Checking 7g --> 5g * Split3" << endl;
  cout << "===========================" << endl;
  //checkTripleCollinear7g<double>();

  cout << endl << endl;

  cout << "===================================================" << endl;
  cout << "Checking qbar q + 4g --> 5g * Split(qbar q g --> g)" << endl;
  cout << "===================================================" << endl;
  //checkTripleCollinear2q5g<qd_real>();

  cout << endl << endl;

  //check_draft<qd_real>();

  return 0;
}
