#include "split3.h"

#undef USING_BASE_MEMBERS2
#define USING_BASE_MEMBERS2 \
  using Base::p;            \
  using Base::n; \
  using Base::z; \
  using Base::MuR2; \
  using Base::nf; \
  using Base::s123; \
  using Base::zspA; \
  using Base::zspB; \
  using Base::wspA; \
  using Base::wspB; \
  using Base::hidx; \
  using Base::hel; \
  using Base::ord; \
  using Base::pole; \
  using Base::poleR; \
  using Base::poleR2; \
  using Base::poleF; \
  using Base::poleX2; \
  using Base::FMHV; \
  using Base::FNMHV1; \
  using Base::FNMHV2; \
  using Base::FNMHV3; \
  using Base::F1mBox; \
  using Base::L0hat; \
  using Base::L1hat; \
  using Base::L2hat; \
  using Base::L3hat; \
  using Base::nc; \
  using Base::sij; \
  using Base::spA; \
  using Base::spB; \
  using Base::r; \
  using Base::rt; \
  using Base::rcc; \
  using Base::rtcc; \
  using Base::X; \
  using Base::Xcc; \
  using Base::yspA; \
  using Base::yspB

template <typename T>
class TestDraftSplit3Base {

public:

  static const int default_order[4];

  typedef StaticMatrix<T,2> SPMatrix;
  typedef StaticMatrix<T,2> CMatrix;

  TestDraftSplit3Base();
  virtual ~TestDraftSplit3Base() {}

  void born_spnmatrix(StaticMatrix< complex<T> > & spmatrix)
  {
    born_spnmatrix(0,spmatrix);    
  }

  void setMomenta(const MOM<T> P123,
                  const  MOM<T> p1, const MOM<T> p2, const MOM<T> p3,
                  const MOM<T> n_)
  {
    p[0] = P123;
    p[1] = p1;
    p[2] = p2;
    p[3] = p3;
    n = n_;
    setMomenta_(p[ord[0]],p[ord[1]],p[ord[2]],p[ord[3]],n);
  }

  void setOrder(const int* ord_) {
    ord[0] = ord_[0];
    ord[1] = ord_[1];
    ord[2] = ord_[2];
    ord[3] = ord_[3];
    setMomenta_(p[ord[0]],p[ord[1]],p[ord[2]],p[ord[3]],n);
    sethidx_();
  }

  void setOrder(int i0, int i1, int i2, int i3) {
    int ord_[4]  = {i0,i1,i2,i3};
    setOrder(ord_);
  }

  void setMuR2(const T mur2) {
    MuR2 = mur2;
  }

  void setHelicity(const int* hels) {
    hel[0] = hels[0];
    hel[1] = hels[1];
    hel[2] = hels[2];
    hel[3] = hels[3];
    sethidx_();
  }

  void setNC(T nc_)
  {
    nc = nc_;
  }

  void setNf(T nf_)
  {
    nf = nf_;
  }

  // for debugging
  int gethidx()
  {
    return hidx;
  }

public:

  void setMomenta_(const MOM<T> P123,
                   const  MOM<T> p1, const MOM<T> p2, const MOM<T> p3,
                   const MOM<T> n_);

  void sethidx_()
  {
    hidx = (hel[ord[0]]+1)/2 + 2*(hel[ord[1]]+1)/2 + 4*(hel[ord[2]]+1)/2 + 8*(hel[ord[3]]+1)/2;
  }

  // log and polylog functions //
  complex<T> mLog(T s);
  static complex<T> rLog(T s1, T s2);
  static complex<T> rDiLog(T s1, T s2);
  static complex<T> L0hat(T s1, T s2);
  static complex<T> L1hat(T s1, T s2);
  static complex<T> L2hat(T s1, T s2);
  static complex<T> L3hat(T s1, T s2);
  EpsTriplet<T> pSS(T s);
  EpsTriplet<T> pSS1(T s);
  
  EpsTriplet<T> pole(const int * o);
  EpsTriplet<T> poleR(const int * o);
  EpsTriplet<T> poleR2(const int * o);
  EpsTriplet<T> poleF(const int * o);
  EpsTriplet<T> poleX2(int type, const int * o);
  complex<T> FMHV(const int * o);
  complex<T> FNMHV1(const int * o);
  complex<T> FNMHV2(const int * o);
  complex<T> FNMHV3(const int * o);
  complex<T> F1mBox(const int * o);

  MOM<T> p[4];
  MOM<T> n;

  T z[4], y[4];
  T MuR2, s12, s23, s13, s123, nc, nf;
  StaticMatrix<T,4,4> sij;
  StaticMatrix<complex<T>,4,4> spA, spB, r, rt, rcc, rtcc, X, Xcc;
  complex<T> zspA[4], zspB[4], wspA[4], wspB[4], yspA[4], yspB[4];
  int hidx, hel[4], ord[4];

};

template <typename T>
const int TestDraftSplit3Base<T>::default_order[4] = {0,1,2,3};

template <typename T>
class TestDraftSplit3g : public TestDraftSplit3Base<T> {
public :

  typedef TestDraftSplit3Base<T> Base;

  using Base::default_order;

  enum FlavoursLoop {GLOOP=0, FLOOP=1, NEQ4, NEQ1, NEQ0,
                     FLAVOURS_LOOP_END};

  TestDraftSplit3g() : Base() {}



public:

  USING_BASE_MEMBERS2;

};



template <typename T>
class TestDraftSplit3q : public TestDraftSplit3g<T> {
public :

  typedef TestDraftSplit3g<T> Base;

  enum FlavoursTree {QBQG, QBGQ};

  enum FlavoursLoop {QBQG_R, QBQG_F, QBQG_NEQ4, QBQG_X, QBQG_L,
                     QBGQ_NEQ4, QBGQ_X, QBGQ_LR,
                     FLAVOURS_LOOP_END};

  using Base::default_order;

  TestDraftSplit3q() : Base() {}

  complex<T> getTree(const std::string & type, int n);
  EpsTriplet<T> getLoop(const std::string & type, int n);

  void check_draft();

public:

  USING_BASE_MEMBERS2;

};



template <typename T>
TestDraftSplit3Base<T>::TestDraftSplit3Base() : MuR2(4.)
{
  nc = 3;
  nf = 5;

  ord[0] = 0;
  ord[1] = 1;
  ord[2] = 2;
  ord[3] = 3;

}



template <typename T>
void TestDraftSplit3Base<T>::setMomenta_(const MOM<T> P123,
                                     const  MOM<T> p1, const MOM<T> p2, const MOM<T> p3,
                                     const MOM<T> n_)
{
  s123 = S(P123);
  const MOM<T> p123t = P123 - s123/dot(P123,n_)*0.5*n_;

  const MOM<T> pp[4] = {p123t,p1,p2,p3};

  z[1] = dot(p1,n_)/dot(P123,n_);
  z[2] = dot(p2,n_)/dot(P123,n_);
  z[3] = dot(p3,n_)/dot(P123,n_);

  zspA[1] = xspA(p1,n_)/xspA(p123t,n_);
  zspA[2] = xspA(p2,n_)/xspA(p123t,n_);
  zspA[3] = xspA(p3,n_)/xspA(p123t,n_);
  zspB[1] = xspB(p1,n_)/xspB(p123t,n_);
  zspB[2] = xspB(p2,n_)/xspB(p123t,n_);
  zspB[3] = xspB(p3,n_)/xspB(p123t,n_);

  wspA[1] = xspA(p1,p123t)/xspA(n_,p123t);
  wspA[2] = xspA(p2,p123t)/xspA(n_,p123t);
  wspA[3] = xspA(p3,p123t)/xspA(n_,p123t);
  wspB[1] = xspB(p1,p123t)/xspB(n_,p123t);
  wspB[2] = xspB(p2,p123t)/xspB(n_,p123t);
  wspB[3] = xspB(p3,p123t)/xspB(n_,p123t);

  for (int i=0; i<4; ++i) {
    for (int j=i+1; j<4; ++j) {
      sij(i,j) = S(pp[i]+pp[j]);
      sij(j,i) = sij(i,j);
      spA(i,j) = xspA(pp[i],pp[j]);
      spA(j,i) = -spA(i,j);
      spB(i,j) = xspB(pp[i],pp[j]);
      spB(j,i) = -spB(i,j);
    }
  }

  for (int i=1; i<4; ++i) {
    for (int j=1; j<4; ++j) {
      if (j==i)
        continue;
      int k = 6-i-j; // k in {1,2,3} | k != i,j
      r(i,j) = (spA(i,j)*zspA[k])/(spA(j,k)*zspA[i]);
      rcc(i,j) = (spB(i,j)*zspB[k])/(spB(j,k)*zspB[i]);
      rt(i,j) = (spB(i,j)*wspB[k])/(spB(j,k)*wspB[i]);
      rtcc(i,j) = (spA(i,j)*wspA[k])/(spA(j,k)*wspA[i]);
      X(i,j) = zspA[i]*spB(i,j)/spB(j,0);
      Xcc(i,j) = zspB[i]*spA(i,j)/spA(j,0);
    }
  }

}

// log and polylog functions //
template <typename T>
complex<T> TestDraftSplit3Base<T>::mLog(T s)
{
  T val_re = log(abs(MuR2/s));
  T val_im = s > 0. ? T(Pi<T>::val()) : 0.;
  return complex<T>(val_re, val_im);
}

template <typename T>
complex<T> TestDraftSplit3Base<T>::rLog(T s1, T s2)
{
  T val_re = log(abs(s1/s2));
  T val_im = s2 > 0. ? T(Pi<T>::val()) : 0;
  val_im -= s1 > 0. ? T(Pi<T>::val()) : 0;
  return complex<T>(val_re, val_im);
}

template <typename T>
complex<T> TestDraftSplit3Base<T>::rDiLog(T s1, T s2) {
  T arg = T(1.)-s1/s2;
  T val_re = real(MyDiLog(arg));
  //      complex<T> arg(1.-s1/s2,T());
  //      T val_re = real(DiLog(arg));
  T val_im = -s1/s2 > 0. ? log(1.-s1/s2)*imag(rLog(s1,s2)) : T();
  return complex<T>(val_re, val_im);
}

template <typename T> complex<T> TestDraftSplit3Base<T>::L0hat(T s1, T s2) {
  return rLog(s1,s2);
}
template <typename T> complex<T> TestDraftSplit3Base<T>::L1hat(T s1, T s2) {
  return rLog(s1,s2)/(s1-s2);
}
template <typename T> complex<T> TestDraftSplit3Base<T>::L2hat(T s1, T s2) {
  return rLog(s1,s2)/pow(s1-s2,2) - T(0.5)/(s1-s2)*(1/s1+1/s2);
}
template <typename T> complex<T> TestDraftSplit3Base<T>::L3hat(T s1, T s2) {
  return rLog(s1,s2)/pow(s1-s2,3) - T(0.5)/((s1-s2)*(s1-s2))*(1/s1+1/s2);
}

template <typename T>
EpsTriplet<T> TestDraftSplit3Base<T>::pSS(T s)
{
  complex<T> lll = mLog(s);
  return EpsTriplet<T>(T(0.5)*lll*lll, lll, T(1.));
}

template <typename T>
EpsTriplet<T> TestDraftSplit3Base<T>::pSS1(T s)
{
  complex<T> lll = mLog(s);
  return EpsTriplet<T>(lll, T(1.),T());
}

template <typename T>
EpsTriplet<T> TestDraftSplit3Base<T>::pole(const int * o)
{
  complex<T> l123 = mLog(s123);
  T lz1 = log(z[o[1]]);
  T lz3 = log(z[o[3]]);
  T lz1z3 = lz1+lz3;

  EpsTriplet<T> pZZ(T(0.5)*(-T(2.)*l123*lz1z3 + lz1*lz1 + lz3*lz3), -lz1z3, T());
  return ( - pSS(sij(o[1],o[2])) - pSS(sij(o[2],o[3])) - pZZ);
}

template <typename T>
EpsTriplet<T> TestDraftSplit3Base<T>::poleR(const int * o)
{
  EpsTriplet<T> finite(-(T(7)/T(2)),T(),T());
  return (finite - pSS(sij(o[1],o[2])) - T(3)/T(2)*pSS1(sij(o[1],o[2])) );
}

template <typename T>
EpsTriplet<T> TestDraftSplit3Base<T>::poleR2(const int * o)
{
  EpsTriplet<T> finite(-(T(7)/T(2)),T(),T());
  return (finite - pSS(sij(o[1],o[2])) - pSS(sij(o[2],o[3]))
          - T(3)/T(2)*pSS1(sij(o[1],o[2])) );
}

template <typename T>
EpsTriplet<T> TestDraftSplit3Base<T>::poleF(const int * o)
{
  EpsTriplet<T> finite(-(T(10)/T(9)),T(),T());
  return finite - (T(2)*pSS1(sij(o[1],o[2])))/T(3);
}

template <typename T>
EpsTriplet<T> TestDraftSplit3Base<T>::poleX2(int type, const int * o)
{
  switch (type) {
  case 1:
    {
      EpsTriplet<T> finite((T(10)/T(9)),T(),T());
      return finite + (T(2)*pSS1(sij(o[1],o[3])))/T(3);
      //EpsTriplet<T> finite((T(3)/T(2)),T(),T());
      //return finite + pSS1(sij(o[1],o[3]))/T(3)  + pSS1(s123)/T(3);
    }
  case 2:
    {
      EpsTriplet<T> finite((T(1)/T(9)),T(),T());
      return finite + T(2)*pSS1(s123)/T(3);
    }
  case 3:
    {
      EpsTriplet<T> finite((T(29)/T(18)),T(),T());
      return finite + T(2)*pSS1(s123)/T(3);
    }
  }
  return EpsTriplet<T>();
}

template <typename T>
complex<T> TestDraftSplit3Base<T>::FMHV(const int * o) {
  return (
          T(0.5)*(Pi<T>::val()*Pi<T>::val()/T(3.) + pow(log(z[o[1]]),2) + pow(log(z[o[3]]),2))
          - rLog(sij(o[1],o[2]),s123)*rLog(sij(o[2],o[3]),s123)
          + rLog(sij(o[1],o[2]),s123)*log((1.-z[o[3]])/z[o[1]])
          + rLog(sij(o[2],o[3]),s123)*log((1.-z[o[1]])/z[o[3]])
          + MyDiLog(-z[o[2]]/z[o[3]])
          + MyDiLog(-z[o[2]]/z[o[1]])
          + MyDiLog(-z[o[1]]/(1.-z[o[1]]))
          + MyDiLog(-z[o[3]]/(1.-z[o[3]]))
          - rDiLog(sij(o[1],o[2]),s123*(1-z[o[3]]))
          - rDiLog(sij(o[2],o[3]),s123*(1-z[o[1]]))
          );
}

template <typename T>
complex<T> TestDraftSplit3Base<T>::FNMHV1(const int * o) {
  return (
          T(0.5)*(Pi<T>::val()*Pi<T>::val()/T(3.) - log(z[o[1]])*rLog(sij(o[2],o[3]),s123) - log(z[o[3]])*rLog(sij(o[1],o[2]),s123))
          - log(1-z[o[3]])*log(z[o[1]]*z[o[3]]/(1-z[o[3]]))
          + log(z[o[1]]*z[o[3]])*rLog(sij(o[1],o[2]),s123)
          - log(1-z[o[3]])*rLog(sij(o[1],o[2]),sij(o[2],o[3]))
          );
}

template <typename T>
complex<T> TestDraftSplit3Base<T>::FNMHV2(const int * o) {
#if 0
  return (
          T(0.5)*(Pi<T>::val()*Pi<T>::val()/T(3.) - log(z[o[1]])*rLog(sij(o[2],o[3]),s123) - log(z[o[3]])*rLog(sij(o[1],o[2]),s123))
          - log(1-z[o[1]])*log(z[o[1]]*z[o[3]]/(1-z[o[1]]))
          + log(z[o[1]]*z[o[3]])*rLog(sij(o[2],o[3]),s123)
          - log(1-z[o[1]])*rLog(sij(o[2],o[3]),sij(o[1],o[2]))
          );
#else
  const int order[] = {o[0],o[3],o[2],o[1]};
  return FNMHV1(order);
#endif
}

template <typename T>
complex<T> TestDraftSplit3Base<T>::FNMHV3(const int * o) {
  return (
          - T(0.5)*(Pi<T>::val()*Pi<T>::val()/T(3.) - log(z[o[1]])*rLog(sij(o[2],o[3]),s123) - log(z[o[3]])*rLog(sij(o[1],o[2]),s123))
          - rLog(sij(o[1],o[2]),s123)*rLog(sij(o[2],o[3]),s123)
          );
}

template <typename T>
complex<T> TestDraftSplit3Base<T>::F1mBox(const int * o) {
  return -2.*(
      Pi<T>::val()*Pi<T>::val()/T(6.) + T(0.5)*pow(rLog(sij(o[1],o[2]),sij(o[2],o[3])),2)
        + rDiLog(s123,sij(o[1],o[2]))
        + rDiLog(s123,sij(o[2],o[3]))
      );
}


template <typename T>
complex<T> symm_ratio(const complex<T> & a, const complex<T> & b)
{
  return T(2)*(b-a)/(b+a);
}

template <typename T>
void check_tree_val(int hh, const complex<T> & a, const complex<T> & b)
{
  std::cout << "============= " << std::endl;
  std::cout << "hel = " << hh << std::endl;
  std::cout << "a = " << a << std::endl;
  std::cout << "b = " << b << std::endl;
  std::cout << "b-a = " << b-a << std::endl;
  std::cout << "2*(b-a)(b+a) = " << T(2)*(b-a)/(b+a) << std::endl;
  if (abs(T(2)*(b-a)/(b+a)) > 1e-30)
    std::cout << "\n  => WARNING!!! <=\n" << std::endl;
}

template <typename T>
void check_loop_val(int hh, const complex<T> & a, const EpsTriplet<T> & b)
{
  return check_loop_val(hh,EpsTriplet<T>(a),b);
}

template <typename T>
void check_loop_val(int hh, const EpsTriplet<T> & a, const EpsTriplet<T> & b)
{
  std::cout << "============= " << std::endl;
  std::cout << "hel = " << hh << std::endl;
  std::cout << "a = " << a << std::endl;
  std::cout << "b = " << b << std::endl;
  std::cout << "b-a = " << b-a << std::endl;
  std::cout << "ratio 2 = " << symm_ratio(a.get2(),b.get2()) << std::endl;
  std::cout << "ratio 1 = " << symm_ratio(a.get1(),b.get1()) << std::endl;
  std::cout << "ratio 0 = " << symm_ratio(a.get0(),b.get0()) << std::endl;
  if (abs(symm_ratio(a.get0(),b.get0())) > 1e-30)
    std::cout << "\n  => WARNING!!! <=\n" << std::endl;
}

  void get_helicity(int h, int n, int * hh)
  {
    int mask = 1;
    for (int i=0; i<n; ++i) {
      hh[i] = 2*(h & mask)-1;
      h >>= 1;
    }
  }


template <typename T>
void TestDraftSplit3q<T>::check_draft()
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(25);
  const Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };
  PhaseSpace<T> ps(5, flavours, 1, 1e3);
  ps.getPSpoint();

  MOM<T> p[4] = {ps[0]+ps[1], ps[2], ps[3], ps[4]};

  ps.getPSpoint();
  MOM<T> n = ps[1];

  for (int i=0; i<4; ++i)
    std::cout << p[i] << std::endl;

  int hvec[4];
  int hh;

  Split3q<T> s3;

  s3.setMomenta(-p[0],p[1],p[2],p[3],n);
  Base::setMomenta(-p[0],p[1],p[2],p[3],n);

  int QQG = Split3q<T>::QBQG;
  int QGQ = Split3q<T>::QBGQ;
  int QQ4 = Split3q<T>::QBQG_NEQ4;
  int QQR = Split3q<T>::QBQG_R;
  int QQF = Split3q<T>::QBQG_F;
  int QQS = Split3q<T>::QBQG_X;
  int QG4 = Split3q<T>::QBGQ_NEQ4;
  int QGLR = Split3q<T>::QBGQ_LR;
  int QGS = Split3q<T>::QBGQ_X;

  int o[] = {0,1,2,3};
  
  std::cout << "======TreeLevel============" << std::endl;
  std::cout << "=====Qqg=============" << std::endl;

  // 3
  hh = 3;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_tree_val(hh, -(zspA[o[2]] * zspA[o[3]])/(rt(o[1],o[2])*spB(o[2],o[3])*spA(o[1],o[2]) )*((X(o[2],o[1]))/(( T(1)-z[o[3]])*z[o[3]])+(sij(o[1],o[2]))/(X(o[3],o[2])*s123*X(o[2],o[1]))) , s3.evalTree(QQG) );

  // 5
  hh = 5;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_tree_val(hh, -(zspA[o[3]]*(rtcc(o[3],o[1])))/(spB(o[1],o[3])*spA(o[2],o[0]))*((sij(o[2],o[3])*(pow(rcc(o[1],o[3]),2)*rtcc(o[1],o[3]))*pow(z[o[1]],2))/(z[o[3]]*sij(o[1],o[3])*rtcc(o[3],o[1]))+(sij(o[1],o[3])*(Xcc(o[3],o[1])))/(z[o[3]]*s123)+(pow(z[o[3]],2)*sij(o[1],o[2])*(pow(rcc(o[3],o[1]),2)))/((T(1)-z[o[3]])*sij(o[1],o[3]))), s3.evalTree(QQG) );

  // 11
  hh = 11;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_tree_val(hh, pow(zspA[o[2]],3)/(zspA[o[3]]*spA(o[1],o[2])*spA(o[2],o[3])), s3.evalTree(QQG) );

  // 13
  hh = 13;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_tree_val(hh, -(pow(zspA[o[1]],2)*zspA[o[2]])/(zspA[o[3]]*spA(o[1],o[2])*spA(o[2],o[3])), s3.evalTree(QQG) );


  std::cout << "======TreeLevel============" << std::endl;
  std::cout << "=====Qgq=============" << std::endl;
  
  // 3
  hh = 3;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_tree_val(hh, (pow(spB(o[1],o[0]),2))/(spB(o[1],o[2])*spB(o[2],o[3])*s123), s3.evalTree(QGQ) );

  // 13
  hh = 13;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_tree_val(hh, -(pow(zspA[o[1]],2))/(spA(o[1],o[2])*spA(o[2],o[3])), s3.evalTree(QGQ) );


  std::cout << "==========OneLoop======================" << std::endl;
  std::cout << "=====N=4=====" << std::endl;

  // 3
  hh = 3;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, pole(o)*s3.evalTree(QBQG)+T(-1)*((zspA[o[2]]*zspA[o[3]])/(spB(o[2],o[3])*spA(o[1],o[2]))*((pow(T(1)-z[o[1]],2)*sij(o[1],o[2]))/(X(o[2],o[1])*z[o[3]]*sij(o[1],o[0]))*FNMHV2(o)-((pow(X(o[3],o[2]),2)*pow(sij(o[2],o[0]),2)*pow(Xcc(o[1],o[2]),2))/((T(1)-z[o[3]])*X(o[2],o[3])*sij(o[1],o[2])*sij(o[3],o[0]))-(X(o[2],o[3]))/((T(1)-z[o[3]])*z[o[3]]))*FNMHV1(o)-(z[o[3]]*sij(o[1],o[2])*pow(sij(o[1],o[3]),3))/(X(o[2],o[1])*X(o[2],o[3])*pow(X(o[3],o[1]),2)*s123*pow(sij(o[1],o[0]),2)*sij(o[3],o[0])*Xcc(o[3],o[1]))*((z[o[1]]*rt(o[1],o[2])*pow(sij(o[2],o[3]),3))/(sij(o[1],o[2])*sij(o[1],o[3])*sij(o[3],o[0])*Xcc(o[1],o[3]))-(pow(X(o[3],o[1]),2)*sij(o[1],o[0])*Xcc(o[3],o[1])*pow(sij(o[2],o[3]),2))/(z[o[3]]*pow(sij(o[1],o[3]),3))+(z[o[1]]*rt(o[3],o[2])*sij(o[1],o[0]))/(sij(o[1],o[3])*Xcc(o[1],o[3]))+(X(o[2],o[3])*z[o[3]]*sij(o[3],o[0]))/(X(o[2],o[1])*sij(o[1],o[3])*Xcc(o[3],o[1])))*FNMHV3(o))), s3.eval(QQ4) );

  // 5
  hh = 5;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, pole(o)*s3.evalTree(QBQG)+(-T(1))*((zspA[o[3]]*spB(o[2],o[0]))/(spB(o[1],o[3]))*(-(X(o[3],o[2])*z[o[2]]*sij(o[3],o[0])*Xcc(o[2],o[3])*Xcc(o[3],o[1])*pow(X(o[1],o[3]),2))/(X(o[2],o[3])*pow(z[o[3]],2)*sij(o[1],o[3])*sij(o[2],o[3]))*FNMHV2(o)+(X(o[3],o[2]))/((T(1)-z[o[3]])*X(o[2],o[3])*sij(o[3],o[0]))*((X(o[3],o[1])*pow(z[o[2]],2))/(X(o[2],o[1]))-(X(o[1],o[3])*X(o[2],o[1])*z[o[1]]*sij(o[1],o[3]))/(X(o[3],o[1])*z[o[3]]*sij(o[1],o[2])*Xcc(o[1],o[3])))*FNMHV1(o)-((X(o[1],o[2])*sij(o[1],o[3])*Xcc(o[3],o[1]))/(z[o[3]]*sij(o[1],o[2])*sij(o[3],o[0]))+(sij(o[1],o[3])*Xcc(o[3],o[1]))/(z[o[3]]*s123*sij(o[3],o[0]))+(z[o[1]]*sij(o[1],o[3]))/(X(o[1],o[2])*X(o[3],o[2])*s123*sij(o[3],o[0])*Xcc(o[1],o[3])))*FNMHV3(o))), s3.eval(QQ4) );

  // 11
  hh = 11;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, (pole(o) + FMHV(o))*s3.evalTree(QBQG), s3.eval(QQ4) );

  // 13
  hh = 13;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, (pole(o) + FMHV(o))*s3.evalTree(QBQG), s3.eval(QQ4) );

  std::cout << "==========OneLoop======================" << std::endl;
  std::cout << "=====R=====" << std::endl;
  
  // 3
  hh = 3;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, (poleR(o))*s3.evalTree(QBQG)+T(-1)*((r(o[2],o[1])*zspA[o[2]]*zspA[o[3]])/(spB(o[2],o[3])*spA(o[1],o[2]))*(-(sij(o[1],o[2]))/(T(2)*X(o[2],o[1])*X(o[3],o[1])*r(o[2],o[1])*s123)*F1mBox(o)-(T(1))/(T(2))*s123*sij(o[2],o[3])*L2hat(sij(o[1],o[2]),s123)+(T(1))/(T(2))*(-T(3)*s123-(T(2)*sij(o[2],o[3]))/(X(o[3],o[2])))*L1hat(sij(o[1],o[2]),s123)+(T(1))/(T(2))+(sij(o[2],o[3]))/(T(4)*sij(o[1],o[2])))), s3.eval(QQR) );

  // 5
  hh = 5;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, (poleR(o))*s3.evalTree(QBQG)+((zspA[o[3]]*spB(o[2],o[0]))/(spB(1,3)*sij(1,3))*(+(rt(o[1],o[2])*rt(o[2],o[1])*sij(o[1],o[3]))/(T(2)*X(o[3],o[1])*s123)*F1mBox(o)-(rt(o[1],o[2])*rt(o[2],o[1])*pow(sij(o[1],o[3]),2)*sij(o[2],o[3]))/(T(2)*X(o[3],o[1])*s123)*L2hat(sij(o[1],o[2]),s123)-(T(3)*rt(o[1],o[2])*rt(o[2],o[1])*pow(sij(o[1],o[3]),2))/(T(2)*X(o[3],o[1])*s123)*L1hat(sij(o[1],o[2]),s123)+(sij(o[1],o[2])*sij(o[2],o[3])*pow(X(o[1],o[2]),2))/(T(2)*X(o[3],o[2]))*L2hat(sij(o[2],o[3]),s123)+(sij(o[1],o[2])*X(o[1],o[2]))/(X(o[3],o[2]))*((X(o[1],o[2])*(T(2)*sij(o[1],o[2])*s123+T(3)*sij(o[2],o[3])*s123-T(3)*sij(o[1],o[2])*sij(o[2],o[3])))/(T(2)*sij(o[1],o[2])*s123)+T(1))*L1hat(sij(o[2],o[3]),s123)+(T(1))/(T(2))*(T(3)*X(o[1],o[2])-(sij(o[2],o[3]))/(X(o[3],o[2])*s123)-(T(3)*(X(o[1],o[2])-T(1))*sij(o[1],o[2]))/(s123)-T(1))*L0hat(sij(o[2],o[3]),s123)-(pow(X(o[1],o[2]),2)*sij(o[2],o[3]))/(T(4)*X(o[3],o[2])*sij(o[1],o[2]))+(X(o[1],o[2])*sij(o[2],o[3]))/(T(2)*X(o[3],o[2])*s123)+X(o[1],o[2])+((T(1)-X(o[1],o[2]))*sij(o[1],o[2]))/(T(4)*s123)-((s123-sij(o[2],o[3]))*sij(o[1],o[2]))/(T(4)*X(o[3],o[2])*pow(s123,2)))), s3.eval(QQR) );

  // 11
  hh = 11;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, s3.evalTree(QBQG)*(poleR(o)+T(-1)*((pow(r(o[1],o[2]),3))/(pow(r(o[1],o[3]),3))*(-(F1mBox(o))/(T(2))-(T(1))/(T(2))*(T(6)*pow(sij(o[1],o[2]),2)+pow(sij(o[2],o[3]),2)-T(6)*sij(o[1],o[2])*s123)*L2hat(sij(o[1],o[2]),s123)+(T(3)*s123-T(2)*sij(o[2],o[3]))*L1hat(sij(o[1],o[2]),s123)+(T(3))/(T(2))*L0hat(sij(o[1],o[2]),s123)+((pow(s123,2)-pow(sij(o[1],o[2]),2)-pow(sij(o[2],o[3]),2)))/(T(2)*pow(r(o[1],o[2]),2))*L2hat(sij(o[2],o[3]),s123)+((sij(o[2],o[3]))/(pow(r(o[1],o[2]),2))-(T(2)*sij(o[1],o[3]))/(r(o[1],o[2])))*L1hat(sij(o[2],o[3]),s123)-(pow(s123+sij(o[2],o[3]),2)-pow(sij(o[1],o[2]),2)-T(2)*sij(o[2],o[3])*sij(o[1],o[2]))/(T(4)*pow(r(o[1],o[2]),2)*s123*sij(o[2],o[3]))-(T(6)*pow(sij(o[1],o[2]),2)-pow(sij(o[2],o[3]),2)-T(2)*sij(o[2],o[3])*sij(o[1],o[2]))/(T(4)*s123*sij(o[1],o[2]))+(sij(o[1],o[3]))/(r(o[1],o[2])*s123)-(T(3))/(T(2))))), s3.eval(QQR) );

  // 13
  hh = 13;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, s3.evalTree(QBQG)*(poleR(o)+((r(o[1],o[2]))/(r(o[1],o[3]))*((F1mBox(o))/(T(2))+(pow(r(o[1],o[2]),2)*pow(sij(o[2],o[3]),2))/(T(2))*L2hat(sij(o[1],o[2]),s123)-T(2)*r(o[1],o[2])*sij(2,3)*L1hat(sij(o[1],o[2]),s123)+(T(3))/(T(2))*L0hat(sij(o[1],o[2]),s123)+((s123-sij(o[1],o[2])))/(T(2)*s123)+(r(o[1],o[2])*sij(o[2],o[3]))/(s123)-(pow(r(o[1],o[2]),2)*pow(sij(o[2],o[3]),2))/(T(4)*sij(o[1],o[2])*s123)))), s3.eval(QQR) );

  std::cout << "==========OneLoop======================" << std::endl;
  std::cout << "=====F=====" << std::endl;

  // 3
  hh = 3;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, -((poleF(o))*s3.evalTree(QBQG)+(-(zspA[o[2]]*zspA[o[3]]*spB(o[1],o[2])*spA(o[2],o[3]))/(T(3))*(-(T(2)*sij(o[2],o[3]))/(X(o[2],o[3]))*L3hat(sij(o[1],o[2]),s123)-(T(1))/(X(o[2],o[1]))*L2hat(sij(o[1],o[2]),s123)+-(T(2))/(X(o[2],o[1])*X(o[3],o[1])*s123)*L1hat(sij(o[1],o[2]),s123)-(T(2))/(X(o[2],o[1])*X(o[3],o[1])*s123*sij(o[2],o[3]))*L0hat(sij(o[1],o[2]),s123)+(T(1))/(T(2)*X(o[2],o[1])*sij(o[1],o[2])*s123)))), s3.eval(QQF) );

  // 5
  hh = 5;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, -((poleF(o))*s3.evalTree(QBQG)+((zspA[o[3]]*sij(o[1],o[3])*spB(o[2],o[0]))/(T(3)*spB(o[1],o[3]))*(T(2)*X(o[3],o[2])*s123*L3hat(sij(o[1],o[2]),s123)-L2hat(sij(o[1],o[2]),s123)+((T(2)*pow(X(o[1],o[2]),2))/(X(o[3],o[2])*s123)+(T(2)*s123*X(o[1],o[2]))/(pow(sij(o[1],o[2]),2))-(T(2))/(sij(o[1],o[2])))*L1hat(sij(o[1],o[2]),s123)+((T(2)*X(o[1],o[2])*(sij(o[1],o[2])+s123))/(pow(sij(o[1],o[2]),2)*s123)-(T(2))/(sij(o[1],o[2])*s123))*L0hat(sij(o[1],o[2]),s123)-(T(1))/(T(2)*sij(1,2)*s123)))), s3.eval(QQF) );

  // 11
  hh = 11;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, -s3.evalTree(QBQG)*(poleF(o)+(-(pow(r(o[1],o[2]),3))/(T(3)*pow(r(o[1],o[3]),3))*((pow(sij(o[1],o[3]),3)-pow(sij(o[2],o[3]),3))*L3hat(sij(o[1],o[2]),s123)-(T(3)*(pow(sij(o[1],o[3]),2)+T(2)*sij(o[2],o[3])*sij(o[1],o[3])))/(r(o[1],o[2]))*L2hat(sij(o[1],o[2]),s123)+(T(3)*sij(o[1],o[3]))/(pow(r(o[1],o[2]),2))*L1hat(sij(o[1],o[2]),s123)-L0hat(sij(o[1],o[2]),s123)-(T(1))/(T(2))*((pow(s123-sij(o[1],o[3]),2))/(sij(o[1],o[2])*s123)-(sij(o[1],o[3]))/(sij(o[1],o[2]))-((s123-sij(o[2],o[3])))/(s123))+-(sij(o[1],o[3]))/(pow(r(o[1],o[2]),2)*s123)+(sij(o[1],o[3])*(T(3)*sij(o[1],o[2])+T(5)*s123+sij(o[2],o[3])))/(T(2)*r(o[1],o[2])*sij(o[1],o[2])*s123)))), s3.eval(QQF) );

  // 13
  hh = 13;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, -s3.evalTree(QBQG)*(poleF(o)+(-(r(o[1],o[2]))/(T(3)*r(o[1],o[3]))*(-T(2)*pow(r(o[1],o[2]),2)*pow(sij(o[2],o[3]),3)*L3hat(sij(o[1],o[2]),s123)+T(3)*r(o[1],o[2])*pow(sij(o[2],o[3]),2)*L2hat(sij(o[1],o[2]),s123)+T(2)*sij(1,3)*L1hat(sij(o[1],o[2]),s123)-(sij(o[2],o[3]))/(sij(o[1],o[2])-s123)*L0hat(sij(o[1],o[2]),s123)+(sij(o[2],o[3]))/(s123)-(r(o[1],o[2])*pow(sij(o[2],o[3]),2))/(T(2)*sij(o[1],o[2])*s123)))), s3.eval(QQF) );

  std::cout << "==========OneLoop======================" << std::endl;
  std::cout << "=====S=====" << std::endl;
  // 3
  hh = 3;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, (T(3)*zspA[o[2]]*zspA[o[3]]*spB(o[1],o[2])*spA(o[2],o[3]))/(X(o[2],o[1])*X(o[3],o[2])*s123)*L1hat(sij(o[1],o[2]),s123), s3.eval(QQS) );

  // 5
  hh = 5;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, -T(2)*s3.evalTree(QBQG)+(zspA[o[3]])/(spB(o[1],o[3])*spA(o[2],o[0]))*((T(3)*sij(o[2],o[0]))/(T(2)*X(o[3],o[1])*s123)*F1mBox(o)-(T(3)*sij(o[1],o[3])*sij(o[2],o[0]))/(X(o[3],o[1])*s123)*L1hat(sij(o[1],o[2]),s123)-(T(3)*sij(o[1],o[3])*sij(o[2],o[0]))/(X(o[3],o[1])*s123)*L1hat(sij(o[2],o[3]),s123)+(T(2)*sij(o[1],o[3])*sij(o[2],o[0]))/(X(o[3],o[2])*sij(o[1],o[2])*s123)-(T(2)*X(o[1],o[3])*X(o[3],o[2])*z[o[2]]*sij(o[2],o[0]))/(X(o[2],o[3])*(T(1)-z[o[3]])*sij(o[1],o[2]))+(T(2)*X(o[1],o[2])*X(o[1],o[3])*z[o[2]]*sij(o[2],o[0]))/(X(o[2],o[3])*z[o[3]]*sij(o[1],o[2]))), s3.eval(QQS) );

  // 11
  hh = 11;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, s3.evalTree(QBQG)*(-(T(3)*r(o[1],o[2]))/(pow(r(o[1],o[3]),2))*((T(1))/(T(2))*F1mBox(o)+sij(o[1],o[2])*L1hat(sij(o[2],o[3]),s123)+sij(o[2],o[3])*L1hat(sij(o[1],o[2]),s123)+L0hat(sij(o[1],o[2]),s123)+L0hat(sij(o[2],o[3]),s123))), s3.eval(QQS) );

  // 13
  hh = 13;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, s3.evalTree(QBQG)*((-T(3)*r(o[1],o[2])*sij(o[2],o[3])*L1hat(sij(o[1],o[2]),s123))), s3.eval(QQS) );

  std::cout << "==========OneLoop======================" << std::endl;
  std::cout << "==========Qgq======================" << std::endl;
  std::cout << "=====N=4=====" << std::endl;
  // 3
  hh = 3;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, s3.evalTree(QBGQ)*(pole(o)+((X(o[2],o[1])*pow(T(1)-z[o[1]],2)*s123)/(z[o[2]]*sij(o[1],o[0]))*FNMHV2(o)+((X(o[2],o[1])*r(o[2],o[3])*sij(o[2],o[3])*Xcc(o[1],o[3]))/(z[o[1]]*sij(o[1],o[0]))+T(1))*FNMHV3(o)-(pow(X(o[3],o[1]),2)*s123*(Xcc(o[1],o[3])*rcc(o[2],o[3])))/(sij(o[1],o[3]))*FNMHV1(o))), s3.eval(QG4) );

  // 13
  hh = 13;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, (pole(o) + FMHV(o))*s3.evalTree(QBGQ), s3.eval(QG4) );


  std::cout << "==========OneLoop======================" << std::endl;
  std::cout << "==========Qgq======================" << std::endl;
  std::cout << "=====S=====" << std::endl;

  // 3
  hh = 3;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, s3.evalTree(QBGQ)*(poleX2(1,o)+(/*+*/(-(T(3)*X(o[2],o[1])*sij(o[2],o[3]))/(X(o[2],o[3]))*L1hat(sij(o[1],o[2]),s123)/*-*/ + (T(2))/(T(3))*L0hat(sij(o[1],o[3]),s123)))), s3.eval(QGS) );

  // 13
  hh = 13;
  get_helicity(hh,4,hvec);
  s3.setHelicity(hvec);
  check_loop_val(hh, s3.evalTree(QBGQ)*(poleX2(3,o)+T(-1)*(((pow(X(o[3],o[2]),2)*pow(sij(o[1],o[2]),2))/(T(2)*pow(X(o[1],o[2]),2))-(pow(sij(o[1],o[3]),2))/(T(2)*pow(X(o[1],o[3]),2)))*L2hat(sij(o[1],o[2]),s123)-T(4)*r(o[1],o[2])*sij(o[2],o[3])*L1hat(sij(o[1],o[2]),s123)+(T(1))/(T(2))*L0hat(sij(o[1],o[2]),s123)+(pow(X(o[3],o[2]),2)*sij(o[1],o[2]))/(T(4)*pow(X(o[1],o[2]),2)*s123)-(X(o[3],o[2]))/(X(o[1],o[2]))-(pow(sij(o[1],o[3]),2))/(T(4)*pow(X(o[1],o[3]),2)*sij(o[1],o[2])*s123)+(s123)/(T(2)*sij(o[1],o[2])))) ,s3.eval(QGS) );
  
}


int main()
{
  typedef qd_real RType;
  TestDraftSplit3q<RType> test;
  test.check_draft();
  
  return 0;
}
