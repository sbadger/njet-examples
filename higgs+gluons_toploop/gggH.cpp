#include <iostream>
#include <getopt.h>
#include <cstdio>

#include "tools/PhaseSpace.h"
#include "ngluon2/Initialize.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

using std::cout;
using std::endl;
using std::complex;

void checkGU(double mt, const int *helicity) {
  const int nn = 5;
  double mH = 1.2;
  StandardModel::setHmass(mH);

  Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::TopHiggs(),
  };

  MOM<double> GUpt[4] = {
    MOM<double>( 8.8317608663278469 , 2.0000000000000000, 5.0000000000000000,  7.0000000000000000),
    MOM<double>( 10.8166538263919679, 4.0000000000000000, 1.0000000000000000, 10.0000000000000000),
    MOM<double>(-4.0650794167490064 ,-2.3469746955372458,-2.3469746955372458, -2.3469746955372458),
    MOM<double>(-15.5833352759708083,-3.6530253044627542,-3.6530253044627542,-14.6530253044627542),
  };
  std::vector<MOM<double> > ps;
  ps.insert(ps.end(), &GUpt[0], &GUpt[4]);

  NParton2<double> amp;
  amp.setProcess(nn, flavours);
  amp.setLoopInduced();

  const double MuR = mt;
  amp.setMuR2(MuR*MuR);

  amp.setHeavyQuarkMass(mt);

  int perms[][4] = {
    {3,0,1,2},
    {3,1,2,0},
    {3,2,0,1},
  };

//  const int helicity[] = {+1,+1,+1,+1};

  EpsTriplet<double> part,sum;
  for (int p=0; p<3; p++) {
    amp.setOrder(perms[p]);
    amp.setHelicity(helicity);
    amp.setMomenta(ps);
    amp.eval(2);
    amp.printCoefficients();
    cout << amp.getcc(1) << endl;
    cout << amp.getcc(2) << endl;
    cout << amp.getcc(3) << endl;
    cout << amp.getrat() << endl;
    sum += amp.getcc(1)+amp.getcc(2)+amp.getcc(3)+(-amp.getrat());
//    sum += -amp.getcc(2);
//    sum += (-amp.getrat());
  }
  cout << "A(1,2,3,H) = " << sum.get0() << endl;

  // +++H from Rozowsky hep-ph/9709423
//  complex<double> phase = 1./(xspA(ps[0], ps[1])*xspA(ps[1], ps[2])*xspA(ps[2], ps[0]));
//  double s = S(ps[0]+ps[1]);
//  double t = S(ps[1]+ps[2]);
//  double u = S(ps[0]+ps[2]);

//  cout << "C4[1|2|3|H] = " << -mt/sqrt(2.)*i_*s*t*(4.*mt*mt-mH*mH)*phase*0.5 << endl;
//  cout << "C4[2|3|1|H] = " << -mt/sqrt(2.)*i_*u*t*(4.*mt*mt-mH*mH)*phase*0.5 << endl;
//  cout << "C4[3|1|2|H] = " << -mt/sqrt(2.)*i_*s*u*(4.*mt*mt-mH*mH)*phase*0.5 << endl;
//  cout << "C3[1|2|3H] + C3[1|2|H3] = " << -mt/sqrt(2.)*i_*s*t*(4.*mt*mt-mH*mH)*phase*(t-u)/(t*(s-mH*mH)) << endl;
//  cout << "C3[1|2|3H] + C3[1|2|H3] = " << -mt/sqrt(2.)*i_*s*t*(4.*mt*mt-mH*mH)*phase*(s-u)/(s*(t-mH*mH)) << endl;

  Flavour<double> flavoursEffTree[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::Higgs(),
  };

  NParton2<double> ampEffTree;
  ampEffTree.setProcess(nn, flavoursEffTree);

  complex<double> tree;
  for (int p=0; p<3; p++) {
    ampEffTree.setOrder(perms[p]);
    ampEffTree.setHelicity(helicity);
    ampEffTree.setMomenta(ps);
    tree += ampEffTree.evalTree();
  }
  cout << "# " << tree << endl;
  cout << "# +++ " << -0.5*i_*pow(mH,4)/(xspA(ps[0], ps[1])*xspA(ps[1], ps[2])*xspA(ps[2], ps[3])) << endl;
//  cout << "# ++- " << 0.5*i_*pow(xspB(ps[1],ps[2]),4)/(xspB(ps[1], ps[2])*xspB(ps[2], ps[3])*xspB(ps[3], ps[1])) << endl;
//  cout << "# +-+ " << 0.5*i_*pow(xspB(ps[3],ps[1]),4)/(xspB(ps[1], ps[2])*xspB(ps[2], ps[3])*xspB(ps[3], ps[1])) << endl;

  cout << "### [" << mt << ", " << abs(sum.get0()) << "],"<< endl;
}


int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  const int hel[4] = {+1,+1,+1,+1};
  checkGU(1.75, hel);

//  const int hel[4] = {+1,+1,+1,+1};
//  for (int mt=0; mt<100; mt++) {
//    check<dd_real>(125.1*pow(1.5,mt));
//  }

  return 1;
}
