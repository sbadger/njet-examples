#include <iostream>

#include "softGluon.h"

template<typename T>
EpsQuintuplet<T> test() {
    Amp_4g2A<T> amp;

    const int hels[]{-1, -1, +1, +1, +1, +1};

    amp.setHelicity(hels);

    return softGluon(amp, T(0.1), 2);
}

int main(int argc, char **argv) {
    cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    cout.precision(8);

    cout << test<double>() << endl;

    return 1;
} 
