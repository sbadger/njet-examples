#include <iostream>
#include <cstdio>
#include <getopt.h>

#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"
#include "ngluon2/Initialize.h"
#include "tools/PhaseSpace.h"

using std::cout;
using std::endl;

//phase space for the single soft limit



int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);


  const int nf = 4;
  Flavour<double>flavours[]= {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::Ax(StandardModel::IL(),StandardModel::IL().C()),
    StandardModel::Ax(StandardModel::IL(),StandardModel::IL().C()),
    };
  
//a matrix with all the non-cyclic perms of the 4 legs
  const int perm[6][nf]={
  {1,2,3,0},
  {1,3,2,0},
  {1,3,0,2},
  {3,1,0,2},
  {2,1,0,3},
  {1,2,0,3}
};

   
const int helicities[]= {+1,+1,+1,+1};

  PhaseSpace<double> phaseSp(nf, flavours);
  phaseSp.getPSpoint(); 
  phaseSp.showPSpoint();
  
  const double MuR=91 ;
  NParton2<double> parAmp;
  parAmp.setProcess(nf, flavours);
  parAmp.setLoopInduced();
  parAmp.setMuR2(MuR*MuR);
  parAmp.setHelicity(helicities);
  parAmp.setMomenta(phaseSp.Mom());
 cout <<  parAmp.eval(1).get0() << endl;
 // parAmp.printCoefficients();
 
  
EpsTriplet<double> amp;
  for(int i=0;i<6; i++)
  { 
    parAmp.setOrder(perm[i]);
    amp += parAmp.eval(1); 
 };

cout << amp <<endl;


   
  return 0;
}
