#include <iostream>
#include <getopt.h>
#include <cstdio>
#include <math.h>

#include "tools/PhaseSpace.h"
#include "ngluon2/Initialize.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

using namespace std;

//hard coded single soft limit for AA ->ggg

template <typename T>

std::vector<MOM<T> > getSoftPoint6(const T lambda)
{
  const T sqS= 1e3;
    std::vector<MOM<T> > Mom(5);

      const T E4= T(0.12)*sqS;
      const T theta1= T(M_PI/3.);
      const T theta2= T(M_PI/5.);
      const T theta3= T(M_PI/7.);
      const T theta4= T(M_PI/4.);
      const T ct1= cos(theta1);
      const T ct2= cos(theta2);
      const T ct3= cos(theta3);
      const T ct4= cos(theta4);
      const T st1= sin(theta1);
      const T st2= sin(theta2);
      const T st3= sin(theta3);
      const T st4= sin(theta4);
  
const T E3 =T((sqS*(-2*E4*lambda + sqS))/(2.*(-(E4*lambda) + ct4*E4*lambda + sqS))); 

Mom[0]= MOM<T>(sqS/2.,0,0,-sqS/2.);
Mom[1]= MOM<T>(sqS/2.,0,0,sqS/2.);
Mom[2]=- E3*MOM<T>(1, ct1*st2, st1*st2, ct2); 
Mom[3]= -E4*lambda*MOM<T>(1, ct1*ct4*st2+(ct1*ct2*ct3-st1*st3)*st4, (ct4*st1*st2+(ct2*ct3*st1+ct1*st3)*st4), (ct2*ct4-ct3*st2*st4));
Mom[4]= -Mom[0]-Mom[1]-Mom[2]-Mom[3];

return Mom;
}

  template <typename T>
  void checkSoft(const Flavour<double>* flavours)
{

  int n= 5;
  std::vector<MOM<T> > mom(n);
  std::vector<MOM<T> >fact_mom(n-1);

  const Flavour<double> g= StandardModel::G();
  const Flavour<double> A= StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());
  


  NParton2<T> amp;
  amp.setProcess(n, flavours);
  

  NParton2<T> fact_amp;


  const Flavour<double> fact_flavs[]= {flavours[0],flavours[1],flavours[2],flavours[4]};
  
   
 
  fact_amp.setProcess(n-1, fact_flavs);


  const T MuR= 91.188;
  amp.setMuR2(MuR*MuR);
  fact_amp.setMuR2(MuR*MuR);
  

  const int  hels[]={-1,-1,+1,+1,+1};

  const int fact_hels[]={-1,-1,+1,+1};

for(int p=0; p<16; ++p){
 const T lambda= pow(10,-p);
 mom= getSoftPoint6<T>(lambda);
  amp.setMomenta(mom);
  amp.setHelicity(hels);
  
  amp.setLoopInduced();


  const MOM<T> p1= mom[0];
  const MOM<T> p2= mom[1];
  const MOM<T> p3= mom[2];
  const MOM<T> p4= mom[3];
  const MOM<T> p5= mom[4];
 
  const complex<T> eikonal= xspA(p3,p5)/xspA(p3,p4)/xspA(p4,p5);

  complex<T> limit = T();

  fact_mom[0]=mom[0];
  fact_mom[1]=mom[1];
  fact_mom[2]=mom[2];
  fact_mom[3]=mom[4];


  fact_amp.setMomenta(fact_mom);
  fact_amp.setHelicity(fact_hels);

  fact_amp.setLoopInduced();
    
 
  limit = eikonal*fact_amp.eval(1).get0();

  cout << "amp/limit" << amp.eval(1).get0()/limit<< endl;
  cout << lambda<< endl;
     }
  }
  int main(int argc, char **argv)
{

   cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
   cout.precision(16);

   const Flavour<double> g= StandardModel::G();
   const Flavour<double> A= StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());

   const Flavour<double> flavs[]={A,A,g,g,g};
 
  // checkSoft<double>(flavs);
   cout<<endl;
   checkSoft<qd_real>(flavs); 
  return 1;
}
