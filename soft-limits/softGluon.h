#ifndef NJET_EXAMPLES_SOFTGLUON_H
#define NJET_EXAMPLES_SOFTGLUON_H

#include <vector>

#include "softPS6.h"

#include "../loop-induced/4g2A.h"
#include "../loop-induced/3g2A.h"

//class to compute the soft factorisation of 4g2A amplitudes

template<typename T>
complex <T> getUnivEikonal(const std::vector <MOM<T>> &order, const unsigned int i_soft) {
    return (2 * dot(order[(i_soft - 1) % 4], order[(i_soft + 1) % 4])) /
           (dot(order[(i_soft - 1) % 4], order[i_soft]) * dot(order[i_soft], order[(i_soft + 1) % 4]));
}

template<typename T>
EpsQuintuplet<T> softGluon(Amp_4g2A<T> amp, const T lambda, const unsigned int i_soft) {

    const int mul{6};
    int hels[mul];

    for (unsigned int i{0}; i < mul; ++i) {
        hels[i] = amp.getHelicity(i);
    }

//        for (auto x: hels) {
//        cout << x << endl;
//    }

    std::vector <MOM<T>> standMom{getSoftPoint6<T>(lambda)};
    std::vector <MOM<T>> nonSoftMom{
            standMom[0],
            standMom[1],
            standMom[2],
            standMom[3],
            standMom[5]
    };

//    for (auto x: nonSoftMom) {
//        cout << x << endl;
//    }

    std::vector <MOM<T>> mom(mul);
    for (unsigned int i{0}; i < mul; ++i) {
        if (i == i_soft) {
            mom[i] = standMom[4];
        } else if (i < i_soft) {
            mom[i] = nonSoftMom[i];
        } else {
            mom[i] = nonSoftMom[i - 1];
        }
    }

//    for (auto x: mom) {
//        cout << x << endl;
//    }

    std::vector <MOM<T>> order1{mom[2], mom[3], mom[4], mom[5]};
    std::vector <MOM<T>> order2{mom[2], mom[4], mom[5], mom[3]};
    std::vector <MOM<T>> order3{mom[2], mom[5], mom[3], mom[4]};

//    for (auto x: order1) {
//        cout << x << endl;
//    }

    complex <T> univEikonal1{getUnivEikonal(order1, i_soft)};
    complex <T> univEikonal2{getUnivEikonal(order2, i_soft)};
    complex <T> univEikonal3{getUnivEikonal(order3, i_soft)};

//    cout << univEikonal1 << endl;

    complex <T> prefact{static_cast<T>(9) * (univEikonal1 + univEikonal2 + univEikonal3)};

//    cout << prefact << endl;

    int soft_hels[mul - 1];

    for (unsigned int i{0}; i < mul; ++i) {
        if (i < i_soft) {
            soft_hels[i] = hels[i];
        } else if (i > i_soft) {
            soft_hels[i - 1] = hels[i];
        }
    }

//    for (auto x: soft_hels) {
//        cout << x << endl;
//    }

    Amp_3g2A<T> soft_amp;
    soft_amp.setHelicity(soft_hels);
    soft_amp.setMomenta(nonSoftMom);

//    cout << soft_amp.evalKinAmp2() << endl;

    EpsQuintuplet<T> sq_Amp{prefact * soft_amp.evalKinAmp2()};

//    cout << sq_Amp << endl;

    return sq_Amp;
}

#endif
 
