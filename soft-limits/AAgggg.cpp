#include <iostream>
#include <getopt.h>
#include <cstdio>
#include <math.h>

#include "tools/PhaseSpace.h"
#include "ngluon2/Initialize.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

using namespace std;

//hard coded single soft limit for AA ->gggg

template <typename T>

std::vector<MOM<T> > getSoftPoint6(const T lambda)
{
  const T sqS= 5e2;
  std::vector<MOM<T> > Mom(6);
  
  const T E3= T(100.);
  const T E5= T(90.);
  const T theta1= T(M_PI/7.);
  const T theta2= T(M_PI/3.);
  const T theta3= T(M_PI/5.);
  const T ct1= cos(theta1);
  const T ct2= cos(theta2);
  const T ct3= cos(theta3);
  const T st1= sin(theta1);
  const T st2= sin(theta2);
  const T st3= sin(theta3);


  const T E4=(pow(E3,2) - pow(ct1,2)*pow(E3,2) + 2*E3*E5*lambda - 2*pow(ct1,2)*ct3*E3*E5*lambda + pow(E5,2)*pow(lambda,2) - pow(ct1,2)*pow(ct3,2)*pow(E5,2)*pow(lambda,2) - 2*E3*sqS - 2*E5*lambda*sqS + pow(sqS,2) - pow(E3,2)*pow(st1,2) - 2*ct2*E3*E5*lambda*pow(st1,2) - pow(ct2,2)*pow(E5,2)*pow(lambda,2)*pow(st1,2) - pow(ct3,2)*pow(E5,2)*pow(lambda,2)*pow(st1,2)*pow(st2,2) - 2*ct1*E3*E5*lambda*st1*st2*st3 - pow(ct1,2)*pow(E5,2)*pow(lambda,2)*pow(st3,2) - pow(E5,2)*pow(lambda,2)*pow(st1,2)*pow(st2,2)*pow(st3,2))/(2.*(-E3 + pow(ct1,2)*E3 - E5*lambda + pow(ct1,2)*ct3*E5*lambda + sqS + ct2*E3*pow(st1,2) + pow(ct2,2)*E5*lambda*pow(st1,2) + ct3*E5*lambda*pow(st1,2)*pow(st2,2)));

  Mom[0]=MOM<T>(sqS/2.,0,0,-sqS/2.);
  Mom[1]= MOM<T>(sqS/2.,0,0,sqS/2.);
  Mom[2]= -E3*MOM<T>(1,st1,0,ct1);
  Mom[3]= -E4*MOM<T>(1,ct2*st1,st1*st2,ct1);
  Mom[4]=-E5*lambda*MOM<T>(1,ct2*st1,ct3*st1*st2 - ct1*st3,ct1*ct3 + st1*st2*st3);
  Mom[5]= -Mom[0]-Mom[1]-Mom[2]-Mom[3]-Mom[4];

  

 
  return Mom;
}


//code that performes the soft factorization for COLOUR ORDERED amplitude

template <typename T>
void checkSoft(const Flavour<double>* flavours)
{
  int n= 6;
  std::vector<MOM<T> > mom(n);
  std::vector<MOM<T> >fact_mom(n-1);

  const Flavour<double> g= StandardModel::G();
  const Flavour<double> A= StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());
  


  
 // const Flavour<double> eikonal_flavs[]={g,g,flavours[4]};
  const Flavour<double> fact_flavs[]= {flavours[0],flavours[1],flavours[2],flavours[3],flavours[5]};
  
  NParton2<T> amp;

  const T MuR= 91.188;
  amp.setProcess(n, flavours);
  amp.setMuR2(MuR*MuR);
  NParton2<T> fact_amp;
  fact_amp.setProcess(n-1, fact_flavs);
  fact_amp.setMuR2(MuR*MuR);

 
  const int  hels[]={-1,-1,+1,+1,+1,+1};

  const int fact_hels[]={-1,-1,+1,+1,+1};




 const  int photonPerms[20][6]={

{0,1,2,3,4,5},

{0,2,1,3,4,5},

{0,2,3,1,4,5},

{0,2,3,4,1,5},

{1,0,2,3,4,5},

{2,0,1,3,4,5},

{2,0,3,1,4,5},

{2,0,3,4,1,5},

{1,2,0,3,4,5},

{2,1,0,3,4,5},

{2,3,0,1,4,5},

{2,3,0,4,1,5},

{1,2,3,0,4,5},

{2,1,3,0,4,5},

{2,3,1,0,4,5},

{2,3,4,0,1,5},

{1,2,3,4,0,5},

{2,1,3,4,0,5},

{2,3,1,4,0,5},

{2,3,4,1,0,5}

};


const int  littlePhotonPerms[12][6]={
{0,1,2,3,4},

{0,2,1,3,4},

{0,2,3,1,4},

{1,0,2,3,4},

{2,0,1,3,4},

{2,0,3,1,4},

{1,2,0,3,4},

{2,1,0,3,4},

{2,3,0,1,4},

{1,2,3,0,4},

{2,1,3,0,4},

{2,3,1,0,4}
};


//going into the limit

  for(int p=0; p<6; ++p){
  const T lambda= pow(10,-p); 
  mom= getSoftPoint6<T>(lambda);
    amp.setMomenta(mom);
  amp.setHelicity(hels);

int ord[]={0,1,2,4,5,3};
int fac_ord[]={0,1,2,5,3};
  amp.setOrder(ord);
  amp.setLoopInduced();


  const MOM<T> p1= mom[0];
  const MOM<T> p2= mom[1];
  const MOM<T> p3= mom[2];
  const MOM<T> p4= mom[3];
  const MOM<T> p5= mom[4];
  const MOM<T> p6= mom[5];
  const MOM<T> ptot= mom[0]+mom[1]+mom[2]+mom[3]+mom[4]+ mom[5];
  const complex<T> eikonal= xspA(p4,p6)/xspA(p4,p5)/xspA(p5,p6);

  

  fact_mom[0]=mom[0];
  fact_mom[1]=mom[1];
  fact_mom[2]=mom[2];
  fact_mom[3]=mom[3];
  fact_mom[4]=mom[5];

  fact_amp.setMomenta(fact_mom);
  fact_amp.setHelicity(fact_hels);
  fact_amp.setOrder(ord);
  fact_amp.setLoopInduced();
 
  EpsTriplet<T> totAmp;
  EpsTriplet<T> totAmpFact;
  complex<T> totLimit;


  

    for(int j=0; j<20; ++j){
    std::vector<int> ordVec(6);

    for(int n=0; n<6; ++n){
      ordVec[n]= photonPerms[j][n];
      };
    amp.setOrder(ordVec);
     totAmp+=amp.eval(1);
      };


     for(int m=0; m<12; ++m){
      std::vector<int> fact_Ord(5);

      for(int l=0; l<6; ++l){
       fact_Ord[l]= littlePhotonPerms[m][l];
        };
       fact_amp.setOrder(fact_Ord);
      totAmpFact= fact_amp.eval(1);
      totLimit+=eikonal*totAmpFact.get0();
      };



  
  cout << "amp/limit" << totAmp.get0()/totLimit << endl;

  complex< T> spinorCheck=((xspA(p4,p6)/(xspA(p4,p5)*xspA(p5,p6)))* (xspB(p3,p6)/(xspB(p3,p5)*xspB(p5,p6)))+ (xspB(p3,p4)/(xspB(p3,p5)*xspB(p5,p4)))* (xspA(p4,p6)/(xspA(p4,p5)*xspA(p5,p6)))+  (xspA(p3,p4)/(xspA(p3,p5)*xspA(p5,p4)))* (xspB(p3,p6)/(xspB(p3,p5)*xspB(p5,p6)))+ (xspA(p3,p4)/(xspA(p3,p5)*xspA(p5,p4)))* (xspB(p4,p6)/(xspB(p4,p5)*xspB(p5,p6)))+  (xspA(p3,p6)/(xspA(p3,p5)*xspA(p5,p6)))* (xspB(p3,p4)/(xspB(p3,p5)*xspB(p5,p4)))+ (xspA(p3,p6)/(xspA(p3,p5)*xspA(p5,p6)))* (xspB(p4,p6)/(xspB(p4,p5)*xspB(p5,p6))));  

complex<T> checkSum= -T(65/16)*((2* dot(p4,p6)/(dot(p4,p5)*dot(p5,p6)))+ (2* dot(p3,p4)/(dot(p3,p5)*dot(p5,p4)))+ (2* dot(p3,p6)/(dot(p3,p5)*dot(p5,p6))));


cout << "trial" << checkSum/spinorCheck << endl;
 }
}


int main(int argc, char **argv)
{

 cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
 cout.precision(16);

  const Flavour<double> g= StandardModel::G();
  const Flavour<double> A= StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C());

  const Flavour<double> flavs[]={A,A,g,g,g,g};
 
 // checkSoft<qd_real>(flavs);
  cout<<endl; 
  checkSoft<double>(flavs);
  cout << endl;
 checkSoft<dd_real>(flavs); 
return 1;
}


