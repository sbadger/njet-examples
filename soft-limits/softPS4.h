#ifndef NJET_EXAMPLES_SOFTPS_H
#define NJET_EXAMPLES_SOFTPS_H


#include <iostream>
#include <getopt.h>
#include <cstdio>
#include <math.h>

#include "tools/PhaseSpace.h"
#include "ngluon2/Initialize.h"
#include "ngluon2/Model.h"
#include "ngluon2/NParton2.h"

using namespace std;

//hard coded single soft limit for AA ->gggg

template <typename T>

std::vector<MOM<T> > getSoftPoint4(const T lambda)
{
  const T sqS= 1e3;
  std::vector<MOM<T> > Mom(4);
  
  const T E3= T(400.);

  const T theta1= T(M_PI/3.);
  const T theta2= T(M_PI/5.);

  const T ct1= cos(theta1);
  const T ct2= cos(theta2);

  const T st1= sin(theta1);
  const T st2= sin(theta2);



  const T E2= ((1 + ct1*ct2)*E3*lambda)/(-(E3*lambda) + ct1*ct2*E3*lambda + sqS);
  Mom[0]=MOM<T>(sqS/2.,0,0,-sqS/2.);
  Mom[1]= E2*MOM<T>(sqS/2.,0,0,sqS/2.);
  Mom[2]= MOM<T>(-(E3*lambda),-(E3*lambda*st2),ct2*E3*lambda*st1,-(ct1*ct2*E3*lambda));
  Mom[3]= -Mom[0] -Mom[1] -Mom[2];

  

 
  return Mom;
}

#endif

