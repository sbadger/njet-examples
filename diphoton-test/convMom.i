template <typename T, unsigned int m>
std::vector<std::array<std::array<T, 4>, m>> convMom::getMom(const std::string& fileName) {
    std::vector<std::array<std::array<T, 4>, m>> all;
    std::array<std::array<T, 4>, m> point;
    std::string delimiter { " " };

    std::ifstream data(fileName);
    std::string line;
    unsigned int j { 0 };
    while (std::getline(data, line)) {
        if (j != m) {
            unsigned int i { 0 };
            std::array<T, 4> mom;
            while (i < 4) {
                size_t pos { line.find(delimiter) };
                std::string comp { line.substr(0, pos) };
                line.erase(0, pos + delimiter.length());
                if (comp != "") {
                    mom[i++] = stod(comp);
                }
            }
            point[j++] = mom;
        } else {
            all.push_back(point);
            j = 0;
        }
    }

    return all;
}

template <typename T, unsigned int m>
void convMom::printMom(const std::vector<std::array<std::array<T, 4>, m>>& all) {
    for (const std::array<std::array<T, 4>, m>& pt: all) {
        for (const std::array<T, 4>& mom: pt) {
            for (const T& comp: mom) {
                std::cout << comp << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }
}
