#include "calcPointsLib.h"

// ifstream
using std::cout;
using std::endl;
// array
using std::array;
// string
using std::string;

void calcPointsLib::banner(const char *contract, const string momFile, const string resultsFile) {
    cout << endl;
    cout << "  NJet: simple use of the BLHA interface for GG->GGAA" << endl;
    cout << "  nb. loop induced, so A0 vanishes" << endl;
    cout << "  Using BLHA contract file " << contract << endl;
    cout << "  Phase space points will be read from " << momFile << endl;
    cout << "  Results will be written to " << resultsFile << endl;
    cout << endl;
}

//void calcPointsLib::init(const char *contract) {
//    int status;
//    OLP_Start(contract, &status);
//    if (status) {
//        cout << "OLP read in correctly" << endl;
//    } else {
//        cout << "seems to be a problem with the contract file..." << endl;
//        exit(1);
//    }
//
//    char olpname[15];
//    char olpversion[15];
//    char olpmessage[255];
//    OLP_Info(olpname, olpversion, olpmessage);
//    cout << "Running " << olpname
//         << " version " << olpversion
//         << " note " << olpmessage << endl;
//
//    cout << endl;
//    cout << "Notation: Tree     = A0.cA0" << endl;
//    cout << "          Loop(-2) = 2*Re(A1.cA1)/eps^2" << endl;
//    cout << "          Loop(-1) = 2*Re(A1.cA1)/eps^1" << endl;
//    cout << "          Loop( 0) = 2*Re(A1.cA1)/eps^0" << endl;
//    cout << endl;
//}

//array<double, 3> doChannel(const int channel, const double LHMomenta_[]) {
//    double out[7];
//    double acc{0.};
//
//    const double zero{0.};
//
//    // set alpha strong QCD
//    const double alphas{0.118};
//    int rstatus_qcd;
//    OLP_SetParameter("alphas", &alphas, &zero, &rstatus_qcd);
//    if (rstatus_qcd == 1) {
//        cout << "Setting AlphaS: OK" << endl;
//    } else if (rstatus_qcd == 0) {
//        cout << "Setting AlphaS: FAIL" << endl;
//    } else {
//        cout << "Setting AlphaS: UNKNOWN" << endl;
//        exit(2);
//    }
//
//    // set alpha QED (answer changes if this changed, so does something)
//    const double alpha{1. / 137.035999084};
//    int rstatus_qed;
//    OLP_SetParameter("alpha", &alpha, &zero, &rstatus_qed);
//    if (rstatus_qed == 1) {
//        cout << "Setting Alpha: OK" << endl;
//    } else if (rstatus_qed == 0) {
//        cout << "Setting Alpha: FAIL" << endl;
//    } else {
//        cout << "Setting Alpha: UNKNOWN" << endl;
//        exit(2);
//    }
//
//    const double mur{91.188};
//
//    cout << endl << "Attempting calculation..." << endl << endl;
//
//    // Perform calculation with timing
//    auto t1 = std::chrono::high_resolution_clock::now();
//    OLP_EvalSubProcess2(&channel, LHMomenta_, &mur, out, &acc);
//    auto t2 = std::chrono::high_resolution_clock::now();
//
//    cout << "---- channel number " << channel << " ----" << endl;
//    cout << "OLP accuracy check: " << acc << endl;
//
//    cout << "Tree            = " << out[3] << endl;
//    cout << "Loop(-2)        = " << out[0] << endl;
//    cout << "Loop(-1)        = " << out[1] << endl;
//    cout << "Loop( 0)        = " << out[2] << endl;
//    cout << " only if NJetReturnAccuracy is used: (fractional error)" << endl;
//    cout << "Loop(-2) error  = " << out[4] << endl;
//    cout << "Loop(-1) error  = " << out[5] << endl;
//    cout << "Loop( 0) error  = " << out[6] << endl;
//    cout << endl;
//
//    cout << "Time: " << std::chrono::duration_cast<std::chrono::seconds>(t2 - t1).count() << "s" << endl
//         << endl;
//
//    // result finite part, fractional error, absolute error
//    return {out[2], out[6], out[6] * out[2]};
//}

