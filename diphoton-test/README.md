# diphoton-test

* Compile everything with `make`.
* Normal execution with `./calcPoints <phase space points file>`
* Batch execution with:
    * `./runPar.sh <phase space points file> <number of phase space points in file>`
    * `./gather.py <phase space points file> <number of phase space points in file>`
* Scale results with `./scale.py <results_file>`
* Compare results with `./compare.py <results_file_1> <results_files_2>`
