#ifndef NJET_EXAMPLES_CALCPOINTSLIB_H
#define NJET_EXAMPLES_CALCPOINTSLIB_H

#include <iostream>
#include <array>
#include <chrono>
#include <string>

/* #include "njet.h" */

namespace calcPointsLib {

    void banner(const char *contract, const std::string momFile, const std::string resultsFile);

    void init(const char *contract);

}

//std::array<double, 3> doChannel(const int channel, const double LHMomenta_[]);

#endif //NJET_EXAMPLES_CALCPOINTSLIB_H
