
************************************************************************

process: g(p1) g(p2) -> g(p3) g(p4) gamma(p5) gamma(p6) (loop induced)

************************************************************************




The .dat files are organized in the following way:


- points_1.dat: phase space points, namely the components of the momenta (in GeV): energy, x-component, y-component, z-component(beam axis)
             
                every block of 6 lines is a point, the order of the lines is p1, p2, p3, p4, p5, p6
             
                after 6 lines there is a blank line and then the subsequent point
             
             
- results_1.dat: the results of the OpenLoops calculation, each line contains the results for a single point
                
                 full matrix element squared, relative accuracy, absolute accuracy (relative*M2) 
             

             
I've generated 5 points for each unresolved limit at NLO: 5 points for parton 3 soft
                                                          
                                                          5 points for parton 4 soft
                                                        
                                                          5 points for partons 3 and 4 collinear
                                                          
                                                          5 points for partons 1 and 3 collinear
                                                          
                                                          5 points for partons 1 and 4 collinear
                                                          
                                                          5 points for partons 2 and 3 collinear
                                                          
                                                          5 points for partons 2 and 4 collinear
                                                          
                                                          --------------------------------------
                                                          
                                                          total = 35 points
                                                          
                                                          
                                                          
                                                          
                                                          
For the computation:

- nc = 3

- nf = 5, no top quark running in the loop

- everything is massless, both external and loop particles

- alpha_strong = 0.118

- alpha_qed = alpha_qed(0) = 1./137.035999084

- renormalization scale mu = 91.188 (GeV)

- the OpenLoops result is returned with all the overall constant factors included: couplings, color, quark charges, average over initial spin and color, simmetry factor

---------------------------------------------------------------------

I've attached the results from NJet in `results_1_njet.dat`. These were calculated with double-double precision (32 digits). I've also included a comparison of the NJet and OpenLoops results in `compare.dat`. The first column shows the NJet result divided by the OpenLoops result and the second column shows the same but with unity subtracted. The ordering and formatting is consistent with your files.

---------------------------------------------------------------------

in results_OL_2.dat you find the OpenLoops computation for the points
far from the infrared limits. In compare_2.dat you find the ratio
NJet/OL and the deviation from unity. The NJet and OL results agree up
to the 6th-7th digit, therefore the agreement in the non-singular
regions is significantly better, even if the difference is of
single-precision order.

At this point we would be interested in knowing how the degradation of
the agreement behaves when one approaches the unresolved limits. The
points I sent you at the beginning were the taken from a region beyond
which the OpenLoops double-precision calculation starts giving too many
pathological results. In points_X1.dat and points_X2.dat I prepared two
analogous selections of phase space points which are still sampled from
the unresolved limits, but less deep into them (with X1 deeper than X2).
It would be very helpful if we could perform the same kind of comparison
with NJet for these points too, and study the spoilage of the accuracy
of the squared matrix elements.

In results_X1_OL.dat and results_X2_OL.dat you find the results from
OpenLoops. The format and the conventions are the same as in the first
selection of points.

---------------------------------------------------------------------
