#include "convMom.h"

// iostream
using std::cout;
// string
using std::string;
// vecot
using std::vector;
// array
using std::array;
// iostream
using std::ios_base;


int main(int argc, char **argv) {
    cout.precision(16);
    cout.setf(ios_base::scientific, ios_base::floatfield);

    assert(argc == 2);
    const string file{argv[1]};

    const vector <array<array<double, 4>,6>> all{convMom::getMom<double, 6>(file)};

    convMom::printMom<double, 6>(all);

    return 0;
}
