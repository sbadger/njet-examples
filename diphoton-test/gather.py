#! /usr/bin/env python3

from argparse import ArgumentParser

from common import get_data, write_out


def cli():
    parser = ArgumentParser(
        description="Collect the results of parallel batch runs (*.part) for phase space points in file 'input' and "
                    "stitch together in the correct order then write out everything to a .dat file or to the terminal.")
    parser.add_argument(
        "input", type=str,
        help="The name of the .dat file with the phase space points for the run, e.g. points_1.dat.")
    parser.add_argument(
        "number", type=int,
        help="The number of parallel runs, i.e. the number of individual phase space points, e.g. 10.")
    parser.add_argument(
        "-o", "--output_file", type=str,
        help="The name of the .dat file to write compiled results to. If not given, print results to the terminal and "
             "don't write to any file.")
    return parser.parse_args()


if __name__ == "__main__":
    args = cli()

    if not args.output_file:
        print(f"Reading in results from files {args.input[:-4]}.[0-{args.number - 1}].part")

    data = [get_data(f"{args.input[:-4]}.{i}.part") for i in range(args.number)]

    write_out(args.output_file, data)
