#! /usr/bin/env python3

from argparse import ArgumentParser
from math import pi

from common import get_data, write_data, write_out


def cli():
    parser = ArgumentParser(
        description="Scale old results by an overall factor. The scaled results will be written to a new file if --output_file is defined, or written to the terminal otherwise")
    parser.add_argument("input_file", type=str, help="The .dat file with the old results, eg. results_1.dat.")
    parser.add_argument("-o", "--output_file", type=str,
                        help="The scaled results will be written to this .dat file, eg. results_2.dat.")
    return parser.parse_args()


if __name__ == "__main__":
    args = cli()

    factor = 4 * 0.118 / pi

    new_data = [[e * factor if ((i == 0) or (i == 2)) else e
                 for i, e in enumerate(row)] for row in get_data(args.input_file)]

    write_out(args.output_file, new_data)
