#ifndef convMom_h
#define convMom_h

#include <string>
#include <vector>
#include <array>
#include <fstream>
#include <iostream>
#include <cassert>

// template definitions

namespace convMom {

    template <typename T, unsigned int m>
        std::vector<std::array<std::array<T, 4>, m>> getMom(const std::string &fileName);

    template <typename T, unsigned int m>
        void printMom(const std::vector<std::array<std::array<T, 4>, m>> &all);

} // namespace convMom

// template implementations

#include "convMom.i"

#endif //convMom_h
