#include <iostream>
#include <fstream>
#include <string>
#include <array>

#include "../loop-induced/phaseSpace.h"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::ios;
using std::ios_base;
using std::array;

template<unsigned int num>
void genMomFile(const array<int, num> rseeds, const string file) {
    for (unsigned int i{0}; i < num; ++i) {
        cout << "Phase space point " << i << ":" << endl;

        vector <MOM<double>> point{getMasslessPhaseSpacePoint<double>(6, true, rseeds[i], 10.)};

        {
            ofstream o(file, ios::app);
            o.setf(ios_base::scientific);
            o.precision(16);
            unsigned int j{0};
            for (auto mom: point) {
                // Convert from all-outgoing to 2->4 convention
                mom *= (++j < 3) ? -1 : 1;
                o << mom.x0 << "  " << mom.x1 << "  " << mom.x2 << "  " << mom.x3 << '\n';
            }
            o << '\n';
        }

        cout << endl;
    }
}

int main(int argc, char **argv) {
    cout << endl;

    string resultsFile;
    if (argc == 2) {
        resultsFile = argv[1];
    } else {
        cout << "Command line argument error!" << endl
             << "Run as ./generateMomenta.cpp [phase space points output file]." << endl <<
             "e.g. ./generateMomenta points_1.dat" << endl << endl;
        exit(1);
    }

    const unsigned int num{10};
    // Omit rseed=4 as it doesn't give an easy point
    const array<int, num> rseeds{1, 2, 3, 5, 6, 7, 8, 9, 10, 11};

    genMomFile<num>(rseeds, resultsFile);

    return 0;
}
