#include "calcPointsPar.h"

// iostream
using std::cout;
using std::endl;
using std::ios_base;
using std::ios;
// vector
using std::vector;
// array
using std::array;
// fstream
using std::ofstream;
// string
using std::string;
// cstdlib
using std::atoi;


int main(int argc, char **argv) {
    cout.setf(ios_base::scientific, ios_base::floatfield);
    cout.precision(8);

    const char* contract{"OLE_contract_diphoton.lh"};

    int channel;
    if (argc == 3) {
        channel = 1;
    } else if (argc == 4) {
        channel = atoi(argv[3]) + 1;
    } else {
        cout << "Run as ./calcPointsPar.cpp [phase space points file] [number of point to run] [number of channel to "
                "run]. If channel number omitted, defaults to the first. All numbers are zero indexed. e.g. "
                "./calcPointsPar points_1.dat 0" << endl;
        exit(1);
    }
    const string momFile{argv[1]};
    const string numS{argv[2]};
    const unsigned int num = atoi(argv[2]);

    const string resultsFile{momFile.substr(0, momFile.length() - 4) + "." + numS + ".part"};

    cout << endl;
    cout << "  NJet: simple use of the BLHA interface for GG->GGAA" << endl;
    cout << "  nb. loop induced, so A0 vanishes" << endl;
    cout << "  Using BLHA contract file " << contract << endl;
    cout << "  Phase space points will be read from " << momFile << endl;
    cout << "  Running phase space point " << num << endl;
    cout << "  Running channel " << channel << endl;
    cout << "  Results will be written to " << resultsFile << endl;
    cout << endl;

    const unsigned int legs{6};
    // d=4 is for length of four-momenta (i.e. 4 dimensions)
    const unsigned int d{4};
    const vector <array<array<double, d>,legs>> Momenta{convMom::getMom<double, legs>(momFile)};
    assert(num < Momenta.size());

    int status;
    OLP_Start(contract, &status);
    if (status) {
        cout << "OLP read in correctly" << endl;
    } else {
        cout << "seems to be a problem with the contract file..." << endl;
        exit(1);
    }

    char olpname[15];
    char olpversion[15];
    char olpmessage[255];
    OLP_Info(olpname, olpversion, olpmessage);
    cout << "Running " << olpname
         << " version " << olpversion
         << " note " << olpmessage << endl;

    cout << endl;
    cout << "Notation: Tree     = A0.cA0" << endl;
    cout << "          Loop(-2) = 2*Re(A1.cA1)/eps^2" << endl;
    cout << "          Loop(-1) = 2*Re(A1.cA1)/eps^1" << endl;
    cout << "          Loop( 0) = 2*Re(A1.cA1)/eps^0" << endl;
    cout << endl;

    // Here the four-momenta are recorded as vectors of length n=5 where the last entry is the mass
    const unsigned int n{5};
    array <array<double, d>, legs> point{Momenta[num]};

    double LHMomenta[legs * n];
    for (unsigned int p{0}; p < legs; ++p) {
        cout << "p" << p + 1 << "=(";
        for (unsigned int mu{0}; mu < d; ++mu) {
            cout << point[p][mu] << (mu == d - 1 ? "" : ",");
            LHMomenta[mu + p * n] = point[p][mu];
        }
        cout << ")" << endl;
        // Set masses
        LHMomenta[d + p * n] = 0.;
    }
    cout << endl;

    double out[7];
    double acc{0.};
    int rstatus;
    int rstatus2;

    const double alphas{0.118};
    const double zero{0.};
    const double mur{91.188};

    OLP_SetParameter("alphas", &alphas, &zero, &rstatus);
    if (rstatus == 1) {
        cout << "Setting AlphaS: OK" << endl;
    } else if (rstatus == 0) {
        cout << "Setting AlphaS: FAIL" << endl;
    } else {
        cout << "Setting AlphaS: UNKNOWN" << endl;
        exit(2);
    }

    // set alpha QED (answer changes if this changed, so does something)
    const double alpha{1. / 137.035999084};

    OLP_SetParameter("alpha", &alpha, &zero, &rstatus2);
    if (rstatus2 == 1) {
        cout << "Setting Alpha: OK" << endl;
    } else if (rstatus2 == 0) {
        cout << "Setting Alpha: FAIL" << endl;
    } else {
        cout << "Setting Alpha: UNKNOWN" << endl;
        exit(2);
    }

    cout << endl << "Attempting calculation..." << endl << endl;
    auto t1 = std::chrono::high_resolution_clock::now();
    OLP_EvalSubProcess2(&channel, LHMomenta, &mur, out, &acc);
    auto t2 = std::chrono::high_resolution_clock::now();

    cout << "---- channel number " << channel << " ----" << endl;
    cout << "OLP accuracy check: " << acc << endl;

    cout << "Tree            = " << out[3] << endl;
    cout << "Loop(-2)        = " << out[0] << endl;
    cout << "Loop(-1)        = " << out[1] << endl;
    cout << "Loop( 0)        = " << out[2] << endl;
    cout << " only if NJetReturnAccuracy is used: (fractional error)" << endl;
    cout << "Loop(-2) error  = " << out[4] << endl;
    cout << "Loop(-1) error  = " << out[5] << endl;
    cout << "Loop( 0) error  = " << out[6] << endl;
    cout << endl;

    cout << "Time: " << std::chrono::duration_cast<std::chrono::seconds>(t2 - t1).count() << "s" << endl
         << endl;

    cout << endl;

    {
        ofstream o(resultsFile, ios::app);
        o.setf(ios_base::scientific);
        o.precision(16);
        // result, relative (fractional/decimal) error, absolute error
        o << out[2] << "  " << out[6] << "  " << out[6] * out[2] << '\n';
    }

    return 0;
}
