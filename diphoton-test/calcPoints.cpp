#include "calcPoints.h"

// iostream
using std::cout;
using std::endl;
using std::ios_base;
using std::ios;
// vector
using std::vector;
// array
using std::array;
// fstream
using std::ofstream;
// string
using std::string;


int main(int argc, char **argv) {
    cout.setf(ios_base::scientific, ios_base::floatfield);
    cout.precision(8);

    const char* contract{"OLE_contract_diphoton.lh"};
    if (argc != 2) {
        cout << "Run as ./calcPoints.cpp <phase space points file>, "
                "e.g. ./calcPoints points_1.dat" << endl;
        exit(1);
    }
    string momFile{argv[1]};
    string resultsFile{"test.dat"};
    const int channels{1};

    calcPointsLib::banner(contract, momFile, resultsFile);

    const unsigned int legs{6};
    // d=4 is for length of four-momenta (i.e. 4 dimensions)
    const unsigned int d{4};
    const vector <array<array<double, d>,legs>> Momenta{convMom::getMom<double, legs>(momFile)};

//    init(contract);
    int status;
    OLP_Start(contract, &status);
    if (status) {
        cout << "OLP read in correctly" << endl;
    } else {
        cout << "seems to be a problem with the contract file..." << endl;
        exit(1);
    }

    char olpname[15];
    char olpversion[15];
    char olpmessage[255];
    OLP_Info(olpname, olpversion, olpmessage);
    cout << "Running " << olpname
         << " version " << olpversion
         << " note " << olpmessage << endl;

    cout << endl;
    cout << "Notation: Tree     = A0.cA0" << endl;
    cout << "          Loop(-2) = 2*Re(A1.cA1)/eps^2" << endl;
    cout << "          Loop(-1) = 2*Re(A1.cA1)/eps^1" << endl;
    cout << "          Loop( 0) = 2*Re(A1.cA1)/eps^0" << endl;
    cout << endl;

    vector <array<double, 3>> results;

    // Here the four-momenta are recorded as vectors of length n=5 where the last entry is the mass
    const unsigned int n{5};
    unsigned int pt{0};
    for (auto &point: Momenta) {
        cout << "==================== Test point " << ++pt << " ====================" << endl;
        double LHMomenta[legs * n];
        for (unsigned int p{0}; p < legs; ++p) {
            cout << "p" << p + 1 << "=(";
            for (unsigned int mu{0}; mu < d; ++mu) {
                cout << point[p][mu] << (mu == d - 1 ? "" : ",");
                LHMomenta[mu + p * n] = point[p][mu];
            }
            cout << ")" << endl;
            // Set masses
            LHMomenta[d + p * n] = 0.;
        }
        cout << endl;

        for (int channel{1}; channel <= channels; ++channel) {
            double out[7];
            double acc{0.};
            int rstatus;
            int rstatus2;

            const double alphas{0.118};
            const double zero{0.};
            const double mur{91.188};

            OLP_SetParameter("alphas", &alphas, &zero, &rstatus);
            if (rstatus == 1) {
                cout << "Setting AlphaS: OK" << endl;
            } else if (rstatus == 0) {
                cout << "Setting AlphaS: FAIL" << endl;
            } else {
                cout << "Setting AlphaS: UNKNOWN" << endl;
                exit(2);
            }

            // set alpha QED (answer changes if this changed, so does something)
            const double alpha{1. / 137.035999084};

            OLP_SetParameter("alpha", &alpha, &zero, &rstatus2);
            if (rstatus2 == 1) {
                cout << "Setting Alpha: OK" << endl;
            } else if (rstatus2 == 0) {
                cout << "Setting Alpha: FAIL" << endl;
            } else {
                cout << "Setting Alpha: UNKNOWN" << endl;
                exit(2);
            }

            cout << endl << "Attempting calculation..." << endl << endl;
            auto t1 = std::chrono::high_resolution_clock::now();
            OLP_EvalSubProcess2(&channel, LHMomenta, &mur, out, &acc);
            auto t2 = std::chrono::high_resolution_clock::now();

            cout << "---- channel number " << channel << " ----" << endl;
            cout << "OLP accuracy check: " << acc << endl;

            cout << "Tree            = " << out[3] << endl;
            cout << "Loop(-2)        = " << out[0] << endl;
            cout << "Loop(-1)        = " << out[1] << endl;
            cout << "Loop( 0)        = " << out[2] << endl;
            cout << " only if NJetReturnAccuracy is used: (fractional error)" << endl;
            cout << "Loop(-2) error  = " << out[4] << endl;
            cout << "Loop(-1) error  = " << out[5] << endl;
            cout << "Loop( 0) error  = " << out[6] << endl;
            cout << endl;

            cout << "Time: " << std::chrono::duration_cast<std::chrono::seconds>(t2 - t1).count() << "s" << endl
                 << endl;

            // result finite part, fractional error, absolute error
            results.push_back({out[2], out[6], out[6] * out[2]});
        }
        cout << endl;
    }

    cout << endl;

    {
        ofstream o(resultsFile, ios::app);
        o.setf(ios_base::scientific);
        o.precision(16);
        for (auto &result: results) {
            // result, relative (fractional/decimal) error, absolute error
            o << result[0] << "  " << result[1] << "  " << result[2] << '\n';
        }
    }

    return 0;
}
