#include <iostream>
#include <getopt.h>
#include <cstdio>

#include "ngluon2/Model.h"
#include "tools/PhaseSpace.h"

#include "chsums/0q4g.h"
#include "chsums/2q2g.h"
#include "chsums/4q0g.h"

#include "chsums/0q5g.h"
#include "chsums/2q3g.h"

using std::cout;
using std::endl;
using std::complex;

void test_pp2j()
{
  const int nn = 4;
  Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };
  const double MuR = 91.188;

  PhaseSpace<double> ps(nn, flavours);
  ps.getPSpoint();
  ps.showPSpoint();

  NJetAmp<double>* sqamp[4] = {
    new Amp0q4g<double>(1.),
    new Amp2q2g<double>(1.),
    new Amp4q0g<double>(1.),
    new Amp4q0g2<double>(1.)
  };

  int hels[16][4] = {
    {-1, -1, -1, -1},
    {+1, -1, -1, -1},
    {-1, +1, -1, -1},
    {+1, +1, -1, -1},
    {-1, -1, +1, -1},
    {+1, -1, +1, -1},
    {-1, +1, +1, -1},
    {+1, +1, +1, -1},
    {-1, -1, -1, +1},
    {+1, -1, -1, +1},
    {-1, +1, -1, +1},
    {+1, +1, -1, +1},
    {-1, -1, +1, +1},
    {+1, -1, +1, +1},
    {-1, +1, +1, +1},
    {+1, +1, +1, +1}
  };

  for (int i=0; i<4; i++) {
    sqamp[i]->setMuR2(MuR*MuR);
    sqamp[i]->setMomenta(ps.Mom());

    cout <<" |A0|^2            = " << sqamp[i]->born() << endl;
    cout <<" 2Re(A1.A0)        = " << sqamp[i]->virt() << endl;
    cout <<" 2Re(A1.A0)/|A0|^2 = " << sqamp[i]->virt()/sqamp[i]->born() << endl;

    /* spin matrix for parton 1 */
    complex<double> spnmat[4] = {};
    sqamp[i]->born_spnmatrix(1, spnmat);
    cout << "|A0|^2_spn(1) = " << endl
      << spnmat[0] << " " << spnmat[1] << endl << spnmat[2] << " " << spnmat[3] << endl;


    complex<double> spnmat_full[4] = {};
    for (int h=0; h<16; h++) {
      cout << " |A0("<<h<<")|^2 = " << sqamp[i]->born(hels[h]) << endl;

      sqamp[i]->born_spnmatrix(1, hels[h], spnmat);
      cout << "|A0("<<h<<")|^2_spn(1) = " << endl
        << spnmat[0] << " " << spnmat[1] << endl << spnmat[2] << " " << spnmat[3] << endl;
      for (int k=0; k<4; k++) spnmat_full[k] += spnmat[k];
    }
    cout << "|A0|^2_spn(1) = " << endl
      << spnmat_full[0] << " " << spnmat_full[1] << endl << spnmat_full[2] << " " << spnmat_full[3] << endl;
  }
}

void test_pp3j()
{
  const int nn = 5;
  Flavour<double> flavours[] = {
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G(),
    StandardModel::G()
  };
  const double MuR = 91.188;

  PhaseSpace<double> ps(nn, flavours);
  ps.getPSpoint();
  ps.showPSpoint();

  NJetAmp<double>* sqamp[2] = {
    new Amp0q5g<double>(1.),
    new Amp2q3g<double>(1.)
  };

  int hels[32][5] = {
    {-1, -1, -1, -1, -1},
    {+1, -1, -1, -1, -1},
    {-1, +1, -1, -1, -1},
    {+1, +1, -1, -1, -1},
    {-1, -1, +1, -1, -1},
    {+1, -1, +1, -1, -1},
    {-1, +1, +1, -1, -1},
    {+1, +1, +1, -1, -1},
    {-1, -1, -1, +1, -1},
    {+1, -1, -1, +1, -1},
    {-1, +1, -1, +1, -1},
    {+1, +1, -1, +1, -1},
    {-1, -1, +1, +1, -1},
    {+1, -1, +1, +1, -1},
    {-1, +1, +1, +1, -1},
    {+1, +1, +1, +1, -1},
    {-1, -1, -1, -1, +1},
    {+1, -1, -1, -1, +1},
    {-1, +1, -1, -1, +1},
    {+1, +1, -1, -1, +1},
    {-1, -1, +1, -1, +1},
    {+1, -1, +1, -1, +1},
    {-1, +1, +1, -1, +1},
    {+1, +1, +1, -1, +1},
    {-1, -1, -1, +1, +1},
    {+1, -1, -1, +1, +1},
    {-1, +1, -1, +1, +1},
    {+1, +1, -1, +1, +1},
    {-1, -1, +1, +1, +1},
    {+1, -1, +1, +1, +1},
    {-1, +1, +1, +1, +1},
    {+1, +1, +1, +1, +1}
  };

  for (int i=1; i<2; i++) {
    sqamp[i]->setMuR2(MuR*MuR);
    sqamp[i]->setMomenta(ps.Mom());

    cout <<" |A0|^2            = " << sqamp[i]->born() << endl;
    cout <<" 2Re(A1.A0)        = " << sqamp[i]->virt() << endl;
    cout <<" 2Re(A1.A0)/|A0|^2 = " << sqamp[i]->virt()/sqamp[i]->born() << endl;

    /* spin matrix for parton 3 */
    complex<double> spnmat[4] = {};
    complex<double> spnmat_full[4] = {};
    double born;
    double born_full;
    for (int h=0; h<32; h++) {
      born = sqamp[i]->born(hels[h]);
      cout << " |A0("<<h<<")|^2 = " << born << endl;
      born_full += born;

      sqamp[i]->born_spnmatrix(3, hels[h], spnmat);

      cout << "|A0("<<h<<")|^2_spn(1) = " << endl
        << spnmat[0] << " " << spnmat[1] << endl << spnmat[2] << " " << spnmat[3] << endl;
      for (int k=0; k<4; k++) spnmat_full[k] += spnmat[k];
    }

    cout << "|A0|^2_spn(1) = " << endl
      << spnmat_full[0] << " " << spnmat_full[1] << endl << spnmat_full[2] << " " << spnmat_full[3] << endl;

    sqamp[i]->born_spnmatrix(3, spnmat);
    cout << "|A0|^2_spn(1) = " << endl
      << spnmat[0] << " " << spnmat[1] << endl << spnmat[2] << " " << spnmat[3] << endl;

    cout <<" |A0|^2            = " << sqamp[i]->born() << endl;
    cout <<" |A0|^2            = " << born_full << endl;

  }
}

int main(int argc, char **argv)
{
  cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
  cout.precision(16);

  test_pp2j();

  test_pp3j();

  return 0;
}
